@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Papers <a href="{{ url('/papers/create') }}" class="btn btn-primary btn-xs" title="Add New Paper"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> {{ trans('papers.subject') }} </th><th> {{ trans('papers.format') }} </th><th> {{ trans('papers.abstract_page') }} </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($papers as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->subject }}</td><td>{{ $item->format }}</td><td>{{ $item->abstract_page }}</td>
                    <td>
                        <a href="{{ url('/papers/' . $item->id) }}" class="btn btn-success btn-xs" title="View Paper"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/papers/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Paper"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/papers', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Paper" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Paper',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $papers->render() !!} </div>
    </div>

</div>
@endsection
