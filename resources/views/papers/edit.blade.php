@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Edit Paper {{ $paper->id }}</h1>

    {!! Form::model($paper, [
        'method' => 'PATCH',
        'url' => ['/papers', $paper->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                {!! Form::label('subject', trans('papers.subject'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('format') ? 'has-error' : ''}}">
                {!! Form::label('format', trans('papers.format'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('format', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('format', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('abstract_page') ? 'has-error' : ''}}">
                {!! Form::label('abstract_page', trans('papers.abstract_page'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('abstract_page', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('abstract_page', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('abstract_page', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('sources') ? 'has-error' : ''}}">
                {!! Form::label('sources', trans('papers.sources'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('sources', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('sources', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('topic') ? 'has-error' : ''}}">
                {!! Form::label('topic', trans('papers.topic'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('topic', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('topic', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('details') ? 'has-error' : ''}}">
                {!! Form::label('details', trans('papers.details'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('details', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('additional_materials') ? 'has-error' : ''}}">
                {!! Form::label('additional_materials', trans('papers.additional_materials'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('additional_materials', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('additional_materials', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('type_of_service') ? 'has-error' : ''}}">
                {!! Form::label('type_of_service', trans('papers.type_of_service'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('type_of_service', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('type_of_service', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('academic_level') ? 'has-error' : ''}}">
                {!! Form::label('academic_level', trans('papers.academic_level'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('academic_level', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('academic_level', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('vip') ? 'has-error' : ''}}">
                {!! Form::label('vip', trans('papers.vip'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('vip', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('vip', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('vip', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pages_count') ? 'has-error' : ''}}">
                {!! Form::label('pages_count', trans('papers.pages_count'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('pages_count', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('pages_count', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('slides_count') ? 'has-error' : ''}}">
                {!! Form::label('slides_count', trans('papers.slides_count'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('slides_count', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('slides_count', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('spacing') ? 'has-error' : ''}}">
                {!! Form::label('spacing', trans('papers.spacing'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('spacing', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('spacing', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('first_deadline') ? 'has-error' : ''}}">
                {!! Form::label('first_deadline', trans('papers.first_deadline'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('first_deadline', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('first_deadline', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('final_deadline') ? 'has-error' : ''}}">
                {!! Form::label('final_deadline', trans('papers.final_deadline'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('final_deadline', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('final_deadline', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', trans('papers.status'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('status', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('preferred_writer') ? 'has-error' : ''}}">
                {!! Form::label('preferred_writer', trans('papers.preferred_writer'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('preferred_writer', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('preferred_writer', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('discount_code') ? 'has-error' : ''}}">
                {!! Form::label('discount_code', trans('papers.discount_code'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('discount_code', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('discount_code', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection