@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Paper {{ $paper->id }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID.</th><td>{{ $paper->id }}</td>
                </tr>
                <tr><th> {{ trans('papers.subject') }} </th><td> {{ $paper->subject }} </td></tr><tr><th> {{ trans('papers.format') }} </th><td> {{ $paper->format }} </td></tr><tr><th> {{ trans('papers.abstract_page') }} </th><td> {{ $paper->abstract_page }} </td></tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <a href="{{ url('papers/' . $paper->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Paper"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['papers', $paper->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Paper',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
@endsection