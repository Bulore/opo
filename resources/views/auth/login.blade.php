<link rel="stylesheet" href="css/login.css">
<script type="text/javascript" src="/js/jquery-2.2.4.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<meta name="viewport" content="width=600" initial-scale=1>

<div class="logo">
    <a href="{{url('/')}}">OriginalPapersOnly</a><a href="{{url('/')}}">.com</a>
</div>

<div class="login-container">
    <form id="user" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <div class="panel-block">
           <div></div>
           <div class="login">Login</div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div>
               Email:
            </div>
            <div>
                @if ($errors->has('email'))
                    <span class="data-error">{{ $errors->first('email') }}</span>
                @endif
                <input id="email" type="email" name="email" value="{{ old('email') }}">
            </div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div>
               Password:
            </div>
            <div>
                @if ($errors->has('password'))
                    <span class="data-error">{{ $errors->first('password') }}</span>
                @endif
                <input type="password" class="password" name="password">
            </div>
        </div>
        <div class="panel-block margin-bottom-20">
            <div></div>
            <div>
                <div class="check-container">
                    <label>
                        <input type="checkbox"  name="remember" hidden>
                        <div class="checkbox"></div>
                        <div class="check-text">
                        <span>Remember me for two weeks</span>
                        </div>
                    </label>
                </div>
                <div class="check-text">
                    <a href="{{ url('/password/reset')}}">Forgot your password?</a>
                </div>
            </div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div class="container-button">
                <button>Sign in</button>
            </div>
        </div>
        <div class="panel-block"></div>
    </form>
</div>
