@extends('layouts.app')

@section('head')
    <link rel="stylesheet" href="{{url('/css/countries.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">First name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Last name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                {{--<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">--}}
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default dropdown-toggle intl-tel-input" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><div class="flag " id="selected-country"></div> <span class="caret"></span></button>
                                        <ul class="dropdown-menu intl-tel-input">
                                            {{--<li><a href="#">Action</a></li>--}}
                                            {{--<li><a href="#">Another action</a></li>--}}
                                            {{--<li><a href="#">Something else here</a></li>--}}
                                            {{--<li role="separator" class="divider"></li>--}}
                                            {{--<li><a href="#">Separated link</a></li>--}}
                                            @foreach($countries as $country)
                                                <li class="country" code="{{$country->code}}" tax="{{$country->tax}}">
                                                    <a href="#"><div class="flag {{$country->code}}"></div>{{$country->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div><!-- /btn-group -->
                                    <span class="input-group-addon" id="phone-prefix">+380</span>
                                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                </div><!-- /input-group -->

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        function changeCountry(){
            $('#selected-country').attr('class', 'flag '+$(this).attr('code'));
            $('#phone-prefix').html('+' + $(this).attr('tax'));
        }

        var currentCountryCode = '{{$currentCountryCode}}';
        var currentCountry = $('.country[code="'+currentCountryCode+'"]');
        changeCountry.call(currentCountry);
        $('.country').click(changeCountry);
    });
</script>
@endsection
