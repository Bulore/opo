<link rel="stylesheet" href="{{url('css/login.css')}}">
<script type="text/javascript" src="{{url('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{url('js/jquery.validate.min.js')}}"></script>
<meta name="viewport" content="width=600" initial-scale=1>

<div class="logo">
    <a href="{{url('/')}}">OriginalPapersOnly</a><a href="{{url('/')}}">.com</a>
</div>

<div class="login-container">
    <form id="user" method="POST" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}
        <div class="panel-block">
           <div></div>
           <div class="login">Reset Password</div>
        </div>
        <div class="margin-bottom-30">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="panel-block margin-bottom-30">
            <div>
               Email:
            </div>
            <div class="flex-5">
                @if ($errors->has('email'))
                    <span class="data-error">{{ $errors->first('email') }}</span>
                @endif
                <input id="email" type="email" name="email" value="{{ old('email') }}">
            </div>
        </div>
        <div class="panel-block padding-bottom-20">
            <div></div>
            <div>
                <button type="submit">Send Password Reset Link</button>
            </div>
        </div>
    </form>
</div>

