<link rel="stylesheet" href="{{url('css/login.css')}}">
<script type="text/javascript" src="{{url('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{url('js/jquery.validate.min.js')}}"></script>
<meta name="viewport" content="width=600" initial-scale=1>

<div class="logo">
    <a href="{{url('/')}}">OriginalPapersOnly</a><a href="{{url('/')}}">.com</a>
</div>

<div class="login-container">
    <form role="form" method="POST" action="{{ url('/password/reset') }}">
        {!! csrf_field() !!}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="panel-block">
           <div></div>
           <div class="login">Reset Password</div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div> E-Mail Address: </div>
            <div class="flex-5">
                @if ($errors->has('email'))
                    <span class="data-error">{{ $errors->first('email') }}</span>
                @endif
                <input id="email" type="email" name="email" value="{{ $email or old('email') }}">
            </div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div> New password: </div>
            <div class="flex-5">
                @if ($errors->has('password'))
                    <span class="data-error">{{ $errors->first('password') }}</span>
                @endif
                <input id="password" type="password" name="password">
            </div>
        </div>
        <div class="panel-block margin-bottom-30">
            <div> Confirm Password: </div>
            <div class="flex-5">
                @if ($errors->has('password_confirmation'))
                    <span class="data-error">{{ $errors->first('password_confirmation') }}</span>
                @endif
                <input id="password_confirmation" type="password" name="password_confirmation">
            </div>
        </div>
        <div class="panel-block padding-bottom-20">
            <div></div>
            <div>
                <button type="submit">Reset Password</button>
            </div>
        </div>
    </form>
</div>

