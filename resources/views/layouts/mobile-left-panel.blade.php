<div class="mobile-left-panel">
    <div class="scrollable">
        @foreach($sections as $section=>$url)
            <a href="{{url($url)}}">
                <div class="item @if(Request::path() == $url) active-item @endif">
                    <span class="tab-name">{{$section}}</span>
                </div>
            </a>
        @endforeach
        <hr>
        <a href="{{url('/panel')}}">
            <div class="item">
                @if(Auth::check())
                <span class="tab-name">Dashboard</span>
                @else
                <span class="tab-name">Log in</span>
                @endif
            </div>
        </a>
    </div>
    
</div>