<link rel="stylesheet" type="text/css" href="/css/app.css">

<div class="mob-header">
	<div class="original-header">
		<div class="logo">
			<a href="{{url('/')}}"><span class="f-blue">OriginalPapersOnly<span class="f-orange">.com</span></span></a>
		</div>
		<div class="mobile-chat-now">
			<div class="chat-item">
				<a href="skype:vladhunter1?call"><div class="chat-icon skype-icon"></div></a>
			</div>
			<div class="vertical-line"></div>
			<open-pop-up class="chat-item popup-button" pop-up-id="call-me-back">
				<div class="chat-icon chat-phone-icon"></div>
			</open-pop-up>
			<div class="vertical-line"></div>
			<a href="mailto:support@originalpapersonly.com">
				<div class="chat-item">
				<div class="chat-icon chat-now-icon"></div>
			</div>
			</a>
		</div>
	</div>
	{{--mobile menu--}}
	<div class="mobile-menu-background">
		<div id="mobile-menu" >
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="mobile-button-sign">
			<a href="{{url('/order')}}"><button class="user-button">New order</button></a>
			
			<a href="{{route('panel')}}"><span class="sign-in-icon"></span></a>
		</div>
	</div>
</div>



<div class="fixed-header z-90 container-header m-display-none">
	<div class="blue-g relative ">
		<div class="container">
			<div class="header">
				<div class="head-top">
					<a href="https://twitter.com/originalpapers1" class="sn-b"> <span class="sn-twiter"></span> </a>
					<a href="https://plus.google.com/u/0/104072062163353165984?hl=en" class="sn-b"> <span class="sn-google"></span> </a> 
					<a href="https://www.facebook.com/Originalpapersonly-1763588037231134/" class="sn-b"> <span class="sn-facebook"></span> </a> 
					<a href="skype:support@originalpapersonly.com?call" class="sn-b">  <span class="sn-skype"></span> </a>
					<div class="container-phone">
						<div class="section-p">
							<span class="country">US:</span>
							<span class="number">+1-929-367-2627</span>
						</div>
						<div class="section-p">
							<span class="country">UK:</span>
							<span class="number">+44-800-088-5728</span>
						</div>
						{{-- <div class="section-p">
							<span class="country">AU:</span>
							<span class="number">+61-180-028-0428</span>
						</div> --}}
					</div>
					<open-pop-up class="white-button popup-button" pop-up-id="call-me-back">Call me back</open-pop-up>
					<a href="{{route('panel')}}"><input class="white-button-t" type="button" value="Sign in"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="box-shadow white relative">
		<div class="container">
			<div class="header">
				<nav class="nav-menu">
					<div class="logo">
						<a href="{{url('/')}}"><span class="f-blue">OriginalPapersOnly<span class="f-orange">.com</span></span></a>
					</div>
					<div class="container-section">
						@foreach($sections as $section=>$url)
							<div class="section">
								<a href="{{url($url)}}" class="@if(Request::path()==$url) active @endif">{{$section}}</a>
							</div>
						@endforeach
					</div>
					<div class="menu-container-button">
						<a href="{{url('/order')}}"><input class="common-button" type="button" value="Order now"></a>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="header-background"></div>

