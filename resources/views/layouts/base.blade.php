<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=1000">
    <title>{{ $seo->title }}</title>
    <meta name="Keywords" content="{{ $seo->keywords }}">
    <meta name="Description" content="{{ $seo->description }}">
    <meta name="google-site-verification" content="kfbAOFFFYXdcLITt1HzWUnNLALeNy882Q3-dECPQEjU" />
    <link rel="shortcut icon" href="{{url('/images/favicon.ico')}}" />
</head>
<body ng-app="app" id="touch-zone">
    @section('header')
        @include('layouts.header')
    @show

    @include('layouts.mobile-left-panel')
    @yield('content')

    @include('layouts.chat-now')

    @section('footer')
        @include('layouts.footer')
        @include('popups.callback')
        @include('popups.thanks-popup')
        @include('popups.error-popup')
    @show

    @section('scripts')

    <script>
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length,c.length);
                }
            }
            return "";
        }

        function setDesktop(value){
            if(value == '1'){ 
                setCookie("isDesktop", "1", 30);
                document.querySelector('meta[name="viewport"]').setAttribute('content','width=1300');
                document.querySelector('.set-mobile').style.display = "block";
                document.querySelector('.set-desktop').style.display = "none";
            } else if(value=='0'){
                setCookie("isDesktop", "0", 30);
                document.querySelector('meta[name="viewport"]').setAttribute('content','width=1000');
                document.querySelector('.set-mobile').style.display = "none";
                document.querySelector('.set-desktop').style.display = "block";
            }
        }
        if(document.documentElement.clientWidth == 1000){
            document.querySelector('.mobile-footer').style.display = "block";
            if(getCookie("isDesktop") == ""){
                setCookie("isDesktop", "0", 30);
            }
        }
        setDesktop(getCookie("isDesktop"));

    </script>

    <script type="text/javascript" src="{{url('/js/webcomponents-lite.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/jquery-2.2.4.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/app.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/jquery.mobile-events.min.js')}}"></script>
    
    <script>
        var popUpError = document.querySelector('pop-up[pop-up-id="pop-up-error"]');
        var app = angular.module('app', []);
        $(function(){
            if(getCookie("isDesktop") == ""){
                $('.mobile-footer').hide();
            }
            setTimeout(function(){
                $('.mobile-footer .set-desktop').click(function(){ setDesktop('1'); });
                $('.mobile-footer .set-mobile').click(function(){ setDesktop('0'); });
            }, 100)
       });
    </script>
    @show

    
</body>
</html>