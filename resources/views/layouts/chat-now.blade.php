<div class="chat-now-block m-display-none">
    <div class="chat-item">
        <a href="skype:support@originalpapersonly.com?chat"><div class="chat-icon skype-icon"></div></a>
	</div>
<div class="vertical-line"></div>
<div class="chat-item popup-button" data-popup-id="call-me-back">
    <div class="chat-icon chat-phone-icon"></div>
    <open-pop-up pop-up-id="call-me-back">Call me back</open-pop-up>
</div>
<div class="vertical-line"></div>
<a href="mailto:support@originalpapersonly.com">
	<div class="chat-item">
		<div class="chat-icon chat-now-icon"></div>
		<span>Chat now</span>
	</div>
</a>
</div>
@include('popups.callback')
