<div class="footer m-display-none">
	<div class="footer-background"></div>
	<div class="content clear-after">
		<a href="{{url('/order')}}">
			<div class="circle">
				<div>
					<div>
						<div>
							<div>click here to</div>
							<div>ORDER NOW</div>
							<div><span class="dollar f-0p7">$</span>11,99 per page</div>
							<div><span class="dollar f-0p7">$</span>11.99</div>
						</div>
					</div>
				</div>
			</div>
		</a>
		<nav class="nav-menu">
			<p>{{Request::url()}}</p>
			<p>{{url('/our-extras')}}</p>
			<div class="section @if(Request::url() == url('/free-inquiry')) active @endif"><a href="{{url("/free-inquiry")}}">Free Inquiry</a></div>
			<div class="section @if(Request::url() == url('/prices') ) active @endif"><a href="{{url("/prices")}}">Prices</a></div>
			<div class="section @if(Request::url() == url('/referal-program') ) active @endif"><a href="{{url("/referal-program")}}">Referral program</a></div>
			<div class="section @if(Request::url() == url('/testimonials') ) active @endif"><a href="{{url("/testimonials")}}">Testimonials</a></div>
			<div class="section @if(Request::url() == url('/faq') ) active @endif"><a href="{{url("/faq")}}">FAQ</a></div>
			<div class="section @if(Request::url() == url('/contacts') ) active @endif"><a href="{{url("/contacts")}}">Contacts</a></div>
			<div class="section @if(Request::url() == url('/privacy') ) active @endif"><a href="{{url("/privacy")}}">Privacy and Cookies Policy</a></div>
			<div class="section @if(Request::url() == url('/terms-of-use') ) active @endif"><a href="{{url("/terms-of-use")}}">Terms of use</a></div>
			<div class="section @if(Request::url() == url('/revision') ) active @endif"><a href="{{url("/revision")}}">Revision</a></div>
			<div class="section @if(Request::url() == url('/payment-auth') ) active @endif"><a href="{{url("/payment-auth")}}">Payment Authorization</a></div>
			<div class="section @if(Request::url() == url('/payment-system') ) active @endif"><a href="{{url("/payment-system")}}">Payment System</a></div>
			<div class="section @if(Request::url() == url('/our-writers') ) active @endif"><a href="{{url("/our-writers")}}">Our Writers</a></div>
			<div class="section @if(Request::url() == url('/our-extras') ) active @endif"><a href="{{url("/our-extras")}}">Our Extras</a></div>
		</nav>
		<div class="contacts f-white">
			<div class="third-width left margin-t-20">
				<div class="phone phone-icon">+1-929-367-2627 &nbsp<span class="f-0p8">(USA Toll-free)</span></div>
				<div class="phone phone-icon">+44-800-088-5728 <span class="f-0p8">(UK Toll-free)</span></div>
				
			</div>
			<div class="third-width left margin-t-20">
				<div class="email email-icon">support@originalpapersonly.com</div>
				<div class="email scype-icon">support@originalpapersonly.com</div>
			</div>
			<div class="third-width left margin-t-20 ">
				<div class="padding-l-20 icons">
					<a href="https://twitter.com/originalpapers1" class="sn-b"> <span class="sn-twiter"></span> </a>
					<a href="https://plus.google.com/u/0/104072062163353165984?hl=en" class="sn-b"> <span class="sn-google"></span> </a> 
					<a href="https://www.facebook.com/Originalpapersonly-1763588037231134/" class="sn-b"> <span class="sn-facebook"></span> </a> 
					<a href="skype:support@originalpapersonly.com?chat" class="sn-b">  <span class="sn-skype"></span> </a>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-section">
		<nav class="container">
			<center>
				<div class="section @if(Request::url() == url('/write-my-essay')) active @endif"> <a href="{{url("/write-my-essay")}}"> Write my essay </a> </div>
				<div class="section @if(Request::url() == url('/write-my-assignment') ) active @endif"> <a href="{{url("/write-my-assignment")}}"> Write my assignment </a> </div>
				<div class="section @if(Request::url() == url('/write-my-homework') ) active @endif"> <a href="{{url("/write-my-homework")}}"> Write my homework </a> </div>
				<div class="section @if(Request::url() == url('/write-my-report') ) active @endif"> <a href="{{url("/write-my-report")}}"> Write my report </a> </div>
				<div class="section @if(Request::url() == url('/write-my-thesis') ) active @endif"> <a href="{{url("/write-my-thesis")}}"> Write my thesis </a> </div>
				<div class="section @if(Request::url() == url('/write-my-paper') ) active @endif"> <a href="{{url("/write-my-paper")}}"> Write my paper </a> </div>
				<div class="section @if(Request::url() == url('/write-my-research-paper') ) active @endif"> <a href="{{url("/write-my-research-paper")}}"> Write my research paper </a> </div>
				<div class="section @if(Request::url() == url('/examples-and-samples') ) active @endif"> <a href="{{url("/examples-and-samples")}}"> Examples and Samples </a> </div>
				<div class="section @if(Request::url() == url('/write-my-papers') ) active @endif"> <a href="{{url("/write-my-papers")}}"> Write my papers </a> </div>
			</center>
		</nav>
	</div>
	<center>
		<div class="inline-block originalpapersonly-all"> <a> OriginalPapersOnly.com &nbsp&nbsp</a> </div>
		<div class="inline-block originalpapersonly-all"> <a> All rights reserved </a> </div>
	</center>
	<a href="http://ridanta.com" id="ridanta"><h1>Powered by Ridanta</h1></a>
</div>
<div class="mobile-footer">
	<p class="set-desktop">Desktop view</p>
	<p class="set-mobile">Mobile view</p>
</div>