@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Academic Paper Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>12]) 
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    Students, studying in college or university need to write academic papers all through their academic course. So to compose a logical and high-quality article you should know about what it takes to write academic papers. 
                </p>
                <p>
                    Academic papers include writing academic essays, research papers, term papers, academic reviews, reports, dissertations, speeches and other forms of academic assignments. Academic papers are written in different formats, for example, MLA style, APA format, Chicago style and Turabian papers
                </p>
                <h4>
                    Preparing to write Academic Papers 
                </h4>
                <p>
                    When you get ready to write academic papers you should definitely know the reason for doing that. You should formulate it as a thesis statement or research question of your Academic papers. You will prefer to write academic papers on topic, that is interesting for you. Structure of most academic papers includes such elements: Title, Abstract, Introduction, Methodology, Results, Discussion, Conclusion and References.
                </p>
                <h4>
                    Standards of Academic Papers 
                </h4>
                <p>
                    Academic papers include writing term papers, research paper, thesis and academic essays that have the same structure as examination paper. To write a paper you should create an outline, which is similar in many disciplines. 
                </p>
                <p>
                    You should include references for your Academic Papers. Most referencing in papers is done in APA and MLA standard. Some academic papers permit footnotes to be included into the main content while different others permit figures and tables toward the end of the text.
                </p>
                <h4>
                    The important steps of writing papers: 
                </h4>
                <p>
                    It is essential to find and develop the topic of academic papers. An interesting approach to write your essay is to think about the topic as about question.
                </p>
                <p>
                    Find appropriate books, articles and other materials to set the background of your academic paper.
                </p>
                <p>
                    When you get the background data, you can complete your essay with data from specific sources. 
                </p>
                <p>
                    Don’t forget to note down the reference of every source to refer to it later.
                </p>
            </div>
        </div>
    </div>
@stop