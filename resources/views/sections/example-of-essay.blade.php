@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-170">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Examples and Samples</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            <div class="container-for-text">
                <div class="example-of-essay">
                    {!! $item->full_text !!}
                </div>
            </div>
        </div>
    </div>
@stop