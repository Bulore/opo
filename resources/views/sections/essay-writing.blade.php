@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Custom Essay Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>1])
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    It is not as simple to write an essay as it seems. To write an essay you should firstly know the subject well enough to have your own ideas about it, and then share them, by writing an essay. Moreover, sometimes it requires a special format. It goes without saying, that it is hard to write an essay. You have to waste so much time and efforts on it. However, we were made to help you!
                </p>
                <h4> What can you get by using OriginalPapersOnly.com: </h4>
                <ul class="orange-list">
                    <li> Ready essay on any subject; </li>
                    <li> High quality of your essay; </li>
                    <li> Access to our website 24 hours a day, 7 days a week. </li>
                </ul>  
                <p>
                    Our Essay’s Experts were writing <b>professional essays</b> for years. We are always here to help you anytime. There is an online customer support service on our website. All you need is to spend 5 minutes to order essay online as the ordering process is as easy as ABC. 
                </p> 
                <h4> Why are we so special? </h4>
                <ul class="orange-list">
                    <li> You can find a lot of companies, which offer custom essay services, but only we guarantee 100% unique works. </li>
                    <li> We are available 24/ 7. </li>
                    <li> We write all essays in conformity with the main formatting styles: APA, Turabian/Chicago, MLA, Harvard, or any other. </li>
                </ul>   
                <p>
                    Our experts are professionals in writing essays, who hold advanced scientific degrees. Thus our team is capable to meet all the requirements of any educational institution. 
                </p>
                <p>
                    We can apply various styles of essay writing, which will satisfy the strictest professors. Moreover, if you don't like something in the <b>custom essay writing</b>, our writers will be happy to alter it to suit your needs free of charge.
                </p>
                <h4> Guarantee of Confidentiality </h4>
                <p>
                    You are afraid that your teacher can recognize that you are not the writer of the essay. Relax. Our service is strictly monitored for confidentiality. All our essays are written individually for every customer. Also, we never publish our essays on the internet. 
                </p>
            </div>
        </div>
    </div>
@stop  