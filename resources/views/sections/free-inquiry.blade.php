@extends('layouts.base')
@section('content')
	<link rel="stylesheet" href="/css/sideblocks.css">
	<link rel="stylesheet" href="/css/free-inquiry.css">
	<script src="js/jquery.validate.min.js"></script>
	<div class="container margin-t-120 m-margin-top-0" ng-controller="freeInquiryCtrl">
		<center>
		<div class="mobile-header m-bold-700">
			<p>Free Inquiry</p>
		</div>
		<div class="header-page m-display-none">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Free Inquiry</div>
		</div>
		</center>	 
		<div class="left-panel m-display-none">
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
		</div>
		<div class="content relative">
			<div class="container-for-text">
				<h4>
					Place a free inquiry below to confirm that a suitable qualified writer is available to work on your assignment.
				</h4>
				<p>
					If the topic of your paper is really complex and you would like to make sure that our professional authors can tackle it, we suggest you make an inquiry.
				</p>
				<p>
					All your requirements and instructions will be considered, and we will contact you as soon as the best writer is found.
				</p>
				<p>
					A confirmation email is usually sent within 5-10 minutes.
				</p>
			</div>
			<div class="container-for-text margin-t-20">
				<div class="href-orange-paging">
					<div ng-click='setOrangePage(1)' ng-class="(orangePage == 1)?'active':''">
						Free Inquiry
					</div>
				</div>
				<form action="/fail" id="inquiry-form" method="post" ng-submit="submitInquiry($event)" id="inquiry-form">
					{{csrf_field()}}
					<div class="container-form padding-t-20">
						<div class="form-group padding-t-20">
							<div class="text">
								<div class="inf-container">
									<div class="inf">
										Select your academic level
									</div>
								</div>
								<span class="middle">Academic Level:</span>
							</div>
							<div class="content">
								<div class="academic-level block">
									@foreach($academicLevels as $level)
										<div class="third-width" ng-click="setLevel({{$level->id}})" ng-class="(data.academic_level_id == {{$level->id}})?'active':''">
											{{$level->name}}
										</div>
									@endforeach
								</div>
							</div>

							<div class="form-group padding-t-20">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Choose the subject field of your task
										</div>
									</div>
									<span class="middle">Type of paper:</span>
								</div>
								<div class="content">
									<div class="input-select">
										<select-box placeholder="Please select" required data-error-message="Please select the necessary type of paper" id="type-of-paper">
		                        			<select-box-header></select-box-header>
		                        			<select-box-content>
		                        				@foreach($types->indexByCategory() as $category => $arr)
		                        					<select-box-option disable>{{$category}}</select-box-option>
		                        					@foreach($arr as $item)
														<select-box-option ng-click="setTypeId({{$item->id}})" data-type="{{$item->id}}">{{$item->name}}</select-box-option>
													@endforeach
		                        				@endforeach
		                        			</select-box-content>
		                        		</select-box>
									</div>
								</div>
							</div>
							<div class="form-group padding-t-20">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Choose the subject field of your task
										</div>
									</div>
									<span class="middle">Subject:</span>
								</div>
								<div class="content">
									<div class="input-select">
										<select-box placeholder="Please select" required data-error-message="Please specify the subject">
		                        			<select-box-header></select-box-header>
		                        			<select-box-content>
		                        				@foreach($subjects as $subject)
		                        					<select-box-option ng-click="setSubjectId({{$subject->id}})">{{$subject->name}}</select-box-option>
		                        				@endforeach
		                        			</select-box-content>
		                        		</select-box>
									</div>
								</div>
							</div>
							<div class="form-group padding-t-20">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Indicate the topic of your paper
										</div>
									</div>
									<span class="middle">Topic:</span>
								</div>
								<div class="content">
									<input type="text" class="input" ng-model="data.topic" name="topic">
								</div>
							</div>
							<div class="form-group padding-t-20">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Type here as many details as possible. The more specific you are - the easier it will be for the writer to complete your assignment correctly.
										</div>
									</div>
									<span class="middle">Short description:</span>
								</div>
								<div class="content">
									<textarea class="input" rows="6" ng-model="data.details" name="description"></textarea>
								</div>
							</div>
							<div class="form-group padding-t-20">
								<div class="text"></div>
								<div class="content">
									<div class="academic-level">
										<div class="padding-l-r-10" ng-click="setSpaced(2)" ng-class="(data.spacing == 2)?'active':''">Single Spaced</div>
										<div class="padding-l-r-10" ng-click="setSpaced(1)" ng-class="(data.spacing == 1)?'active':''">Double Spaced</div>
									</div>
									<div class="right spaced-text"><span ng-bind="data.pages_count">1</span> page = <span ng-bind="data.spacing * 275 * data.pages_count">275</span> words</div>
								</div>
							</div>
							<div class="form-group padding-t-20">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Please enter desired number of pages
										</div>
									</div>
								<span class="middle">Number of pages:</span>
								</div>
								<div class="content">
									<div class="left middle">
										<div class="academic-level pages middle">
											<div ng-click="decrementPage()">-</div>
											<div>
												<input type="number" ng-model="data.pages_count">
											</div>
											<div ng-click="incrementPage()">+</div>
										</div>
									</div>
									<div class="right middle">
										<div class="inline-block middle padding-r-10">
											<div class="inf-container">
												<div class="inf">
													If you need the Power Point Presentation, specify the number of slides you want. 1 slide = 50% of the cost per page
												</div>
											</div>
										<span class="middle">Slides:</span>
										</div>
										<div class="inline-block middle">
											<div class="academic-level pages right">
												<div ng-click="decrementSlide()">-</div>
												<div>
													<input type="number" ng-model="data.slides_count">
												</div>
												<div ng-click="incrementSlide()">+</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clear-after">
								<div class="slides-text right margin-t-10 margin-b-10">
									Please mind that speaker notes are not included by default. If you need speaker notes, it should be reflected in the page count above.
								</div>
							</div>

							<div class="form-group">
								<div class="text">
									<div class="inf-container">
										<div class="inf">
											Please specify the deadline in advance, to have some time to review the paper
										</div>
									</div>
									<span class="middle">First Draft Deadline:</span>
						 		</div>
								<div class="content relative">
									<div class="inline-block margin-r-50 relative" ng-show="data.pages_count<50">
										<div class="academic-level hours">
											<div ng-repeat="hour in deadlines | greater:0 | lessOrEqual:24 " ng-click="setDeadlineId(hour.id)" ng-class="(data.deadline_id==hour.id)?'active':''">
												{$ hour.name $}
											</div>
										</div>
										<div class="hours-text">Hours</div>
									</div>
									<div class="inline-block relative">
										<div class="academic-level hours">
											<div ng-repeat="hour in deadlines | greater:24 | lessOrEqual:336" ng-click="setDeadlineId(hour.id)" ng-class="(data.deadline_id==hour.id)?'active':''">
												{$ hour.name $}
											</div>
										</div>
										<div class="days-text">Days</div>
									</div>
									<div class="inline-block right relative" ng-show="data.pages_count>49">
										<div class="academic-level hours">
											<div ng-repeat="hour in deadlines | greater:336" ng-click="setDeadlineId(hour.id)" ng-class="(data.deadline_id==hour.id)?'active':''">
												{$ hour.name $}
											</div>
										</div>
										<div class="hours-text">Months</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				<div>
					<div class="href-blue-paging">
						<div ng-click='setBluePage(1)' ng-class="(bluePage == 1)?'active':''">
							I am new here
						</div>
						<div ng-click='setBluePage(2)' ng-class="(bluePage == 2)?'active':''">
							sign in
						</div>
					</div>
					<div class="container-blue-paging">
						<div ng-show="bluePage==1" class="page-height">
							<div class="width-400 margin-a">
								<div class="form-group">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Please make sure you have entered the correct email address to receive notifications of order completion status.
											</div>
										</div>
										<span class="middle">Email:</span>
								 	</div>
									<div class="content">
										<input type="email" class="input" ng-model="register.email" name="email">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										Confirm email:
									</div>
									<div class="content">
										<input type="email" class="input" ng-model="register.confirmemail" name="confirmemail">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										First Name:
									</div>
									<div class="content">
										<input type="text" class="input" ng-model="register.name" name="name">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										Last name:
									</div>
									<div class="content">
										<input type="text" class="input" ng-model="register.lastname" name="lastname">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Your password must contain at least 6 symbols (digits and letters only).
											</div>
										</div>
										<span class="middle">Password:</span>
								 	</div>
									<div class="content">
										<input type="password" class="input" ng-model="register.password" name="password">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										Confirm password:
									</div>
									<div class="content">
										<input type="password" class="input" ng-model="register.confirmpassword" name="confirmpassword">
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										Country:
									</div>
									<div class="content">
										<div class="input-select">
			                        		<select-box placeholder="Choose country" required data-error-message="Please choose country">
			                        			<select-box-header></select-box-header>
			                        			<select-box-content>
			                        				@foreach($countries as $country)
			                        					<select-box-option ng-click="setCountryId({{$country->id}})">{{$country->name}}</select-box-option>
			                        				@endforeach
			                        			</select-box-content>
			                        		</select-box>
			                    		</div>
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Please make sure you have entered the correct phone number, so that we could contact you to check on the details of your order and to verify your payment.
											</div>
										</div>
										<span class="middle">Cell phone:</span>
								 	</div>
									<div class="content">
										<input type="text" class="input" ng-model="register.phone" name="phone">
									</div>
								</div>
							</div>
							<div class="margin-a width-600 padding-20 margin-t-20">
								<label>
									<div class="check-container">
						                <input type="checkbox" name="remember" hidden="">
						                <div class="checkbox top"></div>
						                <div class="inline-block top text-check-box f-right width-570">
					                		I accept the <a href="{{url("/terms-of-use")}}" class="text-check-box">Terms of Use</a> and <a href="{{url("/privacy")}}" class="text-check-box">Privacy and Cookies Policy</a>. I express my prior affirmative consent to receive emails and sms from OriginalPapersOnly.com.
				                		</div>
					                </div>
				                	
								</label>
							</div>
						</div>

						<div ng-show="bluePage==2" class="page-height">
							<div class="width-400 margin-a padding-t-20 margin-t-20">
								<div class="form-group">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Please make sure you have entered the correct email address to receive notifications of order completion status. No spam guaranteed.
											</div>
										</div>
										<span class="middle">Email:</span>
								 	</div>
									<div class="content">
										<input type="email" class="input" ng-model="data.email" name="email">
									</div>
								</div>
								<div class="form-group padding-t-20">
								<div class="text">
										<div class="inf-container">
											<div class="inf">
												Please input the password to your existing account
											</div>
										</div>
										<span class="middle">Pasword:</span>
								 	</div>
									<div class="content">
										<input type="password" class="input" ng-model="data.password" name="password">
									</div>
								</div>
							</div>
							<div class="margin-a width-600 padding-20 margin-t-20">
								<label>
									<div class="check-container">
						                <input type="checkbox" name="remember" hidden="">
						                <div class="checkbox top"></div>
						                <div class="inline-block top text-check-box f-right width-570">
					                		I accept the <a href="{{url("/terms-of-use")}}" class="text-check-box">Terms of Use</a> and <a href="{{url("/privacy")}}" class="text-check-box">Privacy and Cookies Policy</a>. I express my prior affirmative consent to receive emails and sms from OriginalPapersOnly.com.
				                		</div>
					                </div>
				                	
								</label>
							</div>	
						</div>
					</div>
				</div>
				<div class="margin-t-20 margin-b-20">
					<center class="margin-t-20">
						<button class="common-button" id="submit"  type="submit">Submit Iquiry</button>
					</center>
				</div>
			</div>
			</form>
			</div>
		</div>

		
	</div>
@stop

@section('scripts')
	@parent
	<script type="text/javascript">
        app.filter('lessOrEqual', function () {
		    return function ( items, value ) {
		        var filteredItems = [];
		        angular.forEach(items, function ( item ) {
		            if ( item.hours <= value ) { filteredItems.push(item); }
		        });
		        return filteredItems;
		    }
		})
		app.filter('greater', function () {
		    return function ( items, value ) {
		        var filteredItems = []
		        angular.forEach(items, function ( item ) {
		            if ( item.hours > value ) { filteredItems.push(item); }
		        });
		        return filteredItems;
		    }
		})

        app.controller('freeInquiryCtrl', function ($scope, $http) {
			$scope.data = { 
				'type': 'register',
				'email': '',
				'confirmemail': '',
				'name': '',
				'last_name': '',
				'password': '',
				'confirmpassword': '',
				'phone': '',
				'country_id': undefined,
				'academic_level_id': {{$academicLevels[0]->id}},
				'type_id': undefined,
				'subject_id': undefined,
				'topic': '',
				'details': '',
				'spacing': 1,
				'pages_count': 1,
				'slides_count': 1,
				'deadline_id': 1,
		 	}

        	$scope.bluePage = 1;
        	$scope.orangePage = 1;

        	$scope.deadlines = {!! json_encode($deadlines) !!};

        	$scope.setLevel = function (level){ $scope.data.academic_level_id = level; };
        	$scope.setSpaced = function (spacing){ $scope.data.spacing = spacing; };
        	
        	$scope.setBluePage = function (bluePage){ $scope.bluePage = bluePage; };

        	$scope.setPage = function (page){ $scope.page = page; };
        	$scope.setTypeId = function (id){ $scope.data.type_id = id; };
        	$scope.setSubjectId = function (id){ $scope.data.subject_id = id; };
        	$scope.setDeadlineId = function (id){ $scope.data.deadline_id = id; };
        	$scope.setCountryId = function (id){ $scope.data.country_id = id; };


        	$scope.incrementPage = function (){ 
        		if($scope.data.pages_count < 999) $scope.data.pages_count++; 
        		else $scope.data.pages_count = 999;
        	};
        	$scope.decrementPage = function (){ 
        		if($scope.data.pages_count > 1) $scope.data.pages_count--;
        		else $scope.data.pages_count = 1;
        	};

        	$scope.incrementSlide = function (){ 
        		if($scope.data.slides_count < 999) $scope.data.slides_count++;
        		else $scope.data.slides_count = 999;
        	};
        	$scope.decrementSlide = function (){ 
        		if($scope.data.slides_count > 0) $scope.data.slides_count--;
        		else $scope.data.slides_count = 0;
        	};

			$scope.submitInquiry = function($event) {
				$event.preventDefault();
				selectBoxValidator("#inquiry-form");
				if($("#inquiry-form").valid() && selectBoxValidator("#inquiry-form")){
					if($scope.bluePage === 2) {
						$scope.data.type = 'login';
					}
					
					$http.post('', $scope.data, {}).then(
						function(data){
							document.location.href = "{{url('/user')}}";
						},
						function(error){ alert('error'); }
					);
				}
				return false;
			};
        });
	</script>

	<script>
		(function () {
			$("#submit").click(function () {
				var content = $("#subject").html();
				if(content == "Choose..."){
					$("#subject-input").addClass("error");
					$("#subject-error").css("display", "block");
					$("#subject").css("color", "#ff7575");
				}
			});
			$(".select").click(function () {
				$(this).closest(".input-select").find(".select-box").removeClass("error");
				$(this).closest(".input-select").find("#subject-error").css("display", "none");
				$(this).closest(".input-select").find("#subject").css("color", "#393939");
			});
			$("#inquiry-form").validate({
				rules:{
					description: {
						required: true,
						minlength: 10,
					},
					topic: {
						required: true,
						minlength: 3
					},
					email: {
						required: true
					},
					confirmemail: {
						required: true
					},
					name: {
						required: true,
						minlength: 3,
					},
					lastname: {
						required: true,
						minlength: 3
					},
					password: {
						required: true,
						minlength: 6
					},
					confirmpassword: {
						required: true,
						minlength: 6
					},
					phone: {
						required: true
					}
				},
				messages: {
					description: "The paper details should contain at least 10 characters",
					topic: "Please input a valid topic",
					email: "Please type in your email",
					confirmemail: "Please confirm in your email",
					name: "This field must contain a minimum of 3 letters",
					lastname: "This field must contain a minimum of 3 letters",
					password: "Password should be at least 6 characters",
					confirmpassword: "Password should be at least 6 characters",
					phone: "Please input a valid phone number"
				}

			});
		})();
	</script>
@stop