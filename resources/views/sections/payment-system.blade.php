@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="css/sideblocks.css">
<div class="container margin-t-120">
	<center>
	<div class="header-page">
		<div class="border"><div><div><div></div></div></div></div>
		<div class="text">Payment System</div>
	</div>
	</center>
	<div class="left-panel">
		@include("sideblocks.priceBlock")
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock")
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content">
		<div class="container-for-text f-black">
			OriginalPapersOnly.com provides you with a number of payment options. Thus, you can use your credit card, debit card, your PayPal account or make a wire transfer (powered by BlueSnap payment gateway) to pay for our services. Citizens of the European Union can pay with their credit card or debit card though BlueSnap.
		</div>
		<div class="container-for-text c-tpt">
			<h1 class="padding-n">PayPal</h1>
			<p>
				PayPal has established itself as the world's most reliable payment processor. 203 markets, 152 million active users and 100 currencies are figures that speak for themselves. Thanks to the service, you can make and receive payments simply using a bank account or your credit or debit card. It only takes a few minutes to sign up! As we accept Guest payments, you do not actually need a PayPal account to make a money transfer using the service. Here are some of the reasons why this payment method is so popular:
			</p>
			<div class="margin-l-20 padding-t-20 padding-b-20">
				<ul class="orange-list">
					<li>	
						<span class="f-blue"> 
							<b>
								100% protection from unauthorized access to your account.
							</b>
						</span>
						Bearing in mind a high percentage of online frauds, PayPal offers advanced protection against any cyber intruders.
					</li>
					<li>	
						<span class="f-blue"> 
							<b>
								Information safety. 
							</b>
						</span>
						PayPal uses advanced encryption algorithms to protect your information and does not share your personal details with any third parties.
					</li>
					<li>	
						<span class="f-blue"> 
							<b>
								High quality products that match the description. 
							</b>
						</span>
						f you are not fully satisfied with your purchase, PayPal will assist you with refunds.
					</li>
					<li>	
						<span class="f-blue"> 
							<b>
								Reliable sellers. 
							</b>
						</span>
						he service does not cooperate with unauthorized sellers, so if a service does not offer the PayPal payment option, you should think twice before using it. PayPal has been cooperating with us since the very beginning.
					</li>
					<li>	
						<span class="f-blue"> 
							<b>
								Instant payments.
							</b>
						</span>
						Transactions with PayPal are exceptionally fast, and your money will reach us almost at once. We might investigate suspicious payments. We can also ask you to authorize your payment.
					</li>
					<li>	
						<span class="f-blue"> 
							<b>
								Transparent transactions.
							</b>
						</span>
						PayPal is free to use, and there are no transactions fees!
					</li>
				</ul>
			</div>
		</div>
		<center>
			<a href="{{url('/order')}}"><input type="button" class="common-button" value="Order now"></a>
		</center>
	</div>
</div>
@stop