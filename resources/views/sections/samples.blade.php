@extends('layouts.base')
@section('content')
<link rel="stylesheet" type="text/css" href="/css/samples.css">
<link rel="stylesheet" href="/css/sideblocks.css">
<div class="container margin-t-120 m-margin-top-0">
	
	<div class="mobile-header m-bold-700">
		<p>Samples</p>
	</div>
	
	<center>
		<div class="header-page m-display-none">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Samples</div>
		</div>
	</center>
	<div class="left-panel m-display-none">
		@include("sideblocks.priceBlock")
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock")
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content">
		<drop-down-ordered-list class="drop-list-white none-order margin-t-n" data-open-only-one>
			<ordered-list-item>
				<ordered-list-header> Business </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Business/Answering Question.doc') }}">Answering Question.doc</a> </p>
					<p> <a href="{{ url('/files/Business/Business Project.doc') }}">Business Project.doc</a> </p>
					<p> <a href="{{ url('/files/Business/Ethics of Enterprise and Exchange.docx') }}">Ethics of Enterprise and Exchange.docx</a> </p>
					<p> <a href="{{ url('/files/Business/Experience Provider (EXER) New Social Media Platform.docx') }}">Experience Provider (EXER) New Social Media Platform.docx</a> </p>
					<p> <a href="{{ url('/files/Business/Response to the Article on Economics.doc') }}">Response to the Article on Economics.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Communication </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Communication/Campaign Proposal.docx') }}">Campaign Proposal.docx</a> </p>
					<p> <a href="{{ url('/files/Communication/Communication– Public Relations Plan.doc') }}">Communication– Public Relations Plan.doc</a> </p>
					<p> <a href="{{ url('/files/Communication/Essay.doc') }}">Essay.doc</a> </p>
					<p> <a href="{{ url('/files/Communication/Homework.docx') }}">Homework.docx</a> </p>
					<p> <a href="{{ url('/files/Communication/I Am a Strategic Leader.docx') }}">I Am a Strategic Leader.docx</a> </p>
					<p> <a href="{{ url('/files/Communication/Representational art.docx') }}">Representational art.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Culture </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Culture/Comparison Analysis.docx') }}">Comparison Analysis.docx</a> </p>
					<p> <a href="{{ url('/files/Culture/Film Has Been Characterized As the Great Modernist Art Form.doc') }}">Film Has Been Characterized As the Great Modernist Art Form.doc</a> </p>
					<p> <a href="{{ url('/files/Culture/Latin American Culture.docx') }}">Latin American Culture.docx</a> </p>
					<p> <a href="{{ url('/files/Culture/Shakespeare Films.docx') }}">Shakespeare Films.docx</a> </p>
					<p> <a href="{{ url('/files/Culture/The BBC News and The South China Morning Post.docx') }}">The BBC News and The South China Morning Post.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> English </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/English/Angry Black White Boy.doc') }}">Angry Black White Boy.doc</a> </p>
					<p> <a href="{{ url('/files/English/Articulating Beliefs.doc') }}">Articulating Beliefs.doc</a> </p>
					<p> <a href="{{ url('/files/English/Chainsaw Fingers.doc') }}">Chainsaw Fingers.doc</a> </p>
					<p> <a href="{{ url('/files/English/Emotions in the Workplace.doc') }}">Emotions in the Workplace.doc</a> </p>
					<p> <a href="{{ url('/files/English/Finding Common Ground.doc') }}">Finding Common Ground.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Environmental studies </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Environmental studies/Climate Change And Global Warming.docx') }}">Climate Change And Global Warming.docx</a> </p>
					<p> <a href="{{ url('/files/Environmental studies/Cîmmunicàtiîn ànd Crisis.doc') }}">Cîmmunicàtiîn ànd Crisis.doc</a> </p>
					<p> <a href="{{ url('/files/Environmental studies/Individual Relational Meeting.doc.docx') }}">Individual Relational Meeting.doc.docx</a> </p>
					<p> <a href="{{ url('/files/Environmental studies/Research Methodology Assignment.doc') }}">Research Methodology Assignment.doc</a> </p>
					<p> <a href="{{ url('/files/Environmental studies/The Disastrous Impact of the Nuclear Weapon upon the Nature.doc') }}">The Disastrous Impact of the Nuclear Weapon upon the Nature.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> History </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/History/Civil Rights.doc') }}">Civil Rights.doc</a> </p>
					<p> <a href="{{ url('/files/History/Racial Discrimination.doc') }}">Racial Discrimination.doc</a> </p>
					<p> <a href="{{ url('/files/History/The Cold War.doc') }}">The Cold War.doc</a> </p>
					<p> <a href="{{ url('/files/History/Timbuktu and Jenne and Duarte Barbosa on the Swahili City States.doc') }}">Timbuktu and Jenne and Duarte Barbosa on the Swahili City States.doc</a> </p>
					<p> <a href="{{ url('/files/History/Why was the Speech of Pope Urban II so Significant in Changing History.docx') }}">Why was the Speech of Pope Urban II so Significant in Changing History.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Literature </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Literature/Cinematic Possibilities.doc') }}">Cinematic Possibilities.doc</a> </p>
					<p> <a href="{{ url('/files/Literature/Cultural Autobiography.doc') }}">Cultural Autobiography.doc</a> </p>
					<p> <a href="{{ url('/files/Literature/Environmental Issues.doc') }}">Environmental Issues.doc</a> </p>
					<p> <a href="{{ url('/files/Literature/Literature - The role of “love” in Siddhartha and in The Bridge of San Louis Rey.docx') }}">Literature - The role of “love” in Siddhartha and in The Bridge of San Louis Rey.docx</a> </p>
					<p> <a href="{{ url('/files/Literature/Morality, Marriage and Country Wife in Jane Austin’s “Pride and Prejudice”.docx') }}">Morality, Marriage and Country Wife in Jane Austin’s “Pride and Prejudice”.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Medicine </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Medicine/Alzheimer Disease.doc') }}">Alzheimer Disease.doc</a> </p>
					<p> <a href="{{ url('/files/Medicine/How Teen Obesity and Diabetes Affect Childhood.doc') }}">How Teen Obesity and Diabetes Affect Childhood.doc</a> </p>
					<p> <a href="{{ url('/files/Medicine/Rapid Response to Disasters and Emergencies.doc') }}">Rapid Response to Disasters and Emergencies.doc</a> </p>
					<p> <a href="{{ url('/files/Medicine/Role of Physical Activity in Preventing Child Obesity.docx') }}">Role of Physical Activity in Preventing Child Obesity.docx</a> </p>
					<p> <a href="{{ url('/files/Medicine/VA Healthcare.doc') }}">VA Healthcare.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Philosophy </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Philosophy/Ethics and Morality.doc') }}">Ethics and Morality.doc</a> </p>
					<p> <a href="{{ url('/files/Philosophy/Inductive and Deductive Arguments.doc') }}">Inductive and Deductive Arguments.doc</a> </p>
					<p> <a href="{{ url('/files/Philosophy/Kohlberg Dilemma Matrix.doc') }}">Kohlberg Dilemma Matrix.doc</a> </p>
					<p> <a href="{{ url('/files/Philosophy/Summa Theologica of Aquinas and its Impact.docx') }}">Summa Theologica of Aquinas and its Impact.docx</a> </p>
					<p> <a href="{{ url('/files/Philosophy/Theory of Knowledge Assignment for the International Baccalaureate.docx') }}">Theory of Knowledge Assignment for the International Baccalaureate.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Psychology </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Psychology/A City Museum Permanent Exhibit.docx') }}">A City Museum Permanent Exhibit.docx</a> </p>
					<p> <a href="{{ url('/files/Psychology/Alternative Ways to Prevent Children from Bullying.docx') }}">Alternative Ways to Prevent Children from Bullying.docx</a> </p>
					<p> <a href="{{ url('/files/Psychology/Annotated Bibliography.docx') }}">Annotated Bibliography.docx</a> </p>
					<p> <a href="{{ url('/files/Psychology/Collectivism and Individualism.docx') }}">Collectivism and Individualism.docx</a> </p>
					<p> <a href="{{ url('/files/Psychology/Phychology- Language & Cognition.docx') }}">Phychology- Language & Cognition.docx</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Research paper </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Research paper/Aboriginal Education Studies.doc') }}">Aboriginal Education Studies.doc</a> </p>
					<p> <a href="{{ url('/files/Research paper/Democracy.docx') }}">Democracy.docx</a> </p>
					<p> <a href="{{ url('/files/Research paper/Dynamic Physical Education for Secondary School Students.doc') }}">Dynamic Physical Education for Secondary School Students.doc</a> </p>
					<p> <a href="{{ url('/files/Research paper/Ethical Issues in Research.doc') }}">Ethical Issues in Research.doc</a> </p>
					<p> <a href="{{ url('/files/Research paper/Reseaarch paper- Using Mixed Methods.doc') }}">Reseaarch paper- Using Mixed Methods.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Sociology </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Sociology/Critical Thinking Assignment.doc') }}">Critical Thinking Assignment.doc</a> </p>
					<p> <a href="{{ url('/files/Sociology/Discussion.doc') }}">Discussion.doc</a> </p>
					<p> <a href="{{ url('/files/Sociology/Marx’ Concept of “Social Class”.doc') }}">Marx’ Concept of “Social Class”.doc</a> </p>
					<p> <a href="{{ url('/files/Sociology/Post Test Exercise.doc') }}">Post Test Exercise.doc</a> </p>
					<p> <a href="{{ url('/files/Sociology/Stratification Media Analysis.doc') }}">Stratification Media Analysis.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header> Theater studies </ordered-list-header>
				<ordered-list-content>
					<p> <a href="{{ url('/files/Theater studies/Costume and makeup.docx') }}">Costume and makeup.docx</a> </p>
					<p> <a href="{{ url('/files/Theater studies/The History of Costume and makeup.docx') }}">The History of Costume and makeup.docx</a> </p>
					<p> <a href="{{ url('/files/Theater studies/Theater Studies- Ratings.doc') }}">Theater Studies- Ratings.doc</a> </p>
					<p> <a href="{{ url('/files/Theater studies/Theatre studies- Little Shop of Horrors.docx') }}">Theatre studies- Little Shop of Horrors.docx</a> </p>
					<p> <a href="{{ url('/files/Theater studies/“O” Show at Bellagio at Las Vegas.doc') }}">“O” Show at Bellagio at Las Vegas.doc</a> </p>
				</ordered-list-content>
			</ordered-list-item>
		</drop-down-ordered-list>
		<div class="left-panel d-display-none">
			@include("sideblocks.priceBlock")
		</div>
	</div>
</div>
@stop

@section('scripts')
	@parent
	<script>
		$('.functions .container').click(function(event){
		    var index = $(this).index() + 4;
		    var top = $('.orange-items .item:nth-child('+index+')').offset().top; 
		    top -= 70;
		    $('body,html').animate({scrollTop: top}, 1500); 
		});
	</script>
@stop
