@extends('layouts.base')
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/howItWorks.css">
	<link rel="stylesheet" href="/css/sideblocks.css">
	
	<div class="mobile-header m-bold-700">
		<p>How it work</p>
	</div>

	<div class="container margin-t-170 m-margin-top-0">
		<div class="functions m-display-none">
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-1"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Order form<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-2"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Submit<br>payment
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-3"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Order in <br>progress
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level" >
							<div class="fourth-level">
								<div class="five-level function-icon-4"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Order <br>delivered
				</div>
			</div>
		</div>
		<div class="left-panel m-display-none">
			@include("sideblocks.priceBlock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">
			<div class="items">
				<div class="item">
					<div class="header">1. Order form</div>
					<div class="content padding-t-20">
						<div class="left-block">
							<div class="items-icon-1"></div>
						</div>
						<div class="right-block">
							<div class="section">
								<div class="header arrow">How many pages should I choose?</div>
								<div class="content">
									It depends on the number of words that you need to have in your paper. We usually work with double-spaced pages 1-inch margin on all the sides which have about 270 words, and a single-spaced page will contain about 550 words. 
								</div>
							</div>
							<div class="section">
								<div class="header arrow">When can I download my order?</div>
								<div class="content">
									We will always finish your paper before the needed deadline. Please remember that the deadline that you indicate is for the first draft of your paper only. The countdown of your deadline starts only when you make the payment. If there will be any questions and you'll need a revision, it will have its own deadline.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">Can I first get my paper and pay later?</div>
								<div class="content">
									No. Our writers will start to write your order as soon as it will be prepaid. The deadline countdown also starts after the payment. You can also feel free to see the examples of our works samples in order to evaluate the quality we offer. 
								</div>
							</div>
							<div class="section">
								<div class="header arrow">Can I trust you?</div>
								<div class="content">
									Of course, you can trust us. There are thousands of satisfied and grateful clients of OriginalPapersonly.com and believe us, you will be one of those. Also, we do not need any specific information of yours, only your name, phone, address and four last digits of your pay card. You need to fill in your basic information for authorization purposes. 
								</div>
							</div>
						</div>
						<ul class="blue-list full-width">
							<li>
								Fill in the required information (make sure it is right as you won't be able to change anything in this form later);
							</li>
							<li>
								In case, it is your first order at OriginalPapersonly.com your account will be created automatically. After filling the form you will receive a confirmation email. 
							</li>
						</ul>
					</div>
				</div>
				<div class="item">
					<div class="header">2. Submit payment</div>
					<div class="content padding-t-20">
						<div class="left-block">
							<div class="items-icon-2"></div>
						</div>
						<div class="right-block">
							<div class="section">
								<div class="header arrow">What do I pay for?</div>
								<div class="content">
									You pay only for the body of your paper. Title pages and references are free. We will format your paper according to your requirements, it is also free
								</div>
							</div>
							<div class="section">
								<div class="header arrow">What is authorization?</div>
								<div class="content">
									You need to authorize for us to see that you are the person who made an order and the payment not someone who somehow has access to your credit card. This step of the whole process is of an utmost importance especially, with big orders with short deadlines
								</div>
							</div>
							<div class="section">
								<div class="header arrow">Do you provide discounts?</div>
								<div class="content">
									Yes, we offer discounts to our appreciated clients. You can learn more about our discounts here.
								</div>
							</div>
						</div>
						<ul class="blue-list full-width">
							<li>
								We accept Mastercard, AmEx, Visa, Discover.
							</li>
							<li>
								Your payment is processed through a secure online payment system
							</li>
						</ul>
					</div>
				</div>
				<div class="item">
					<div class="header">3. Order in progress</div>
					<div class="content padding-t-20">
						<div class="left-block">
							<div class="items-icon-3"></div>
						</div>
						<div class="right-block">
							<div class="section">
								<div class="header arrow">Who is going to write my paper?</div>
								<div class="content">
									We have a staff of the professionals who will be happy to works with you. All of our writers are experts in particular fields so that you will definitely find someone who may help you with your paper. All of our writers have either Bachelor's or Master's Degrees. 
									<br>
									You can ask a writer who will accomplish your order to write either in British or American English. 

								</div>
							</div>
							<div class="section">
								<div class="header arrow">Can I communicate with my writer?</div>
								<div class="content">
									Definitely, you can. We have a messaging system for you; you can find it in your personal profile options. You won't be able to communicate with your writer via phone or email, for privacy reasons only.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">How can I send my files to the writer?</div>
								<div class="content">
									You have to provide a writer with as much information as it is possible in order to receive the high-quality paper. You can use "Files" section to upload all the information you have. The system recognizes all major file formats, still if there are any problems concerning the upload you can contact our Support Representatives with the help of live chat.
									<p> It will be better to provide all the material in .doc or .pdf. formatting.</p>
								</div>
							</div>
						</div>
						<ul class="blue-list full-width">
							<li>
								Upload additional materials (if needed)
							</li>
							<li>
								Contact with Customer Support Representatives  or your writer
							</li>
							<li>
								Wait for your order to be completed
							</li>
						</ul>
					</div>
				</div>
				<div class="item">
					<div class="header">4. Order delivered</div>
					<div class="content padding-t-20">
						<div class="left-block">
							<div class="items-icon-4"></div>
						</div>
						<div class="right-block">
							<div class="section">
								<div class="header arrow">How will I get my order?</div>
								<div class="content">
									We will write you an email when your order is ready. Then you will receive the completed order to your profile page and you will need to go to the “Files” section of your profile. There you will have an opportunity to preview your paper.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">
									What is a preview?
								</div>
								<div class="content">
									Firstly you will receive a preview of your paper with a watermark on it. After the reading of a preview, you can assure if it suits all the requirements then you can either approve it or send it back to us. If you need a revision, you will have to set a deadline for a new version of your paper. Revision instructions are obligatory for you to provide.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">I want something to be changed in my paper</div>
								<div class="content">
									You are able to have three free revisions if you want to change something in your paper.
									<br>
									You can request a revision before or after your paper approval. In the second case, you will have to address to our Support Representatives.
									<br>
									After the approval of your paper, you will have 7 days for a revision and up to 14 days if your order was above 20 pages.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">
									How can I be sure that my paper is not plagiarized?
								</div>
								<div class="content">
									We can provide you with the official document of plagiarism report.
								</div>
							</div>
							<div class="section">
								<div class="header arrow">
									Is Turnitin used to check my paper for plagiarism?
								</div>
								<div class="content">
									Every single paper will be checked for plagiarism however we do not use Turnitin. This website saves all the papers into their database. You can only check your paper once with the help of this service, it will not be original and will be shown as plagiarized after you checked it with Turnitin. We use WebCheck in order to avoid any databases. It is one of the best services to check the originality of the paper. It will show really well how plagiarized is the paper if it really is.
								</div>
							</div>
						</div>
						<ul class="blue-list full-width">
							<li>
								Preview the completed order in .jpg format
							</li>
							<li>
								Approve and download your paper in Word .doc
							</li>
							<li>
								Send for a revision (free of charge!)
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="left-panel d-display-none">
				@include("sideblocks.priceBlock")
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script type="text/javascript">
		$('.functions .container').click(function(event){
		    var index = $(this).index() + 1;
		    var top = $('.items .item:nth-child('+index+')').offset().top; 
		    top -= 70;
		    $('body,html').animate({scrollTop: top}, 1500); 
		});
	</script>
@stop