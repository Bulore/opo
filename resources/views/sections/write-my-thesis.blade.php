@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Can You Write My Thesis for Me?</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>If you have difficulties with writing your thesis, if you don't know how to write it, but you want to get an "A" in your class - it is time to meet OriginalPapersOnly.com! Have you ever asked yourself questions like, "<i>Who can do my thesis for me?</i>", "<i>Can I find real professionals to write my thesis for me?</i>"</p>
                <p class="color-blue f-w-900">We Offer Students' Problems Solution - OriginalPapersOnly.com!</p>
                <p>In my life I have faced the same problems and was very confused. I had no idea of how to write my thesis. That is when I understood that I couldn't manage without assistance. I visited my relatives and asked, "Can you write my thesis, please?" But they couldn't. I worried about failing my thesis. A friend of mine <b>recommended visiting OriginalPapersOnly.com</b>. I confided that I've never trusted so called, "online helpers" and never believed they could do my thesis. Now I realize how wrong I was! OriginalPaperOnly.com was able to <i>do my thesis</i> better than anyone else!</p>
                <p class="color-blue f-w-900">OriginalPapersOnly.com Is Worth Trusting!</p>
                <p>OriginalPapersOnly.com is not a typical web-site. There are more than 1000+ writers who are ready to write my thesis for me ANY time and possess all the necessary skills in different fields of knowledge. From now on OriginalPapersOnly.com will always be the only agency <b>I trust to write my thesis</b> (dissertation/research paper/term paper/essay, etc). Master's thesis, college thesis, doctoral thesis, graduate thesis - these and other such works can easily be entrusted to OriginalPapersOnly.com writing agency without doubt about accuracy.</p>
                <p>I was impressed with how affordable OriginalPapersOnly.com prices were. I was sure that they would do my thesis like no one else could. Plus, I got the discounts. OriginalPapersOnly.com is absolutely trustworthy, as it utilizes the PayPal and other reliable payment systems in order to make payment procedure as convenient as possible. That guarantees that my money and the writer team will do my thesis for me the best way.</p>
                <p>Remember, when you begin to work with OriginalPapersOnly.com writing agency, you will definitely get non-plagiarized, mistake-free and high-level thesis writing.</p>
                <p class="color-blue f-w-900">Is There Money Back Guarantee?</p>
                <p>In the off-chance you are not satisfied with the thesis, OriginalPapersOnly.com writing agency <b>has a money back guarantee</b>, so you can be confident your money will be returned in this case.</p>
                <p>When the time came to get my thesis paper, I was more than just impressed! The non-plagiarized thesis met all the given requirements and it had the highest quality.</p>
                <p>If you want to have your thesis paper written in an excellent manner, doubt no more - contact OriginalPapersOnly.com and be sure to get the perfect result! Just send your "<i>Can you write my thesis, please</i>" request and all your hopes will be granted!</p>
            </div>
        </div>
    </div>
@stop