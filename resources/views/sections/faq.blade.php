@extends('layouts.base')
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/faq.css">
	<link rel="stylesheet" href="/css/sideblocks.css">
	
	<div class="mobile-header m-bold-700">
		<p>FAQ</p>
	</div>

	<div class="container margin-t-170 m-margin-top-0">
		<div class="functions m-display-none">
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-1"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Your order<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-2"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Security & <br>Authorization
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-3"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Service<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-4"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Your Writer<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-5"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Extras<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-6"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Delivery<br><br>
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-7"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Revision<br><br>
				</div>
			</div>
		</div>
		<div class="left-panel m-display-none">
			@include("sideblocks.priceBlock")
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">
			
			<div class="orange-items">
				<h1>Frequently Asked Questions</h1>
				<div class="white-line"></div>
				<div class="padding-t-20 padding-b-20 f-gray-b">
					Here are our most frequently asked questions about our services. Please, find out  answeres below.
				</div>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">My order</h1>
					<ordered-list-item class="">
						<ordered-list-header>How much does it cost?</ordered-list-header>
						<ordered-list-content>
							<p>
								We have an automatic calculator, which is on our Home page. So you can know the price quickly and conveniently. Here is also our “Prices” page, check it out.
							</p>
							<p>
								To know the cost you should consider 4 factors. Such as deadline, academic level, the number of pages, and optional extras. As you can see, our Pricing policyis highly flexible and will help you get the product that will suit your needs.
							</p>
							<p>
								We strongly warn our visitors against online writing services offering you a custom paper for less than $7 per page. In the overwhelming majority of cases, such websites are a fraud as it is virtually impossible to produce a quality paper for this money. Even if you get a good paper, it can eventually end up in an online essay database and thus will be regarded as plagiarized by your tutors.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>How many pages should be in my paper?</ordered-list-header>
						<ordered-list-content>
							<p>
								The amount of pages depends on how many words you need. One single spaced page has 550 words, and one double spaced page has 275 words. You can use our automatic calculator to count the price, just specify the number of pages for the body of your essay. We provide yoy The Title and Reference pages for free.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>How much time does it take to finish my paper?</ordered-list-header>
						<ordered-list-content>
							<p>
								You choose the deadline. 3 hours is the minimum time, during which we can complete your order. The countdown to deadline will start as soon as you have made the payment. Please note that your chosen deadline is true for the first draft of your paper, the one without revisions. Revision takes slightly more time, and this must be taken into consideration while making an order.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Security &amp; Authorization</h1>
					<ordered-list-item>
						<ordered-list-header>
							Is my personal information safe?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Our service is 100% confidential. Even though you are asked to provide your personal information, it is only used to improve the quality of our cooperation, to contact you in case of any urgent issues regarding your order or to authorize that your credit card has not been stolen. Your personal information is never disclosed to any third parties. The Customer Support Representatives at OriginalPapersOnly.com will also use this information to reach you to clarify any questions regarding your paper, if necessary. For further details about your privacy, please view our Privacy Policy page.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Is it safe to pay beforehand?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Thousands of our clients have been satisfied with the security of our service. There is a very good chance that you will be, too! The payment information that you provide during your order goes directly to the payment processor through a secure web protocol. We receive just the confirmation of payment, your first and last names, the address, the 4 last digits of your card, and your phone number. This information is available to us via a request to the payment processor and is used for authorization purposes only. More information about our payment systems can be found here.
							</p>	
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>What is “authorization”?</ordered-list-header>
						<ordered-list-content>
							<p>
								We take appropriate measures to protect our customers from fraud. We must make sure that our client is a real person and that his/her credit card is not being used as a means to place an order with us. If necessary, our Risk Department will do everything possible to find out the payers' identity and their means. We hope you understand how important authorization is. 
							</p>
							<p>
								There are several types of authorization: phone authorization, email authorization, and credit card authorization in some cases. Please review our Authorization Policy and remember that we may deny our services to unauthorized customers.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							I have not received my authorization  code, what should I do?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								We will send you two codes: one as an email and the other one as a text message. That is why it is important to provide your mobile number, not the landline one. You must enter both codes in the appropriate fields in your personal account. Additionally, double check if your device supports sms and if this feature has not been blocked by your carrier. Please do not try to submit the email authorization code for the phone authorization, the codes are different. It may take up to 20 minutes for the codes to reach you. For additional assistance, contact Customer Support Representatives.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Service</h1>
					<ordered-list-item>
						<ordered-list-header>Should I pay beforehand for your services?</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, it is. The writer will only start working after we have received your payment. With all our guarantees, you can be sure that your money is secure and you will receive what you are paying for. We will write a quality paper according to your specific guidelines, and if you still hesitate about using our service, well, don’t – just view our sample papers and place an order.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>Is it cheating?</ordered-list-header>
						<ordered-list-content>
							<p>
								We provide you with just an example of how your work should be done. You have an amazing opportunity to learn from some of the best writers out there and model your future papers accordingly. You can treat our service as an online library tailored to your specific needs. And everyone knows that going to the library for research is NOT cheating.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>Are your papers pre-written?</ordered-list-header>
						<ordered-list-content>
							<p>
								No. Our company does not have a pre-written paper database. We understand that selling pre-written papers is basically cheating on our former customers, and actions like this are not acceptable to us. We have a very strict policy against it, and if you find out that your paper was pre-written, we suggest you notify the Customer Support Representatives and receive a 100% refund. All our papers are written from scratch.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Do you format my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								We understand that formatting is an important part of your grade. All custom written papers are formatted according to your requirements – free of charge! If you don't provide these instructions, your formatting will be double spaced 12pt Arial font with 1 inch margins. Please note that whichever format you require, all the pages are double spaced by default, and the average page has 275 words.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Will my papers be plagiarism free?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								All our papers are 100% unique and properly cited. We use a trusted plagiarism detection checker to ensure this. We are aware of the serious impact plagiarism can have on your academic performance, so we carefully analyze all our papers. Unlike popular plagiarism detection engines used by many universities (e.g. Turnitin.com), we never report the results to any public database. Therefore, such plagiarism prevention is 100% safe for you. 
							</p>
							<p>
								The problem of most plagiarism detection systems is that they save all of the submitted papers in a database, making it impossible to check a document for plagiarism more than once. Hence, if you try to check the paper a second time, the system will show a 100% level of plagiarism.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I get my money back if I don’t like the delivered product?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								When you use our service, you can be sure that your money does not go to waste. If you think that the requirements of your order were not met, you can request a free revision. You can also cancel your order and expect to get your money back according to our Money Back Guarantee policy.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I make money by using your service?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, thanks to our Referral Program, you are able not only to receive high-quality assignments, however earn some money as well. You just need to register and send out the code to your friends. It is more than easy, read more here.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Writer</h1>
					<ordered-list-item>
						<ordered-list-header>
							Who will write a paper for me?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								You can notify us if you want your writer to be British, American, etc. Just remember that all our writers are experts in their respective fields. They have been hand-picked by our Quality Assurance Department and have years of writing experience.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I choose my previous writer again?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, we provide such an option. Just choose "My previous writer" in the "Preferred writer" section of the order form. You will be asked to enter the ID number of your writer. Please note that this option is available for orders with a 1 day deadline and above only. The price will be increased due to the deadline you specify and independently from the author's level. If your writer is not available, another professional writer will be provided to complete your assignment.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I provide my writer  specific materials for my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								You may wish to contact your writer directly to make sure that he/she has fully understood your instructions. We have a user-friendly messaging system that allows you to contact our Customer Support Representatives or your writer any time you want. All you need to do is to find your order in your account and choose the person, to whom you want to send a message. For security purposes, you may not mention your personal details or ask the writer to disclose his personal information.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							What if I need the writer to use specific materials for my order?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								We strongly encourage you to provide the writer with as many documents as possible. After all, the quality of your paper will depend on this. Each order has the section "Files", and you can upload your documents directly for the writer to see them. Our system accepts most types of files up to 20 MB in size. If you cannot upload your document, you can contact the Customer Support Representatives who will gladly help you deliver your documents to your writer. You can also send your files to support@originalpapersonly.com and mention your order number in the message subject.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Additionally</h1>
					<ordered-list-item>
						<ordered-list-header>
							What should I do to receive a high-quality paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								You are sure to receive a high-quality paper with every single order you place. Still, you can choose one of our best writers to work on your paper. To do so, select the "TOP writer" option in the "Preferred writer" section. The price of your order will increase automatically once you choose this feature.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							What should I do to become your VIP client?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								You have this opportunity if you choose our "VIP customer service" feature. In this case, all your requests will be handled in the first place. Also, a VIP client receives SMS notifications when the order is given to the writer and when it is completed. If you choose this option, you will always be number one, just like your order. It is available for $14.99 only. For more details, see the Pricing section of our website.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I get an  official plagiarism report for my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								You can get one if you want to. Simply choose "I want to receive official Plagiarism report" in the order form. In this case, a report generated by the professional plagiarism checker WebCheck will be uploaded for you right after the paper approval. Prices start at $9.99. See the Prices section for additional information.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							How can I order an Abstract for my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								With OriginalPapersOnly.com, you can be sure of every single word your paper contains. Writing an abstract isn't a problem for us either, so you can add it while placing an order by choosing "Add an Abstract page to my paper. Please note that this type of work differs from paper writing, so the writer will need a different set of skills to write a professional abstract. That is why it will cost you additional $14.99 as stated in our Prices section.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I get a discount?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Our discount policy can be found on our Prices page. If you qualify for a discount, please, contact our Customer Support Representatives.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Do you provide title and reference pages for free?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, we provide them for free. For example, when you order a 5 page paper, you receive 7 pages in total – 5 pages for the body of the paper, then the title page and the reference page
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can I get  sources my writer used when working on my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								When you would like to have the materials used as the sources in your paper, select the "Sources used" extra. Once chosen, you will get a soft copy of such materials. They are available in .doc/.docx/.pdf formats, as print screens or images of the pages used. You can download the materials in your control panel after your order is ready.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Is there a table of contents for my document?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, we do! :) Because we understand that nothing creates a better first impression of your paper than a great looking table of contents. Our writers will make sure you succeed. Just choose the "Table of Contents" extra on the order page in your control panel.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							Can you additionally proofread my paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								Yes, one of our proficient editors can proofread your work after the writer. You can get Editor's Check extra on your order page or ask Customer Support Representatives to add it.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Delivery</h1>
					<ordered-list-item>
						<ordered-list-header>
							How will you deliver my finished paper?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								When the writer finishes working on your order, you will be notified via email and/or phone. To receive your paper, you will be required to log in to your account at OriginalPapersOnly.com and approve the preview version. A preview is a watermarked image of your paper. You will need to review your order and provide additional comments on it, if necessary. We offer three free revisions. If there’s anything you’d like to get changed in your paper, you can easily ask for a free revision. If you are completely satisfied with how your paper looks and would like to receive an editable MS Word version, just press the “Approve” button.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>
							What can I do  if I forget password to access my account?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								If you forget your password to access your account, there is a "Reset password" option. You can reset your password using only your email address. Just be sure to remember your email!
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>What should I do If the deadline has been missed?</ordered-list-header>
						<ordered-list-content>
							<p>
								If the deadline has been missed, you need to immediately notify our Customer Support Representatives and explain the details. 
							</p>
							<p>
								Our representatives will examine your case and will explain what should be done. Whether the deadline has been missed because of the writer or because of the customer, a new deadline needs to be arranged. If the paper was not delivered on time because of the writer, the price will be recalculated and a proper refund will be made. If we are late because we are revising your order on your instructions or because you did not mention the full details from the very beginning, please bear with us and let us finish the work properly. 
							</p>
							<p>
								For more details, please view our Money Back Guarantee page.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Revision</h1>
					<ordered-list-item>
						<ordered-list-header>
							What should I do If I don't like the delivered product?
						</ordered-list-header>
						<ordered-list-content>
							<p>
								We aim for the highest quality, so if you are not satisfied with your paper, feel free to ask for a revision. At OriginalPapersOnly.com, we offer three free revisions for you to get the result you want. However, we have a strict policy about the order instructions. The revision instructions must not contradict the initial ones. No new documents will be taken into consideration in case of revision. After you have approved your paper, you have a week to notify the Customer Support Representatives if you want any changes to be made in your document. If our Quality Assurance Department decides that the paper was completed according to your initial instructions, then your revision request will be rejected.
							</p>
							<p>
								For additional information, please consult our Revision Policy.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>What is a “preview”?</ordered-list-header>
						<ordered-list-content>
							<p>
								A preview is an image of the paper with watermarks set by our company. You will need to review your paper and provide comments on it, if any. We offer three free revisions. Hence, if you find something that needs to be improved, be sure to ask for a revision. You must be as specific as possible and try to tell us all your instructions in just one letter. If everything is fine and you want to receive an MS Word version, all you need to do is press the "Approve" button and you will get your final version in a matter of seconds.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header>How can I revise my paper?</ordered-list-header>
						<ordered-list-content>
							<p>
								There is nothing easier: go to your order page in your account, find and press "Send for revision" button, include all the revision instructions in the pop-up window, and specify a time frame for revision. Please note that the last step is important. Your writer will stick to all your revision instructions to ensure the highest quality of the final product. The revision option is also possible after you have pressed the “Approve” button, but in this case you will need to contact Customer Support Representatives for any of your 3 free revisions.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
			</div>
			
			<div class="left-panel d-display-none">
				@include("sideblocks.priceBlock")
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script>
	
		function addHover(index){
			$('.functions .container:nth-child('+ index +') .first-level').addClass('hover');
		}
		function removeHover(index){
			$('.functions .container:nth-child('+ index +') .first-level').removeClass('hover');
		}
		
		$(function(){
			setTimeout(function(){
				var items = $('.functions .container .first-level');
				Array.prototype.forEach.call(items, function(item, index){
					var i = index+1;
					setTimeout("addHover('"+i+"')", i*200);
					setTimeout("removeHover('"+i+"')", i*200 + 600);
				});
			}, 200);
		})
		
		$('.functions .container').click(function(event){
		    
		    var index = $(this).index() + 4;
		    var top = $('.orange-items .item:nth-child('+index+')').offset().top; 
		    top -= 70;
		    $('body,html').animate({scrollTop: top}, 1500); 
		});
	</script>
@stop