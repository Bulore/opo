@extends('layouts.base')
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/samples.css">
	<link rel="stylesheet" href="/css/sideblocks.css">
	<link rel="stylesheet" href="/css/testimonials.css">

	<link rel="stylesheet" href="{{url('css/user/carousel.min.css')}}">
	
	<div class="mobile-header m-bold-700">
		<p>Testimonials</p>
	</div>

	<div class="container margin-t-170 m-margin-top-0">
		<div class="header-page  m-display-none">


			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					@foreach($sliderComments as $index=>$comment)
						<div class="item @if($index == 0) active @endif">
							<div class="comment-rating">
								<div class="comment-stars" style="width: {{$comment->stars*16}}px;"></div>
							</div>
							<div class="comment-block">
								<p>
									{{$comment->text}}
								</p>
							</div>
							<div class="carousel-caption">
								<span class="comment-author">{{$comment->name}}, {{$comment->from}}</span>
								<span class="comment-date">{{$comment->date}}</span>
							</div>
						</div>
					@endforeach
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon icon-prev" aria-hidden="true"></span>
					<span class="sr-only"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon icon-next" aria-hidden="true"></span>
					<span class="sr-only"></span>
				</a>
			</div>



		</div>


		<div class="left-panel m-display-none">
			@include("sideblocks.priceBlock")
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>


		<div class="content">
			<h1 class=" m-display-none">Testimonials</h1>
			<div class="white-line"></div>
			<div class="testimonials">
				@foreach($comments as $comment)
				<div class="testimonial-item">
						<div class="rating-block">
							<div class="stars"></div>
							<div class="fill-stars" data-ammount="{{$comment->stars}}"></div>
							<p>{{$comment->name}}, {{$comment->from}}</p>
							<div class="ellipse">
							</div>
						</div>
						<div class="comment-block">
							<span>Subject:</span>
							<span>{{$comment->subject}}</span>
							<span>Pages:</span>
							<span>{{$comment->pages}}</span>
							<span>{{$comment->date}}</span>
							<hr>
							<p>{{$comment->text}}</p>
						</div>
				</div>
				@endforeach
				<center>
					{{$comments->render()}}
				</center>
			</div>
			<div class="left-panel d-display-none">
				@include("sideblocks.priceBlock")
			</div>
			<h1 class=" m-display-none margin-t-50">Facebook</h1>
			<div class="white-line margin-b-35"></div>
			<div class="comments">
				<div class="fb-comments" data-href="https://www.facebook.com/Originalpapersonly-1763588037231134/" data-width="730" data-numposts="5"></div>
			</div>
		</div>
	</div>
	<div id="fb-root"></div>
@stop

@section('scripts')
	@parent
	<script src="{{url('js/user/bootstrap.min.js')}}"></script>
	<script>
		$(function(){
			function setRaiting(){
				$('.fill-stars').each(function(index){
					var ammount = $(this).attr('data-ammount');
					var constWidth = ($(document).width()<1169) ? 26.1 : 19;
					$(this).css('width', ammount*constWidth);

				})
			}
			setRaiting();
			window.onresize = function(){setRaiting()};
		})
		$(function(){
			if($(document).width() > 1169){
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			}
		})
	</script>
@stop