@extends('layouts.base')
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/prices.css">
	<link rel="stylesheet" href="/css/sideblocks.css">
	
	<div class="mobile-header m-bold-700">
		<p>Prices</p>
	</div>

	<div class="container margin-t-170 m-margin-top-0">
		<div class="functions m-display-none">
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-1"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Prices starting <br>at $11.99
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-2"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					10% <br>discount
				</div>
			</div>
			<div class="container">
				<div class="first-level">
					<div class="second-level">
						<div class="third-level">
							<div class="fourth-level">
								<div class="five-level function-icon-3"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="text">
					Loyalty <br>Program
				</div>
			</div>
		</div>
		<div class="left-panel m-display-none">
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">
			<h1 class="m-display-none">Prices</h1>
			<div class="left-panel d-display-none">
				@include("sideblocks.priceBlock")
			</div>
			<div class="white-line"></div>
			<p>
				Prices are listed in USD per 1 page, which is typed in Arial font 12pt, double-spaced. The average page has approximately 275 words.
			</p>
			<div class="clear-after m-display-none">
				<div class="flex-justify-center">
					<div class="clear-after inline-block left-panel width-400">
						@include("sideblocks.priceBlock", ["prefix" => 'second'])
					</div>
				</div>
			</div>
			<p>
				Please bear in mind that VAT is not included in the prices listed. It is only charged to our customers from the European Union. It will be added to the cost of the order in the process of payment. Also, a currency conversion fee might be applied as you will be charged in the US dollars.
			</p>
			<h4>Power Point Presentation Slides:</h4>
			<p>1 slide = 50% of cost per page</p>
			<h1>Loyalty Program</h1>
			<p>What can you get with our loyalty program?</p>
			<p>
				Our Loyalty Program is a special system that allows you to get bonuses for each and every order you place with us and use them as payment for your future orders.
			</p>
			<p>How can you use our loyalty program?</p>
			
			<div class="flex-justify-space-around flex-wrap not-work-flex max-full-width">
				<div class="orange-block-over">
					<div class="orange-block">
						<div class="content">
							Place your order and get your first 10% bonus from your payment after it is completed.
						</div>
						<div class="number">1</div>
					</div>
				</div>
				<div class="orange-block-over">
					<div class="orange-block">
						<div class="content">
							Approve your order and transfer bonuses to your Store credit balance in your profile.
						</div>
						<div class="number">2</div>
					</div>
				</div>
				<div class="orange-block-over">
					<div class="orange-block">
						<div class="content">
							Pay for the next order with your bonuses. Apply discounts from our emails, if any.
						</div>
						<div class="number">3</div>
					</div>
				</div>
			</div>

			<p>
				Log in to see more details in “My bonuses” section about how to get the most from our loyalty program.
			</p>
			<h1>We will Help You with Admission and Application</h1>
			<p>
				At OriginalPapersonly.com, you will always receive A1 help with admission to the university you dreamt to enter, or with an application for the job you need. We know how it may be important and we understand how it can affect your life. Be sure, we will do our best to help you with the task you need to accomplish. Our writers are the best to find, they are able to accomplish even the hardest task you may receive. You can entrust it only to our proficient writers who obtained their Master's degree and have practiced in writing for more than 5 years. Believe us, we won't ever let you down!
			</p>
			<p>
				You shouldn't worry about the revealing of your personal information. You can be sure that we have taken all security measures and all personal information that you give us is protected against loss. We do not need any specific information of yours, only your name, phone number, address and four last digits of your pay card.

			</p>
			<h4>
				We can provide you with 4 types of admission papers:
			</h4>
			<ul class="orange-list">
				<li>Admission essay</li>
				<li>Application letter</li>
				<li>Personal statement</li>
				<li>Cover letter</li>
			</ul>
			<h4>
				To see the price of your order, use the form in the right upper side of the web page.
			</h4>
			<h1>Our Prices are Based On:</h1>
			<h4>1. Type of writing</h4>
			<p>
				You can choose from several options that are available on our online writing service – editing/proofreading, writing from scratch, problem solving and multiple choice questions. Prices for the admission and application help are usually higher than for the mentioned above services.
			</p>
			<h4>2. Deadline</h4>
			<p>
				Deadline of the order. You have 7 options to choose from. You should mention that the more time you give us to write your paper, the lower the price is.
			</p>
			<h4>3. Academic level</h4>
			<p>
				You can choose from Undergraduate, Bachelor or Professional. The Undergraduate level covers high school and college. If you study for a Bachelor’s degree you need to choose Bachelor level. The Professional level will help you with a Master's or a Ph.D. level work. The style of your paper will be appropriate for the academic level that you choose.
			</p>
			<h4>4. Extras:</h4>
			<div class="padding-l-20 margin-l-20">
				<ul class="orange-list">
					<li>
						<div> <b>Preferred writer </b> </div>
						You have an option "My previous writer" in your profile, in order to see the ID number of the writer who has already written orders for you. This option can be used only for the 24 hour or more deadlines. It also influences the price. You can also select "Regular writer", without any changing of the price. The ‘Advanced regular writer’ option will provide you with a more experienced author for an additional fee still, it will be lower than for the ‘TOP Writers’.
					</li>
					<li>
						<div> <b>VIP customer service </b> </div>
						If you want your order to become the first priority for our Support Representatives you can choose this extra feature. This service will provide you with instant answers to all the requests and question that you will have. Also, you will receive text notifications about the progress of your writer. The service is only $14.99.
					</li>
					<li>
						<div> <b>Plagiarism report</b> </div> 
						With this additional option you can receive a plagiarism report if you need an official proof for the originality of your paper. We can provide you with the official document of plagiarism report.
					</li>
					<li>
						<div> <b>Abstract page </b> </div>
						You can choose this feature and you will receive an Abstract page for your paper. Its price is just $14.99!
					</li>
					<li>
						<div> <b>Sources used </b> </div>
						If you need to get a soft copy of the materials that were used to complete your paper you can select this extra service. We will provide your sources in .doc/.docx/.pdf formats. Or as print screens or images of the pages used. Each source is available for $4.99.
					</li>
					<li>
						<div> <b>Table of contents </b> </div>
						You can ask your writer to complete your paper with a table of content which will definitely make a great first impression of your paper. Just think of that and our writers will help you with it. Find this extra on the order page of your control panel for only $9.99.
					</li>
					<li>
						<div> <b>Editor's Check</b> </div>
						Want to make sure that your paper is of the highest quality? Choose this extra option and get in touch with our best editors and they will read your paper from head to tail after your writer.
					</li>
				</ul>
			</div>
			<h1>Discounts</h1>
			<p>We offer the following discounts:</p>
			<div class="inline-block middle">
				<div class="discount margin-b-20">
					<div class="b-img">5%</div>
					Discount is applicable if the price of your order is above $500.
				</div>
				<div class="discount">
					<div class="b-img">10%</div>
					Discount is applicable if the price of your order is above $1000.
				</div>
			</div>
			<div class="shared inline-block middle f-gray-b">
				<div class="item">
					<div class="text middle">
						Like Us on Facebook for Discounts and Freebies!
					</div>
					<div class="shared-button middle" data-href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FOriginalpapersonly-1763588037231134%2F&ext=1473179829&hash=AeYOZ9qS5dNfJwkL">Share</div>
				</div>
				<div class="item">
					<div class="text middle">
						Follow Us on Twitter for Discounts and Freebies!
					</div>
					<div class="follow-button middle" data-href="https://twitter.com/intent/follow?screen_name=originalpapers1">Follow</div>
				</div>
			</div>

			<div class="gradient-for-bottom-text">
			<div class="padding-t-20 margin-5"></div>
				<h1>Additional Information</h1>
				<p>
					You can be sure that our online service will deliver your paper due to arranged time only. However, if you fail to provide us with the materials or the information necessary to complete the order, we can ask for a delay. Be sure to provide your author with even tiniest details of the task it will ensure the delivery of your paper even before the deadline. This will also provide you with additional time to request a revision if needed.
				</p>
				<p>
					With OriginalPapersonly.com you should not be worried about the originality of your paper. It will be 100% plagiarism free. The quality of the paper is guaranteed by our professional writers and editors that will never let their clients down.
				</p>
				<p>
					The writing staff of OriginalPapersonly.com always follows all your instructions precisely. That is why you need to be clear while providing your writer with the paper details. Please, be sure to include as many details as it is possible.
				</p>
				<p>
					Please note that our service is prepaid. Your writer will begin working on your paper only after you pay for it. Please remember that the deadline that you indicate is for the first draft of your paper only. The countdown to your deadline starts only when you make the payment. If there will be any questions and you'll need a revision, it will have its own deadline.
				</p>
				<p>
					We appreciate every single client of OriginalPapersonly.com we have and make our best to provide you with the best quality you can find.
					Every customer is very valuable to us so we are looking forward to working with you. We are always open for your suggestions aboutmaking our service better. We should mention that special discounts cannot be applied to the orders that cost less than 30$ and you cannot combine a discount. VAT is non-refundable. Please note that Discounts and Extras, such as TOP Writer, Advanced regular writer, Abstract page, Plagiarism report, VIP customer service, Sources used, Table of contents/Outline, and Editor's Check are not subject to refund either.

				</p>
			</div>
		</div>
	</div>
	
@stop