@extends('layouts.base')
@section('content')
	<link rel="stylesheet" href="/css/sideblocks.css">
	<div class="container margin-t-120">
		<center>
		<div class="header-page">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Referral Program</div>
		</div>
		</center>
		<div class="left-panel">
			@include("sideblocks.priceBlock")
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">
			<div class="container-for-text c-tpt padding-b-n">
				<div class="padding-l-20">
					Nobody can describe OriginalPapersOnly better than our customers. You can tell your friends about OriginalPapersOnly.com and start earning money already today. This Program allows you to earn 10% on all your referrals, and your friends save 10% on their first orders.
					<h1>Joining the program is quick and easy</h1>
				</div>
			</div>
			<div class="padding-l-20">
				<div class="padding-l-20">
					<div class="flex-justify-space-around flex-wrap">
						<div class="orange-block-over">
							<div class="orange-block">
								<div class="content">
									Register to receive <br>a code
								</div>
								<div class="number">1</div>
							</div>
						</div>
						<div class="orange-block-over">
							<div class="orange-block">
								<div class="content">
									Share the 10% discount<br> with friends
								</div>
								<div class="number">2</div>
							</div>
						</div>
						<div class="orange-block-over">
							<div class="orange-block">
								<div class="content">
									Earn 10% on <br>all referrals 
								</div>
								<div class="number">3</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-for-text c-tpt padding-t-n">
				<div class="padding-l-20">
					<h1>
						We look forward to meeting your friends, and you will get a reward for intruducing them to the service.
					</h1>
					<ul class="orange-list">
						<li>
							Log in to your account or simply register
						</li>
						<li>
							Find the referral discount code in your account
						</li>
						<li>
							Tell your friends about our service and share the code
						</li>
						<li>
							Your friends receive a 10% discount on their first order. You get a 10% referral commission on all the orders!
						</li>
					</ul>
				</div>
				<drop-down-ordered-list data-open-only-one class="none-order">
					<h1 class="margin-l-35 padding-t-n">Earn money with OriginalPapersOnly.com</h1>
					<ordered-list-item class="">
						<ordered-list-header> Will I be able to save 10% with the referral code as well? </ordered-list-header>
						<ordered-list-content>
							<p>
								The discount code will work only for your friend. At the same time, you can always benefit from our discounts. Find the full list here.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header> How many times will the code work for my friend? </ordered-list-header>
						<ordered-list-content>
							<p>
								The referral code will give your friend a one time discount only. All his or her future orders can get a 5% or a 10% discount. You can find more information about our discount policy on this page.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header> How can I track the amount of money earned? </ordered-list-header>
						<ordered-list-content>
							<p>
								All the statistics is available in your personal account at OriginalPapersOnly.com in the section Referral Program. Simply log in to see the details.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header> How can I make a withdrawal from my account? </ordered-list-header>
						<ordered-list-content>
							<p>
								The process of withdrawing money is easy. Simply send your request to us at support@originalpapersonly.com not later than on 12th day of each month. Please note that it is only possible to withdraw $100 or more. You will receive your earnings between the 16th and the 20th day.
							</p>
						</ordered-list-content>
					</ordered-list-item>
					<ordered-list-item>
						<ordered-list-header> What payment systems do you support? </ordered-list-header>
						<ordered-list-content>
							<p>
								You can get the money you earn to your PayPal, Webmoney or Skrill account.
							</p>
						</ordered-list-content>
					</ordered-list-item>
				</drop-down-ordered-list>
				
				<center class="margin-t-35">
					<a href="{{url('/panel')}}">
						<button class="common-button">Join now</button>
					</a>
				</center>
			</div>
		</div>
	</div>
@stop