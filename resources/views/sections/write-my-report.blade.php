@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Can You Write My Report for Me?</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            <div class="container-for-text">
                <p class="color-blue f-w-900">Have No Time for Report Writing? OriginalPapersOnly.com Is at Your Service!</p>
                <p>When you are exhausted because of all your everyday troubles and have no time for it, when you are overwhelmed with the idea "Who will do my report for me?" - OriginalPapersOnly.com professional team is ready to give you a hand! I was too busy to do my book report or even look for some information for it. I knew I <b>needed help to do my book report</b>, but I didn't know where or who I could get it from. The deadline was coming and I still had no idea how to do my report.</p>
                <p>The only thing I understood was that I'd have to pay for writing my report. I decided to ask some of my friends for help, but nobody wanted to take responsibility for it. When I learned about OriginalPapersOnly.com company, I decided immediately to ask them to write my report for me. The support staff was very friendly and they promised they would do my report for me in a tight deadline. They assured me that they would make my report in proper time and it would be <b>free from plagiarism.</b></p>
                <p class="color-blue f-w-900">What Is the Procedure of Report Ordering?</p>
                <p>First of all, you send a request. After that they assign you a writer. I had an opportunity to maintain contact with the writer who turned out to be very skillful and creative. When I asked him, "<i>Can you write my report for me?</i>" I had no doubt that he would do my report perfectly. I was concerned about the <b>quality of my report</b>, plagiarism and how long it would take to write my report. But my writer assured me that he'd write a report of premium quality and plagiarism-free. He promised that he and his team would write my report for me within my time frame.</p>
                <p>When the work is done, you have different options. You can take your work and if there are no mistakes, just hand it in successfully. In the case there is something that doesn't meet your requirements, you can always ask for a free revision or even get a refund.</p>
                <p>Be certain, professionals at OriginalPapersOnly.com will write book report for you in proper time. I can in all sincerity guarantee that the skilled staff of OriginalPapersOnly.com will always respond to the "<i>Can you write a book report for me</i>" calls of troubled students!</p>
                <p>When my writer finished my order, I got a <b>non plagiarized report</b> of the highest quality. Now I recommend OriginalPapersOnly.com writing agency to all my friends because I am sure they will make my report the way I want and need it.</p>
                <p><b>Make your life easier with OriginalPapersOnly!</b></p>
            </div>
        </div>
    </div>
@stop