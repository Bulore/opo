@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="/css/sideblocks.css">
<link rel="stylesheet" href="/css/our-writers.css">
<div class="container margin-t-120">
	<center>
	<div class="header-page">
		<div class="border"><div><div><div></div></div></div></div>
		<div class="text">Our Writers</div>
	</div>
	</center>
	<div class="left-panel">
		@include("sideblocks.priceBlock")
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock")
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content">
		<div class="container-for-text f-black">
			OriginalPapersOnly.com is a constantly evolving company. We are truly interested in working with real professionals. Our team of skilled writers is fully committed to delivering the highest level of work, and more than a thousand authors are ready to provide assistance with over 60 various subjects.
		</div>
		<div class="container-for-text c-tpt">
			<div class="clear-after">
				<div class="left width-300">
					<ul class="undered-list">
						<li class="clear-after">
							<span class="right">500+ writers</span>
							<span class="left f-blue"><b>Literature</b></span>
						</li>
						<li class="clear-after">
							<span class="right">1000+ writers</span>
							<span class="left f-blue"><b>Law</b></span>
						</li>
						<li class="clear-after">
							<span class="right">300+ writers</span>
							<span class="left f-blue"><b>History</b></span>
						</li>
						<li class="clear-after">
							<span class="right">400+ writers</span>
							<span class="left f-blue"><b>Business</b></span>
						</li>
					</ul>
				</div>
				<div class="right width-300">
					<ul class="undered-list">
						<li class="clear-after">
							<span class="right">250+ writers</span>
							<span class="left f-blue"><b>Medicine and Health</b></span>
						</li>
						<li class="clear-after">
							<span class="right">500+ writers</span>
							<span class="left f-blue"><b>Art</b></span>
						</li>
						<li class="clear-after">
							<span class="right">215+ writers</span>
							<span class="left f-blue"><b>Technology</b></span>
						</li>
						<li class="clear-after">
							<span class="right">500+ writers</span>
							<span class="left f-blue"><b>Management</b></span>
						</li>
					</ul>
				</div>
			</div>
			<div class="clear-after padding-t-5 margin-t-5">
				We are proud to work with an expert team of writers. You can read more about our best authors below.
			</div>
		</div>
		<div class="container-comment padding-t-20 padding-b-20">
			@foreach($comments as $comment)
			<div class="comment">
				<div class="left-block">
					<div class="author">
						#{{ $comment->write }}
						<div>
							<div class="value">240</div>
							<div class="rating">5</div>
						</div>
					</div>
				</div>
				<div class="content f-white">
					"Thanks a lot, Sandle! It's amazing how you were able to put those words into order so that they read well. You also saved me so much time."
					<div class="padding-t-20">
						<div class="left"><b>Gail, CT</b></div>
						<div class="right"><a href="">More testimonials</a></div>
					</div>
				</div>
				<div class="right-block">
					<button class="common-button">Order with this writer</button>
				</div>
			</div>
			@endforeach
		</div>
		
		<drop-down-ordered-list data-open-only-one class="none-order">
			<h1 class="margin-t-n margin-l-35">Frequently Asked Questions:</h1>
			<ordered-list-item>
				<ordered-list-header>Who will write my paper?</ordered-list-header>
				<ordered-list-content>
					<p>
						One of our experienced degree-holding writers will work on your document and is ready to meet all your requirements.
					</p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item class="">
				<ordered-list-header>Can I work with my previous writer?</ordered-list-header>
				<ordered-list-content>
					<p>
						If you were satisfied with your previous document and would like the same author to write another paper for you, choose “My previous writer” feature and specify your authors' ID in the order form.
					</p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item class="">
				<ordered-list-header>Where is my writer from?</ordered-list-header>
				<ordered-list-content>
					<p>
						We cooperate with writers from all over the world. If you need an author from any specific country, simply mention this in the paper details in the order form. You can also let us know if you need your document to be written in British or American English.
					</p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header>How can I communicate with my writer?</ordered-list-header>
				<ordered-list-content>
					<p>
						In your personal account at OriginalPapersOnly.com, you will find a convenient messaging system, which will enable you to communicate with your writer on any issue related to your order. You can send a message to your author as soon he/she gets down to work.
					</p>
				</ordered-list-content>
			</ordered-list-item>
			<ordered-list-item>
				<ordered-list-header>What is the selection process in your company?</ordered-list-header>
				<ordered-list-content>
					<p>
						Our company is highly interested in real professionals, so we have a special department that is responsible for the hiring process. Thus, we will be looking for the following: certificates that can prove the candidate's level of proficiency, his/her knowledge of formatting styles and successful completion of grammar/vocabulary tests. This way, we can ensure that writers will deliver papers of the highest quality.
					</p>
				</ordered-list-content>
			</ordered-list-item>
		</drop-down-ordered-list>
	</div>
</div>
@stop