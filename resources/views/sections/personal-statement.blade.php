@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0"">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">First-Rate Personal Statement Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>10]) 
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    The aim of OriginalPapersOnly.com is the best quality of all papers, written by us.
                </p>
                <p>
                    When you order  personal statement from our organization, you pay for the high quality and 100% originality. 
                </p>
                <p>
                    Custom personal statement writing services we offer are really different from what you have seen before.
                </p>
                <p>
                    You can purchase a personal statement at whatever point you require - we are open for requests every minute. 
                </p>
                <p>
                    Personal statements are truly difficult to write in the case if you have too little time or excessively poor writing abilities. Meanwhile, personal statement chooses a ton in your future, as on it depends where you will be in quite a long while after the actual writing and whether you will be accepted to your preferred institution. That is why the help of our expert custom personal statement writing service is vital in the application process.
                </p>
                <h4>
                    Order personal statement writing at OriginalPapersOnly.com
                </h4>
                <p>
                    You can find such quality and affordable prices only at OriginalPapersOnly.com. 
                </p>
                <p>
                    All the personal statements of our writers are composed are checked for plagiarism several times, which excludes the probability of plagiarism. 
                </p>
                <p>
                    Save money and time, by entrusting your personal statement to our experts at OriginalPapers.com.
                </p>
                <p>
                    Leave your worries behind and submit your request at OriginalPapersOnly.com! You can either order personal statement if you know that you can’t write it by yourself, or send your own personal statement for proofreading, our experts will detect every error you may have missed and will guarantee flawlessness of your written work. With our help you can be sure that you will enter the college of your dreams!
                </p>
            </div>
        </div>
    </div>
@stop