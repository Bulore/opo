@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Custom Report Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>2])
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    Many Students consider report writing very difficult and boring task, that’s why they don’t pay enough attention to it. That’s wrong approach, as even small errors can affect your grades.
                </p>
                <p>
                    In order to avoid risk and achieve bad results, you can ask someone, who can write excellent report for you. Contact OriginalPapersOnly.com and hire an expert who will fulfill the task for you. The only thing you need to do is give our support team a topic and the supplementary information you want to be included in the report. Then pay for the work of the author and receive your report! 
                </p>
                <h4> What can you get by using OriginalPapersOnly.com: </h4>
                <ul class="orange-list">
                    <li> Reports on any topic </li>
                    <li> High quality custom report writing </li>
                    <li> Reports, written by professionals with scientific degrees </li>
                    <li> Reliable service </li>
                </ul>
                <p>
                    There are only smart and talented people in our team. Moreover, all of them are responsible and always try their best to write a perfect report for you. Everyone understands that it can be hard to write a report. Fortunately, all our writers have degrees in one or several scientific areas.
                </p>
                <p>
                    You will receive original report, free of plagiarism. No one will question its authenticity.
                </p>
                <h4> Custom Report Writing Service from the best custom writing company </h4>
                <p>
                    OriginalPapersOnly.com gives you the opportunity to pay for our services through the most widespread payment systems; for the convenience of our customers it will be very straightforward and quick. It can pleasantly surprise our first-time customers. A large number of our customers are satisfied by our report writing services. We are continually improving our skills in report writing and do truly high quality work, which deserves the most elevated appraisals.
                </p>
            </div>
        </div>
    </div>
@stop