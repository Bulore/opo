@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container  margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Custom Homework Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>9]) 
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    Homework writing is a usual part of educational process. But not everyone can handle it without problems. Have you been requested to do homework on a troublesome subject, and you have never created persuading  homework before? At that point our custom homework writing service OriginalPapersOnly.com is for you. By requesting online homework help, you will have spare time, which you can spend  on some other actions.
                </p>
                <p>
                    OriginalPapersOnly.com is specialized on writing homework . When you order custom homework from us, you get the best quality work that will persuade any of your teachers that you deserve appreciation.
                </p>
                <h4>
                    Why you should choose OriginalPapersOnly.com
                </h4>
                <p>
                    Our custom homework writing service has specialists in all subjects. 
                </p>
                <p>
                    Homework can be done in different citation and styles, you simply need to pick the one you require.
                </p>
                <p>
                    Our service can provide you online homework help whenever you require it - we are accessible 24/7. 
                </p>
                <p>
                    All works composed by our homework service are 100% unique. 
                </p>
                <p>
                    Homework writing service at OriginalPapersOnly.com produces perfect homework, gaining the trust of many clients around the world. Profoundly proficient specialists at OriginalPapersOnly.com have advanced education and awesome involvement experience in custom homework writing.
                </p>
                <p>
                    Our online homework service allows you to request custom homework writing from scratch or change a current work. Request custom homework from OriginalPapersOnly.com and make sure to get appeciation from your teacher. Specialists of our homework service never deny alteration requests. A lot of homework writing services claim uniqueness of their written work, however OriginalPapersOnly.com works are always 100% unique. Request homework here and nobody will ever blame you for plagiarism.
                </p>
                <h4>
                    It is easy to work with us
                </h4>
                <p>
                    The flexible schedule of discounts is constantly accessible at our custom homework writing service. We offer a good value for money. Give your homework to our custom homework writing service and you won't feel sorry about it! We work with students from different countries and with the best writers. More than 8000 clients have effectively believed in us to do their homework with no second thoughts. Every one of them have gotten appreciation from their teachers. And now we can gladly say that OriginalPapers.com is the best custom homework writing service. 
                </p>
            </div>
        </div>
    </div>
@stop