@extends('layouts.base')
@section('content')
<link rel="stylesheet" type="text/css" href="/css/contacts.css">
<link rel="stylesheet" href="css/sideblocks.css">

<div class="mobile-header m-bold-700">
	<p>What Is Turnitin.Com?</p>
</div>

<div class="container margin-t-120 m-margin-top-0">
	<center>
	<div class="header-page m-display-none">
		<div class="border"><div><div><div></div></div></div></div>
		<div class="text">What Is Turnitin.Com?</div>
	</div>
	</center>
	<div class="left-panel">
        @include("sideblocks.priceBlock")
        @include("sideblocks.formatblock")
        @include("sideblocks.contactblock")
        @include("sideblocks.moneyBack")
        @include("sideblocks.acceptblock")
        @include("sideblocks.protectedblock")
        @include("sideblocks.servicesblock")
    </div>
    <div class="content">
        <div class="container-for-text">
        	<p>Turnitin is an online plagiarism detection tool that is popular with educational institutions across the world.</p>
        	<h4>Why We Don’t Use Turnitin</h4>
        	<p>
        		Turnitin.com stores all the submitted papers in a database, which is later used to detect plagiarism in students’ papers. This means that if you write a paper and check it for plagiarism using the service, it will be detected as plagiarized when your professor checks your paper again.
        	</p>
        	<p>
        		In other words, your assignment can only be submitted to Turnitin once. Otherwise, it will NOT be marked as original.
        	</p>
        	<h4>Our Guarantees</h4>
        	<p>
        		All the papers at OriginalPapersOnly.com are plagiarism-free. We use the professional WebCheck software to make sure that your papers are completely original. This innovative plagiarism-checker finds any instances of plagiarism and will not share your the text of your paper with any third parties.
        	</p>
        	<p>
        		Thus, we can be 100% sure that we deliver high quality papers and that all our clients are satisfied.
        	</p>
        </div>
    </div>
</div>
@stop