@extends('layouts.base')
@section('content')
	<link rel="stylesheet" href="css/sideblocks.css">

	<div class="mobile-header m-bold-700">
		<p>Privacy and Cookies policy</p>
	</div>
	
	<div class="container margin-t-120 m-margin-top-0">
		<center>
		<div class="header-page m-display-none">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Privacy and Cookies policy</div>
		</div>
		</center>
		<div class="left-panel m-display-none">
			@include("sideblocks.priceBlock")
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">
			<div class="container-for-text f-black">
				<p>
					This Privacy and Cookies Policy is a legal instrument designed to govern your and our behavior with regard to the usage of the website and related services.
				</p>
				<p>
					This Privacy and Cookies Policy of use is a legally binding instrument. Therefore, our relations regarding privacy and cookies shall be governed by this Privacy and Cookies Policy only. By expressing your consent to be legally bound by this Privacy and Cookies Policy, you enter into an agreement with us. Therefore, we and you shall have mutual legal rights and obligation.	
				</p>
				<p>
					You express your sufficient consent to be legally bound by this Privacy and Cookies Policy when you open the website in the browser on your device. Please do not view our website and do not place any order if you disagree with any of the following rules.
				</p>
				<p>
					Below we will describe different important issues of privacy and cookies you may face while using the website. Please study this Privacy and Cookies Policy carefully before proceeding to use the website.
				</p>
				<p>
					For your convenience we will refer to the OriginalPapersOnly.com as “website”. We refer to anyone who opens the website in the browser on the device as “you”.
				</p>
			</div>
			<div class="container-for-text c-tpt">
				<h1 class="padding-n">Privacy and Cookies Policy</h1>
				<h4>Applicability. Consent.</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
						This Privacy and Cookies Policy is the special form of agreement. As stated above, you express your sufficient consent to enter into this agreement once you open the website in the browser on your device.
					</li>
					<li>
						You enter into this agreement once you express you sufficient consent.
					</li>
					<li>
						We may ask you to express your sufficient consent repeatedly from time to time on our own discretion.
					</li>
					<li>
						Your use of the website shall be prohibited as unlawful unless you agree to enter into agreement on this Privacy and Cookies Policy.
					</li>
					<li>
						Please leave our website immediately and do not order any services if you disagree with this Privacy and Cookies Policy.
					</li>
					<li>
						As follows, this Privacy and Cookies Policy is applicable to you during the entire use of the website.
					</li>
				</ol>
				
				<h4>General Provisions</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
					 	We wish to provide you with the highest quality of products and services. To do that, we believe it is necessary to ensure a proper and simple identification procedure, communication manner and your possibility to choose.
					</li>
					<li>
						We obtain a limited amount of information about you when you enjoy our services. In any case, the sole purpose of this procedure is to make your user experience better.
					</li>
					<li>
						By giving your consent to this Privacy and Cookies Policy, you agree that the purpose of obtaining your information is sufficiently specified, explicit and legitimate. You agree that your information is adequate, relevant, accurate, complete, up-to-date and not excessive in relation to the purpose.
					</li>
					<li>
						The information obtained subject to this Privacy and Cookies Policy is deemed to be fairly and lawfully obtained. We don’t knowingly obtain your information without your previous explicit unambiguous consent.
					</li>
					<li>
						Primarily, you sub mit your information personally. However, we may obtain the information independently upon your consent.
					</li>
					<li>
					 	You may submit the information once you:
					 	<ol class="blue-alpha-list margin-l-20 padding-l-20">
							<li>
								fill any forms or questionnaires;
							</li>
							<li>
								identify your personality;
							</li>
							<li>
								communicate to us in any form possible.
							</li>
					 	</ol>
					</li>
					<li>
					 	At the same time, we may obtain the information by:
					 	<ol class="blue-alpha-list margin-l-20 padding-l-20">
							<li>
								watching your activities and interactions within our website;
							</li>
							<li>
								monitoring your transactions within our website;
							</li>
							<li>
								using cookies and/or other similar technologies.
							</li>
					 	</ol>
					</li>
					<li>
						We never obtain the information concerning or revealing your:
						<ol class="blue-alpha-list margin-l-20 padding-l-20">
							<li>
								racial or ethnic origin,
							</li>
							<li>
								political opinions,
							</li>
							<li>
								religious, philosophical or other beliefs of a similar nature,
							</li>
							<li>
								trade union membership,
							</li>
							<li>
								physical or mental health and condition,
							</li>
							<li>
								sexual life;
							</li>
							<li>
								criminal convictions.
							</li>
						</ol>
					</li>
				</ol>
				<h4>Information We Obtain When You Browse the Website</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
						Once you open the website in browser on your device we automatically begin to obtain the following information:
						<ol class="blue-alpha-list margin-l-20">
							<li>
								browser type,
							</li>
							<li>
								operating system, and
							</li>
							<li>
								access time.
							</li>
						</ol>
					</li>
					<li>
						This information does not enable us to identify you. We do not make any attempts to find out the identities of those visiting our website.
					</li>
					<li>
						This information is used only to improve the content of our web pages, to customize it and/or make the layout of our pages better.
					</li>
				</ol>
				<h4>Information We Obtain When You Purchase the Services</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
						We will ask you to provide us with some of your information once you desire not only to browse the website, but to place an order and request the text material to be prepared. In this case we will obtain the following information:
						<ol class="blue-alpha-list margin-l-20">
							<li>
								your e-mail,
							</li>
							<li>
								your phone number,
							</li>
							<li>
								copy of your driver’s license or your national ID,
							</li>
							<li>
								limited credit card information.
							</li>
						</ol>
					</li>
					<li>
						We may use your email address to send you notifications considering the most important stages of order fulfillment such as clarification of any issues, unread messages, and order completion.
					</li>
					<li>
						You express your implied consent by pressing the "Submit form" button to notifications of special offers and discounts, advertisement and promotional materials by any means including telephone number, email or other message.
					</li>
					<li>
						When you place an order, we may ask you to provide us with a copy of your driver’s license or your national ID and your credit card information to authorize your payment. Only your first and last name as well as the last four digits of your credit card need to be visible.
					</li>
				</ol>
				<h4>Cookies</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
						To make your user experience smoother, we apply cookies. A cookie is a small data file that is used as a unique identifier of your visit. It consists of numbers and letters; it is sent from our servers and stored on your device (computer, mobile phone, etc.) when you access our website.
					</li>
					<li>
						Cookies record your preferences when you navigate our web pages and perform certain functions – they check if you are logged in, save the ordered items, etc.
					</li>
					<li>
						You can manage or disable cookies any time, but due to their vital role of enhancing/enabling usability on our website we cannot guarantee that our service will be able to function properly after cookies are disabled.
					</li>
					<li>
						We use only the following types of cookies:
						<ol class="blue-alpha-list margin-l-20">
							<li>
								Session cookies keep details about the user's experience temporarily. They are deleted as soon as you close the page. Such cookies can be used to enhance security when a user accesses Internet banking, for example. They also facilitate the use of web mail.
							</li>
							<li>
								Persistent cookies are stored on your computer in between browser sessions. Thus, your preferences or actions are remembered across the site.
							</li>
						</ol>
					</li>
					<li>
						We may use the cookies in the following cases and for the following purposes:
						<ol class="blue-alpha-list margin-l-20">
							<li>
								In the order forms. You will not be able to apply for our writing services with cookies disabled.
							</li>
							<li>
								To let you stay logged in.
							</li>
							<li>
								To provide you with 24/7 online support via live chat.
							</li>
							<li>
								To track your activity via Google Analytics.
							</li>
						</ol>
					</li>
					<li>
						At any time, you shall be free to block and disable cookies in your browser. Please note that our services may not work correctly and may not work at all once you will disable the cookies.
					</li>
					<li>
					 	In addition, we use Google Analytics to help us analyze how this website is accessed. The data generated by a cookie about your use of the website is transmitted to Google. This information is then used to evaluate visitors' use of the website and to compile statistical reports on the website's activity.
					</li>
					<li>
						To find out more about cookies, including how to control and delete them, visit www.aboutcookies.org.
					</li>
					<li>
						To opt out of being tracked by Google Analytics across all websites, visit http://tools.google.com/dlpage/gaoptout.
					</li>
					<li>
						In any case, we never obtain personally identifiable information by cookies.
					</li>
				</ol>
				<h4>Communication Manner</h4>
				<ol class="orange-number-list margin-l-20">
					<li>
						We try to meet the highest standards when collecting and using personal information. For this reason, we take any complaints we receive about this issue very seriously. We encourage people to bring it to our attention if they think that our collection or use of information is unfair, misleading or inappropriate. We would also welcome any suggestions for improving our procedures.
					</li>
					<li>
						We are happy to provide any additional information or explanation needed. Any requests for this should be sent to support@originalpapersonly.com.
					</li>
					<li>
						We try to be as open as we can in terms of giving people access to their personal information. Individuals can find out if we hold any personal information about them by making a "subject access request" under the Data Protection Act 1998.
					</li>
					<li>
						If we do hold information about you, we will:
						<ol class="blue-alpha-list margin-l-20">
							<li>
								give you a description of it;
							</li>
							<li>
								tell you why we are holding it;
							</li>
							<li>
								tell you to whom it could be disclosed;
							</li>
							<li>
								let you have a copy of the information in an intelligible form.
							</li>
						</ol>
					</li>
					<li>
						To make a request to us about any personal information, we may need you to put it in writing and address it to our Customer Support Representatives at support@originalpapersonly.com.
					</li>
					<li>
						If you would like to opt out of emails and SMS notifications from us, please let us know by contacting us. After we receive your request, we will stop sending you messages without unreasonable delay.
					</li>
				</ol>
				<h4>Final Provisions</h4>
				<ol class="orange-number-list  gradient-for-bottom-text padding-l-r-20">
					<li>
						All the information in the order, inquiry pages, and your personal account is processed with the use of the SSL-certificate, which guarantees complete confidentiality and safety of all the transactions.
					</li>
					<li>
						We will take reasonable efforts to ensure your safety and prevent third-party access to the personal information collected. We shall keep our employees duly trained and instructed to maintain your information safe. However, we will disclose the information collected should our legal obligations require us to do so.
					</li>
					<li>
						This Privacy and Cookies Policy forms the entire understanding between you and us. Neither you nor we shall rely on any verbal or written information unless it is set out or expressly referred to this Privacy and Cookies Policy.
					</li>
					<li>
						Your or our failure to enforce the performance of any provision contained in this Privacy and Cookies Policy will not constitute a waiver of rights to subsequently enforce such provision or any other provision contained in this Privacy and Cookies Policy.
					</li>
					<li>
						Notices and reports under this Privacy and Cookies Policy shall be considered received within one day from the moment when the report or notice was sent.
					</li>
					<li>
						Nothing in this Privacy and Cookies Policy establish any kind of partnership, employer-employee relationship or joint venture between the you and us. Neither you nor we shall be authorized to act as an agent for each other, nor shall you or we enter into any agreement or contract on behalf of the each other as representative or agent.
					</li>
					<li>
						Nothing in this Privacy and Cookies Policy shall initiate any obligations for third parties and neither third party shall have any rights arising from this Privacy and Cookies Policy.
					</li>
					<li>
						Communications between you and us shall be made in English. Messages, inquires, requests, applications and/or consents shall be sent by any means of communication.
					</li>
					<li>
					 	If any provision of this Privacy and Cookies Policy is found as unenforceable by any appropriate authority, such provision shall be modified, rewritten or interpreted to include as much of its nature and scope as will render it enforceable.
					</li>
					<li>
						Headings above the terms of this Privacy and Cookies Policy are intended to increase the legibility of this Privacy and Cookies Policy only. The content and meaning of terms placed under a particular heading is, therefore, not limited to the meaning and content of the heading. Headings shall not be considered in interpreting this Privacy and Cookies Policy.
					</li>
					<li>
						This Privacy and Cookies Policy may be changed, amended or modified on our sole and absolute discretion without any notices or warnings.
					</li>
					<li>
						This Privacy and Cookies Policy is an agreement concluded in the electronic form. The electronic form of this Privacy and Cookies Policy shall have the same legal force as it was signed in ink.
					</li>
					<li>
						This Privacy and Cookies Policy was last updated on August 10, 2015.
					</li>
				</ol>
			</div>
		</div>
	</div>
@stop