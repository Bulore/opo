@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-170">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Examples and Samples</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            @foreach($items as $item)
                <div class="container-for-text padding-10px-40px">
                    <a href="{{ url("/example/$item->id") }}"><p class="color-blue f-w-900">{{ $item -> title }}</p></a>
                    <hr class="example-horizontal-line">
                    <p>{{ $item -> short_text }}</p>
                </div>
            @endforeach

            <center>{{ $items->links() }}</center>
        </div>
    </div>
@stop