@extends('layouts.base')
@section('content')
	<link rel="stylesheet" href="/css/sideblocks.css">
	<link rel="stylesheet" href="/css/order-now.css">
	<link rel="stylesheet" href="{{url('/css/countries.css')}}">

	<div class="container margin-t-120 m-margin-top-0" ng-controller="freeInquiryCtrl">
		<div class="mobile-header m-bold-700">
			<p>Order Now</p>
		</div>
		<center>
		<div class="header-page m-display-none">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Order Now</div>
		</div>
		</center>	
		<div class="left-panel m-display-none">
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.contactblock")
		</div>

		<div class="content relative">
			<div class="container-for-text ">
				<p>
					Please Fill in the form below and try to include as many details as possible. The more specific you are about your order, the easier it will be for the writer to complete your assignment perfectly.
				</p>
			</div>
			<div class="container-for-text margin-t-20 padding-b-n">
				<tab-panel>
			
					<tab-nav data-autoactive-disable>
						<tab-nav-item ng-click='goToStep(1, $event)' ng-class="(orangePage == 1)?'active':''" >Contact information</tab-nav-item>
						<tab-nav-item ng-click='goToStep(2, $event)' ng-class="(orangePage == 2)?'active':''">Paper details</tab-nav-item>
						<tab-nav-item ng-click='goToStep(3, $event)' ng-class="(orangePage == 3)?'active':''">Price calculator</tab-nav-item>
					</tab-nav>
					
					<tab-content>
						<tab-content-item ng-class="(orangePage == 1)?'active':''">
							<form action="" id="order-form">
								<div class="href-blue-paging">
									<div ng-click='setBluePage(1)' ng-class="(bluePage == 1)?'active':''">
										I am new here
									</div>
									<div ng-click='setBluePage(2)' ng-class="(bluePage == 2)?'active':''">
										sign in
									</div>
								</div>
								<div class="container-blue-paging">
									<div ng-show="bluePage == 1" id="registration-form">
										<div class="width-400 margin-a">
											<div class="form-group margin-t-20">
												<div class="text">
													<div class="inf-container">
														<div class="inf">
															Please make sure you have entered the correct email address to receive notifications of order completion status.
														</div>
													</div>
													<span class="middle">Email:</span>
												</div>
												<div class="content">
													<input type="email" class="input" name="email" ng-model="registration.email">
												</div>
											</div>
											<div class="form-group padding-t-20">
												<div class="text">
													First Name:
												</div>
												<div class="content">
													<input type="text" class="input" name="name" ng-model="registration.name">
												</div>
											</div>
											<div class="form-group padding-t-20">
												<div class="text">
													Last name:
												</div>
												<div class="content">
													<input type="text" class="input" name="last_name" ng-model="registration.last_name">
												</div>
											</div>
											<div class="form-group padding-t-20">
												<div class="text">
													<div class="inf-container">
														<div class="inf">
															Please make sure you have entered the correct phone number, so that we could contact you to check on the details of your order and to verify your payment.
														</div>
													</div>
													<span class="middle">Cell phone:</span>
												</div>
												<div class="content">
													<input type="text" class="input"  name="phone" ng-model="registration.phone">
												</div>
											</div>
										</div>
										<div class="margin-a width-600 padding-20 margin-t-20 relative">
											<label>
												<div class="check-container content">
													<input type="checkbox" name="remember"  class="checked" ng-model="registration.remember" data-additional-classes="margin-l-35 f-left">
													<div class="checkbox top"></div>
													<div class="inline-block top text-check-box f-right width-570">
														I accept the <a href="{{url("/terms-of-use")}}" class="text-check-box">Terms of Use</a> and <a href="{{url("/privacy")}}" class="text-check-box">Privacy and Cookies Policy</a>. I express my prior affirmative consent to receive emails and sms from OriginalPapersOnly.com.
													</div>
												</div>
											</label>
										</div>
									</div>
									<div ng-show="bluePage==2" id="login-form">
										<div class="width-400 margin-a padding-t-20 margin-t-20" >
											<div class="form-group">
												<div class="text">
													<div class="inf-container">
														<div class="inf">
															Please make sure you have entered the correct email address to receive notifications of order completion status. No spam guaranteed.
														</div>
													</div>
													<span class="middle">Email:</span>
											 	</div>
												<div class="content">
													<input type="text" class="input" name="email" ng-model="login.email">
												</div>
											</div>
											<div class="form-group padding-t-20">
											<div class="text">
													<div class="inf-container">
														<div class="inf">
															Please input the password to your existing account
														</div>
													</div>
													<span class="middle">Password:</span>
											 	</div>
												<div class="content">
													<input type="password" name="password" class="input" ng-model="login.password">
												</div>
											</div>
										</div>

										<div class="margin-a width-600 padding-20 margin-t-20">
											<label>
												<div class="check-container content">
									                <input type="checkbox" name="remember" class="checked" ng-model="login.remember" data-additional-classes="margin-l-35 f-left">
									                <div class="checkbox top"></div>
									                <div class="inline-block top text-check-box f-right width-570">
								                		I accept the <a href="{{url("/terms-of-use")}}" class="text-check-box">Terms of Use</a> and <a href="{{url("/privacy")}}" class="text-check-box">Privacy and Cookies Policy</a>. I express my prior affirmative consent to receive emails and sms from OriginalPapersOnly.com.
							                		</div>
								                </div>

											</label>
										</div>
									</div>
								</div>
								<div class="steps-button-container">
									<div></div>
									<div>
										<button class="common-button " ng-click="goToStep(2, $event)">GO TO STEP 2</button>
									</div>
									<div></div>
								</div>
							</form>
						</tab-content-item>
						<tab-content-item ng-class="(orangePage == 2)?'active':''">
							<form action="" id="order-form-2">
								<div class="container-form padding-t-20">
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Please select the necessary type of paper
												</div>
											</div>
											<span class="middle">Type of paper:</span>
									 	</div>
										<div class="content">
											<div class="input-select">
				                        		<select-box placeholder="Please select" required data-error-message="Please select the necessary type of paper" id="type-of-paper">
				                        			<select-box-header></select-box-header>
				                        			<select-box-content>
				                        				@foreach($types->indexByCategory() as $category => $arr)
				                        					<select-box-option disable>{{$category}}</select-box-option>
				                        					@foreach($arr as $item)
																<select-box-option ng-click="setTypeId({{$item->id}})" data-type="{{$item->id}}">{{$item->name}}</select-box-option>
															@endforeach
				                        				@endforeach
				                        			</select-box-content>
				                        		</select-box>
				                    		</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Choose the subject field of your task
												</div>
											</div>
											<span class="middle">Subject:</span>
									 	</div>
										<div class="content">
											<div class="input-select">
				                        		<select-box placeholder="Please select" required data-error-message="Please specify the subject">
				                        			<select-box-header></select-box-header>
				                        			<select-box-content>
				                        				@foreach($subjects as $subject)
				                        					<select-box-option ng-click="setSubjectId({{$subject->id}})">{{$subject->name}}</select-box-option>
				                        				@endforeach
				                        			</select-box-content>
				                        		</select-box>
				                    		</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													General format: 275 words per page, legible font (e.g. Arial) 12 pt, double-spaced
												</div>
											</div>
											<span class="middle">Paper Format:</span>
									 	</div>
										<div class="content">
											<div class="academic-level flex flex-children">
												<div ng-click="setPaperFormat(1, 'MLA')" ng-class="(paperFormat == 1)?'active':''">MLA</div>
												<div ng-click="setPaperFormat(2, 'APA')" ng-class="(paperFormat == 2)?'active':''">APA</div>
												<div ng-click="setPaperFormat(3, 'Chicago/Turabian')" ng-class="(paperFormat == 3)?'active':''" class="flex-2">Chicago/Turabian</div>
												<div ng-click="setPaperFormat(4, 'Harvard')" ng-class="(paperFormat == 4)?'active':''">Harvard</div>
												<div ng-click="setPaperFormat(5, 'Other')" ng-class="(paperFormat == 5)?'active':''">Other</div>
											</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text"></div>
										<div class="content">
											<label>
												<div class="check-container">
									                <input type="checkbox" name="remember" hidden="" ng-click="setAbstractPage()" name="abstract_page">
									                <div class="checkbox top"></div>
									                <div class="inline-block top text-check-box f-right">
								                		Add an Abstract page to my paper
							                		</div>
								                </div>
											</label>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Indicate the number of sources on which you need to base your paper
												</div>
											</div>
											<span class="middle">Source:</span>
									 	</div>
										<div class="content">
											<div class="middle">
												<div class="academic-level pages middle">
													<div ng-click="decrementSource()">-</div>
													<div>
														<input type="number" ng-model="source">
													</div>
													<div ng-click="incrementSource()">+</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Indicate the topic of your paper
												</div>
											</div>
											<span class="middle">Topic:</span>
									 	</div>
										<div class="content">
											 <input type="text" class="input" name="topic" ng-model="topic">
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Type here as many details as possible. The more specific you are - the easier it will be for the writer to complete your assignment correctly.
												</div>
											</div>
											<span class="middle">Paper Details:</span>
									 	</div>
										<div class="content">
											<textarea class="input" rows="6" name="details" ng-model="details"></textarea>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text"></div>
										<div class="content">
											<label>
												<div class="check-container">
									                <input type="checkbox" name="remember" name="plagiarism_report" ng-click="setPlagiarismReport()" hidden="">
									                <div class="checkbox top"></div>
									                <div class="inline-block top text-check-box f-right">
							                		 	I want to receive official Plagiarism report
							                		</div>
								                </div>
											</label>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Will the writer need any additional materials? You will be able to upload any files at your Personal Account. The account will be automatically created after the order placement.
												</div>
											</div>
											<span class="middle">Additional Materials:</span>
									 	</div>
										<div class="content">
											<div class="academic-level flex flex-children">
												<div ng-click="setAdditionalMaterials(1, 'Not needed')" ng-class="(additionalMaterials == 1)?'active':''" class="f-0p8">
													<div>Not needed</div>
												</div>
												<div ng-click="setAdditionalMaterials(2, 'Needed, I will provide them later')" ng-class="(additionalMaterials == 2)?'active':''" class="flex-2 f-0p8">
													<div>Needed, I will provide them later</div>
												</div>
												<div ng-click="setAdditionalMaterials(3, 'Needed, I won\'t be able to provide them')" ng-class="(additionalMaterials == 3)?'active':''" class="flex-2 f-0p8 flex j-content-center aligh-c">
													<div>Needed, I won't be <br> able to provide them</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="pay">
									<div class="right">
										<span class="middle">Approximate price:&nbsp&nbsp</span>
										<span class="cost f-blue middle">$ {$ price | number:2 $}</span>
									</div>
								</div>
								
								<div class="steps-button-container">
									<div>
										<div class="back-to-step" ng-click="goToStep(1, $event)">Back to step 1</div>
									</div>
									<div>
										<button class="common-button" ng-click="goToStep(3, $event)" id="submit">GO TO STEP 3</button>
									</div>
									<div></div>
								</div>
							</form>
						</tab-content-item>
						<tab-content-item ng-class="(orangePage == 3)?'active':''">
							<div class="container-form padding-t-20">
								<div class="form-group padding-t-20">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												"Writing from scratch" means that you will get a custom paper that will be written according to your instructions. If you need to revise your essay to make sure it's well organized and there are no mistakes, you should select "Editing/proofreading" type of service.
											</div>
										</div>
										<span class="middle">Type of service:</span>
									</div>
									<div class="content">
										<div class="academic-level flex flex-children">
											<div ng-click="setTypeService(1, 'Writing from scratch')" ng-class="(typeService == 1)?'active':''">Writing from scratch</div>
											<div ng-click="setTypeService(2, 'Editing/proofreading')" ng-class="(typeService == 2)?'active':''">Editing/proofreading</div>
										</div>
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Select your academic level
											</div>
										</div>
										<span class="middle">Academic Level:</span>
									</div>
									<div class="content">
										<div class="academic-level flex flex-children">
											@foreach($academicLevels as $level)
												<div ng-click="setLevel({{$level->id}})" ng-class="(level == {{$level->id}})?'active':''">
													{{$level->name}}
												</div>
											@endforeach
										</div>
									</div>
								</div>
									<div class="form-group padding-t-20">
										<div class="text"></div>
										<div class="content">
											<label>
												<div class="check-container">
													<input type="checkbox" name="remember" name="vip" ng-click="setVip()" hidden="">
													<div class="checkbox top"></div>
													<div class="inline-block top text-check-box f-right">
							                		<span class="middle">
							                			I want to order VIP customer service
							                		</span>
														<div class="inf-container">
															<div class="inf">
																VIP customer service means that replying to your messages and answering your calls will be our first order of business. Additionally, you will receive SMS notifications when a writer is assigned to your order, and when it is completed.
															</div>
														</div>
													</div>
												</div>
											</label>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Please enter desired number of pages
												</div>
											</div>
											<span class="middle">Number of pages:<span>
										</div>
										<div class="content clear-after">
											<div class="left middle">
												<div class="academic-level pages middle">
													<div ng-click="decrementPage()">-</div>
													<div>
														<input type="number" ng-model="pages">
													</div>
													<div ng-click="incrementPage()">+</div>
												</div>
											</div>
											<div class="right middle m-full-width">
												<div class="inline-block middle padding-r-10 m-padding-t-30 m-full-width m-padding-l-40">
													<div class="inf-container">
														<div class="inf">
															If you need the Power Point Presentation, specify the number of slides you want. 1 slide = 50% of the cost per page
														</div>
													</div>
												<span class="middle">Slides:<span>
												</div>
												<div class="inline-block middle  m-padding-t-30">
													<div class="academic-level pages right">
														<div ng-click="decrementSlide()">-</div>
														<div>
															<input type="number" ng-model="slides">
														</div>
														<div ng-click="incrementSlide()">+</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group padding-t-20 m-padding-t-30">
										<div class="text m-padding-t-30"></div>
										<div class="content m-padding-t-30">
											<div class="academic-level">
												<div class="padding-l-r-10" ng-click="setSpaced(2)" ng-class="(spaced == 2)?'active':''">Single Spaced</div>
												<div class="padding-l-r-10" ng-click="setSpaced(1)" ng-class="(spaced == 1)?'active':''">Double Spaced</div>
											</div>
											<div class="right spaced-text">{$ pages $} page = {$ spaced * 275 * pages $} words</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text">
											<div class="inf-container">
												<div class="inf">
													Please specify the deadline in advance, to have some time to review the paper
												</div>
											</div>
											<span class="middle">First Draft Deadline:<span>
										</div>
										<div class="content relative">
											<div class="inline-block margin-r-50 relative" ng-show="pages<50">
												<div class="academic-level hours">
													<div ng-repeat="hour in deadlines | greater:0 | lessOrEqual:24 " ng-click="setDeadlineId(hour.id)" ng-class="(deadlineId==hour.id)?'active':''">
														{$ hour.name $}
													</div>
												</div>
												<div class="hours-text">Hours</div>
											</div>
											<div class="inline-block relative">
												<div class="academic-level hours">
													<div ng-repeat="hour in deadlines | greater:24 | lessOrEqual:336" ng-click="setDeadlineId(hour.id)" ng-class="(deadlineId==hour.id)?'active':''">
														{$ hour.name $}
													</div>
												</div>
												<div class="days-text">Days</div>
											</div>
											<div class="inline-block right relative" ng-show="pages>49">
												<div class="academic-level hours">
													<div ng-repeat="hour in deadlines | greater:336" ng-click="setDeadlineId(hour.id)" ng-class="(deadlineId==hour.id)?'active':''">
														{$ hour.name $}
													</div>
												</div>
												<div class="hours-text">Months</div>
											</div>
										</div>
									</div>
									<div class="form-group padding-t-20">
										<div class="text"></div>
										<div class="content">
											<div class="padding-l-20">
												{{-- <div class="margin-t-5">
													<div class="inf-container">
														<div class="inf">
															A complete version of the paper will be delivered to you by this time.
														</div>
													</div>
													<span class="middle f-0p9 text-check-box">
														The deadline for the first draft is 30 May 06 PM
													</span>
												</div> --}}
												<div class="margin-t-5">
													<div class="inf-container">
														<div class="inf">
															If you must turn in the paper earlier, please adjust the first draft deadline accordingly, so that you have enough time to review the paper and request a revision in case of need.
														</div>
													</div>
													<span class="middle f-0p9 text-check-box">
														We estimate that your final submission deadline is {$ getDeadlineDate() | date:'dd MMM h a' $}
													</span>
												</div>
											</div>
										</div>
									</div>
								<div class="form-group padding-t-20 prefered-writer">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												You can request a specific writer or one of our TOP writers to work on your order
											</div>
										</div>
										<span class="middle">Preferred writer:</span>
								 	</div>
									<div class="content">
										<div class="academic-level flex flex-children prefered-writer">
											<div ng-click="setPreferred(1, 'Regular writer')" ng-class="(preferred == 1)?'active':''">
												<div class="middle">Regular writer</div>
											</div>
											<div ng-click="setPreferred(2, 'Advanced regular writer')" ng-class="(preferred == 2)?'active':''">
												<div class="middle">Advanced regular writer</div>
											</div>
											<div ng-click="setPreferred(3, 'My previous writer')" ng-class="(preferred == 3)?'active':''">
												<div class="middle">My previous writer</div>
											</div>
											<div ng-click="setPreferred(4, 'TOP writer: Fulfilled by top 10 writers')" ng-class="(preferred == 4)?'active':''">
												<div class="middle">TOP writer: Fulfilled by top 10 writers</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group padding-t-10">
									<div class="text"></div>
									<div class="content">
										<div class="padding-l-20">
											<div class="margin-t-5">
												<span class="middle f-0p9 text-check-box" ng-click="diskount=1">
													<a href="" class="text-check-box">Have a discount?</a>
												</span>
												<div class="inf-container">
													<div class="inf">
														Please be aware that discounts are not cumulative and they cannot be applied to orders under $30.00
													</div>
												</div>
											</div>
										</div>
										<div ng-show="diskount==1" class="clear-after padding-t-10">
											<div>
												<div>
													<span class="data-error-message f-left padding-l-20" id="discount"> Wrong discount code </span>
												</div>
												<div>
													<div class="left width-200">
														<input type="text" class="input " name="discount" ng-model="codeDiscount">
													</div>
													<div class="right width-200">
														
														<button class="sicount-button" ng-click="getDiscount()">Check Price with Discount</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group padding-t-20">
									<div class="text">
										<div class="inf-container">
											<div class="inf">
												Please choose one of the systems to proceed with the payment
											</div>
										</div>
										<span class="middle">Preferred payment system:</span>
								 	</div>
									<div class="content">
										<div class="academic-level flex flex-children">
											<div class="padding-l-r-10" ng-click="setPaymentSystem(1)" ng-class="(paymentSystem == 1)?'active':''">
												<span class="credit-icon middle inline-block" ng-class="(paymentSystem == 1)?'white-filtr':''"></span>
												<span class="middle">Credit Card</span>
											</div>
											<div class="padding-l-r-10" ng-click="setPaymentSystem(2)" ng-class="(paymentSystem == 2)?'active':''">
												<span class="pay-pal-icon middle inline-block"></span>
											</div>
										</div>
									</div>
								</div>
								</div>
								<div class="pay">
									<div class="right">
										<span class="middle">Approximate price:&nbsp&nbsp</span>
										<span class="cost f-blue middle">$ {$ price | number:2 $}</span>
									</div>
								</div>
								<div class="steps-button-container">
									<div>
										<div class="back-to-step" ng-click="goToStep(2, $event)">Back to step 2</div>
									</div>
									<div class="button-container">
										<button class="common-button" ng-click="submitOrder($event)">Proceed to Secure Payment</button>
										@include('user.layouts.spinner')
									</div>
									<div></div>
								</div>
							</div>
						</tab-content-item>
					</tab-content>
				</tab-panel>
			</form>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script type="text/javascript">

        app.filter('lessOrEqual', function () {
		    return function ( items, value ) {
		        var filteredItems = [];
		        angular.forEach(items, function ( item ) {
		            if ( item.hours <= value ) { filteredItems.push(item); }
		        });
		        return filteredItems;
		    }
		})
		app.filter('greater', function () {
		    return function ( items, value ) {
		        var filteredItems = []
		        angular.forEach(items, function ( item ) {
		            if ( item.hours > value ) { filteredItems.push(item); }
		        });
		        return filteredItems;
		    }
		})

        app.controller('freeInquiryCtrl', function ($scope, $http) {
			$scope.URLToArray = function (url) {
				var request = {};
				var pairs = url.substring(url.indexOf('?') + 1).split('&');
				for (var i = 0; i < pairs.length; i++) {
					if(!pairs[i])
						continue;
					var pair = pairs[i].split('=');
					request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
				}
				return request;
			};
			$scope.request = $scope.URLToArray(document.location.href);

			if($scope.request.type){
				var typeText = document.querySelector('#type-of-paper [data-type="' + $scope.request.type + '"]').innerHTML;
				document.querySelector('#type-of-paper').setOption(typeText);
			}

			$scope.codeDiscount = "";

			$scope.success = parseInt('{{ Auth::check() ? 1 : 0 }}');
        	$scope.discount = 0;
			
			$scope.hours = undefined;
        	$scope.days = undefined;
        	$scope.month = undefined;

        	$scope.paperFormat = 1;
        	$scope.paperFormatStr = 'MLA';
        	$scope.source = 0;
        	$scope.additionalMaterials = 1;
        	$scope.additionalMaterialsStr = "Not needed";
        	$scope.typeService = 1;
        	$scope.typeServiceStr = 'Writing from scratch';

			$scope.deadlines = {!! json_encode($deadlines) !!};
        	$scope.level = parseInt($scope.request.academic_level) || {{$academicLevels[0]->id}};
        	$scope.slides = 0;
        	$scope.pages = parseInt($scope.request.pages) || 1;
        	$scope.spaced = 1;
        	$scope.preferred = 1;
        	$scope.preferredStr = 'Regular writer';
        	$scope.paymentSystem = 1;
			$scope.typeId = parseInt($scope.request.type) || undefined;
			$scope.subjectId = undefined;
			$scope.deadlineId = parseInt($scope.request.deadline) || $scope.deadlines[$scope.deadlines.length-1].id;
			$scope.countryId = undefined;
			$scope.userId = undefined;

			$scope.bluePage = 1;
        	$scope.orangePage = parseInt('{{ Auth::check() ? 2 : 1 }}');

			$scope.vip = false;
			$scope.plagiarismReport = false;
			$scope.abstractPage = false;
			$scope.vipPrice = 15;
			$scope.plagiarismReportPrice = 10;
			$scope.abstractPagePrice = 15;
			$scope.advancedWriterCoef = 1.3;
			$scope.topWriterCoef = 1.58;
			$scope.login = { email:'', password:'', _token:"{{csrf_token()}}", remember: '' };
			$scope.registration = { email:'', name:'', last_name:'', phone:'', country:"{{$countryCode}}".toLowerCase(), _token:"{{csrf_token()}}", remember: '' };
			$scope.details = '';
			$scope.topic = '';

			$scope.types = {!! json_encode($types->indexById()) !!};
			$scope.price = 0;

        	$scope.vars = 'typeId + subjectId + deadlineId + countryId + hours + days + vip + plagiarismReport + abstractPage + pages + slides + source + preferred + paymentSystem + spaced + level + typeService + paperFormat + additionalMaterials';

        	$scope.$watch($scope.vars, function () {
				$scope.calcPrice();
        	}, true);
        	$scope.$watch('pages', function(nextVal, prevVal) {
				if(nextVal>999){
					$scope.pages = 999;
				}
			}, true);
			$scope.$watch('source', function(nextVal, prevVal) {
				if(nextVal>999){
					$scope.source = 999;
				}
			}, true);
			$scope.$watch('slides', function(nextVal, prevVal) {
				if(nextVal>999){
					$scope.slides = 999;
				}
			}, true);

        	$scope.setPaperFormat = function (paperFormat, str){
        		$scope.paperFormat = paperFormat;
        		$scope.paperFormatStr = str;
        	};
        	$scope.setAdditionalMaterials = function (value, str){ $scope.additionalMaterials = value; $scope.additionalMaterialsStr = str;};
        	$scope.setTypeService = function (typeService, str){
        		$scope.typeService = typeService;
        		$scope.typeServiceStr = str;
        	};
        	$scope.setLevel = function (level){
        		$scope.level = level;
        	};
        	$scope.setSpaced = function (spaced){
        		$scope.spaced = spaced;
        	};
        	$scope.setPreferred = function (preferred, str){
        		$scope.preferred = preferred;
        		$scope.preferredStr = str;
        	};
        	$scope.setPaymentSystem = function (paymentSystem){
        		$scope.paymentSystem = paymentSystem;
        	};

			$scope.setTypeId = function (id){ $scope.typeId = id; };
			$scope.setSubjectId = function (id){ $scope.subjectId = id; };
			$scope.setDeadlineId = function (id){ $scope.deadlineId = id; };
			$scope.setCountryId = function (id){ $scope.countryId = id; };
			$scope.setPhone = function (country, code){
				$scope.registration.country = country; 
				$scope.registration.code = code;
			};


        	$scope.setHours = function (hours){ $scope.hours = hours; };
        	$scope.setDays = function (days){ $scope.days = days; };

			$scope.setVip = function() { $scope.vip = !$scope.vip;};
			$scope.setPlagiarismReport = function() { $scope.plagiarismReport = !$scope.plagiarismReport;};
			$scope.setAbstractPage = function() { $scope.abstractPage = !$scope.abstractPage;};

        	$scope.incrementPage = function (){ 
        		if($scope.pages < 999) $scope.pages++; 
        		else $scope.pages = 999;
        	};
        	$scope.decrementPage = function (){ 
        		if($scope.pages > 1) $scope.pages--; 
        		else $scope.pages = 1;
        	};

        	$scope.incrementSlide = function (){ 
        		if($scope.slides<999) $scope.slides++;
        		else $scope.slides = 999;
        	};
        	$scope.decrementSlide = function (){ 
        		if($scope.slides>0) $scope.slides--;
        		else $scope.slides = 0;
        	};

        	$scope.setBluePage = function (bluePage){ $scope.bluePage = bluePage; };

        	$scope.incrementSource = function (){ 
        		if($scope.source < 999) $scope.source++; 
        		else $scope.source = 999;
        	};
        	$scope.decrementSource = function (){ 
        		if($scope.source > 0) $scope.source--; 
        		else $scope.source = 0;
        	};

			
			$scope.getDeadlineDate = function(){
				var date = new Date();
				var deadline
				for(var i = 0; i < $scope.deadlines.length; i++){
					if($scope.deadlines[i].id == $scope.deadlineId){
						deadline = $scope.deadlines[i].hours / 24;
					}
				}
				date.setDate(date.getDate() + deadline);
				return date;
			}

			$scope.calcPrice = function() {
				if($scope.level === undefined || $scope.typeId === undefined || $scope.deadlineId === undefined) return;
				var basePrice = parseFloat($scope.getPagePrice());
				if($scope.typeService == 2) basePrice*= 0.6;
				if($scope.preferred == 2) basePrice*= 1.3;
				else if($scope.preferred == 4) basePrice*= 1.58;
				var pagesPrice = basePrice*($scope.pages*$scope.spaced);
				var slidesPrice = (basePrice/2)*$scope.slides;
				$scope.price =   pagesPrice + slidesPrice + (+$scope.vip)*$scope.vipPrice + (+$scope.plagiarismReport)*$scope.plagiarismReportPrice + (+$scope.abstractPage)*$scope.abstractPagePrice;
				if($scope.discount) $scope.price *= 0.9;
			};

			$scope.getPagePrice = function() {
				for(var key in $scope.types[$scope.typeId][0].prices){
					var item = $scope.types[$scope.typeId][0].prices[key];
					if(item.deadline_id == $scope.deadlineId
						&& item.academic_level_id == $scope.level) {
						return item.price;
					}
				}
				return 0.0;
			};


			$scope.getDiscount = function(){
				if($scope.discount == 0){
					$http.get('{{ url("/check-discount") }}', {params: {'code': $scope.codeDiscount}}, {}).then(
						function(data){
							if(data.data == "1"){
								$('#discount').removeClass('show-eror');
								$('input[ng-model="codeDiscount"]').attr('disabled', '');
								$('input[ng-model="codeDiscount"]').closest('div').addClass('discount-valid');
								$scope.discount = 1;
								$scope.calcPrice();
							} else{
								$('#discount').addClass('show-eror');
							}
						},
						function(error){
							popUpError.open();
						}
					);
				}
			}


			$scope.getUser = function($event) {
				if($event) $event.preventDefault();
				
				if($scope.bluePage == 1) {
					var data = $scope.registration;
					var url = '{{ url('/ajax-register-user') }}';
				} else {
					var data = $scope.login;
					var url = '{{ url('/ajax-get-user') }}';
				}

				$http.post(url, data, {}).then(
					function(data){
						$scope.userId = data.id;
						$scope.success = 1;
						$scope.orangePage = 2;
					},
					function(error){
						if($scope.bluePage == 1) {
							validWithServer(error.data, '#registration-form');
						}else{
							validWithServer(error.data, '#login-form');
						}
					}
				);
				return false;

			};

			$scope.goToStep = function(step, e){
				e.preventDefault();
				
				switch(step) {
			  		case 1:{
			  			if(!$scope.success) $scope.orangePage = 1;
			  			break;
			  		}
				    
			  		case 2:{
			  			if($scope.success) $scope.orangePage = 2;
	        			else $scope.getUser(); 
			  			break;
			  		}
				    
				    case 3:{

				    	if($scope.success){
				    		selectBoxValidator('#order-form-2');
				    		if($('#order-form-2').valid() && selectBoxValidator('#order-form-2')) $scope.orangePage = 3;
				    	}
	        			else $scope.getUser();
				    	break;
				    }
				}
			}

			$scope.submitOrder = function($event) {
				$event.preventDefault();
				$('#container-floatingCirclesG').css('opacity',1);
				var data = {
					'_token': '{{ csrf_token() }}',
					'academic_level_id': $scope.level,
					'type_id': $scope.typeId,
					'subject_id': $scope.subjectId,
					'format':$scope.paperFormatStr,
					'abstract_page': $scope.abstractPage,
					'sources': $scope.source,
					'topic': $scope.topic,
					'details': $scope.details,
					'plagiarism_report': $scope.plagiarismReport,
					'additional_materials': $scope.additionalMaterialsStr,
					'type_of_service': $scope.typeServiceStr,
					'vip': $scope.vip,
					'preferred_writer': $scope.preferredStr,
					'spacing': $scope.spaced,
					'pages_count': $scope.pages,
					'slides_count': $scope.slides,
					'deadline_id': $scope.deadlineId,
					'preferred_writer_index': $scope.preferred,
					'service_type_index': $scope.typeService,
					'discount_code': $scope.codeDiscount
				};


				$http.post('', data, {}).then(
					function(data){
						document.location.href = data.data.url;
					},
					function(error){ alert(error); }
				);
				return false;
			};
        });
	</script>
	<script>
		(function () {

			$("#submit").click(function () {
				var content = $("#subject").html();
				if(content == "Please select"){
					$("#subject-input").addClass("error");
					$("#subject-error").css("display", "block");
					$("#subject").css("color", "#ff7575");
				}
			});
			$(".select").click(function () {
				$(this).closest(".input-select").find(".select-box").removeClass("error");
				$(this).closest(".input-select").find("#subject-error").css("display", "none");
				$(this).closest(".input-select").find("#subject").css("color", "#393939");
			});

		
			$("#order-form-2").validate({
				rules:{
					topic: {
						required: true,
						minlength: 5
					},
					details: {
						required: true,
						minlength: 5
					}
				},
				messages: {
					details: "The paper details should contain at least 3 words. English only",
					topic: "This field must contain a minimum of 5 letters",
				}
			});
		})();
	</script>
	<script>
		$(function () {
			function changeCountry(){
				$('#selected-country').attr('class', 'flag '+$(this).attr('code'));
				$('#phone-prefix1').html('+' + $(this).attr('tax'));
			}
			var currentCountryCode = '{{$countryCode}}'.toLowerCase();
			var currentCountry = $('.country[code="'+currentCountryCode+'"]');
			changeCountry.call(currentCountry);
			$('.country').click(changeCountry);
		});
	</script>
@stop
