@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Custom Speech Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>3])
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    The properly written speech is an art. It's important to have abilities and style for composing a speech. Just great speech can help you to intrigue audience. People often dismiss this perspective and make basic mistakes. Indeed, even the cleverest individuals and specialists experience issues with speech writing
                </p>
                <p>
                    Are you able to write a great speech? Even if the answer is no, it is not a problem, since you can get your custom speech with our speech writing services. Ask us to write a speech for you, and save yourself from all problems with speech writing. Use our speech writing services and become really awesome speaker.
                </p>
                <h4> OriginalPapersOnly.com provides the best speech writing services </h4>
                <p>
                    You can order any speech for various subjects and lengths. 
                </p>
                <p>
                    Our well instructed and gifted writers are experts in writing speeches. They can effortlessly write speech on any topic and for the most difficult audience.
                </p>
                <p>
                    Our specialists will write the speech in the most limited time in consistence with required style.
                </p>
                <p>
                    Experts of our speech writing service OriginalPapersOnly.com don't take long to understand requirements and wishes of the customer. Our speech writing service can also help you to improve your existing speech. This doesn't require a lot of efforts on our part, and you will get a discount in such case.
                </p>
                <h4> Be original with OriginalPapersOnly.com </h4>
                <p>
                    The writers of OriginalPapersOnly.com make completely original works. No one can blame you for plagiarism. Request speech writing service from us and turn into a great speaker. We are genuinely the best in writing speeches for rather little money, as our clients are mostly students, whose primary occupation is studying. That is the reason we can offer a flexible system of discounts for our customers. What's more, you will have the capacity to get a full refund in case if you don’t like something in your speech. In the event that the custom composed discourse does not meet your requirements, you can request as many free amendments as you need.
                </p>
                <p>
                    It is possible that you are not as good as you want in your field, and you might get low grades. However, remember that you can always use our speech writing service at OriginalPapersOnly.com!
                </p>
            </div>
        </div>
    </div>
@stop