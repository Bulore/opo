<div class="mobile-money-back">
    <div class="m-guarantee money-back">
        <p>100%</p>
        <p>money back</p>
        <p>guarantee</p>
    </div>
    <div class="m-guarantee completely-anonymous">
        <p>guarantee</p>
        <p>completely</p>
        <p>anonymous</p>
    </div>
    <div class="m-guarantee plagiarism-free">
        <div class="inner-plagiarism-fre">
            <p>100%</p>
            <p>plagiarism</p>
            <p>free</p>
        </div>
    </div>
</div>