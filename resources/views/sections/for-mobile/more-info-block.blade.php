<div class="mobile-more-info">
    <div class="mobile-block-header">
        More info
    </div>
    <hr class="m-horizontal-line">
    <drop-down-ordered-list data-open-only-one class="none-order drop-list-blue">
        <ordered-list-item>
            <ordered-list-header> What formats of papers are available? </ordered-list-header>
            <ordered-list-content>
                <div class="padding-l-20 margin-l-5 padding-b-20">
                    <div class="column full-width">
                        <div>Double-spaced (275 words per page) </div>
                        <div>Single-spaced (550 words per page) </div>
                        <div>Optimal font (e.g. Arial) 12pt</div>
                        <div>1 inch margins on all sides</div>
                    </div>
                </div>
            </ordered-list-content>
        </ordered-list-item>

        <ordered-list-item>
            <ordered-list-header> Our writers are specialists in a wide range of subjects in fields like: </ordered-list-header>
            <ordered-list-content>
                <div class="padding-l-20 margin-l-5  padding-b-20">
                    <div class="column full-width">
                        <div>Social Sciences</div>
                        <div>Hard Sciences</div>
                        <div>Nursing studies</div>
                        <div>Finance and Management</div>
                        <div>Computer Sciences</div>
                        <div>Literature and Languages</div>
                        <div>Modern and Fine arts</div>
                    </div>
                </div>
            </ordered-list-content>
        </ordered-list-item>

        <ordered-list-item>
            <ordered-list-header> Our services </ordered-list-header>
            <ordered-list-content class="left-block">
                <div class="inner-container">
                    <div class="services padding-b-20">
                        <form action="">
                            <label>
                                <a href="{{url("/essay-writing")}}"><input type="radio" name="service" checked><div>Essay writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/report-writing")}}"><input type="radio" name="service"><div>Report writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/speech-writing")}}"><input type="radio" name="service"><div>Speech writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/dissertation-writing")}}"><input type="radio" name="service"><div>Dissertation writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/thesis-writing")}}"><input type="radio" name="service"><div>Thesis writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/research-paper-writing")}}"><input type="radio" name="service"><div>Research paper writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/term-paper-writing")}}"><input type="radio" name="service"><div>Term paper writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/assignment-writing")}}"><input type="radio" name="service"><div>Assignment writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/homework-writing")}}"><input type="radio" name="service"><div>Homework writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/personal-statement")}}"><input type="radio" name="service"><div>Personal statement writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/cause-work-writing")}}"><input type="radio" name="service"><div>Cause work writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/academic-paper-writing")}}"><input type="radio" name="service"><div>Academic paper writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/college-paper-writing")}}"><input type="radio" name="service"><div>College paper writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/article-writing")}}"><input type="radio" name="service"><div>Article writing</div></a>
                            </label>
                            <label>
                                <a href="{{url("/writing-tips")}}"><input type="radio" name="service"><div>Writing tips</div></a>
                            </label>
                        </form>
                    </div>
                </div>
            </ordered-list-content>
        </ordered-list-item>

        <ordered-list-item>
            <ordered-list-header> Why Using OriginalPapersOnly is Not Cheating? </ordered-list-header>
            <ordered-list-content>
                <p>
                    OriginalPapersOnly.com doesn’t plan to fool or upset your teachers. By requesting papers from us, you will have a chance to improve your writing skills and learn more about paper's formats. We encourage you to take our written paper as a basis to write your paper by yourself.
                </p>
            </ordered-list-content>
        </ordered-list-item>

        <ordered-list-item>
            <ordered-list-header> Terms and Regulations </ordered-list-header>
            <ordered-list-content>
                <p>
                    The following privacy notice explains our policy regarding the personal information we collect about you in the process of your cooperation with. 
                </p>
                <a href="{{url('/privacy')}}">Read</a>
                <br>
                <br>
                <p>
                    Terms of Use
                </p>
                <p>
                    Your use of this Website constitutes your agreement with the terms and conditions as stated below. If you disagree with any of these terms and conditions, do not use this Website. 
                </p>
                <a href="{{url('/terms-of-use')}}">Read</a>
                <br>
            </ordered-list-content>
        </ordered-list-item>
    </drop-down-ordered-list>
</div>