@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120 m-margin-top-0">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Custom Dissertation Writing Service</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock", ['indexServise'=>4])
        </div>
        <div class="content">
            <div class="container-for-text">
                <p>
                    Students, who have to write a dissertation always have a lot of questions, regarding technique, rules and defense procedure.  But usually it is complicated to find answers for these questions, even with the help of libraries. However, not all students have sufficient time and energy to read every book and find important information.
                </p>
                <p>
                    OriginalPapersOnly.com can help you to write your dissertation, thesis, proposal or anything else. If you choose to entrust this important work to us, you will be surprised with your high grades. You shouldn’t worry about contacting our service to help you with your dissertation, as it is extremely important for your future career. We were made to save your time and efforts.  
                </p>
                <h4> Professional help for your future </h4>
                <p>
                    Our experts can write dissertations of any level.  We write all dissertations, regarding your requirements. Professional writers can assist you at any stage of the writing procedure.  You can also order dissertation editing or proofreading help.
                    <br>
                    We guarantee 100% uniqueness of all our works.
                </p>
                <p>
                    We have hundreds of authors, who can write a high-quality dissertation for you. Our experts in various fields will handle with dissertation quickly, so you will have time to make necessary amendments. You will find nowhere else such writing service, which takes care of all the details about dissertations.
                </p>
                <p>
                    Moreover, we can guarantee that if you order your dissertation from OriginalPapersOnly.com, you will get a qualified author in the given subject. Some of our writers have over 5 years of experience in writing dissertations. They respond rapidly to demands for changes in your dissertation composing and will finish your work in advance.  
                </p>
                <h4> It is easy to order dissertation at OriginalPapersOnly.com </h4>
                <p>
                    A lot of custom writing services offer dissertations, yet not all of them meet the expectations of present day clients. One of the aims of OriginalPapersOnly.com is to perform quality works fast. Frankly speaking, dissertation is the most complicated of all requests, it demands responsibility and knowledge. OriginalPapersOnly.com has a flexible system of discounts, which depends on your request and duration of your cooperation with OriginalPapersOnly.com.
                </p>
                <p>
                    Writing dissertation requires a lot of time, however we offer reasonable prices. Our experts care about your academic achievements. They are glad to help you become successful.
                </p>
            </div>
        </div>
    </div>
@stop