@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Can You Write My Assignment?</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            <div class="container-for-text">
                <p class="f-w-700 color-blue"><i>- written for us by Catherine R., a college student</i></p>
                <p>If you're a student, then you know how hard a student's life can be. Why? For many reasons. First of all, life can seem gloomy when your professor asks you to write an assignment and you realize you can't do it by yourself. I had the exact same situation not a long time ago.</p>
                <p>How did I manage to get out of it? I will say only three words: OriginalPapersOnly.com writing agency! I realized I wasn't able to write <b>anything on the given topic</b>. I needed a highly-skilled writer to do my assignment for me. While I was desperately looking for some online writers who would write my assignment for me, some guy on a forum sent me a link to OriginalPapersOnly.com</p>
                <p class="color-blue f-w-900">How Can I Contact OriginalPapersOnly.com?</p>
                <p>I couldn't believe that the only thing I had to do was send them a request with a "can you write my assignment" request. I was so impressed to receive an immediate reply!</p>
                <p>They explained to me that they would <b>gladly write my assignment</b> and would be ready to write my future university assignments as well. I must admit that OriginalPapersOnly.com is definitely the most professional team to write my assignment for me.</p>
                <p>The support staff assured me that the work process would be very simple and I would have no regrets after the work is delivered. I provided all the instructions on how to write my assignment. I realized that they would write my assignment for me for a very cheap price! Besides, they promised to do my assignment before the deadline.</p>
                <p class="color-blue f-w-900">Can I Stay in Touch with My Writer?</p>
                <p>Yes, you can! OriginalPapersOnly.com support staff also provided me with means to keep in touch with my writer so that they could do my assignment following all my requirements. I couldn't be more sure that the author would do my assignment for me and only for me. By this I mean that I was sure it won't be resold or published, or something like that.</p>
                <p>I appreciate the <i>opportunity to have my assignment revised</i> in case I find something wrong with it. The agency also gave a money-back guarantee in case I was unsatisfied with the job performed.</p>
                <p>As a result, <b>my assignment was amazing!</b> I got an "A" and my professor was really happy with the work. I know that next time I will go to OriginalPapersOnly.com ONLY to do my assignment for me. I am absolutely certain that their team will do my assignment in the best way possible!</p>
                <p><b>OriginalPapersOnly.com writing agency turned out to be a dream come true! Their professional writers are waiting for your "can you write my assignment" call!</b></p>
            </div>
        </div>
    </div>
@stop