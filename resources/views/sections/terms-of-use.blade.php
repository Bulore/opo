@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="css/sideblocks.css">

<div class="mobile-header m-bold-700">
	<p>Terms of Use</p>
</div>

<div class="container margin-t-120 m-margin-top-0">

	<center>
		<div class="header-page  m-display-none">
			<div class="border"><div><div><div></div></div></div></div>
			<div class="text">Terms of Use</div>
		</div>
	</center>

	<div class="left-panel  m-display-none">
		@include("sideblocks.priceBlock")
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock")
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content">
		<div class="container-for-text f-black">
			<p>
				These Terms of Use are a legal instrument designed to govern your and our behavior with regard to the usage of this website or mobile app, and related services.
			</p>
			<p>
				These Terms of Use are a legally binding instrument. Therefore, our relations shall be governed by these Terms of Use only. By expressing your consent to be legally bound by these Terms of Use, you enter into an agreement with us. Therefore, we and you shall have mutual legal rights and obligations.
			</p>
			<p>
				You express your sufficient consent to be legally bound by these Terms of Use when you open OriginalPapersOnly.com Service and WriteMyPapers Essay Help Mobile App (together the Service) on your device. Please do not view neither our website nor mobile app and do not place any order if you disagree with any of the following rules.
			</p>
			<p>
				Below we describe various important issues you may face while using the Service. Please study these Terms of Use carefully before you proceed to using the Service.
			</p>
			<p>
				For your convenience, we refer to OriginalPapersOnly.com website and WriteMyPapers Essay Help Mobile App as “Service”. We refer to anyone who opens our Service and mobile app on the device as “You”.
			</p>
		</div>
		<div class="container-for-text c-tpt">
			<h1 class="padding-n">Terms of Use</h1>
			<h4>Applicability. Consent.</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					These Terms of Use are a special form of agreement. As stated above, You express your sufficient consent to enter into this Agreement once You start viewing the Service on your device.
				</li>
				<li>
					You enter into this agreement once You express your sufficient consent.
				</li>
				<li>
					We may ask You to express your sufficient consent repeatedly from time to time at our own discretion.
				</li>
				<li>
					Your use of the Service shall be prohibited as unlawful unless You agree to enter into an agreement on these Terms of Use.
				</li>
				<li>
					Please leave our Service immediately and don’t order any services if you disagree with these Terms of Use.
				</li>
				<li>
					As follows, these Terms of Use are applicable to You during the entire use of the Service.
				</li>
			</ol>
			<h4>Products and Service</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					Our main service is Copywriting. By Copywriting we mean preparation of unique text materials subject to individual orders. This description determines the whole interaction between You and us.
				</li>
				<li>
					By text materials we mean digital writings of any size on any topic.
				</li>
				<li>
					These text materials are received from our writers. The writers are skilled professionals on our team. These Terms of Use do not govern any relations with our writers.
				</li>
				<li>
					We ensure that our writers prepare completely unique text materials tailored exactly to your needs.
				</li>
				<li>
					We ask You to describe your needs in details when You submit an order.
				</li>
				<li>
					You may submit an order using a special order form on our Service.
				</li>
				<li>
					In addition, You can view the Service free of charge for information purposes only. Any other use of the Service is subject to a separate agreement between You and us.
				</li>
				<li>
					We can solve multiple choice tests and various problems for You. You may upload your test or your problem for us to solve.
				</li>
				<li>
					 In addition, we may accept your own texts for editing or proofreading to check for any spelling, grammatical, punctuation and stylistic errors or plagiarized parts.
				</li>
			</ol>
			<h4>Order Placement</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					 You are free to place an order for preparation of unique text material. To do so, simply use the order form on our Service.
				</li>
				<li>
					You shall accurately fill the order form with the correct information. This information will be the first input for us. As follows, these initial data are of great importance.
				</li>
				<li>
					You are strictly prohibited from filing the order form with untrue, false or misleading information.
				</li>
				<li>
					You shall provide the basic personal information for us to fill in the order form. This personal information will be stored and saved subject to our Privacy Policy.
				</li>
				<li>
					We may notify You about the most important stages of placing an order via SMS or email.
				</li>
				<li>
					To meet your requirement precisely, we encourage You to use some options when placing the order. Please choose any or all of these options:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							Preferred writer:
							<ol class="blue-strip-list margin-l-20 padding-l-20">
								<li>
									Regular writer – a qualified writer will prepare the text material for You
								</li>
								<li>
									TOP writer – the most experienced writer will prepare the text material for You,
								</li>
								<li>
									My previous writer – your previous writer will prepare the text material for You within 24 hours or more for additional price due to the specified deadline. You shall be automatically assigned to the TOP writer if your previous writer is not available,
								</li>
								<li>
									Advanced regular writer – You will get an experienced writer for an additional fee,
								</li>
							</ol>
						</li>
						<li>
							VIP customer service – your messages and requests shall be processed in the first place, and You will receive relevant SMS notifications,
						</li>
						<li>
							Abstract page – an additional Abstract page will be written for You,
						</li>
						<li>
							Plagiarism report – a detailed report with plagiarism level detection shall be uploaded for You upon the order approval.
						</li>
						<li>
							Editor's Check – You will receive your paper additionally proofread after the writer finishes working on it.
						</li>
					</ol>
				</li>
				<li>
					Your price will be automatically calculated once the order is duly placed.
				</li>
			</ol>
			<h4>Payment. Price.</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					You shall pay a price for our services.
				</li>
				<li>
					You shall keep in mind that we may change the price paid for our services at our own discretion at any time. You shall carefully check the latest price as specified on our Service before You submit your order.
				</li>
				<li>
					Of course, we may charge additional fee for optional services such as “Preferred writer”, “VIP customer service”, “Abstract page”, “Plagiarism report” and others.
				</li>
				<li>
					We do not include VAT to our price list. VAT will be charged if You are located in EU. However, we may charge VAT even if You are located outside the EU in case appropriate state authority requires us to do so.
				</li>
				<li>
			 		At our own discretion, we may decide that your order can not be priced subject to the fee quotation placed on the Service. In this case we will charge You to pay another price. In any case, You will have a possibility to cancel your order before You pay.
				</li>
				<li>
					We will invite You to pay for our services in advance if we reasonably expect the order to be fulfilled.
				</li>
				<li>
					Additionally, You may save the money paid as credit. In this case, the money will be saved on your personal account and may be used for your current or future orders.
				</li>
				<li>
					Special pricing applies to the multiple choice tests and other various problems. You will need to complete a special order form for these types of orders. Any question costs money to answer, and problem solving is generally more expensive.
				</li>
				<li>
					You may use a number of payment options to pay in the most convenient way. For the time being, we accept Visa cards, MasterCard cards, PayPal and Webmoney.
				</li>
			</ol>
			<h4>Order Fulfillment</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					The text material becomes available in your personal account panel once the ordered text material is prepared.
				</li>
				<li>
				 	You may approve the text material if You find it to be of the appropriate quality. The prepared text material will be deemed as approved if You do not express any claims for 14 days in a row. Otherwise, your order will be automatically approved in 14 days.
				</li>
				<li>
					To monitor the quality of our work, we may invite You to participate in a survey related to the quality of the text material received, work of the writer, and our Customer Support Representatives. Usually, we send these types of invitations in 7 days once the order is approved.
				</li>
				<li>
					The writer will not change more than 30% of the text if You order text editing or proofreading services.
				</li>
			</ol>
			<h4>Warranties</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We would like to be sure that You will not use our Service and text materials for any illegal purposes. As well, we are pleased to assure You that our services fully comply with all the requirements applicable. In order to do so, we implement the system of mutual warranties applicable to You and us.
				</li>
				<li>
					By accepting these Terms of Use You warrant that:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							You have duly read and understood these Terms of Use,
						</li>
						<li>
							You will properly cite all the information and/or ideas used from the ordered text materials,
						</li>
						<li>
							You will use the text materials solely as examples for research, reference, and/or for You to learn how to properly write a text material in a particular citation style (MLA, APA, Chicago, Turabian, Harvard, etc.),
						</li>
						<li>
							You are in agreement that our Service acquires payments for the time and effort that goes into gathering, organizing, correcting, editing, posting, and delivering these reference materials and the maintenance, administration, and advertising of our Service,
						</li>
						<li>
							You will not reproduce, distribute, publish, transmit, modify, display or create derivative works from or exploit the text materials and/or contents of our Service without our previous written consent,
						</li>
						<li>
							You will destroy all the text materials prepared by our writers immediately once your use of the material is complete. No copies shall be made for distribution, and no parts of any text material shall be used without proper citation,
						</li>
						<li>
							You will not put your name on the text materials,
						</li>
						<li>
							You provide us with all the true and valid information upon our request in any form.
						</li>
					</ol>
				</li>
				<li>
					At the same time, we warrant that:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							we have enough intellectual property rights to grant You with the right to use text materials as described in these Terms of Use,
						</li>
						<li>
							the text materials prepared will have the plagiarism level lower than 10% (except bibliographical references and cliched phrases),
						</li>
						<li>
							we will follow all your reasonable instructions and reasonable requirements to the text material,
						</li>
						<li>
							we will conduct the reasonably necessary research to prepare the ordered text material,
						</li>
						<li>
							we strive to make our Service available at all times. However, we will not be liable if, for any reason, the site is unavailable for some period of time. Access to this site may be suspended at any time without a prior notice being given.
						</li>
					</ol>	
				</li>	
			</ol>
			<h4>Intellectual Property Rights</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					To fulfill your order in a proper way, we will prepare unique text material that has not existed before. You pay only for the preparation of the text, but not for the intellectual property rights on this text.
				</li>
				<li>
					As follows, we shall remain the only owner of the text material and we reserve all the intellectual property rights to the text material.
				</li>
				<li>
					We take care of the highest quality of the prepared text materials. As follows, we do our best to ensure that these texts are free of plagiarism and do not infringe any copyrights possible. To the best of our knowledge, these texts do not violate any rights of any third parties.
				</li>
				<li>
				 	In order to authorize You for legal usage of the text material, we grant You with a limited right to use the unique text material. This limited right to use unique text material shall remain in force and be valid for six month since the payment for your order is initiated.
				</li>
				<li>
				 	In any case, all the intellectual property rights shall belong to us.
				</li>
				<li>
					We do not appreciate plagiarism. We combat plagiarism with all reasonable efforts. As follows, we reserve the right to cancel any agreement, contract or arrangement with any person who condones or attempts to pass plagiarized text as original when asking for editing or proofreading.
				</li>
				<li>
					We do not condone, encourage, or knowingly take part in plagiarism or any other acts of academic fraud or dishonesty.
				</li>
				<li>
					We regard the plagiarism level to be acceptable if it is below 10%. In case the plagiarism level is higher, you may apply for revision or refund.
				</li>
				<li>
					Bibliographical references (in-text referencing and bibliography page at the end of the text materials) and cliched phrases (idioms, standard phrases, connectors and other frequently used phrases) shall not be regarded as plagiarism and shall not be included in the plagiarism level calculation.
				</li>
				<li>
					We do not keep the text materials prepared in any kind of databases. We may publish any text material as a content or as a sample essay. We do this to protect our writer's work in cases when You claim a refund when work has been already completed.
				</li>
			</ol>
			<h4>Acceptable Use</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					As stated above, we grant You with a limited right to use the unique text material. Below we explain how your right of use is limited. You shall comply with these limitations for your own safety and security.
				</li>
				<li>
					You purchase the text material for your personal, noncommercial use only. You may also use the text material for research and/or reference only.
				</li>
				<li>
			 		You shall not change the text material purchased.
				</li>
				<li>
					You shall not cause any possible harm to us by using the prepared text material.
				</li>
				<li>
					You shall not pass the text material to third parties or distribute the text material in any way for payment or for any other purpose. You shall not transfer any rights related to the text material to any third parties.
				</li>
				<li>
					You shall not put your name on the text materials.
				</li>
				<li>
					You agree that any text materials are provided only as a model, example document for research use, and any text and/or ideas from our document that You refer to, or otherwise use in any way in your own original paper must be properly cited and attributed to our Service.
				</li>
				<li>
					You shall not use the provided text materials to pass any online tests possible. Also, You shall not use any online facilities provided on our Service to pass any online tests possible.
				</li>
				<li>
					You shall not use the provided text materials as legal or other professional advice. You shall consult your professional adviser for legal or other advice.
				</li>
				<li>
					You shall not use the provided text materials to get any grade, mark or any academic achievement.
				</li>
			</ol>
			<h4>Referral Program</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We appreciate You inviting third parties to enjoy our services. As follows, we encourage You to receive up to 10% of the total sum paid by the Referrals You may involve.
				</li>
				<li>
					After the activation of the Referral Program, please submit to your account some additional information, which is required for processing your Referral Program earnings.
				</li>
				<li>
					Your earnings under the Referral Program may be used either to cover your own costs to enjoy our services, or to be withdrawn by you to PayPal or Webmoney accounts.
				</li>
				<li>
					For convenience, we shall not withdraw the earnings under your Referral Program unless you earn 100 USD or more. In addition, you may initiate a withdrawal with our Customer Support Representatives at least four days prior to the payment period.
				</li>
				<li>
				 	Normally, You can withdraw your earnings under your Referral Program between the 16th and the 20th day each month. However, we may change this term at our own discretion.
				</li>
				<li>
					You are free to refuse the Referral Program if you would like to. Furthermore, we may cancel this Referral Program immediately in any case. In both cases we may withhold the improperly collected earnings under the Referral Program.
				</li>
				<li>
					We encourage you not to use spam, web spam, including forums, social website spam or any other illegal means of promoting. All your emails must adhere to the CAN-SPAM Act of 2003, and/or any laws or regulations that govern your behavior in your jurisdiction. You shall not post referral discount codes to any coupon websites. Also, you shall not use our Referral Program for the purpose of cheating and collecting earnings unfairly, for example, creating fake double accounts in order to collect earnings.
				</li>
			</ol>
			<h4>Communication Manner</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We are always pleased to ensure proper informational interaction with You. As follows, we ask You to provide us with your email address and your telephone number to send You notifications as well as promotional materials.
				</li>
				<li>
					By accepting these Terms of Use, you express your explicit consent to receive notifications, advertisement and promotional materials.
				</li>
				<li>
					We may use any contact information (emails and phone numbers) submitted to the Service for the purposes stated in the "Order placement" paragraph of this document. If you would like to opt out of emails and SMS notifications from us, please let us know by contacting us. Once we receive your request, we will stop sending You this information without reasonable delay.
				</li>
				<li>
					Your email and telephone number may also be used for promotional and marketing purposes, to notify You of special offers and discounts, etc. Both your email and your phone number must be authorized. If You choose "VIP Customer Service", notifications will be sent to your mobile device informing You of when the writer starts working on your text material and when it is ready.
				</li>
				<li>
					For your convenience, we will follow these basic rules of communication with You stated below:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							our advertisement and marketing emails will always contain sender’s physical address, the sender’s information (such as the Service name and contact telephone number) and a one-click unsubscribe link.,
						</li>
						<li>
							our advertisement messages will always be clearly marked as advertising,
						</li>
						<li>
							our advertisement emails will always contain the sender's physical address and a one-click unsubscribe link,
						</li>
						<li>
							the sender’s information will be available within 30 days minimum since the information has been sent,
						</li>
						<li>
							we may send advertisement messages to your telephone number. You will always be able to opt out of such ads simply by replying with an SMS to the advertisement message,
						</li>
						<li>
							all the complaints about any subscriptions as well as answers to our advertisement messages shall be processed without unreasonable delay,
						</li>
						<li id="guarantee-revision">
							you will not be charged for any feedback, complaint, opt-out answer, subscription cancellation request or for processing other complaints you may have.
						</li>
					</ol>
				</li>
			</ol>
			<h4 >Revision</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We take pride in providing prime quality assistance. We are always ready to revise your text material if it does not entirely meet your expectations. We will revise your text material three times for free if your revision request follows our main terms below.
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							<b>Instructions:</b> Your revision guidelines must be clear, they should not contradict your initial instructions. If our Quality Assurance Department decides that your initial instructions have been satisfied, your revision request will be rejected.
						</li>
						<li>
							<b>Submission:</b> You can submit your revision request using the "Send for revision" button on your personal account page before approving the text material. Thus, view the document fully in the preview mode before approving your order. If You cannot clearly see the text in the preview mode due to technical problems, contact our Customer Support Representatives and ask them for another way to see the work preview. If due to some reason You approved your order but still need a revision, you should contact Customer Support Representatives and provide them with your revision instructions. Additionally, You will have to provide an appropriate time frame. Failing to do so will result in your order not being sent for revision.
						</li>
						<li>
							<b>Deadline:</b> You can request any of our three free revisions at any time before pressing the "Approve" button. After the order approval, however, you have 7 days to ask for a revision. If your order exceeds 20 pages, we can provide a free revision if requested within 14 days after approval. Your order will be considered as delivered on time if its first version was not late.
						</li>
						<li>
							<b>The number of revisions:</b> We can complete 3 (three) revisions absolutely for free only if they satisfy the conditions listed above. Please note that You can write as many revision instructions as necessary in each message.
						</li>
					</ol>
				</li>
				<li >
					In case the revision instructions you provide us with do not correspond to the above-mentioned requirements or your three free revisions have already been used, a new order for editing/proofreading needs to be placed, in which You must clearly specify the changes that are required in the text material.
				</li>
				<li id="money-back">
					Please pay attention to the fact that You have 14 days to approve the revised text material. The time until approval is calculated from the point when the last version was uploaded to your personal order page. After this time, the text material (or its part) is approved automatically.
				</li>
			</ol>
			<h4 >Refunds and Money Back Guarantee</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
				 	We provide You with a limited possibility to claim the money paid (make refund). This refund may be issued only to original source of payment. To make a successful refund You shall strictly observe the following rules.
				</li>
				<li>
					Before You approve the text material prepared, You may apply for:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							a 100% refund if:
							<ol class="blue-strip-list margin-l-20 padding-l-20">
								<li>
									there was any payment mistake (identical orders, double payment, etc). You will need to report the case to us immediately. You will only get a 100% refund if the writer has not been assigned, or
								</li>
								<li>
									we find that we are not able to provide You with a writer. In this case we will contact You and initiate the refund ourselves, or
								</li>
								<li>
									You no longer need the text material because the deadline has passed, but it wasn't delivered to You. In this case, You will not receive text material and will not have the right to use any materials that we might have sent You before on this order.
								</li>
							</ol>
						</li>
						<li>
							a 70% refund if:
							<ol class="blue-strip-list margin-l-20 padding-l-20">
								<li>
									the writer has been assigned, and less than half the deadline has passed. In this case the writer has started working on your text material and should be compensated.
								</li>
							</ol>
						</li>
						<li>
							a 50% refund if:
							<ol class="blue-strip-list margin-l-20 padding-l-20">
								<li>
									the writer has been assigned to your order, and more than a half of the deadline has passed, or
								</li>
								<li>
									we are not able to provide You with a writer for your revision.
								</li>
							</ol>
						</li>
					</ol>
				</li>
				<li>
					Once You initiate a refund, You are not granted with any property on intellectual property rights on the prepared text material. We revoke and cancel any property rights or intellectual property granted or may be granted to You if You initiate a refund.
				</li>
				<li>
					Sending your order for a revision means that You approve the text material generally, but require minor adjustments. If You send your text material for revision, You will not be able to get 100% money back.
				</li>
				<li>
					However, the refund option is not applicable to:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							credited money,
						</li>
						<li>
							VAT,
						</li>
						<li>
							discounts and Extras, such as VIP customer service, TOP Writer, Abstract, Plagiarism report, Sources Used, Table of Contents/Outline, and Editor's Check;
						</li>
						<li>
							cases when Quality Assurance Department decides that your initial instructions are satisfied,
						</li>
						<li>
							cases when requirements of these Terms of Use are not met,
						</li>
						<li>
							cases, when you have already pressed the “Approve” button,
						</li>
						<li>
							Multiple Choice Question assignments and Problem Solving assignments are not subjected to any refund. Using answers provided by the writer automatically means their approval. In case writer scores less than 50% of correct answers you can be qualified for partial credit. The amount of credit is decided by the Customer Experience Manager separately in each case.
						</li>
					</ol>
				</li>
			</ol>
			<h4>Quality Evaluation Procedure</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We are always ready to compensate You for any unpleasant experience that may occur because of unforeseen circumstances.
				</li>
				<li>
					We have developed a special procedure which will help You to assert your rights and also voice any concerns to a trained Manager whose main responsibility is to make you satisfied.
				</li>
				<li>
					Quality Evaluation is a negotiation procedure between You and a Customer Experience Manager about the possibility of refund or any other compensation for any disturbing experience. To get fast and professional assistance, we highly recommend that you initiate the Quality Evaluation procedure with our Customer Experience Manager, but not with the payment system.
				</li>
				<li>
					You are qualified for your order to be evaluated in the following cases only:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							if the order has been delivered, but You are not satisfied with the quality,
						</li>
						<li>
							if the order is approved, but there was detected plagiarism and if You are ready to provide an official proof of plagiarism from reliable plagiarism detection software.
						</li>
					</ol>
				</li>
				<li>
					To pass the necessary procedure and receive the Quality Evaluation status for your order, You shall:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							contact Customer Support Representatives and inform them about your problem,
						</li>
						<li>
							check the automatically generated message and confirm the initiation of Quality Evaluation with your Manager,
						</li>
						<li>
							provide solid arguments to support your claim for compensation (plagiarism report/other evidence of incompetence of the writer).
						</li>
					</ol>
				</li>
				<li>
					You will be able to get the Quality Evaluation resolution within 14 days since You satisfy all the requirements of these Terms of Use. You will have another 14 days to come to an agreement with the Manager. Otherwise, the Quality Evaluation procedure will be automatically closed, and no future refund will be possible.
				</li>
				<li>
					In order to avoid Quality Evaluation and receive a high quality text material on time, please follow these simple advices:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							authorize your payment in a timely manner to reduce the likelihood of delays,
						</li>
						<li>
							provide an outline before proceeding with the payment to make sure that You and your writer are on the same track,
						</li>
						<li>
							use the “Preferred Writer” option for all the orders,
						</li>
						<li>
							check messages and notifications from Customer Support Representatives and writers daily (if it is not an urgent order) or hourly (in case of an urgent order) and provide timely replies to the comments,
						</li>
						<li>
							be precise about your revision comments. The writer will make the necessary adjustments provided that You explain what went wrong.
						</li>
					</ol>
				</li>
				<li>
					Of course, You shall keep in mind that failure to provide information required for Quality Evaluation within 14 days will result in the annulment of the Quality Evaluation procedure, and no refund will be possible thereafter.
				</li>
			</ol>
			<h4>Limitation of Liability</h4>
			<ol class="orange-number-list margin-l-20">
				<li>
					We endeavor to determine the amount of your and our responsibility clearly to mitigate your and our risks. In order to do so, we apply the following set of rules to persons, liable for any violation.
				</li>
				<li>
					You shall release and hold us and our employees, officers, directors, shareholders, agents, representatives, affiliates, subsidiaries, advertising, promotion and fulfillment agencies, any third-party providers or sources of information or data and legal advisers harmless from any and all losses, damages, rights, claims, and actions of any kind arising from or related to the text materials, including but not limited to:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							telephone, electronic, hardware or software, network, Internet, email, or computer malfunctions, failures or difficulties of any kind,
						</li>
						<li>
							failed, incomplete, garbled or delayed computer transmissions,
						</li>
						<li>
							any condition caused by events beyond our control of that may cause the text material to be delayed, disrupted, or corrupted,
						</li>
						<li>
							any injuries, losses or damages of any kind arising in connection with or as a result of utilizing our services, and
						</li>
						<li>
							any printing or typographical errors in any materials associated with our services.
						</li>
					</ol>
				</li>
				<li>
					We will not be liable to You in any of the cases including but not limited to the cases where:
					<ol class="blue-alpha-list margin-l-20 padding-l-20">
						<li>
							You will not understand and remember the text material prepared,
						</li>
						<li>
							You will not achieve the desired result by using the text material prepared,
						</li>
						<li>
							You will misuse the text material prepared,
						</li>
						<li>
							You will suffer any unwanted consequences by using the text material prepared.
						</li>
					</ol>
				</li>
				<li>
					In addition, you agree to defend, indemnify, and hold us and our affiliates harmless from any claim, suit or demand, including attorney's fees, made by a third party due to or arising out of You utilizing of our services, your violation or breach of these Terms of Use, your violation of any rights of a third party, or any other act or omission by You.
				</li>
				<li>
				 	In no event shall we be liable for any direct, indirect, punitive, incidental, special or consequential damages arising out of, or in any way connected with the use of this Service, or any information provided on the Service.
				</li>
				<li>
					We are not liable for any damages arising in contract, tort or otherwise from the use of, or inability to use this site or any material contained in it, or from any action or decision taken as a result of using the Service.
				</li>
				<li>
					Our Service offers links to other sites, thereby enabling You to leave this Service and go directly to the linked site. We are not responsible for the content of any linked site or any link on a linked site. We are not responsible for any transmission received from any linked site.
				</li>
				<li>
					Neither we, nor any of our affiliates and/or partners shall be liable for any unethical, inappropriate, illegal, or otherwise wrongful use of the text materials material received from our Service. This includes plagiarism, lawsuits, loss of positions or remunerations, failure, suspension, or any other disciplinary and legal actions. You are solely responsible for any and all disciplinary or legal actions arising from the improper, unethical, and/or illegal use of the material.
				</li>
				<li>
					We will not be liable for any delays or technical problems in delivery of the text material resulting from any malfunction of any mail-server or any Internet Service Provider.
				</li>
				<li>
					We do not guarantee any particular grade, and You cannot ask for refund in case You were poorly assessed.
				</li>
			</ol>
			<h4>Final provisions</h4>
			<ol class="orange-number-list gradient-for-bottom-text padding-l-r-20">
				<li>
					These Terms of Use form an entire understanding between You and us. Neither You nor we shall rely on any verbal or written information unless it is set out or expressly referred to in these Terms of Use Agreement.
				</li>
				<li>
					Your or our failure to enforce the performance of any provision contained in these Terms of Use will not constitute a waiver of rights to subsequently enforce such provision, or any other provision contained in these Terms of Use.
				</li>
				<li>
					Notices and reports under these Terms of Use shall be considered as read within one day from the moment when the report or notice were sent.
				</li>
				<li>
					Nothing in these Terms of Use shall establish any kind of partnership, employer-employee relationship or joint venture between You and us. Neither You nor we shall be authorized to act as an agent for each other, nor shall You or we enter into any agreement, or contract on behalf of each other as representative or agent.
				</li>
				<li>
					Nothing in these Terms of Use shall initiate any obligations for third parties and neither third party shall have any rights arising from these Terms of Use.
				</li>
				<li>
					Communications between You and us should be accomplished in English. Messages, inquires, requests, applications and/or consents shall be sent by any means of communication.
				</li>
				<li>
					In case any provision of these Terms of Use is found as unenforceable by any appropriate authority, such provision shall be modified, rewritten or interpreted to include as much of its nature and scope as will render it enforceable.
				</li>
				<li>
					The headings above the provisions of these Terms of Use are intended to increase the legibility of these Terms of Use only. The content and meaning of terms placed under a particular heading is, therefore, not limited to the meaning and content of the heading. Headings shall not be considered in interpreting these Terms of Use.
				</li>
				<li>
					All the relations under these Terms of Use shall be governed by the law of the United Kingdom only.
				</li>
				<li>
					This Policy may be changed, amended or modified on our sole and absolute discretion without any notices or warnings.
				</li>
				<li>
					These Terms of Use is an agreement concluded in electronic form. Electronic form of these Terms of Use shall have the same legal force as it was signed in ink.
				</li>
				<li>
					These Terms of Use were last updated on August 27, 2015.
				</li>	
			</ol>
			<!-- <div class="">
			</div> -->
		</div>
	</div>
</div>
@stop