@extends('layouts.base')
@section('content')
<link rel="stylesheet" type="text/css" href="/css/contacts.css">
<link rel="stylesheet" href="css/sideblocks.css">

<div class="mobile-header m-bold-700">
	<p>Contact Us</p>
</div>

<div class="container margin-t-120 m-margin-top-0">
	<center>
	<div class="header-page m-display-none">
		<div class="border"><div><div><div></div></div></div></div>
		<div class="text">Contacts</div>
	</div> 
	</center>
	<div class="left-panel">
		@include("sideblocks.priceBlock") 
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock", ['additionalClass' => true])
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content m-display-none">
		<div class="container-contact">
			<h1 class="margin-t-n">Contact Customer Support Representatives 24/7</h1>
			<div class="margin-t-20 margin-b-20">
				
			</div>

			<div class="button-phone">
	            <div class="buttons">
	                <open-pop-up class="contact-btn call-back" pop-up-id="call-me-back"><span class="btn-icon btn-phone-icon"></span>Call me back</open-pop-up>
	            </div>
	            <div class="buttons">
	                <a href="mailto:support@originalpapersonly.com">
		                <button class="contact-btn chat-now"><span class="btn-icon btn-chat-icon"></span>Chat now</button>
		            </a>
	            </div>
	            <div class="buttons">
	                <a href="skype:support@originalpapersonly.com?chat">
	                	<button class="contact-btn call-skype"> <span class="btn-icon btn-skype-icon"></span> Call us on Skype</button>
                	</a>
	            </div>
	        </div>

			<div class="contacts clear-after">
				<div class="inline-block margin-t-20 margin-r-20 top">
					<div class="phone phone-icon margin-b-20">+1-929-367-2627 &nbsp<span class="f-0p8">(USA Toll-free)</span></div>
					<div class="phone phone-icon margin-b-20">+44-800-088-5728 <span class="f-0p8">(UK Toll-free)</span></div>
				</div>
				<div class="inline-block margin-t-20 margin-l-20 padding-l-20 top">
					<div class="email email-icon margin-b-20">support@originalpapersonly.com</div>
					<div class="email scype-icon margin-b-20">support@originalpapersonly.com</div>
				</div>
			</div>

			<div id="map" class="margin-t-20 margin-b-20"> 
				<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=PnzlZ26ASKVOGqpkE5NpLOfP_HE97Kb9&amp;width=100&#37;&amp;height=100&#37;&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
			</div>
			<div class="contacts">
				<div class="locating">
					504 Pine Song Lane,<br>
					Virginia Beach,<br>
					VA, 23451<br>
				</div>
				<center>
					<div class="shared inline-block middle margin-t-20 padding-b-20">
						<div class="item">
							<div class="text middle">
								Like Us on Facebook for Discounts and Freebies!
							</div>
							<div class="shared-button middle" data-href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FOriginalpapersonly-1763588037231134%2F&ext=1473179829&hash=AeYOZ9qS5dNfJwkL">Share</div>
						</div>



						<div class="item">
							<div class="text middle">
								Follow Us on Twitter for Discounts and Freebies!
							</div>
							<div class="follow-button middle" data-href="https://twitter.com/intent/follow?screen_name=originalpapers1">Follow</div>
						</div>
					</div>
				</center>
			</div>
		</div>
		<center>
			<div class="container-form margin-t-20 f-white padding-t-20 padding-b-20">
				<center>
					<div class="width-500 padding-t-20 padding-b-20">
						<div class="margin-b-20">
							Enter your email to get an immediate bonus, our special offers and exciting updates.
						</div>
						<div class="margin-b-20 bonuses">
							<input type="text" class="input margin-r-20" id="email-bonus" placeholder="Your email" name="email">
							<input type="button" class="common-button" value="Send my bonus" id="send-bonus">
						</div>
						<div class="margin-b-20">
							(Unsubscribe any time with just a click)
						</div>
						<div class="f-0p7">
							By signing up you give your consent to receive promo emails from us.
						</div>
					</div>
				</center>
			</div>
		</center>	
	</div>
	@include('popups.thank-for-subscribe')
</div>
@stop

@section('scripts')
	@parent
	<script>
	    (function(){
			$("#send-bonus").click(function(){
		    	var bonus = $("#email-bonus").val();
		    	var popup = document.querySelector('pop-up[pop-up-id="thank-for-subscribe"]');
		    	$.ajax({
		    		url: "{{url("/subscribe")}}",
		    		type: "GET",
		    		data: {email : bonus},
		    		success: function() { 
		    			validWithServer("", ".container-form", '.bonuses', '.bonuses');
		    			$("#email-bonus").val("");
		    			popup.open();
		    		},
		    		error: function(data) {
		    			if(data.status == 400){
		    				validWithServer(data.responseText, ".container-form", '.bonuses', '.bonuses');
		    			} else if(data.status == 500){
		    				popUpError.open();
		    			}
		    		}
		    	});
	    	});
	    })();
    </script>
@stop