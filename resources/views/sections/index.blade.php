@extends('layouts.base')

@section('content')
	
	<link rel="stylesheet" type="text/css" href="/css/priceCalc.css">
	<link rel="stylesheet" href="css/sideblocks.css">

	<div class="mobile-header m-bold-700">
		<p>Home</p>
	</div>
	<div class="container margin-t-170 m-margin-top-0" >
		<div class="priceCalc " ng-controller="indexCtrl" ng-mousemove="mousemove($event)" ng-mouseup="mouseup()">
			<div class="first-border-price">
				<div class="second-border-price">
					<div class="third-border-price">
						<div class="first-level">
							<div class="first-block">
								<span class="char1">A</span>
								<span class="char2">C</span>
								<span class="char3">A</span>
								<span class="char4">D</span>
								<span class="char5">E</span>
								<span class="char6">M</span>
								<span class="char7">I</span>
								<span class="char8">C</span>
								<span class="char9"> </span>
								<span class="char10">L</span>
								<span class="char11">E</span>
								<span class="char12">V</span>
								<span class="char13">E</span>
								<span class="char14">L</span>
							</div>
							<div class="second-block">
								<span class="char1">D</span>
								<span class="char2">E</span>
								<span class="char3">A</span>
								<span class="char4">D</span>
								<span class="char5">L</span>
								<span class="char6">I</span>
								<span class="char7">N</span>
								<span class="char8">E</span>
							</div>
							<div class="third-block">
								<span class="char1">N</span>
								<span class="char2">U</span>
								<span class="char3">M</span>
								<span class="char4">B</span>
								<span class="char5">E</span>
								<span class="char6">R</span>
								<span class="char7"> </span>
								<span class="char8">O</span>
								<span class="char9">F</span>
								<span class="char10"> </span>
								<span class="char11">P</span>
								<span class="char12">A</span>
								<span class="char13">G</span>
								<span class="char14">E</span>
								<span class="char15">S</span>
							</div>
						</div>
						<div class="second-level">
							<div class="first-block">
								@foreach($levels as $key=>$level)
									<span class="word{{$key+1}}" ng-click="setLevel({{$key+1}}, {{$level->id}})" ng-class="(level == {{$key+1}})?'active':''">
										{{$level->name}}
									</span>
								@endforeach
								<span class="active-b word1 word{$ level $}"></span>
							</div>
							<div class="second-block">
								@foreach($deadlines as $key=>$deadline)
									<span class="word{{$key+1}}" ng-click="setDay('{{$deadline->name}}', {{$key+1}}, {{$deadline->id}})" ng-class="(day == '{{$deadline->name}}')?'active':''">
										{{$deadline->name}}
									</span>
								@endforeach
								<span class="active-b word1 word{$ activeDay $}"></span>
							</div>
							<div class="third-block">
								<span class="word1" ng-click="setPage(0.489957, 1)">0</span>
								<span class="word2" ng-click="setPage(0.617502, 5)">5</span>
								<span class="word3" ng-click="setPage(0.777134, 10)">10</span>
								<span class="word4" ng-click="setPage(0.908182, 15)">15</span>
								<span class="word5" ng-click="setPage(1.03756, 20)">20</span>
								<span class="word6" ng-click="setPage(1.16906, 25)">25</span>
								<span class="word7" ng-click="setPage(1.31135, 30)">30</span>
								<span class="word8" ng-click="setPage(1.44344, 40)">40</span>
								<span class="word9" ng-click="setPage(1.55239, 50)">50</span>
								<span class="word10" ng-click="setPage(1.70418, 60)">60</span>
								<span class="word11" ng-click="setPage(1.83918, 70)">70</span>
								<span class="word12" ng-click="setPage(1.94971, 80)">...</span>
							</div>
						</div>
						<div class="third-level" ng-click="order()">
							<div>click here to</div>
							<div>ORDER NOW</div>
							<div>$11,99 per page</div>
							<div class="hidden" ng-class="(true)?'block':''">${$ (price*page) | number:2 $}</div>
							<div class="first-block">
								<span class="char1">T</span>
								<span class="char2">O</span>
								<span class="char3">T</span>
								<span class="char4">A</span>
								<span class="char5">L</span>
								<span class="char6"> </span>
								<span class="char7">P</span>
								<span class="char8">R</span>
								<span class="char9">I</span>
								<span class="char10">C</span>
								<span class="char11">E</span>
							</div>
							<div class="second-block loading-pulse" ng-show="animation">
								<span class="char1"><span></span></span>
								<span class="char2"><span></span></span>
								<span class="char3"><span></span></span>
							</div>
						</div>
						<div class="second-level-container-button">
							<div class="second-level-button" ng-click="setVisibleInput()" ng-mousedown="mousedown()" ng-bind="page">1</div>
							<input type="number" class="input hidden" ng-show="visibleInput" ng-model="page" ng-value="page" ng-class="(true)?'block':''">
						</div>
					</div>
				</div>
			</div>
			<div class="mobile-header-info ">
				<p class="info-header m-bold-700 m-margin-0">Custom Writing Service You Can Trust</p>
				<p>OriginalPapersOnly.com is a professional research paper, essay, dissertation and thesis writing company designed to serve the needs of college and graduate students through experienced authors and editors.</p>
			</div>
			<div class="mobile-order-now-block">
				<a href="{{url('/order')}}">Order now</a>
			</div>
			<div class="we-offer">
				<div class="we-offer-container-1">
					We deliver 100% plagiarism <span class="f-orange"> FREE </span>  papers
				</div>
				<div class="we-offer-container-2">
					<span class="f-orange">1000+</span> expert degree-holding writers
				</div>
				<div class="we-offer-container-3">
					Affordable prices starting at <span class="f-orange">$11.99</span>
				</div>
				<div class="we-offer-container-4">
					Flexible <span class="f-orange">Discount</span> System
				</div>
				<div class="we-offer-container-5">
					Services available <span class="f-orange">24/7</span>
				</div>
			</div>
		</div>
	</div>
	<div class="container margin-t-20 m-margin-top-0 padding-t-20">
		<div class="left-panel">
			@include("sideblocks.priceBlock")
			{{--mobile blocks--}}
			@include("sections.for-mobile.money-back-block")
			@include("sections.for-mobile.more-info-block")
			{{--end of mobile blocks--}}
			@include("sideblocks.tiredAndStressedBlock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")

		</div>
		<div class="content">
			<div class="container-for-text m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20">
					Online Writing Service You Are Looking For
					<br>
					Paper Writing Services That Meet Your Expectations
				</div>
				<span>
					OriginalPapersonly.com is a company that will provide you with professional research papers, essays, dissertations and thesis written and checked by experienced authors and editors.
				</span>
			</div>
			<div class="container-for-text c-tpt clear-after m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20">
					Reasons Why Our Paper Writing Services Are Better:
				</div>
				<ul class="orange-list">
					<li>
						We offer you professional writers and customer support representatives. There is no place for amateurs in our high-quality paper writing team. 
					</li>
					<li>
						Every client is of special importance, which is why we have evolved reasonable prices we can guarantee that every ‘Help me with my paper!’ call would be heard and answered immediately!
					</li>
					<li>
						Our clients' papers are always written due to the highest standards of language and quality according to our immense experience and professionalism.
					</li>
					<li>
						An assiduity is our main feature - we always eager to achieve more, which is why invariable development is our principal approach to custom papers writing service.
					</li>
					<li>
						Our writers were specially trained to meet clients' instructions.
					</li>
					<li>
						Forget about writing papers that can only bring you a headache just resort to our custom writing service. We will make your academic life easier!
					</li>
					<li>
						All our essays are written from scratch and carefully checked by our professional editors as well as special software for plagiarism.
					</li>
				</ul>
				<div class="t-right right">
					Our service will provide a maximum result for the money you spend, you are welcome to entrust solutions of your academic problems to our professional writers and editors at OriginalPapersonly.com custom writing service.
				</div>
			</div>
			<div class="container-for-text c-tpt margin-b-20 m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20">
					Custom Writing Service You Can Trust
				</div>
				<span>
					Make an order and see for yourself! Writing is not a kind of activity anyone can handle, so why do you have to waste your efforts and precious time for a kind of activity that bring you no enjoyment and desirable result no matter how hard you try? You can leave your paper for our writers and direct your efforts to something more interesting. Our Online Writing Service will make it for you.
				</span>
			</div>
			<div class="container-for-text relative anonymos m-display-none">
				<div class="anonymos-icon absolute"></div>
				<div>
					<div class="padding-b-5">
						<b>Completely Anonymous</b>
					</div>
					<div>
						You can be sure that we have taken all security measures and all personal information that you give us is protected against loss. We won’t use it for any other purposes. Misuse or alteration is inadmissible. In order to learn more please read Privacy and Cookies Policy.
					</div>
				</div>
			</div>
			<div class="container-for-text c-tpt margin-t-20 m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20 margin-t-20">
					We have expert writers in:
				</div>
				<div class="padding-l-20 margin-l-5">
					<ul class="column orange-list hover">
						<li>Accounting</li>
						<li>Anthropology</li>
						<li>Architecture</li>
						<li>Art</li>
						<li>Astronomy</li>
						<li>Biology</li>
						<li>Business</li>
						<li>Chemistry</li>
					</ul>
					<ul class="column orange-list hover">
						<li>Criminology</li>
						<li>Economics</li>
						<li>Education</li>
						<li>English</li>
						<li>Finance</li>
						<li>Geography</li>
						<li>History</li>
						<li>Internet technologies</li>
					</ul>
					<ul class="column orange-list hover">
						<li>Linguistics</li>
						<li>Literature</li>
						<li>Management</li>
						<li>Marketing</li>
						<li>Mathematics</li>
						<li>Medicine</li>
						<li>Music</li>
						<li>Nursing</li>
					</ul>
					<ul class="column orange-list hover">
						<li>Philosophy</li>
						<li>Physics</li>
						<li>Political science</li>
						<li>Psychology</li>
						<li>Religious studies</li>
						<li>Sociology</li>
						<li>Statistics</li>
						<li>Other sciences</li>
					</ul>
				</div>
			</div>
			<div class="container-for-text c-tpt m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20 ">
					You Can Resort to Our Online Writing Service When
				</div>
				<ul class="orange-list">
					<li>
						You cannot find an answer to a really bothering question like "Who can help me to write my paper?"
					</li>
					<li>
						You know that writing won’t make any use for your future job placement.
					</li>
					<li>
						You have a lot of more important tasks that need your immediate response and writing task will certainly take too much precious time.
					</li>
					<li>
						You are looking for a professional help to answer your ‘Help me with my paper!’ call.
					</li>
					<li>
						You want to receive the best quality of your paper and you seek for online writing service with professional writers.
					</li>
					<li>
						You don’t want to spend your money in vain but to insert them wisely for a paper that will get the best mark you could expect.
					</li>
				</ul>
			 	Online Writing Service OriginalPapersonly.com will provide you with a paper of the best quality possible. Our editors will check your paper several times after writers and before you will receive it.  This service will make sure that the paper you receive answers all needed standards. We hire only the best writers as well as editors that have profound knowledge of English and wide experience in various subjects we can offer you help with. We have done everything to make your collaboration with OriginalPapersonly.com pleasant, enjoyable, and as productive as possible. 
			</div>
			<div class="container-for-text c-tpt m-display-none">
				<div class="f-1p7 f-blue f-BritannicBold margin-b-20">
					We Provide You Only With High-Quality Service
				</div>
				<div class="clear-after">
					<div class="half-width left">
						<div class="padding-b-20 padding-r-20">
							<b>You don't cheat when you use our online writing service OriginalPapersonly.com. Want to know why?</b>
						</div>
					</div>
					<div class="half-width left">
						<div class="padding-l-20">
							<b>This is our response for your ‘Help me with my paper!’ call</b>
						</div>
					</div>
				</div>
				<div class="clear-after">	
					<div class="half-width left">
						<div class="padding-r-20">
							Our main task is to help you to improve your writing skills. Our online writing service has no desire to deceive and disappoint your professors. We write papers with your help and you will see this. Only with our service, you will be able to learn how formatting and structuring rules. You can take our paper as the example of your own one. You don't need to pass a completed paper as one of yours.
						</div>
					</div>
					<div class="half-width left">
						<div class="padding-l-20">
							Our online writing service will definitely help you with every single question you can possibly have concerning your academic writing tasks. You can rely on us anytime you need; we've already made thousands of students' lives easier. We have many years of online writing services experience. What is more important is that we have no database of our papers in order to avoid any possibility of disclosure of information. That is why we won't trick you with already pre-written works. OriginalPapersonly.com online writing service is worth to be trusted.
						</div>
					</div>
				</div>
			</div>
			<div class="container-for-text c-tpt">
				<div class="container-comments">
					<div class="f-1p7 f-blue f-BritannicBold margin-b-20 m-display-none">
						Testimonials	
					</div>
					<div class="mobile-block-header">
						Testimonials
					</div>
					<hr class="m-horizontal-line">
					<div class="comment">
						<div class="author">Steve, NJ</div>
						<div class="base-block">
							<div class="rating" data-ammount="5"></div>
							<div class="content">
								I usually don't use custom writing paper service like this, but I was in a pinch. You guys are great! All my questions were answered in a timely manner and now I know what to do if any problems with academic writing arise - ask you for help! Thanks a bunch, you saved my neck!!!
							</div>
						</div>
					</div>
					<div class="comment">
						<div class="author">Paul, CO</div>
						<div class="base-block">
							<div class="rating" data-ammount="4"></div>
							<div class="content">
								I believe that your work is exceptional and I highly appreciate your assistance in writing my essay. Now it will certainly meet the expectations of my professor!
							</div>
						</div>
					</div>
					<div class="comment m-display-none">
						<div class="author">Peter, NM</div>
						<div class="base-block">
							<div class="rating" data-ammount="5"></div>
							<div class="content">
								I have used other services before, but OriginalPapersOnly.com is the best one yet. The writer (David) met my needs and was very professional. I have recommended your writing services to my friends. They have won me over as a customer. I was amazed at the customer service that I received and again the writer is the best that I had. Thank you for everything, and I look forward to working with you again. You are college lifesavers.
							</div>
						</div>
					</div>
				</div>
				<div class="clear-after margin-t-20 margin-r-20">
					<a class="right f-gray-b" href="{{ url('/testimonials') }}">View all Testimonials</a>
				</div>
			</div>
			<div class="padding-20 margin-20"></div>
		</div>
	</div>
@stop
@section('scripts')
	@parent
	<script type="text/javascript">
		$(function(){
			function setRaiting(){
				$('.rating').each(function(index){
					var ammount = $(this).attr('data-ammount');
					var constWidth = ($(document).width()<1169) ? 36 : 18;
					$(this).css('width', ammount*constWidth);

				})
			}
			setRaiting();
			window.onresize = function(){setRaiting()};
		})
        app.controller('indexCtrl', function ($scope) {
			$scope.deadlines = {!! $deadlines !!};
			$scope.levels = {!! $levels !!};
			$scope.mtx = {!! $mtx !!};
        	$scope.level = 1;
        	$scope.levelId = $scope.levels[0].id;
        	$scope.day = '14 Days';
			$scope.deadlineId = $scope.deadlines[0].id;
        	$scope.activeDay = 1;
			$scope.page = 1;
        	$scope.visibleInput = 0;
        	$scope.price = 11.99;
        	$scope.animation = 1;

			var vars = 'level + activeDay';
			$scope.$watch(vars, function() {
				$scope.price = $scope.mtx[$scope.level-1][$scope.activeDay-1];
			}, true);

			$scope.$watch('page', function(nextVal, prevVal) {
				if(nextVal>999){
					$scope.page = 999;
				}
			}, true);

			$scope.order = function() {
				var data = {
					'academic_level': $scope.levelId,
					'deadline': $scope.deadlineId,
					'pages': $scope.page,
				};
				var params = jQuery.param(data);
				document.location.href = '{{ route('order') }}'+'?'+params;
			};

        	$scope.setDay = function(day, index, id){ $scope.day = day; $scope.activeDay = index; $scope.deadlineId = id;}
        	$scope.setLevel = function(level, id){ $scope.level = level; $scope.levelId = id;}
        	$scope.setPage = function(rad, amount){ 
        		if(amount>70){ $scope.visibleInput=1; }
        		else{ $scope.visibleInput=0; }
        		$scope.page = amount;
        		$scope.animation = 0;
        		$('.second-level-container-button').css({'transform':'rotate('+rad+'rad)'}); 
        		$('.second-level-button').css({'transform':'rotate('+(rad*-(1))+'rad)'});
        		$('.second-level-container-button .input').css({'transform':'rotate('+(rad*-(1))+'rad)'});  
        	}
        	$scope.setVisibleInput = function(){ $scope.visibleInput=!$scope.visibleInput; }
        	
        	$scope.priceCalc = {
        		down : false,
    			left : ($(window).width()-1170)/2+444,
        		ABx : 100,
        		ABy : 0,
        	};

			$scope.mousedown = function() {
			    $scope.priceCalc.down = true;
			    $(".second-level-button").blur(); 
			};
			$scope.mouseup = function() {
			    $scope.priceCalc.down = false;
			    $('body').removeClass('user-select-none');
			}

        	$scope.mousemove = function(event){ 
        		if(!$scope.priceCalc.down) return; 
        		$scope.animation = 0;
        		$('body').addClass('user-select-none');
				var CDx = event.pageX-$scope.priceCalc.left;
				var CDy = event.pageY-390;
				
				var mABCD = Math.sqrt(Math.pow($scope.priceCalc.ABx,2) + Math.pow($scope.priceCalc.ABy,2)) * Math.sqrt(CDx*CDx + CDy*CDy);
				var ABCD = ($scope.priceCalc.ABx*CDx) + ($scope.priceCalc.ABy*CDy);
				
				var rad = Math.asin(ABCD/mABCD);
				if(event.pageY>390){
					rad -= 1.57;
					rad *= -1;
					rad += 1.57;
				}
				if(rad > 0.489957 && rad < 1.98 ){
					var onePageInRad = 0.02737977;
					var dif = 0.462577;
					$('.second-level-container-button .input').blur();
					if(rad > 1.31136){
						dif = 0.89141075;
						onePageInRad = 0.01319575;
					}
					if(rad < 1.825984){
						$scope.visibleInput = 0;
						$scope.page = parseInt((rad - dif) / onePageInRad);
					}else{
						rad = 1.94971;
						$scope.page = 70;
						$scope.visibleInput = 1;
						$('.second-level-container-button .input').focus();
					}
					$('.second-level-container-button').css({'transform':'rotate('+rad+'rad)'});
					$('.second-level-button').css({'transform':'rotate('+(rad*-(1))+'rad)'}); 
					$('.second-level-container-button .input').css({'transform':'rotate('+(rad*-(1))+'rad)'}); 
				} 
        	}
        });
	</script>
@stop
