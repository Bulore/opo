@extends('layouts.base')
@section('content')
	<link rel="stylesheet" type="text/css" href="/css/payment-auth.css">
	<link rel="stylesheet" href="css/sideblocks.css">
	
	<div class="mobile-header m-bold-700">
		<p>Authorization</p>
	</div>
	
	<div class="container margin-t-120 m-margin-top-0">
		<center>
			<div class="header-page m-display-none">
				<div class="border"><div><div><div></div></div></div></div>
				<div class="text">Authorization</div>
			</div>
		</center>
		<div class="left-panel m-display-none">
			@include("sideblocks.priceBlock")
			@include("sideblocks.formatblock")
			@include("sideblocks.contactblock")
			@include("sideblocks.moneyBack")
			@include("sideblocks.acceptblock")
			@include("sideblocks.protectedblock")
			@include("sideblocks.servicesblock")
		</div>
		<div class="content">     
			<div class="guide-container">
				<div class="guide-row">
					<div>
						<img src="{{ url('/images/payment-auth/guide-1.jpg') }}" alt="">
					</div>
					<div> <div>1</div> </div>
					<div>
						<div>
							<h1>Take your credit card and identity document</h1>
							<div>
								An identity document is any document that can authorize your personality, e.g. a driving license, passport, library card, student card, etc.
							</div>
						</div>	
					</div>
				</div>
				<div class="guide-row">
					<div>
						<div>
							<h1>Hide all unnecessary information</h1>
							<div>
								All we need to see on your credit card are the last 4 digits (to compare them with the information from the payment processor), the Cardholder's name, the issuing bank, and the type of the card; as for the ID, we require your name and country only.
							</div>
						</div>
					</div>
					<div> <div>2</div> </div>
					<div>
						<img src="{{ url('/images/payment-auth/guide-2.jpg') }}" alt="">
					</div>
				</div>
				<div class="guide-row">
					<div>
						<img src="{{ url('/images/payment-auth/guide-3.jpg') }}" alt="">
					</div>
					<div> <div>3</div> </div>
					<div>
						<div>
							<h1>Take a photo or make a digital copy of the document</h1>
							<div>
								Use your phone, camera or scanner.
							</div>
						</div>
					</div>
				</div>
				<div class="guide-row">
					<div>
						<div>
							<h1>Send the pictures</h1>
							<div>
								Send the copies to our email, or send a fax, or upload them directly to your control panel by following the link: http://admin.writemypapers.org/authorization/
								<br> <br>
								Fax: 1-800-776-46-43
								support@writemypapers.org
							</div>
						</div>
					</div>
					<div> <div>4</div> </div>
					<div>
						<img src="{{ url('/images/payment-auth/guide-4.jpg') }}" alt="">
					</div>
				</div>
				<div class="guide-row">
					<div></div>
					<div> <div>5</div> </div>
					<div></div>
				</div>
				<center> <h1 class="margin-b-n">That's it. Your payment is now authorized!</h1> </center>
			</div>
			
			<h1 class="margin-l-35">Frequently Asked Questions:</h1>
			
			<drop-down-ordered-list data-open-only-one class="none-order">
				<ordered-list-item>
					<ordered-list-header> What is account and payment authorization? </ordered-list-header>
					<ordered-list-content> 
						Account authorization is the process of validating all the information that you are submitting to our website to establish yourself as the rightful owner of the account and the actual cardholder. OriginalPapersOnly.com may ask a Customer to authorize their account and the provided information to protect the buyer, his/her personal data and the payment they are submitting from any kind of online fraud or identity theft. The authorization procedure has been adopted to eliminate the smallest possibility of online financial crime and to protect the Customers’ data from unauthorized usage.
					</ordered-list-content>
				</ordered-list-item>
				
				<ordered-list-item>
					<ordered-list-header> Why should I authorize my account and payment? </ordered-list-header>
					<ordered-list-content>
						All our Customers are suggested to authorize their accounts to make sure that no other person will use them for any purposes without the actual Account Holder’s permission. If you authorize your account, you confirm that your contact details are valid and that we must treat your requests with extra attention. Once your account is authorize, you can be 100% sure that all your personal data submitted to our website has been seen by you and a representative of our Risk department only. We do not store or share your data with any third parties. On the contrary, we protect it after it is authorize.
					</ordered-list-content>
				</ordered-list-item>
				
				<ordered-list-item>
					<ordered-list-header> What documents should I provide? </ordered-list-header>
					<ordered-list-content> 
						We need you to send us the pictures or scans of the credit card, which was used for the order payment, and any document that may confirm your identity, e.g. a passport, driving license, library card, student identity card or any other document that might include your full name, a portrait photo, age, birth date, address, and citizenship status, though we do not need all of this.
					</ordered-list-content>
				</ordered-list-item>
				
				<ordered-list-item>
					<ordered-list-header> What information do you need to see on my credit card? </ordered-list-header>
					<ordered-list-content>
						All we require are the last four digits on your card, your full name on it, the type of the credit card, and the issuing bank. The full card number together with the CVV code and the expiry date is irrelevant to us. We will never ask you to provide this information under any circumstances. Please use a pencil, your finger, or a piece of paper to hide these details while taking a picture of the card. 
					</ordered-list-content>
				</ordered-list-item>
					
				<ordered-list-item>
					<ordered-list-header>
						What information do you need to see on my identity document?
					</ordered-list-header>
					<ordered-list-content>
							We do not need all the details from your ID. We just have to see your full name and the address there. We are absolutely not interested in any other details and continuously ask our customers to blackout all other details that are irrelevant to us to avoid any possible misunderstandings.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						What will you do with the information from the documents?
					</ordered-list-header>
					<ordered-list-content>
							We will authorize that the provided data matches the information about your transaction from the payment system.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						How will you know I provided the correct details?
					</ordered-list-header>
					<ordered-list-content>
							The payment processor always shares transaction details with us. Our Risk Department is aware of the main details of any payment. They are: the last 4 digits of the credit card, the full name of the cardholder, the type of credit card, the issuing bank, the cardholder’s billing address and his/her current residence.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						How do you check the information?
					</ordered-list-header>
					<ordered-list-content>
							The provided documents are reviewed by the Risk Department of our company. They compare the billing information from the payment processor with the information on the copies of the credit card and identity document. If everything matches, in most cases it will prove that payment is received from the rightful owner of the credit card and that it was authorized by him. Otherwise, it can mean that the payer does not have all the documents to confirm the validity of their payment, so other methods of authorization may be implemented.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						Can I use someone else's card if the cardholder allows me to do that?
					</ordered-list-header>
					<ordered-list-content>
							That’s absolutely fine, but we still need to make sure that the cardholder is aware of the transaction. You may ask him to provide the above-mentioned documents with hidden/blacked-out unnecessary information and forward them to us. We may also ask you about the cardholder's contact number to authorize your payment over the phone.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						What if I am not feeling comfortable sharing my documents over the Internet?
					</ordered-list-header>
					<ordered-list-content>
							We authorize the documents primarily for your financial safety. The requested information is visible for us anyway, and we just need to check it with the one on your documents. While authorizing your credit card along with the ID, we can see that this is definitely you who paid, not someone else who might have stolen your credit card information and tried to make a payment on our website. All suspicious transactions are reported to the Internet Fraud Investigating Bureau, the stolen credit card gets blocked, and the money remains safe.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						Is it safe to provide my personal data to OriginalPapersOnly.com?
					</ordered-list-header>
					<ordered-list-content>
							It is absolutely safe to do so. We do not store your documents. Once uploaded to our website, only one person can see them. A representative of our Risk Department checks them for validity and concurrency with the data you have provided and immediately deletes them permanently. It takes no more than 16 hours for the Risk Department to check those documents and delete them from our servers. You can be 100% sure that we will never disclose the information you have provided to us to any third parties. Our company’s target is to run the business honestly. Each and every Customer as well as his/her information is protected, and your safety and anonymity are our TOP priority.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						Is authorization really necessary?
					</ordered-list-header>
					<ordered-list-content>
							Online sales and purchases are always a risk. We try to eliminate the slightest possibility of the known and wide-spread credit card-not-present fraud and identity theft. The consequences and losses are deplorable for both the seller and the buyer. You are able to report an unauthorized usage of your credit card to our Risk department, get you money back and help us catch the bad guys.
					</ordered-list-content>
				</ordered-list-item>
				<ordered-list-item>
					<ordered-list-header>
						OK. I understand, I am now ready to authorize my payment, how do I do that?
					</ordered-list-header>
					<ordered-list-content>
							Several ways of providing the documents are at your disposal: email, fax or upload file in the section Authorization in your account. The process is easier than it seems: to see the tutorial, just scroll to the top.
					</ordered-list-content>
				</ordered-list-item>
			</drop-down-ordered-list>
			<div class="left-panel d-display-none">
				@include("sideblocks.priceBlock")
			</div>
		</div>
	</div>
@stop