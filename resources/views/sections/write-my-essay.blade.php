@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="/css/sideblocks.css">
    <link rel="stylesheet" href="/css/write-my.css">
    <div class="container margin-t-120">
        <center>
            <div class="header-page">
                <div class="border"><div><div><div></div></div></div></div>
                <div class="text">Can You Write My Essay for Me?</div>
            </div>
        </center>
        <div class="left-panel">
            @include("sideblocks.priceBlock")
            @include("sideblocks.formatblock")
            @include("sideblocks.contactblock")
            @include("sideblocks.moneyBack")
            @include("sideblocks.acceptblock")
            @include("sideblocks.protectedblock")
            @include("sideblocks.servicesblock")
        </div>
        <div class="content">
            <div class="container-for-text">
                <p class="f-w-700 color-blue"><i>- written for us by Melissa C., a college student</i></p>
                <p>When it comes to writing an essay, every student starts feeling nervous and wonders:</p>
                <p class="margin-n">"Who can write my college essay?"</p>
                <p class="margin-n">"Where can I find someone who will agree to write my essay for free?"</p>
                <p class="margin-n">"Is there anybody who has enough knowledge to write my essay for me?"</p>
                <p class="margin-n">"Can somebody do my essay for a fee that a student's budget can cover?"</p>
                <p class="margin-n">And I am no exception. You know why? The thing is that this essay will show whether I have learnt the material and I am afraid to fail and lose my reputation as a diligent student.</p>
                <p class="color-blue f-w-900">What Is Typical Student Behavior?</p>
                <p class="margin-n">Those students who have never been good at essay writing desperately look for someone who can either help them write it, or write it for them. If their search fails, they just get a failing grade which is usually an indicator that the student didn't get help. Getting unqualified help that will produce mistakes and fail to meet a deadline is no better.</p>
                <p>After one bad experience I began to look for someone to write my essay. I wanted to find the most reliable company with experts who could write my essay for me. I sent a lot of letters with "can you write my essay?" requests. But none of the "can you write my essay" letters were ever answered. I needed real professionals, highly qualified and able to do my essay without any mistakes. Luckily, I stumbled upon OriginalPapersOnly.com and finally found somebody who could do my essay for me!</p>
            </div>
        </div>
    </div>
@stop