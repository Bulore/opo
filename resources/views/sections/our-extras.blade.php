@extends('layouts.base')
@section('content')
<link rel="stylesheet" type="text/css" href="/css/extras.css">
<link rel="stylesheet" href="css/sideblocks.css">
<div class="container margin-t-120">
	<center>
	<div class="header-page">
		<div class="border"><div><div><div></div></div></div></div>
		<div class="text">Extras</div>
	</div>
	</center>
	<div class="left-panel">
		@include("sideblocks.priceBlock")
		@include("sideblocks.formatblock")
		@include("sideblocks.contactblock")
		@include("sideblocks.moneyBack")
		@include("sideblocks.acceptblock")
		@include("sideblocks.protectedblock")
		@include("sideblocks.servicesblock")
	</div>
	<div class="content">
		<div class="container-for-text c-tpt">
			<h1 class="padding-n margin-n">ENJOY THE ADVANTAGES OF OUR EXTRAS</h1>
		</div>	
		<div class="container-for-text blue blue-block-background-1">
			<h1 class="f-white padding-n">VIP customer service</h1>
			<p class="f-white">
				Everyone has the right to be a VIP customer! We introduced this extra for you to feel the benefits of premium support features. You will be able to get instant information in case the writer needs any additional files from you. You will generally have more control over your order – the project updates will let you know what’s going on with your paper at any given moment via instant text messages. If you have any problems or questions, our Customer Support Team will make them their TOP priority!
			</p>
		</div>
		<div class="container-for-text c-tpt">
			<div class="container-comments">
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Tracey, Perth
					</div>
					<div class="base-block">
						<div class="content">
							I was so excited when I found out that there is this cool place to help you out with boring old homework, and even more thrilled when I found out how awesome their VIP customer service is. I got the best essay writer to write for me without extra delay and they even kept me updated the whole time with emails and messages.
						</div>
					</div>
				</div>
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Martha, Sydney
					</div>
					<div class="base-block">
						<div class="content">
							Customer service that is truly meant to take care of you is provided in the VIP customer service option. I love it because they really make sure that your essay is given immediate attention by the writer and they always send you latest updates on how it is going. I have never been disappointed.
						</div>
					</div>
				</div>
				<div class="clear-after">
					<div class="right padding-r-20 padding-t-20">
						<a href="">View all Testimonials</a>
					</div>
				</div>
				<center>
					<button class="common-button margin-b-20">Add to order</button>
				</center>
			</div>
		</div>
		<div class="container-for-text blue  blue-block-background-2">
			<h1 class="padding-n f-white">Preferred writer</h1>
			<p class="f-white">
				It is an open secret that the quality of writing is directly proportional to the writer’s talent, skills, and experience. Although we hire only the best writers, this extra enables you to have only the best of the best authors working on your paper! Try out our premium “TOP writer” or reasonably priced “Advanced regular writer” features and feel the difference! For those who have ordered texts from our writers before and enjoyed the quality, we also offer the “My previous writer” feature.
			</p>
		</div>
		<div class="container-for-text c-tpt">
			<div class="container-comments">
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Chloe, Dublin
					</div>
					<div class="base-block">
						<div class="content">
							Having a preferred writer to write your essays is sensible. It’s like having a perfume that becomes your own thing LOL! That way you have a specific voice and characteristics in your essays that are consistent and I think professors really look for that even though they will never say it! Always check this option for the best grades!
						</div>
					</div>
				</div>
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Frank, Geleen
					</div>
					<div class="base-block">
						<div class="content">
							There are some of these star essay writers here and at first I thought that I can live without them but once I checked the Preferred Writers option because I noticed it does not cost too much extra. And what a difference it made! It was the best essay I had ever submitted for evaluation, and since then I have never looked back. I only get ace grades now!
						</div>
					</div>
				</div>
				<div class="clear-after">
					<div class="right padding-r-20 padding-t-20">
						<a href="">View all Testimonials</a>
					</div>
				</div>
				<center>
					<button class="common-button margin-b-20">Add to order</button>
				</center>
			</div>
		</div>
		<div class="container-for-text blue f-white  blue-block-background-3">
			<h1 class="padding-n f-white">Plagiarism report</h1>
			<p class="f-white">
				Plagiarism is a common problem of modern academic writing. To be on the safe side, we cooperate with WebCheck, one of the best anti-plagiarism services online. Order this extra, and you’ll have an official proof that your paper is 100% originality. Get top anti-plagiarism safety for a reasonable price:
			</p>
			<p class="f-white">
				Double-spaced pages: 1-10 pages - $9.99, 11 and more pages - $10.99 plus $1 for each additional page;
			</p>
			<p class="f-white">
				Single-spaced pages: 1-5 pages - $9.99, 6 and more pages - $11.99 plus $2 for each additional page.
			</p>
		</div>
		<div class="container-for-text c-tpt">
			<div class="container-comments">
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Ian, Edinburgh
					</div>
					<div class="base-block">
						<div class="content">
							Plagiarism is just like this huge fishing net that is waiting to catch unsuspecting students in its spread. And I always take the precaution of ordering this report on any writing that I have to send in, and that is my secret for never having to lose a point for plagiarism. It always works!
						</div>
					</div>
				</div>
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Pam, Glasgow
					</div>
					<div class="base-block">
						<div class="content">
							This is one option that every student needs no matter how bright and hardworking he is! You can never be too careful about avoiding plagiarism, so it’s better to double check your paper!
						</div>
					</div>
				</div>
				<div class="clear-after">
					<div class="right padding-r-20 padding-t-20">
						<a href="">View all Testimonials</a>
					</div>
				</div>
				<center>
					<button class="common-button margin-b-20">Add to order</button>
				</center>
			</div>
		</div>
		<div class="container-for-text blue f-white  blue-block-background-4">
			<h1 class="padding-n f-white">Abstract page</h1>
			<p class="f-white">
				Writing a good Abstract, or a proper summary of your paper, can be more difficult than writing the paper itself. You can work your way through multipage manuals on the web and spend hours experimenting, or just rely on our writers’ skills and expertise, especially if you need it for publication or if it is one of the requirements of your writing style. For a small additional fee you will get a summary of your paper that will exceed everyone's expectations!
			</p>
		</div>
		<div class="container-for-text c-tpt">
			<div class="container-comments">
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Ian, Edinburgh
					</div>
					<div class="base-block">
						<div class="content">
							Plagiarism is just like this huge fishing net that is waiting to catch unsuspecting students in its spread. And I always take the precaution of ordering this report on any writing that I have to send in, and that is my secret for never having to lose a point for plagiarism. It always works!
						</div>
					</div>
				</div>
				<div class="comment">
					<div class="author">
						<div class="author-icon"></div>
						Pam, Glasgow
					</div>
					<div class="base-block">
						<div class="content">
							This is one option that every student needs no matter how bright and hardworking he is! You can never be too careful about avoiding plagiarism, so it’s better to double check your paper!
						</div>
					</div>
				</div>
				<div class="clear-after">
					<div class="right padding-r-20 padding-t-20">
						<a href="">View all Testimonials</a>
					</div>
				</div>
				<center>
					<button class="common-button">Add to order</button>
				</center>
			</div>
		</div>
		<div class="container-for-text c-tpt padding-t-n padding-b-n">
			<h4 class="margin-t-n">Sources used</h4>
			<p class="f-blue">
				When you need the materials used as the sources in your paper, select this extra. Once chosen, you will get a soft copy of these files. Available in .doc/.docx/.pdf formats or as print screens / images of the pages used. You can download the sources on your order page after your paper is ready.
			</p>
		</div>
		<div class="gradient-for-bottom-text margin-n">
			<h4>Table of contents</h4>
			<p class="f-blue">
				Nothing creates a better first impression of your paper than a great looking table of contents. Our writers will make sure you succeed. Find this extra in the order details of your control panel.
			</p>
			<h4>Editor's Check</h4>
			<p class="f-blue">
				Let us give your papers even more quality with this helpful extra. You will receive your paper only once our expert editor reads it after the writer. You can add this extra on your order page. The price for this additional check will be calculated automatically.
			</p>
			<div class="padding-b-20 flex-justify-space-around flex-wrap not-work-flex max-full-width">
				<div class="orange-block-over margin-r-35">
					<div class="orange-block">
						<div class="content block">
							<h4 class="margin-n">YOU SAVE:</h4>
							<div class="clear-after padding-t-5">
								<div class="left f-left">
									<div class="arrow relative padding-l-20 margin-5">FREE email delivery</div>
									<div class="arrow relative padding-l-20 margin-5">FREE reference page</div>
									<div class="arrow relative padding-l-20 margin-5">FREE formatting</div>
									<div class="arrow relative padding-l-20 margin-5">FREE FREE title page</div>
									<div class="arrow relative padding-l-20 margin-5">FREE revisions</div>
									<div class="arrow relative padding-l-20 margin-5">FREE plagiarism check</div>
									<div class="padding-l-20 margin-5  padding-t-5"><b>Total savings:</b></div>
								</div>
								<div class="right f-right">
									<div class="margin-5"><b>$3</b></div>
									<div class="margin-5"><b>$6</b></div>
									<div class="margin-5"><b>$5</b></div>
									<div class="margin-5"><b>$5</b></div>
									<div class="margin-5"><b>from $6/page</b></div>
									<div class="margin-5"><b>$11</b></div>
									<div class="margin-5 padding-t-5"><b>from $34</b></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="orange-block-over margin-r-n">
					<div class="orange-block">
						<div class="content block">
							<h4 class="margin-n">YOUR DISCOUNTS:</h4>
							<div class="clear-after padding-t-5">
								<div class="left f-left">
									<div class="arrow relative padding-l-20 margin-5">If your order price is above $1000</div>
									<div class="arrow relative padding-l-20 margin-5">If your order price is above $500</div>
								</div>
								<div class="right f-right">
									<div class="margin-5"><b>10%</b></div>
									<div class="margin-5"><b>5%</b></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop