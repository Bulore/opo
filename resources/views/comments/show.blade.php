@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Comment {{ $comment->id }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID.</th><td>{{ $comment->id }}</td>
                </tr>
                <tr><th> {{ trans('comments.subject') }} </th><td> {{ $comment->subject }} </td></tr><tr><th> {{ trans('comments.pages') }} </th><td> {{ $comment->pages }} </td></tr><tr><th> {{ trans('comments.stars') }} </th><td> {{ $comment->stars }} </td></tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <a href="{{ url('comments/' . $comment->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Comment"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['comments', $comment->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Comment',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
@endsection