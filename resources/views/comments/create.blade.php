@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Create New Comment</h1>
    <hr/>

    {!! Form::open(['url' => '/comments', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                {!! Form::label('subject', trans('comments.subject'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('subject', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('pages') ? 'has-error' : ''}}">
                {!! Form::label('pages', trans('comments.pages'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('pages', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('pages', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('stars') ? 'has-error' : ''}}">
                {!! Form::label('stars', trans('comments.stars'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('stars', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('stars', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('text') ? 'has-error' : ''}}">
                {!! Form::label('text', trans('comments.text'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('text', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', trans('comments.name'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('from') ? 'has-error' : ''}}">
                {!! Form::label('from', trans('comments.from'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('from', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('from', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection