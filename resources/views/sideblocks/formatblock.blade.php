

<div class="left-block m-display-none">
    <div class="left-panel-header">
        <span class="label6"></span>
        <span>Paper Format</span>
            <span>
                  <div class="left-panel-arrow"></div>
            </span>
    </div>
    <div class="left-panel-body">
        <div class="inner-container">
            <div class="format">
                <ul class="orange-list">
                    <li>Double-spaced (275 words per page)</li>
                    <li>Single-spaced (550 words per page)</li>
                    <li>Optimal font (e.g. Arial) 12 pt</li>
                    <li>1 inch margins on all sides</li>
                </ul>
            </div>
        </div>
    </div>
</div>

