<div class="left-block m-display-none"  ng-controller="protectedCtrl">
    <div class="left-panel-header">
        <span class="label4"></span>
        <span>Protected</span>
        <span>
              <div class="left-panel-arrow"></div>
        </span>
    </div>
    <div class="left-panel-body">
        <div class="inner-container">
            <div class="protected">
                <span></span>
                <span></span>
                <span>
                    <p>100%</p>
                    <p>Plagiarism</p>
                    <p>FREE</p>
                </span>
                <span>
                    <p>Checked: {$ curDate | date:'dd MMM yyyy' $}</p>
                    <a href="{{url('/why-not-turnitin')}}">Why not Turnitin?</a>
                </span>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    @parent
    <script type="text/javascript">
        app.controller('protectedCtrl', function ($scope) { $scope.curDate = new Date(); });
    </script>
@stop