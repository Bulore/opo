<div class="left-block m-display-none">
    <div class="left-panel-header">
        <span class="label5"></span>
        <span>Our services</span>
        <span>
            <div class="left-panel-arrow"></div>
        </span>
    </div>
    <div class="left-panel-body">
        <div class="inner-container">
            <div class="services">
            <?php if(!isset($indexServise)) $indexServise = -1; ?>
                <form action="">
                    <label>
                        <a class="radio-service" href="{{url("/essay-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 1) checked @endif >
                            <div>Essay writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/report-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 2) checked @endif >
                            <div>Report writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/speech-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 3) checked @endif >
                            <div>Speech writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/dissertation-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 4) checked @endif >
                            <div>Dissertation writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/thesis-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 5) checked @endif >
                            <div>Thesis writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/research-paper-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 6) checked @endif >
                            <div>Research paper writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/term-paper-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 7) checked @endif >
                            <div>Term paper writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/assignment-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 8) checked @endif >
                            <div>Assignment writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/homework-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 9) checked @endif >
                            <div>Homework writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/personal-statement")}}"> 
                            <input type="radio" name="service" @if($indexServise == 10) checked @endif >
                            <div>Personal statement writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/cause-work-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 11) checked @endif >
                            <div>Coursework writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/academic-paper-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 12) checked @endif >
                            <div>Academic paper writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/college-paper-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 13) checked @endif >
                            <div>College paper writing</div>
                        </a>
                    </label>
                    <label>
                        <a class="radio-service" href="{{url("/article-writing")}}"> 
                            <input type="radio" name="service" @if($indexServise == 14) checked @endif >
                            <div>Article writing</div>
                        </a>
                    </label>
                    {{-- <label>
                        <a class="radio-service" href="{{url("/writing-tips")}}"> 
                            <input type="radio" name="service" @if($indexServise == 15) checked @endif >
                            <div>Writing tips</div>
                        </a>
                    </label> --}}
                </form>
            </div>
        </div>
    </div>
</div>