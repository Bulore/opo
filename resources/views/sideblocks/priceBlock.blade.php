<div class="left-block @if(isset($helperClass)) {{$helperClass}} @endif" @if(isset($prefix)) ng-controller="{{$prefix}}priceCalcCtrl" @else ng-controller="priceCalcCtrl" @endif >
    <div class="m-padding-20px m-backgroundcolor-white">
        <div class="left-panel-header m-display-none">
            <span class="label1"></span>
            <span>Get a price</span>
            <span>
                  <div class="left-panel-arrow"></div>
            </span>
        </div>

        <div class="left-panel-body">
            <div class="mobile-block-header">
                Get a price
            </div>
            <hr class="m-horizontal-line">
            <div class="inner-container" @if(isset($prefix)) id="{{$prefix}}get-a-price-block" @else id="get-a-price-block" @endif>
                <div class="input-select">
                    <p>Type of paper:</p>
                    <select-box required placeholder="Please select" data-error-message="Please select type of paper">
                        <select-box-header></select-box-header>
                        <select-box-content>
                            @foreach($types->indexByCategory() as $category => $arr)
                            <select-box-option disable>{{$category}}</select-box-option>
                                @foreach($arr as $item)
                                    <select-box-option ng-click="typeId = {{ $item->id }}">{{$item->name}}</select-box-option>
                                @endforeach
                            @endforeach
                        </select-box-content>
                    </select-box>
                </div>
                <div class="input-select">
                    <p>Academic level:</p>
                   <div class="academic-level flex flex-children full-width f-0p9">
                        @foreach($academicLevels as $k=>$level)
                            <div ng-click="academicLevel = {{$level->id}}" ng-class="(academicLevel == {{$level->id}})?'active':''" class="flex-2 @if($k == 0) f-0p9 @endif" >
                                {{$level->name}}
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="input-select">
                    <p>Deadline:</p>
                    
                    <select-box required placeholder="Please select" data-error-message="Please select deadline">
                        <select-box-header></select-box-header>
                        <select-box-content>
                            @for($i=0; $i<count($deadlines); $i++)
                                <select-box-option ng-click="deadline = {{ $deadlines[$i]->id }}">{{ $deadlines[$i]->name }}</select-box-option>
                            @endfor
                        </select-box-content>
                    </select-box>
                </div>
                <div class="input-select">
                    <p>Pages:</p>
                    <div class="pages-words">
                        <div class="academic-level pages middle half-width">
                            <div ng-click="pages = pages-1" class="m-flex-3">-</div>
                            <div class="m-flex-2">
                                <input type="number" value="1" ng-model="pages">
                            </div>
                            <div ng-click="pages = pages+1" class="m-flex-3">+</div>
                        </div>
                        <div class="words-count-block">
                            <span>Words: </span>
                            <span class="words-amount" ng-bind="275*pages">275</span>
                        </div>
                    </div>
                </div>
                <div class="order">
                    <center>
                        <span class="price" ng-bind="price + '$'">0$</span>
                        <div>
                            <button class="common-button" ng-click="submitOrder($event)">Order now</button>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    @parent
    <script type="text/javascript">
        var prefix = @if(isset($prefix)) "{{$prefix}}" @else "" @endif;

        app.controller(prefix+'priceCalcCtrl', function ($scope) {

            var types = {!! json_encode($types->indexById()) !!};
            $scope.academicLevel = {{ $academicLevels[0]->id }};
            $scope.pages = 1;
            $scope.deadline = -1;
            $scope.typeId = -1;
            $scope.price = 0;

            $scope.$watch('pages', function(nextVal, prevVal) {
                if(nextVal>999){
                    $scope.pages = 999;
                }else if(nextVal<1){
                    $scope.pages = 1;
                }
            }, true);
            
            $scope.vars = 'academicLevel + pages + deadline + typeId';

            $scope.$watch($scope.vars, function () {
                $scope.calcPrice();
            }, true);

            $scope.getPagePrice = function() {
                var searchResult = $.grep(types[$scope.typeId][0].prices, function(item){
                    return item.deadline_id == $scope.deadline &&
                            item.academic_level_id == $scope.academicLevel;
                });

                if(searchResult.length == 0) {
                    return 0.0;
                }

                return searchResult[0].price;
            }

            $scope.calcPrice = function() {
                if( $scope.deadline == -1 || $scope.typeId == -1) return;
                var pagePrice = $scope.getPagePrice();
                $scope.price = $scope.pages * pagePrice;
            }

            $scope.submitOrder = function(e){
                e.preventDefault();
                
                selectBoxValidator('#'+prefix+'get-a-price-block');
                if(!selectBoxValidator('#'+prefix+'get-a-price-block')) return 0;
                if( $scope.deadline == -1 || $scope.typeId == -1) return;
                 var data = {
                    'academic_level': $scope.academicLevel,
                    'type': $scope.typeId,
                    'deadline': $scope.deadline,
                    'pages': $scope.pages,
                };
                var params = $.param(data);
                document.location.href = '{{ route('order') }}?'+params;
            }
        });
    </script>
@stop
