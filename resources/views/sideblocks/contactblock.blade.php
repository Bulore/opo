
<div class="left-block">
    <div class="left-panel-header m-display-none">
        <span class="label2"></span>
        <span>Contact Us</span>
        <span>
              <div class="left-panel-arrow"></div>
        </span>
    </div>
    <div class="left-panel-body">
        <div class="mobile-block-header">
            Contact Us
        </div>
        <hr class="m-horizontal-line">
        <div class="mobile-contactus-block">
            <div class="contact-container">
                
                <div class="container-btn padding-t-10">
                    <open-pop-up pop-up-id="call-me-back" class="contact-btn call-back"> 
                        <span class="btn-icon btn-phone-icon"></span> Call me back 
                    </open-pop-up>
                </div>
                
                <div class="container-btn padding-t-10">
                    <a href="mailto:support@originalpapersonly.com"> 
                        <button class="contact-btn chat-now"> 
                            <span class="btn-icon btn-chat-icon"></span> Chat now
                        </button>
                    </a>
                </div>
               
                <div class="container-btn padding-t-10">
                    <a href="skype:support@originalpapersonly.com?call">
                        <button class="contact-btn call-skype"> 
                            <span class="btn-icon btn-skype-icon"></span> Call us on Skype
                        </button>
                    </a>
                </div>

                <p class="m-bold-400 m-color-blue m-font-size-34px">Toll-free for US customers:</p>
                <p class="m-bold-700 m-color-blue m-margin-25"><span class="phone"></span> +1-929-367-2627</p>
                <p class="m-bold-400 m-color-blue m-font-size-34px">Toll-free for UK customers:</p>
                <p class="m-bold-700 m-color-blue m-margin-25"><span class="phone"></span> +44-800-088-5728</p>
            </div>
        </div>
        <div class="inner-container m-display-none">
            <div class="contact-container">
                <p>Toll-free for US customers:</p>
                <p><span class="phone"></span> +1-929-367-2627</p>
                <p>Toll-free for UK customers:</p>
                <p><span class="phone"></span> +44-800-088-5728</p>
                <p>Request information by e-mail:</p>
                <p><span class="email-icon"></span> support@originalpapersonly.com</p>
                <p>We are in social networks:</p>
                <div class="social">
                    <a href="https://twitter.com/originalpapers1"> <span class="sn-twiter"></span> </a>
                    <a href="https://plus.google.com/u/0/104072062163353165984?hl=en"> <span class="sn-google"></span> </a> 
                    <a href="https://www.facebook.com/Originalpapersonly-1763588037231134/"> <span class="sn-facebook"></span> </a> 
                </div>
                <p>See our location:</p>
                <p><span class="location"></span> 504 Pine Song Lane, Virginia Beach, VA, 23451</p>
                
                <div class="container-btn padding-t-10">
                    <open-pop-up pop-up-id="call-me-back" class="contact-btn call-back"> 
                        <span class="btn-icon btn-phone-icon"></span> Call me back 
                    </open-pop-up>
                </div>
                
                <div class="container-btn padding-t-10">
                    <a href="mailto:support@originalpapersonly.com"> 
                        <button class="contact-btn chat-now"> 
                            <span class="btn-icon btn-chat-icon"></span> Chat now
                        </button>
                    </a>
                </div>
               
               <div class="container-btn padding-t-10">
                    <a href="skype:support@originalpapersonly.com?call">
                        <button class="contact-btn call-skype"> 
                            <span class="btn-icon btn-skype-icon"></span> Call us on Skype
                        </button>
                    </a>
               </div>
               
            </div>
        </div>
    </div>
</div>