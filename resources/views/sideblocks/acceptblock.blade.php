
<div class="left-block @if(isset($additionalClass)) m-display-none @endif">
    <div class="left-panel-header m-display-none">
        <span class="label3"></span>
        <span>We accept</span>
            <span>
                  <div class="left-panel-arrow"></div>
            </span>
    </div>
    <div class="left-panel-body m-backgroundcolor-grey">
        <div class="mobile-block-header m-text-align-center m-font-size-34px m-color-seagrey">
            We Accept:
        </div>
        <hr class="m-horizontal-line">
        <div class="inner-container">
            <div class="accept">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</div>