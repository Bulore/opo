<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<link rel="stylesheet" href="{{url('/css/app.css')}}">
<script src="{{url('/js/angular.min.js')}}"></script>
<script src="{{url('/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{url('/js/user/user.js')}}"></script>
<script src="{{url('js/jquery.validate.min.js')}}"></script>
<div class="admin-container margin-b clear-after" ng-app="translations" ng-controller="translationsCtrl">
	<center>	
	<div class="width-300 margin-t-50">
        <div class="input-select">
            <div class="select-box">
                <span class="selected">Please select your url</span>
                <span></span>
            </div>
            <div class="select-option scroll">
            	<span ng-repeat="x in data | unique:'url'" value="{$ x.url $}" class="select" ng-mouseup="setUrl(x.url)">{$ x.url $}</span>
            </div>
        </div>
    </div>
    </center>
    <div>
        <center>
            <div class="selectBox">
                <div class="selectBoxTop">
                    <span class="selected"></span>
                    <span class="selectArrow"></span>
                </div>
                <div class="selectOptions">
                    <span ng-repeat="x in data | unique:'url'" value="{$ x.url $}" class="selectOption" ng-mouseup="setUrl(x.url)">{$ x.section $}</span>
                </div>
            </div>
        </center>
    </div>
    <div class="clear-after">
        <div class="third-width left center">
            <center><h4>Keywords</h4></center>
        </div>
        <div class="third-width left center">
            <center><h4>Title</h4></center>
        </div>
        <div class="third-width left center">
            <center><h4>Description</h4></center>
        </div>
    </div>
    <div>
        <center>
            <div class="center-only-children padding-b">
                <div class="form-group margin-b-10 clear-after"  ng-repeat="x in data | filter:{url:url}:true">
                    <div class="third-width left padding-r">
                        <textarea class="input width-90p" rows="5" ng-model="x.keywords" ng-change="AddData(x.id, x.keywords, x.title, x.description)">{$ x.keywords}</textarea>
                    </div>
                    <div class="third-width left padding-l">
                        <textarea class="input width-90p" rows="5" ng-model="x.title" ng-change="AddData(x.id, x.keywords, x.title, x.description)">{$ x.title}</textarea>
                    </div>
                    <div class="third-width left padding-l">
                        <textarea class="input width-90p" rows="5" ng-model="x.description" ng-change="AddData(x.id, x.keywords, x.title, x.description)">{$ x.description}</textarea>
                    </div>
                </div>
            </div>
            <button class="orange-button margin-l-25" ng-click="sendAllData()">Update all</button>
        </center>
    </div>
    <script>
        var translations = angular.module('translations', []);
        translations.filter('unique', function() {
            return function(input, key) {
                var unique = {};
                var uniqueList = [];
                for(var i = 0; i < input.length; i++){
                    if(typeof unique[input[i][key]] == "undefined"){
                        unique[input[i][key]] = "";
                        uniqueList.push(input[i]);
                    }
                }
                return uniqueList;
            };
        });
        translations.controller('translationsCtrl', function ($scope) {
            $scope.data = {!! $seo !!};
            $scope.url = $scope.data[0].url;
            $scope.changedData = {};
            $scope.setUrl = function(index) {
                $scope.url = index;
            };
            $scope.AddData = function(id, keywords, title, description) {
                $scope.changedData[id] = {'keywords':keywords, 'title':title, 'description':description};
            };
           
            $scope.sendAllData = function() {
                $.ajax({
                    url: '{{url("/seo")}}',
                    method: 'post',
                    data: {'data': JSON.stringify($scope.changedData)},
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                    success: function (data) {
                        alert('Success');
                    },
                    error: function (error) {
                        alert('Error');
                    }
                });
            }
        });
    </script>
</div>
</body>
</html>