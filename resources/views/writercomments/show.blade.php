@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Writer-comment {{ $writercomment->id }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>
                <tr>
                    <th>ID.</th><td>{{ $writercomment->id }}</td>
                </tr>
                <tr><th> {{ trans('writer-comments.text') }} </th><td> {{ $writercomment->text }} </td></tr><tr><th> {{ trans('writer-comments.value') }} </th><td> {{ $writercomment->value }} </td></tr><tr><th> {{ trans('writer-comments.rating') }} </th><td> {{ $writercomment->rating }} </td></tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">
                        <a href="{{ url('writer-comments/' . $writercomment->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Writer-comment"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['writer-comments', $writercomment->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Writer-comment',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
@endsection