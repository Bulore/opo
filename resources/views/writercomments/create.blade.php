@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Create New Writer-comment</h1>
    <hr/>

    {!! Form::open(['url' => '/writer-comments', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('text') ? 'has-error' : ''}}">
                {!! Form::label('text', trans('writer-comments.text'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('text', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('value') ? 'has-error' : ''}}">
                {!! Form::label('value', trans('writer-comments.value'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('value', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('rating') ? 'has-error' : ''}}">
                {!! Form::label('rating', trans('writer-comments.rating'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('rating', null, ['class' => 'form-control', 'step' => '0.1']) !!}
                    {!! $errors->first('rating', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <select name="writer_id" id="">
        @foreach($writers as $writer)
            <option value="{{$writer->id}}">{{$writer->first_name}}</option>
        @endforeach
    </select>

    <select name="client_id" id="">
        @foreach($clients as $client)
            <option value="{{$client->id}}">{{$client->first_name}}</option>
        @endforeach
    </select>



    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

</div>
@endsection