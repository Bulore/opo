@extends('layouts.app')

@section('content')
<div class="container">

    <h1>Writer-comments <a href="{{ url('/writer-comments/create') }}" class="btn btn-primary btn-xs" title="Add New Writer-comment"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th> {{ trans('writer-comments.text') }} </th><th> {{ trans('writer-comments.value') }} </th><th> {{ trans('writer-comments.rating') }} </th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($writercomments as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $item->text }}</td><td>{{ $item->value }}</td><td>{{ $item->rating }}</td>
                    <td>
                        <a href="{{ url('/writer-comments/' . $item->id) }}" class="btn btn-success btn-xs" title="View Writer-comment"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                        <a href="{{ url('/writer-comments/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Writer-comment"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/writer-comments', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Writer-comment" />', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Writer-comment',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ));!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $writercomments->render() !!} </div>
    </div>

</div>
@endsection
