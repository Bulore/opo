<link rel="stylesheet" href="/css/callback.css">
<pop-up pop-up-id="call-me-back">
    <pop-up-content>
        <span class="call">Call Me Back</span>
        <close-pop-up></close-pop-up>
        <hr>
        <p>Please leave your contact information and we will call you ASAP.</p>
        <form action="" id="call-form" class="clear-after">
            <div class="input-select">
                <p>Subject of the call:</p>
                
                <select-box placeholder="I am interested in placing an order" id="select-box">
                    <select-box-header></select-box-header>
                    <select-box-content>
                        <select-box-option>I am interested in placing an order</select-box-option>
                        <select-box-option>I have a question regarding payment for my order (including authorization)</select-box-option>
                        <select-box-option>I want to talk about the quality of my order</select-box-option>
                        <select-box-option>I have a general question about your services</select-box-option>
                    </select-box-content>
                </select-box>

            </div>
            <div class="input-select">
                <p>Please indicate your name:</p>
                <input type="text" name="name">
            </div>
            <div class="input-select">
                <p>Please indicate your phone number:</p>
                 <input type="text" name="phone" placeholder="+1 929 367 2627">
            </div>
            <div class="input-select">
                <p>Any additional comments or requests?</p>
                <textarea name="text" cols="30" rows="5" placeholder="1000 symbols max"></textarea>
            </div>
            <div class="button clear-after">
                <close-pop-up class="cancel-button">Cancel</close-pop-up>
                <button class="call-button" type="button">Call me back</button>
            </div>
        </form>
    </pop-up-content>
</pop-up>

