<pop-up pop-up-id="pop-up-error" class="pop-up-error">
	<pop-up-content>
        <span class="call">Error</span>
        <close-pop-up></close-pop-up>
        <hr>
        <center>
            <div> <h2>Oops! Something went wrong.</h2> </div>
        </center>
    </pop-up-content>
</pop-up>   