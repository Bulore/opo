<div class="base-block" style="color: #455175;padding-top: 30px;font-family: &quot;Arial&quot;;max-width: 900px;height: 400px;background-image: url(&quot;{{url('/images/email.png')}}&quot;);">
	<div style="color: #455175;width: 550px;margin: auto;">
		<div class="logo" style="color: #455175;margin-bottom: 40px;font-weight: 700; font-family: &quot;Arial Black&quot;;">
			<div style="text-align: right;">OriginalPapersOnly<span class="orange" style="color: #feab32;">.com</span></div>
		</div>
		<div style="color: #455175;">
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Thank you for subscribing.
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 50px;">
				<div style="color: #455175;">
					Get your bonus by entering this code: <b>{{ $code }}</b>
				</div>
			</div>
			
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Click <a href="{{ url('/unsubscribe/'.$subscription->id) }}">here</a> to unsubscribe.
			</div>
			
		</div>
	</div>
</div>