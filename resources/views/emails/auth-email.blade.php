<div class="base-block" style="color: #455175;padding-top: 30px;font-family: &quot;Arial&quot;;max-width: 900px;height: 400px;background-image: url(&quot;{{url('/images/email.png')}}&quot;);">
	<div style="color: #455175;width: 550px;margin: auto;">
		<div class="logo" style="color: #455175;margin-bottom: 40px;font-weight: 700;">
			<div style="text-align: right;">OriginalPapersOnly<span class="orange" style="color: #feab32;">.com</span></div>
		</div>
		<div style="color: #455175;">
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Hello, {{$user->email}}!
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 10px;font-size: 24px;font-weight: 700;font-family: &quot;Arial Black&quot;;">
				To authorize your email go to: 
			</div>
			<div class="text-container" style="color: #feab32;margin-bottom: 10px;">
				<a href="{{$url}}">authorize</a>
			</div>
		</div>
	</div>
</div>