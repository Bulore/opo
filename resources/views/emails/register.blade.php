<div class="base-block" style="color: #455175;padding-top: 30px;font-family: &quot;Arial&quot;;max-width: 900px;height: 400px;background-image: url(&quot;{{url('/images/email.png')}}&quot;);">
	<div style="color: #455175;width: 550px;margin: auto;">
		<div class="logo" style="color: #455175;margin-bottom: 40px;font-weight: 700; font-family: &quot;Arial Black&quot;;">
			<div style="text-align: right;">OriginalPapersOnly<span class="orange" style="color: #feab32;">.com</span></div>
		</div>
		<div style="color: #455175;">
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Dear {{ $user->name }},
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				I would like to welcome you to <a href="originalpapersonly.com" class="orange" style="color: #feab32;">OriginalPapersOnly.org!</a>
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				To log into your personal account, please use this data:
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				<div style="color: #455175;">
					User email: <b>{{ $user->email }}</b>
				</div>
				<div style="color: #455175;">
					User password: <b>{{ $password }}</b>
				</div>
			</div>
			
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Please also authorize your email address to make sure I can contact you on urgent matters and your order will be delivered on time.
			</div>
			<div class="text-container" style="color: #455175;margin-bottom: 10px;">
				Thank you for your interest in our services!<br>
				Client manager at originalpapersonly.com
			</div>
		</div>
	</div>
</div>






