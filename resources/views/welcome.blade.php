@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.
                </div>
                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi corporis dignissimos iusto
                    nesciunt, reprehenderit vel. Animi aut iure modi molestias necessitatibus numquam obcaecati officia
                    omnis placeat quidem, repudiandae sed, tempore?
                </div>
                <div>Atque consequuntur cupiditate deleniti enim eos inventore libero minima minus molestias nam nisi,
                    nulla obcaecati officia optio praesentium rerum similique vel? Eaque ipsa natus nemo nobis quasi,
                    repellendus rerum sint.
                </div>
                <div>A debitis ea enim ex fugiat magni molestias nesciunt omnis placeat quam quidem, quisquam ratione
                    reiciendis rem, similique suscipit vitae? Architecto asperiores error fuga iure minima nam possimus
                    rem, voluptate!
                </div>
                <div>Aliquam consequatur et ex fugiat hic laboriosam nulla perspiciatis quae saepe sunt ullam, velit.
                    Atque beatae, explicabo ipsam maxime, nobis non obcaecati officia perferendis porro quas quidem sit
                    soluta, tempora.
                </div>
                <div>Eos et excepturi facilis fugit harum illo in quas reprehenderit. Ab corporis debitis deleniti dolor
                    eaque eligendi enim exercitationem fugiat, id, ipsum itaque laborum laudantium nemo quaerat
                    repellat. Quas, sapiente.
                </div>
                <div>Ab adipisci aliquam aperiam, asperiores aut beatae consectetur, consequatur consequuntur eos
                    explicabo fugit id incidunt libero, maxime modi necessitatibus nemo odit optio porro quo repellendus
                    similique velit veniam voluptate voluptatibus.
                </div>
                <div>Accusantium amet asperiores, enim explicabo fuga incidunt obcaecati rerum vel. Atque dolorum iste
                    quis quo ut? Accusamus aspernatur, doloribus et ipsa molestias nam necessitatibus pariatur quia
                    quisquam sunt ullam voluptates.
                </div>
                <div>Aut autem ducimus, eaque est fugiat, fugit illo laboriosam laborum laudantium, officia pariatur
                    provident quaerat quasi quod repellat tenetur velit. At autem eligendi harum hic inventore quo,
                    reiciendis repellendus vero?
                </div>
                <div>Accusantium assumenda earum eum fugiat magnam! Fugiat possimus rerum vitae! Accusantium asperiores
                    dignissimos dolor dolorum, eligendi facere provident quaerat quasi qui quia, quibusdam quis repellat
                    sunt temporibus tenetur, veritatis voluptatibus.
                </div>
                <div>At neque odit sed! Amet consectetur earum eius fugiat laborum non repellat sapiente sint voluptates
                    voluptatibus? A accusantium consectetur doloremque modi numquam perspiciatis tempora veniam?
                    Aspernatur id neque provident similique.
                </div>
                <p>{{Request::url()}}</p>
                <p>{{Request::path()}}</p>
            </div>
        </div>
    </div>
    <script>
    </script>
</div>
@endsection
