<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="shortcut icon" href="{{url('/images/favicon.ico')}}" />
    <link rel="stylesheet" href="{{url('/css/user/commonUser.css')}}">
    <script src="{{url('/js/angular.min.js')}}"></script>
    <script src="{{url('/js/dirPagination.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/webcomponents-lite.js')}}"></script>
    <script src="{{url('/js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{url('/js/user/user.js')}}"></script>
    <script src="{{url('/js/jquery.mobile-events.min.js')}}"></script>
    <script src="{{url('js/jquery.validate.min.js')}}"></script>
</head>
<body ng-app="app" ng-controller="appCtrl" id="touch-zone">
@section('header')
    @include('user.layouts.header')
@show
<div class="user-container">
    @include('user.layouts.leftPanel')
    @yield('content')
</div>
@section('footer')
    @include('user.layouts.footer')
    @include('popups.callback')
    @include('popups.error-popup')
    @include('user.popups.pop-up-show-message')
    <script>
        var popUpError = document.querySelector('pop-up[pop-up-id="pop-up-error"]');
    </script>
@show
</body>
</html>