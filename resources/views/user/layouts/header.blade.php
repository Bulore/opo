<header>
    <div class="nav-menu">
        <a href="{{url('/')}}" class="mobile-display-none">OriginalPapersOnly</a><a href="{{url('/')}}" class="mobile-display-none">.com</a>
        <div id="mobile-menu">
            <div>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="order-icon">
            <div class="relative">
                <button class="common-button" ng-click="leftPanelClick('/user/new-order',$event)">New order</button>
                @if(!$user->phone_authorized or !$user->email_authorized)
                    <span class="warning-icon-avatar"></span>
                @endif
                <div class="icon">
                    <div class="inf">
                        <p>{{ request()->user()->name }}</p>
                        <p>ID #{{ request()->user()->id }}</p>
                        @if(!$user->phone_authorized or !$user->email_authorized)
                        <hr>
                        @endif
                        @if(!$user->phone_authorized)
                            <a href="" ng-click="leftPanelClick('{{ url("/user/authorization") }}', $event)" class="warning">
                                <span class="warning-icon"></span>
                                Phone Authorization needed
                            </a>
                        @endif
                        @if(!$user->email_authorized)
                            <a href="" ng-click="leftPanelClick('{{ url("/user/authorization") }}', $event)" class="warning">
                                <span class="warning-icon"></span>
                                Email Authorization needed
                            </a>
                        @endif
                        <a href="{{url('/logout')}}">
                            <div class="sign-out">Sign out</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="background"></div>
@include("user.layouts.spinner")
