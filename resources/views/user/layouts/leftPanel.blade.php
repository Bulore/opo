<div class="left-panel">
    <div class="scrollable">
        @foreach($sections as $arr)
            <a href="{{$arr['url']}}" ng-click="leftPanelClick('{{$arr['url']}}',$event)">
                <div class="item @if(Request::url()==$arr['url']) active-item @endif">
                    <span class="{{$arr['icon']}}"></span>
                    <span class="tab-name">{{$arr['name']}}</span>
                    @if($arr['amount'] != '')
                        <span class="amount" ng-bind="sectionWithAmmount['{{$arr['name']}}']"></span>
                    @endif
                </div>
            </a>
        @endforeach
        <hr>
        <a href="{{url('/logout')}}">
            <div class="item">
                <span class="icon-sign"></span>
                <span class="tab-name">Sign out</span>
            </div>
        </a>
        <div class="padding-b-20"></div>
        <div class="button-phone">
            <div class="container-btn">
                <open-pop-up class="contact-btn chat-now" pop-up-id="call-me-back" >
                    <span class="btn-icon btn-phone-icon"></span>
                    Call me back
                </open-pop-up>
            </div>
            
            <div class="container-btn">
                <a href="mailto:support@originalpapersonly.com">
                    <button class="contact-btn chat-now">
                        <span class="btn-icon btn-chat-icon"></span>
                        Chat now
                    </button>
                </a>
            </div>
            
            <div class="container-btn">
                <a href="skype:support@originalpapersonly.com?chat">
                    <button class="contact-btn chat-now">
                        <span class="btn-icon btn-skype-icon"></span> 
                        Call us on Skype
                    </button>
                </a>
            </div>
           
            <p>Toll-free for US customers:</p>
            <p><span class="phone"></span> +1-929-367-2627</p>
            <p>Toll-free for UK customers:</p>
            <p><span class="phone"></span> +44-800-088-5728</p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var appElement = document.querySelector('[ng-app=app]');
        var $scope = angular.element(appElement).scope();
        $scope.$apply(function() {
            $scope.sections = {!! json_encode($sections) !!};
            $scope.sectionWithAmmount = [];
            
            (function(){
                for(var i in $scope.sections){
                    if($scope.sections[i]['amount'] != ''){
                        $scope.sectionWithAmmount[$scope.sections[i]['name']] = $scope.sections[i].amount;
                    }
                }
            })();
        });
    })
</script>

