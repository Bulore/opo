<footer>
    <a href="{{url("/privacy")}}">Privacy and Cookies Policy</a>
    <a href="{{url("/terms-of-use")}}">Term of Use</a>
    <a href="{{url("/terms-of-use#money-back")}}">Money Back</a>
    <a href="{{url("/terms-of-use#guarantee-revision")}}">Guarantee Revision</a>
    <a href="{{url("/contacts")}}">Policy Contacts</a>
    <a href="{{url("/payment-auth")}}">Payment Authorization</a>
    <a href="{{url("/faq")}}">FAQ</a>
</footer>
