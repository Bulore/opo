<pop-up pop-up-id="pop-up-send-message-file">
	<pop-up-content>
        <tab-panel>
            <tab-nav>
                <tab-nav-item class="active">Create new message</tab-nav-item>
                <tab-nav-item>Upload file</tab-nav-item>
                <close-pop-up></close-pop-up>
            </tab-nav>
            <tab-content>
                <tab-content-item>
                    <form action="" id="new-message-form">
                        {{ csrf_field() }}
                        <div class="form-item">
                            <div class="input-select">
                                <p>Recipient</p>
                                <select-box @if($order->writer_id != 0) placeholder="Writer #{{$order->id}}" @else placeholder="Support" @endif>
                                    <select-box-header></select-box-header>
                                    <select-box-content>
                                        @if($order->writer_id != 0)
                                        <select-box-option ng-click="messages.newUserData.content.to_type = 'Writer'; messages.newUserData.content.to_id = '{{$order->writer_id}}'">Writer #{{$order->id}}</select-box-option>
                                        @endif
                                        @if($order->internal_status >= App\Paper::STATUS_ASSIGNED_MANAGER)
                                        <select-box-option ng-click="messages.newUserData.content.to_type = 'Manager'; messages.newUserData.content.to_id = '{{$order->support_id}}'">Support</select-box-option>
                                        @endif
                                    </select-box-content>
                                </select-box>
                            </div>
                        </div>
                        <div class="form-info">
                            Mind that for security purposes all questions regarding personal or billing information must be addressed directly to the Customer Support Representatives. The writer will not be able to assist you on this matter. Moreover, this way we will ensure your privacy and safety.
                        </div>
                        <div class="form-item">
                            <div class="input-select">
                                <p>Type of message</p>
                                <select-box placeholder="Choose type of message">
                                    <select-box-header></select-box-header>
                                    <select-box-content>
                                        <select-box-option ng-click="messages.newUserData.content.type = 0">Additional questions about the paper</select-box-option>
                                        <select-box-option ng-click="messages.newUserData.content.type = 1">I need a revision</select-box-option>
                                        <select-box-option ng-click="messages.newUserData.content.type = 2">Compliant</select-box-option>
                                        <select-box-option ng-click="messages.newUserData.content.type = 3">Other</select-box-option>
                                    </select-box-content>
                                </select-box>
                            </div>
                        </div>
                        <div class="form-item">
                            <div class="input-select">
                                <p>Message</p>
                                <textarea name="text" id="" cols="30" rows="10" placeholder="1000 symbols max" ng-model="messages.newUserData.content.text"></textarea>
                            </div>
                        </div>
                        <div class="popup-footer padding-t-20">
                            <center>
                                <button class="common-button" type="button" ng-click="messages.newUserData.send()">Send</button>
                            </center>
                        </div>
                    </form>
                </tab-content-item>
                <tab-content-item>
                    <form action="" id="new-file-form">
                        {{ csrf_field() }}
                        <div class="form-item">
                            <div class="input-select">
                                <p>Recipient</p>
                                <select-box @if($order->writer_id != 0) placeholder="Writer #{{$order->writer_id}}" @else placeholder="Support" @endif>
                                    <select-box-header></select-box-header>
                                    <select-box-content>
                                        @if($order->writer_id != 0)
                                        <select-box-option  ng-click="files.newUserData.content.to_type = 'Manager'; files.newUserData.content.to_id = '{{$order->writer_id}}'">Writer #{{$order->writer_id}}</select-box-option>
                                        @endif
                                        <select-box-option ng-click="files.newUserData.content.to_type = 'Manager'; files.newUserData.content.to_id = '{{$order->support_id}}'">Support</select-box-option>
                                    </select-box-content>
                                </select-box>
                            </div>
                        </div>

                        <div class="form-item flex margin-t-20 m-margin-t-50">
                            <div class="choose-container flex-1 margin-r-20 m-margin-r-35">
                                <label>
                                    <button class="common-button upload-file-button" type="button">Choose file</button>
                                    <input type="file" class="hidden" name="file" id="file">
                                </label>
                            </div>
                            <div class="input-div flex-4">
                                <input type="text" id="file-input" name="file" class="input full-width" placeholder="">
                            </div>
                        </div>
                        <div class="popup-footer margin-t-20 m-margin-t-50">
                            <center>
                                <button class="common-button" type="button" ng-click="files.newUserData.send()">Send</button>
                            </center>
                        </div>
                    </form>
                </tab-content-item>
            <tab-content>
        </tab-panel>
    </pop-up-content>
</pop-up>   
<script>
    var PopUpMF = document.querySelector('pop-up[pop-up-id="pop-up-send-message-file"]');
    var PopUpMFTabs = {
        'message':  document.querySelector('pop-up[pop-up-id="pop-up-send-message-file"] tab-nav tab-nav-item:nth-child(1)'),
        'file': document.querySelector('pop-up[pop-up-id="pop-up-send-message-file"] tab-nav tab-nav-item:nth-child(2)')
    }
</script>