<pop-up pop-up-id="pop-up-success-bonuses">
	<pop-up-content>
        <span class="call">Success</span>
        <close-pop-up></close-pop-up>
        <hr>
        <center>
            <div> <h2>Great! Your bonuses were transferred to your Store credit. You can use them to pay for any of your orders.</h2> </div>
        </center>
    </pop-up-content>
</pop-up>   