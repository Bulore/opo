<pop-up pop-up-id="pop-up-show-message">
	<pop-up-content>
        <span class="call">Success</span>
        <close-pop-up></close-pop-up>
        <hr>
        <center>
            <div> <h2 class="pop-up-message">Success</h2> </div>
        </center>
    </pop-up-content>
</pop-up> 
<script>
	function popUpShowMessage(message) {
		var popUp = document.querySelector('pop-up[pop-up-id="pop-up-show-message"]');
		var content = popUp.querySelector('.pop-up-message');
		content.innerHTML = message || content.innerHTML;
		popUp.open();
	}
</script>  