<pop-up pop-up-id="pop-up-change-password" class="pop-up-error">
	<pop-up-content>
        <span class="blue-color bold-blue font-size-171em margin-0">Change password</span>
        <close-pop-up></close-pop-up>
        <hr>
        <div>
            <form id="change-password-form">
                <div class="form-item">
                    <p class="form-text">Current password</p>
                    <input class="input" type="password" placeholder="Type here" ng-model="pass.old_password" name="old_password">
                </div>
                <div class="form-item">
                    <p class="form-text">New password</p>
                    <input class="input" type="password" placeholder="Type here" ng-model="pass.password" name="password">
                </div>
                <div class="form-item">
                    <p class="form-text">Re-type new password</p>
                    <input class="input" type="password" placeholder="Type here" ng-model="pass.password_confirmation" name="password_confirmation">
                </div>
	            <div class="popup-footer padding-t-20">
	                <button class="common-button" type="button" ng-click="changePassword()">Submit</button>
	            </div>
            </form>
        </div>
    </pop-up-content>
</pop-up>   