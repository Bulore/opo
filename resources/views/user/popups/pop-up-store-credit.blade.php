<pop-up pop-up-id="pop-up-store-credit">
	<pop-up-content>
        <span class="blue-color bold-blue font-size-171em margin-0">Save as Store Credit</span>
        <close-pop-up></close-pop-up>
        <hr>
        <div>
            <p class="blue-color font-size-087em lh-2em">Once your first order with us is approved, you automatically get 10% of the order cost as a bonus for your future orders. Then you will get bonuses EACH time you use our services and the amount of bonus will depend on the number of pages that you order:</p>
            <center>
                <div class="popup-body-button">
                    <div class="your-bonuses-block">
                        Your bonuses:
                        <span class="bonuses-amount">${{ request()->user()->bonuses }}</span>
                    </div>
                </div>
            </center> 
            <hr>
            <div class="popup-footer">
                <center>
                    <button class="common-button" type="submit">Submit</button>
                </center>
            </div>
        </div>
    </pop-up-content>
</pop-up>   
