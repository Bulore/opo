<pop-up pop-up-id="pop-up-edit-information">
	<pop-up-content>
        <span class="blue-color bold-blue font-size-171em margin-0">Edit Additional Information</span>
        <close-pop-up></close-pop-up>
        <hr>
        <div>
            <form id="edit-information-form">
                <div class="popup-body">
                    <div class="form-item">
                        <p class="form-text">Night Calls Allowed</p>
                    </div>
                    <div class="form-item flex-items-center blue-color input-radio">
                        <label class="margin-l-10">
                            <input type="radio" ng-model="additionalInfChange.night_calls" ng-value="1">
                            <div>Yes</div>
                        </label>
                        <span class="margin-r-20"></span>
                        <label>
                            <input type="radio" ng-model="additionalInfChange.night_calls" ng-value="0">
                            <div>No</div>
                        </label>
                    </div>
                    <div class="form-item">
                        <p class="form-text">Alternative Email</p>
                        <input class="input" type="email" placeholder="Type here" ng-model="additionalInfChange.alt_email"> 
                    </div>
                    <div class="form-item">
                        <p class="form-text">Alternative phone</p>
                        <input class="input" type="text" placeholder="Type here" ng-model="additionalInfChange.alt_phone">
                    </div>
                    <div class="form-item">
                        <p class="form-text">Preferred Language Style</p>
                        <div class="input-select">
                            <select-box plaseholder="{$ additionalInf.preferred_language $}">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-click="setPreferredLanguage('No preference')">No preference</select-box-option>
                                    <select-box-option ng-click="setPreferredLanguage('US English')">US English</select-box-option>
                                    <select-box-option ng-click="setPreferredLanguage('UK English')">UK English</select-box-option>
                                    <select-box-option ng-click="setPreferredLanguage('Australian English')">Australian English</select-box-option>
                                </select-box-content>
                            </select-box>                        
                        </div>
                    </div>
                </div>
                <div class="popup-footer padding-t-20">
                    <button class="common-button" type="button" ng-click="editAdditionalInformation()">Submit</button>
                </div>
            </form>
        </div>
    </pop-up-content>
</pop-up>   
<script>
    var popUpEditInf = document.querySelector('pop-up[pop-up-id="pop-up-edit-information"]');
</script>