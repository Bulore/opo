@extends('user.layouts.base')
@section('content')
    @include('user.sections.block.preferred-writers', compact('writers'))
@stop