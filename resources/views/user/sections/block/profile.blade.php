<div class="content">
    <link rel="stylesheet" href="{{url('css/user/profile.css')}}">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            
            $scope.$apply(function() {
                $scope.countries = {!! json_encode($countries) !!};

                $scope.occupations = ["Arts", "Charities", "Construction", "Design", "Education", "Engineering", "Environment", "Finance & Accounting", "Government &amp; Politics", "Health", "Hospitality", "Housing", "IT &amp; Computors", "Leisure", "Marketing & PR", "Media", "Recruitment", "Retail & FMCG", "Science", "Social care", "Student", "Travel & Transport", "Other"];
                
                $scope.fieldsOfStudy = ["Physics", "Astronomy or other Space Science", "Philosophy", "Engineering", "Math", "History", "Chemistry", "Biology or other Life Science", "Foreign language (Useful type)", "Computer Science", "Agriculture", "Geology or other Earth Science", "Architecture", "Literature", "Law", "Geography", "Music", "Economics", "Study of Some Foreign Place or Culture", "Archaeology", "Anthropology", "Religion or Theology", "Art", "Education", "Foreign Language", "Political Science", "Drama or Film", "Phys Ed, Sports Management or other Major Designed For Athletes", "Journalism or 'Communications'", "Business", "Psychology", "Sociology", "Other"];

                $scope.academicLevels = ["Elementary School", "Middle School", "High School", "College", "Associate's Degree", "Bachelor's Degree", "Master's Degree", "Doctoral Degree", "Professional Degree", "Undergraduate", "Other"];


                $scope.setPreferredLanguage = function(x){ $scope.additionalInfChange.preferred_language = x };

                $scope.additionalInf = {
                    "night_calls": "{{$user->night_calls}}",
                    "alt_email": "{{$user->alt_email}}",
                    "alt_phone": "{{$user->alt_phone}}",
                    "preferred_language": "{{$user->preferred_language}}",
                    "_token": "{{csrf_token()}}",
                }

                $scope.additionalInfChange = angular.copy($scope.additionalInf);

                $scope.editAdditionalInformation = function(){
                    var data = $scope.additionalInfChange;
                    $.ajax({
                        url: "{{url('/user/edit')}}",
                        data: data,
                        method: "POST",
                        success: function(data){
                            $scope.additionalInf = angular.copy($scope.additionalInfChange);
                            $scope.$apply();
                            popUpEditInf.close();
                        },
                        error: function(){ 
                            validWithServer(error.responseText, '#edit-information-form', ".form-item", "p");
                        }
                    });
                };

                $scope.pass = {
                    "old_password": '',
                    "password": '',
                    "password_confirmation": '',
                    "_token": "{{csrf_token()}}",
                }

                $scope.changePassword = function() {
                    $.ajax({
                        url: "{{url('/user/change-password')}}",
                        data: $scope.pass,
                        method: "POST",
                        success: function(data){
                            $scope.additionalInf = angular.copy($scope.additionalInfChange);
                            $scope.$apply();
                            $('#popup-id').find('.close_popup').click();
                        },
                        error: function(error){ 
                            validWithServer(error.responseText, '#change-password-form', ".form-item", "p");
                        }
                    });
                };

                $scope.survey = {
                    'country': '',
                    'countryResidence': '',
                    'birthDay': '',
                    'gender': '',
                    'dateGraduation': '',
                    'academicLevel': '',
                    'otherAcademicLevel': '',
                    'anotherAcademicLevel': '',
                    'anotherOtherAcademicLevel': '',
                    'fieldOfStudy': '',
                    'otherFieldOfStudy': '',
                    'anotherFieldOfStudy': '',
                    'anotherOtherFieldOfStudy': '',
                    'occupation': '',
                    'otherOccupation': '',
                    'anotherOccupation': '',
                    'anotherOtherOccupation': '',
                    'familyStatus': '',
                }
                $scope.$watch('replaceCountry', function(nextVal, prevVal) {
                    if(nextVal === true){
                        $scope.countryResidence = $scope.country;
                    }
                }, true);

                $scope.anotherEducation = false;
                $scope.anotherOccupation = false;

                $scope.submitSurvey = function() {
                    selectBoxValidator('#survey-form');
                    if(!selectBoxValidator('#survey-form')) return 0;
                    
                    $.ajax({
                        url: "{{url('/user/complete-survey')}}",
                        data: $scope.survey,
                        method: "GET",
                        success: function(){ alert('success'); },
                        error: function(){ popUpError.open() }
                    });
                };

            });
            deInitialise();
            initialise();

        })

        
    </script>

    <div class="content-header margin-b-30">
        <span class="header-name">Profile</span>
        <span class="id">#{{$user->id}}</span>
    </div>
    <div class="inner-content mobile-margin-bottom-75">
        <div class="table margin-r-30 float-l">
            <div class="table-header">
                Profile 
                <span class="table-icon profile-icon"></span>
            </div>
            <div class="table-item">
                <span class="table-column bold-blue">Client ID:</span>
                <span>{{$user->id}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Name:</span>
                <span>{{$user->name}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Last Name:</span>
                <span>{{$user->last_name}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Email:</span>
                <span>{{$user->email}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Country:</span>
                <span>{{$user->country ? $user->country->name : ''}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Phone:</span>
                <span>{{$user->phone}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Timezone:</span>
                <span>GMT {{$user->timezone}}</span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue">Local time:
            <div class="inf-container">
                <div class="inf">
                   Please make sure you have entered the correct email address to receive notifications of order completion status
                </div>
            </div>
            </span><span>{{$user->local_time}}</span></div>
            <div class="inner-tab-content-block">
                <open-pop-up class="blue-button" pop-up-id="pop-up-change-password">Change password</open-pop-up>
            </div>
        </div>
        <div class="table">
            <div class="table-header">
                Additional information
                <span class="table-icon profile-plus-icon"></span>
            </div>
            <div class="table-item">
                <span class="table-column bold-blue width-180">
                Night Calls Allowed:
                <div class="inf-container">
                    <div class="inf">
                       Please make sure you have entered the correct email address to receive notifications of order completion status
                    </div>
                </div>
                </span>
                <span ng-bind="(additionalInf.night_calls==1)?'Yes':'No'"></span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue width-180">
                    Alternative Email:
                    <div class="inf-container">
                        <div class="inf">
                           Please make sure you have entered the correct email address to receive notifications of order completion status
                        </div>
                    </div>
                </span>
                <span ng-bind="additionalInf.alt_email"></span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue width-180">
                    Alternative Phone:
                    <div class="inf-container">
                        <div class="inf">
                           Please make sure you have entered the correct email address to receive notifications of order completion status
                        </div>
                    </div>
                </span>
                <span ng-bind="additionalInf.alt_phone"></span>
            </div>
            <hr>
            <div class="table-item">
                <span class="table-column bold-blue width-180">
                    Preferred Language Style:
                    <div class="inf-container">
                        <div class="inf">
                           Please make sure you have entered the correct email address to receive notifications of order completion status
                        </div>
                    </div>
                </span>
                <span ng-bind="additionalInf.preferred_language"></span>
            </div>
            <div class="inner-tab-content-block float-none-for-tablet">
                <open-pop-up class="blue-button width-100" pop-up-id="pop-up-edit-information">Edit</button>
            </div>
        </div>
        <div class="table">
            <div class="table-header">Email Preferences
                <span class="table-icon message-settings-icon"></span>
            </div>
            <div class="table-item">
                <span class="bold-blue line-height-25 mobile-line-height-45">
                    Please authorize your email address to adjust your email preferences
                </span>
            </div>
            <div class="inner-tab-content-block">
                <a href="{{url("/user/authorization")}}" class="no-underline" ng-click="leftPanelClick('{{url("/user/authorization")}}',$event)">
                    <button class="blue-button">Email authorization</button>
                </a>
            </div>
        </div>
    </div>
    @if(!$user->survey_completed)
        <div class="inner-content margin-top-20">
            <div class="table-header">
                Survey
                <span class="table-icon label-icon"></span>
            </div>
            <div class="form">
                <p class="blue-color">Help us improve and get a discount! Fill in this short survey and get <span class="bold-blue">10% off your next order</span></p>
                <form action="" id="survey-form">
                    <div class="form-item">
                        <div class="form-text">Country Of Origin:</div>
                        <div class="input-select">
                            <select-box required placeholder="Please select your country of origin">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in countries" ng-click="survey.country = item.name"> {$ item.name $} </select-box-option>
                                </select-box-content>
                            </select-box>
                            <label>
                                <input type="checkbox"  name="remember" hidden ng-model="replaceCountry">
                                <div class="checkbox"></div>
                                <span class="blue-color font-size-087em mobile-font-size-06em">Country Of Residence is the same as County Of Origin</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-item" ng-show="!replaceCountry">
                        <div class="form-text">Country Of Residence:</div>
                        <div class="input-select">
                            <select-box required placeholder="Please select your country of origin">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in countries" ng-click="survey.countryResidence = item.name"> {$ item.name $} </select-box-option>
                                </select-box-content>
                            </select-box>
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text">Date Of Birth:</div>
                        <div class="input-div">
                            <input type="text" placeholder="MM/DD/YYYY" ng-model="survey.birthDay">
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text">Gender:</div>
                        <div class="input-select">
                            <select-box required placeholder="Please select your gender">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-click="survey.gender = 'Male'">Male</select-box-option>
                                    <select-box-option ng-click="survey.gender = 'Female'">Female</select-box-option>
                                </select-box-content>
                            </select-box>
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text">Date Of Graduation:</div>
                        <div class="input-div">
                            <input type="text" placeholder="MM/DD/YYYY" ng-model="survey.dateGraduation">
                        </div>
                    </div>
                    <div class="form-item margin-bottom-0">
                        <div class="form-text">Education:</div>
                        <div class="input-select ">
                            <select-box required placeholder="Please select your Academic Level">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in academicLevels" ng-click="survey.academicLevel = item">
                                        {$ item $}
                                    </select-box-option>
                                </select-box-content>
                            </select-box>
                           
                            <div class="input-div margin-t-15" ng-show="survey.academicLevel == 'Other'">
                                <input type="text" placeholder="Academic Level custom" ng-model="survey.otherAcademicLevel" class="input-other">
                            </div>
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text"></div>
                        <div class="input-select">
                            <select-box required placeholder="Please select your Field of Study">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in fieldsOfStudy" ng-click="survey.fieldOfStudy = item">
                                        {$ item $}
                                    </select-box-option>
                                </select-box-content>
                            </select-box>

                            <div class="input-div margin-t-15" ng-show="survey.fieldOfStudy == 'Other'">
                                <input type="text" placeholder="Field of study custom" ng-model="survey.otherFieldOfStudy" class="input-other">
                            </div>
                            <a href="" ng-click="anotherEducation = true" ng-show="!anotherEducation">Add another education</a>
                        </div>
                    </div>
                    <div ng-show="anotherEducation">
                        <div class="form-item">
                            <div class="form-text"></div>
                            <div class="input-select">
                                <select-box ng-required="anotherEducation" placeholder="Please select your Academic Level">
                                    <select-box-header></select-box-header>
                                    <select-box-content>
                                        <select-box-option ng-repeat="item in academicLevels" ng-click="survey.anotherAcademicLevel = item">
                                            {$ item $}
                                        </select-box-option>
                                    </select-box-content>
                                </select-box>

                                <div class="input-div margin-t-15" ng-show="survey.anotherAcademicLevel == 'Other'">
                                    <input type="text" placeholder="Academic Level custom" ng-model="survey.otherAnotherAcademicLevel" class="input-other">
                                </div>
                            </div>
                        </div>
                        <div class="form-item">
                            <div class="form-text"></div>
                            <div class="input-select">
                                <select-box ng-required="anotherEducation" placeholder="Please select your Field of Study">
                                    <select-box-header></select-box-header>
                                    <select-box-content>
                                        <select-box-option ng-repeat="item in fieldsOfStudy" ng-click="survey.anotherFieldOfStudy = item">
                                            {$ item $}
                                        </select-box-option>
                                    </select-box-content>
                                </select-box>

                                <div class="input-div margin-t-15" ng-show="survey.anotherFieldOfStudy == 'Other'">
                                    <input type="text" placeholder="Field of study custom" ng-model="survey.otherAnotherFieldOfStudy" class="input-other">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text">Occupation:</div>
                        <div class="input-select ">
                            <select-box  placeholder="Please select your occupation">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in occupations" ng-click="survey.occupation = item">
                                        {$ item $}
                                    </select-box-option>
                                </select-box-content>
                            </select-box>

                            <div class="input-div margin-t-15" ng-show="survey.occupation == 'Other'">
                                <input type="text" placeholder="Custom occupation" ng-model="survey.otherOccupation" class="input-other">
                            </div>
                            <a href="" ng-click="anotherOccupation = true" ng-show="!anotherOccupation">Add another occupation</a>
                        </div>
                    </div>
                    <div class="form-item" ng-show="anotherOccupation">
                        <div class="form-text"></div>
                        <div class="input-select ">
                            <select-box ng-required="anotherOccupation" placeholder="Please select your occupation">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-repeat="item in occupations" ng-click="survey.anotherOccupation = item">
                                        {$ item $}
                                    </select-box-option>
                                </select-box-content>
                            </select-box>
            
                            <div class="input-div margin-t-15" ng-show="survey.anotherOccupation == 'Other'">
                                <input type="text" placeholder="Custom occupation" ng-model="survey.otherAnotherOccupation" class="input-other">
                            </div>
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-text">Family status:</div>
                        <div class="input-select">
                            <select-box required placeholder="Please select your Family Status">
                                <select-box-header></select-box-header>
                                <select-box-content>
                                    <select-box-option ng-click="survey.familyStatus = 'Single'">Single</select-box-option>
                                    <select-box-option ng-click="survey.familyStatus = 'Married'">Married</select-box-option>
                                    <select-box-option ng-click="survey.familyStatus = 'Divorced'">Divorced</select-box-option>
                                </select-box-content>
                            </select-box>
                        </div>
                    </div>
                    <div class="margin-top-50"></div>
                    <div class="form-item ">
                        <div class="flex jc-center full-width">
                            <button class="common-button" type="button" ng-click="submitSurvey()">Get my discount</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
    @include('user.popups.pop-up-change-password')
    @include('user.popups.pop-up-edit-information')
</div>

