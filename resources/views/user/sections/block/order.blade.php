<div class="content">
	<link rel="stylesheet" href="{{url('css/user/order.css')}}">
	<script>
		$(document).ready(function(){
			var appElement = document.querySelector('[ng-app=app]');
		    var $scope = angular.element(appElement).scope();
		    $scope.$apply(function() {
		    	
		    	$scope.orderFilter = '';
    			$scope.sortType = '';
    			$scope.sortReverse = false;

		        $scope.data = {!! $orders->toJson() !!};
		        $scope.columns = $scope.data[0];
		        $scope.filterPages = '';
		        $scope.filterWriterId = '';
		        $scope.filterId = '';
		        $scope.typeOfService = '';
		        $scope.typeOfPaper = '';
		        $scope.subject = '';
		        
		        $scope.setTypeOfService = function(x){ $scope.typeOfService = x; };
		        $scope.setTypeOfPaper = function(x){ $scope.typeOfPaper = x; };
		        $scope.setSubject = function(x){ $scope.subject = x; };

		        $scope.typeOfServices = {!! json_encode($types) !!};
		        $scope.typeOfPapers = {!! $paperTypes->toJson() !!};
		        $scope.subjects = {!! $subjects->toJson() !!};

  				$scope.setOrderFilter = function(val){ $scope.orderFilter = val; };

		        $scope.isHidden = function(x) {
		        	var hiddenColumn = ['Writer ID', 'Status', 'Subject'];
		        	return (hiddenColumn.indexOf(x) != -1) ? 'mobile-display-none' : '';
		        }
		        $scope.remove = function(x){
		        	var removeColumn = ['paper.messages_count', 'paper.unread_messages', 'paper.type_name', 'paper.type_of_service'];
		        	return (removeColumn.indexOf(x) != -1) ? false : true;
		        }
		        $scope.notEmptyMessage = function(key, index){
		        	if(key == "ID" && $scope.data[index]["paper.messages_count"] != 0) return true;
		        	else return false;
		        }

		        $scope.SortType = function(key){
			        $scope.sortReverse = !$scope.sortReverse;
			        $scope.sortType = key;
			        $scope.data.sort(function (a, b) {
			            if (a[key] > b[key]) {
			                return $scope.sortReverse? -1 : 1;
			            }
			            if (a[key] < b[key]) {
			                return $scope.sortReverse? 1: -1;
			            }
			            return 0;
			        });
			    };
		    });
		    deInitialise();
    		initialise();
		})
	</script>
	<div class="content-header margin-b-15">
		<span class="header-name">Orders</span>
		<span id="search-button"></span>
		<div class="search-block">
			<div class="search-block-container">
				<input type="text" placeholder="Order No" ng-model="filterId" data-role="none" class="input">
				<div class="input-select">
					
					<select-box placeholder="Type of service" class="valid-error">
						<select-box-header></select-box-header>
						<select-box-content class="min-width-250">
							<select-box-option ng-click="setTypeOfService('')">All</select-box-option>
							<select-box-option ng-repeat="item in typeOfServices" ng-click="setTypeOfService(item)">{$ item $}</select-box-option>
						</select-box-content>
					</select-box>

				</div>
				<div class="input-select">
					
					<select-box placeholder="Type of paper" class="valid-error">
						<select-box-header></select-box-header>
						<select-box-content class="min-width-250">
							<select-box-option ng-click="setTypeOfPaper('')">All</select-box-option>
							<select-box-option ng-repeat="item in typeOfPapers" ng-click="setTypeOfPaper(item.name)">{$ item.name $}</select-box-option>
						</select-box-content>
					</select-box>

				</div>
				<div class="input-select">

					<select-box placeholder="Subject" class="valid-error">
						<select-box-header></select-box-header>
						<select-box-content class="min-width-250">
							<select-box-option ng-click="setSubject('')">All</select-box-option>
							<select-box-option ng-repeat="item in subjects" ng-click="setSubject(item.name)">{$ item.name $}</select-box-option>
						</select-box-content>
					</select-box>

				</div>
				<input type="text" class="input" placeholder="Writer ID" ng-model="filterWriterId" data-role="none">
				<input type="text" class="input" placeholder="Pages" ng-model="filterPages" data-role="none">
			</div>
		</div>	
	</div>
	<div class="inner-content" >
		<div class="tabs-container">
			<div class="tabs">
	            <span class="tab" ng-click="setOrderFilter('Current')">Current</span>
	            <span class="tab" ng-click="setOrderFilter('Unpaid')">Unpaid</span>
	            <span class="tab" ng-click="setOrderFilter('Completed')">Completed</span>
	            <span class="tab" ng-click="setOrderFilter('Cancelled')">Cancelled</span>
	            <span class="tab active-tab" ng-click="setOrderFilter('')">All</span>
	            <hr>
	        </div>
			<table class='data-table'>
				<thead>
					<tr>
				        <th></th>
				        <th ng-repeat="(key, value) in columns" ng-click="SortType(key)" ng-class="isHidden(key)" ng-if="remove(key)">
			                <span>{$ key $}</span>
				        </th>
				    </tr>
				</thead>
				<tbody>
					<tr ng-repeat="row in data | filter:{ $:orderFilter,
					'Quantity': filterPages,
					'Writer ID': filterWriterId,
					'ID': filterId,
					'Subject': subject,
					'paper.type_of_service': typeOfService,
					'paper.type_name': typeOfPaper}" ng-click="order(row.ID,$event)" ng-init="$rootIndex = $index">
						<td> &nbsp;&nbsp;<span class="message-icon" ng-if="notEmptyMessage('ID', $rootIndex)"></span>&nbsp;&nbsp; </td>
						<td ng-repeat="(key, value) in row" ng-class="isHidden(key)" ng-if="remove(key)">
							{$ value $}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

