<div class="content">
    <link rel="stylesheet" href="{{url('css/user/howitworks.css')}}">
    <link rel="stylesheet" href="{{url('css/user/carousel.min.css')}}">
    <div class="content-header margin-b-15">
        <span class="header-name">How it works</span>
    </div>

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <img src="{{url('images/user-howitwork/1.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/2.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/3.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/4.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/5.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/6.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/7.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/8.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/9.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/10.png')}}" alt="..." class="margin-b-20">
        <img src="{{url('images/user-howitwork/11.png')}}" alt="..." class="margin-b-20">
    </div>
</div>  
