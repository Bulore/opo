<div class="content">
    <link rel="stylesheet" href="{{url('css/user/store-credit.css')}}">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            $scope.$apply(function() {
                
                $scope.orderFilter = '';
                $scope.sortType = '';
                $scope.sortReverse = false;

                $scope.data = [];
                $scope.columns = $scope.data[0];

                $scope.setOrderFilter = function(val){ $scope.orderFilter = val; };

                $scope.isHidden = function(x) {
                    var hiddenColumn = ['Writer ID', 'Status', 'Subject'];
                    return (hiddenColumn.indexOf(x) != -1) ? 'mobile-display-none' : '';
                }

                $scope.SortType = function(key){
                    $scope.sortReverse = !$scope.sortReverse;
                    $scope.sortType = key;
                    $scope.data.sort(function (a, b) {
                        if (a[key] > b[key]) {
                            return $scope.sortReverse? -1 : 1;
                        }
                        if (a[key] < b[key]) {
                            return $scope.sortReverse? 1: -1;
                        }
                        return 0;
                    });
                };
            });
            deInitialise();
            initialise();
        })
    </script>
    <div class="content-header margin-b-15">
        <span class="header-name"> My Store Credit </span>
        <p>This is the money you can use to pay for your future orders. Simply choose "Store Credit" as the payment option when placing an order or contact our Support Team and ask them to apply your Store Credit via live chat.</p>
    </div>
    <div class="your-bonuses-block margin-b-30">
        Your Store Credit balanse:
        <span class="bonuses-amount">${{request()->user()->store_credit}} </span>
    </div>
    
    <div class="inner-content">
        <div class="tabs-container">
            <div class="tabs">
                <span class="tab active-tab" data-block-id="first-tab">How it works</span>
                <span class="tab" data-block-id="second-tab">Statistics</span>
                <hr>
                <div id="first-tab" class="tab-content active-tab-content">
                    <div class="table margin-r-30 mobile-margin-bottom-75">
                        <div class="table-header">How to Earn <span class="table-icon earn-icon"></span></div>
                        <div class="table-item">
                            <span class="table-column padding-bottom-20 normal-height">
                                1. Give a discount to your friend and earn money with our
                                <a href="{{url('/user/referral-program')}}" class="read-more" ng-click="leftPanelClick('{{url("/user/referral-program")}}',$event)">Referral Program!</a>
                            </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column">
                                <span> 2. Transfer the bonuses you get from our</span> 
                                <a href="{{url('/user/bonuses')}}" class="middle" class="read-more middle" ng-click="leftPanelClick('{{url("/user/bonuses")}}',$event)">Loyalty Program!</a>
                            </span>
                        </div>
                        <hr>
                        <div class="table-item"><span class="table-column">3. Receive money back for price recalculations or cancellations of your orders.</span></div>
                    </div>
                    <div class="table mobile-margin-bottom-75">
                        <div class="table-header">Benefits<span class="table-icon benefits-icon"></span></div>
                        <div class="table-item">
                            <span class="table-column">
                                <span class="circle"></span>
                                No waiting. Instant payment for your order.
                            </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column">
                                <span class="circle"></span>
                                No additional transaction fees.
                            </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column">
                                <span class="circle"></span>
                                No payment verification needed.
                            </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column">
                                <span class="circle"></span>
                                Life-time availability of the money.
                            </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column">
                                <span class="circle"></span>
                                Immediate transfer to Store Credit, while refund takes 5-7 days.
                            </span>
                        </div>
                    </div>
                </div>
                <div id="second-tab" class="tab-content">
                    <p class="oops" ng-if="data.length == 0">No Data!</p>
                    <table class='data-table'>
                        <thead>
                            <tr>
                                <th ng-repeat="(key, value) in columns" ng-click="SortType(key)" ng-class="isHidden(key)">
                                    <span>{$ key $}</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in data">
                                <td ng-repeat="(key, value) in row">
                                    {$ value $}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="save-popup">
        <div class="popup_overlay" id="popup-id">
            <div class="popup">
                <span class="close_popup">X</span>
                <p class="blue-color bold-blue font-size-171em">Save as Store Credit</p>
                <hr>
                <div class="popup-body">
                    <p class="blue-color font-size-087em">
                        Once your first order with us is approved, you automatically get 10% of the order cost as a bonus for your future orders. Then you will get bonuses EACH time you use our services and the amount of bonus will depend on the number of pages that you order:
                    </p>
                    <div class="popup-body-button">
                        <div class="your-bonuses-block">
                            Your bonuses:
                            <span class="bonuses-amount">$1.20</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="popup-footer">
                    <button class="user-button" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>