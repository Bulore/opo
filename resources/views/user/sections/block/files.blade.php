<div class="content">
    <link rel="stylesheet" href="{{url('css/user/files.css')}}">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            
            $scope.$apply(function() {

                $scope.getPicture = function(isImage){
                    return (isImage)?'picture-icon':'file-icon';
                }
                
                var urlsFile = {
                    showMore: '{{ url('/user/more-files') }}',
                    save: '{{ url('/user/save-file') }}',
                    read: "{{url('/user/read-file')}}",
                }

                $scope.files = new $scope.UserData({!! $files->toJson() !!},'Files', urlsFile, '{{request()->user()->name}}', '{{csrf_token()}}');
                
                $scope.short = function(text){
                    if(String(text).length < 100) return false;
                    return true;
                }

                $scope.setRverse = function(item){
                    if($scope.files.isUser(item)) return 'message-reverse';
                    return '';
                }
                
                $scope.getName = function(file){
                    if(!$scope.files.isUser(file)) return "Dear " + "{{$user->name}}";
                    return "To "+file.to_type+" #" + file.to_id;
                }

            });
            deInitialise();
            initialise();
        })
    </script>
    <div class="content-header margin-b-30">
        <span class="header-name">Files</span>
    </div>
    <div class="inner-content">
        <div class="tabs-container">
            <div class="tabs">
                <span class="tab active-tab" data-block-id="first-tab">All</span>
                <span class="tab" data-block-id="second-tab">
                    Unread 
                    <span class="amount-unread">
                        {$ sectionWithAmmount['Files'] $}
                    </span>
                </span>
                <hr>
                <div id="first-tab" class="tab-content active-tab-content" ng-click="changePage()">
                    <div class="inner-tab-content">
                        <div class="message-container" ng-repeat="file in files.data">
                            <div class="icon-container">
                                <div class="writer-icon bold-blue popup-button" 
                                     ng-class="files.getIcon(file)"
                                     data-popup-id="popup-id"
                                     ng-click="files.setRecipient(file)">
                                    Reply
                                </div>
                                <p ng-bind="files.getNameSender(file)"></p>
                                <p ng-if="!files.isUser(file)">#{$ file.from_id $}</p>
                            </div>
                            <div class="message" ng-click="files.setRead(file.id, files.isUser(file))" >
                                <div class="header-inf">
                                    <div ng-bind="getName(file)"></div>
                                    <div>
                                        <span class="new" ng-class="(file.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="file.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <span class="inner-message-icon middle" ng-class="getPicture(file.isimage)"></span>
                                    <span class="middle inline-block">
                                        {$ file.filename $}
                                    </span>
                                    <a href="{{url('/user/download')}}/{$ file.id $}">
                                        <span class="download inline-block middle">Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="second-tab" class="tab-content" ng-click="changePage()">
                    <div class="inner-tab-content">
                         <div class="message-container" ng-repeat="file in files.unreadData" >
                            <div class="icon-container">
                                <div class="writer-icon bold-blue popup-button" 
                                     ng-class="files.getIcon(file)"
                                     data-popup-id="popup-id"
                                     ng-click="files.setRecipient(file)">
                                    Reply
                                </div>
                                <p ng-bind="files.getNameSender(file)"></p>
                                <p ng-if="!files.isUser(file)">#{$ file.from_id $}</p>
                            </div>
                            <div class="message" ng-click="files.setRead(file.id, messages.isUser(file))" >
                                <div class="header-inf">
                                    <div ng-bind="getName(file)"></div>
                                    <div>
                                        <span class="new" ng-class="(file.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="file.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <span class="inner-message-icon middle" ng-class="getPicture(file.isimage)"></span>
                                    <span class="middle inline-block">
                                        {$ file.filename $}
                                    </span>
                                    <a href="{{url('/user/download')}}/{$ file.id $}">
                                        <span class="download inline-block middle">Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-tab-content-block">
            <center>
                <button type="button" class="blue-button border-radius-n mobile-font-size-08em" ng-click="files.showMore()" ng-if="!files.isLast">
                    Show More Files
                </button>
            </center>
        </div>
    </div>
    <div class="popup_overlay" id="popup-id">
        <div class="popup">
            <span class="close_popup">X</span>
            <div class="tabs-container">
                <div class="tabs">
                    <span class="tab active-tab" data-block-id="first-popup-tab">Upload file</span>
                    <hr>
                    <div id="first-popup-tab" class="tab-content active-tab-content">
                         <form action="" id="new-file-form">
                            {{ csrf_field() }}
                            <div class="form-item">
                                <div class="form-text">Recipient</div>
                                <div class="input-select">
                                    <div class="select-box">
                                        <span class="selected">{$ files.newUserData.to $}</span>
                                        <span></span>
                                    </div>
                                    <div class="select-option">
                                        <span class="select message-recipient active"
                                              ng-click="files.newUserData.setTo('Writer')">
                                            Writer #{$ files.newUserData.content.to_id $}
                                        </span>
                                        <span class="select message-recipient"
                                              ng-click="files.newUserData.setTo('Support')">
                                            Support
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-item flex">
                                <div class="choose-container">
                                    <label>
                                        <button class="user-button upload-button" type="button">Choose file</button>
                                        <input type="file" class="hidden" name="file">
                                    </label>
                                </div>
                                <div class="input-div">
                                    <input type="text" id="file-input" name="file" class="input" placeholder="">
                                </div>
                            </div>
                            <div class="popup-footer">
                                <button class="user-button" type="button" ng-click="files.newUserData.send()">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
