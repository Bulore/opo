<div class="content">
    <link rel="stylesheet" href="{{url('css/user/contact-us.css')}}">
    <link rel="stylesheet" href="{{url('css/callback.css')}}">
    <div class="content-header margin-b-30">
        <span class="header-name">Contact Us</span>
    </div>
    <div class="inner-content contact-page">
        <div class="button-phone">
            <div class="buttons">
                <open-pop-up class="contact-btn call-back" pop-up-id="call-me-back"><span class="btn-icon btn-phone-icon"></span>Call me back</open-pop-up>
            </div>
            <div class="buttons">
                <a href="mailto:support@originalpapersonly.com">
                    <button class="contact-btn chat-now"><span class="btn-icon btn-chat-icon"></span>Chat now</button>
                </a>
            </div>
            <div class="buttons">
                <a href="skype:support@originalpapersonly.com?chat">
                    <button class="contact-btn call-skype"> <span class="btn-icon btn-skype-icon"></span> Call us on Skype</button>
                </a>
            </div>
        </div>
        <div class="container-contacts">
            <div class="phone-contacts inline-block">
                <p><span class="phone"></span> +1-929-367-2627 <span class="font-size-078">(USA Toll-free)</span></p>
                <p><span class="phone"></span> +44-800-088-5728 <span class="font-size-078">(UK Toll-free)</span></p>
            </div>
            <div class="email-contacts inline-block">
                <p class="flex-items-center"><span class="mail"></span>support@originalpapersonly.com</p>
                <p class="flex-items-center"><span class="mail"></span>feedbackmanager@originalpapersonly.com</p>
            </div>
        </div> 
        <div id="map">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=PnzlZ26ASKVOGqpkE5NpLOfP_HE97Kb9&amp;width=100&#37;&amp;height=100&#37;&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
        </div>
        <div class="location">
            <div class="location-icon"></div>
            <div class="location-info">
                504 Pine Song Lane, <br>
                Virginia Beach, <br>
                VA, 23451
            </div>
        </div>
        <center>
            <div class="shared inline-block middle margin-t-20 padding-b-20">
                <div class="item">
                    <div class="text middle margin-r-20">
                        Like Us on Facebook for Discounts and Freebies!
                    </div>
                    <div class="shared-button middle" data-href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FOriginalpapersonly-1763588037231134%2F&ext=1473179829&hash=AeYOZ9qS5dNfJwkL">Share</div>
                </div>
                <div class="item">
                    <div class="text middle margin-r-20">
                        Follow Us on Twitter for Discounts and Freebies!
                    </div>
                    <div class="follow-button middle" data-href="https://twitter.com/intent/follow?screen_name=originalpapers1">Follow</div>
                </div>
            </div>
        </center>
    </div>
        
    @include('popups.thanks-popup')
    
    <script type="text/javascript">
        $(".shared-button").click(function() {
            var share = window.open($(this).attr("data-href"), "Share", "width=600,height=400,top=100,left=200,resizable=no,scrollbars=no");
            share.focus();
        });
        $(".follow-button").click(function() {
            var share = window.open($(this).attr("data-href"), "Share", "width=600,height=400,top=100,left=200,resizable=no,scrollbars=no");
            share.focus();
        });
    </script>
</div>