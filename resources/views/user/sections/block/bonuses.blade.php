<div class="content">
    <link rel="stylesheet" href="{{url('css/user/bonuses.css')}}">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            
            $scope.$apply(function() {
                $scope.data = {!! json_encode($bonuses) !!};
                $scope.columns = $scope.data[0];
            });
            deInitialise();
            initialise();
        })
    </script>
    <div class="content-header margin-b-15">
        <span class="header-name">Bonuses</span>
        <div class="inf-container">
            <div class="inf inf-bottom">
                Here you can find all your bonuses. You are
                welcome to use this money as the payment for
                any of your orders! Just click on the "Save as
                Store Credit" button and your bonuses will be
                transferred to your Store credit.
            </div>
        </div>
    </div>
    <div class="your-bonuses-block margin-b-30">
        Your bonuses:
        <span class="bonuses-amount">${{ request()->user()->bonuses }}</span>
    </div>
    <div class="inner-tab-content-block inline-block  padding-l-30">
        <open-pop-up class="blue-button" pop-up-id="pop-up-store-credit">Save as Store Credit</open-pop-up>
    </div>
    <div class="inner-content">
        <div class="tabs-container">
            <div class="tabs">
                <span class="tab active-tab" data-block-id="first-tab">How it works</span>
                <span class="tab" data-block-id="second-tab">Statistics</span>
                <hr>
                <div id="first-tab" class="tab-content active-tab-content">
                    <div class="table margin-r-30">
                        <div class="table-item"><span class="line-height-25 bold-blue mobile-line-height-45">Become our returning client and pay for your orders using your bonuses!</span></div>
                        <div class="table-item"><span class="line-height-25 mobile-line-height-45">Once your first order with us is approved, you automatically get 10% of the order cost as a bonus for your future orders. Then you will get bonuses EACH time you use our services and the amount of bonus will depend on the number of pages that you order:</span></div>
                        <div class="table-item align-center mobile-justify-center"><span class="line-height-25 mobile-line-height-45">
                        <span class="bold-900">1-30</span> pages - <span class="bold-900">5%</span> BONUS <br>
                        <span class="bold-900">30-50</span> pages - <span class="bold-900">10%</span> BONUS <br>
                        <span class="bold-900">50+</span> pages - <span class="bold-900">15%</span> BONUS</span>
                    </div>
                    </div>
                    <div class="table">
                        <div class="table-item"><span class="number">1</span><span class="table-column bold-blue margin-top-7">Place an order</span><span class="bonus-table-icon bonus-file-icon"></span></div>
                        <hr>
                        <div class="table-item"><span class="number">2</span><span class="table-column bold-blue margin-top-7">Get your bonus</span><span class="bonus-table-icon bonus-hand-icon"></span></div>
                        <hr>
                        <div class="table-item"><span class="number">3</span><span class="table-column bold-blue">Click on the Save as Store Credit button to transfer your payment</span><span class="bonus-table-icon bonus-button-icon"></span></div>
                        <hr>
                        <div class="table-item"><span class="number">4</span><span class="table-column bold-blue">Wait up to 24 hours till your request is processed</span><span class="bonus-table-icon bonus-time-icon"></span></div>
                        <hr>
                        <div class="table-item"><span class="number">5</span><span class="table-column bold-blue">Place a new order through your control panel and pay with your bonuses!</span><span class="bonus-table-icon bonus-moneybox-icon"></span></div>
                    </div>
                    <p class="blue-color line-height-25 font-size-087em mobile-line-height-45">However, that is not all! Some of our very loyal clients have lifetime discounts and use them actively. In case you prefer to use your lifetime discount instead of the new bonus program, you can! When placing an order you can decide yourself and either, apply your lifetime discount or let the bonus system do its work. Thus, you either save by using your discount right away or you save with our new bonus program after approval for your next orders!</p>
                </div>
                <div id="second-tab" class="tab-content">
                    <p class="oops" ng-if="data.length == 0">No Data!</p>
                    <table class='data-table table-flex'>
                        <thead>
                            <tr>
                                <th ng-repeat="(key, value) in columns" ng-click="SortType(key)">
                                    <span>{$ key $}</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in data">
                                <td ng-repeat="(key, value) in row">
                                    {$ value $}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('user.popups.pop-up-store-credit')
    @include('user.popups.pop-up-success-bonuses')
    <script>
        $('.common-button').click(function() {
            $.ajax({
                url: "/user/bonuses-as-store-credit",
                data: {},
                method: "GET",
                beforeSend: function(){
                    $('#container-floatingCirclesG').css('opacity', 1);
                },
                complete: function(){
                    $('#container-floatingCirclesG').css('opacity', 0);
                },
                success: function(data){
                    document.querySelector('pop-up[pop-up-id="pop-up-store-credit"]').close();
                    document.querySelector('pop-up[pop-up-id="pop-up-success-bonuses"]').open(200);
                },
                error: function(data){
                    popUpError.open();
                }
            }); 
        });
    </script>
</div>

