<div class="content">
    <link rel="stylesheet" href="{{url('css/user/referral.css')}}">
    <div class="content-header margin-b-30">
        <span class="header-name">Referral Program</span>
    </div>
    <div class="inner-content">
        <tab-panel>
        	<tab-nav>
        		<tab-nav-item class="active">General</tab-nav-item>
        		<tab-nav-item>Promotion</tab-nav-item>
        		<tab-nav-item>Statistics</tab-nav-item>
        		<tab-nav-item>Payment history</tab-nav-item>
        		<tab-nav-item class="m-display-none">FAQ</tab-nav-item>
        	</tab-nav>
        	<tab-content>
        		<tab-content-item>
        			<div class="flex m-row">
        				<div class="flex-1 margin-r-30">
        					<div class="table">
					            <div class="table-header bold-blue">  About  </div>
					            <div class="table-item">
					                <div>
					                	<p>
						                	We are happy to inform you, that we have launched referral program, which will help you increase your earnings or save up on your orders at OriginalPapersOnly.com
						                </p>
						                <p>
						                	From now on, you will be able to receive extra cash for every client, whom you refer to our website. The brilliance of this program is that you do not simply receive additional 10% bonus from every order of your referral client, but your referrals get 10% discount on his/her first order. You can either use the amount earned on the orders you place at our website or withdraw your balance.
						                </p>
					                </div>
					            </div>
					        </div>
        				</div>
        				<div class="flex-1">
        					<div class="table">
					            <div class="table-header">  Referral discount and link  </div>
					            
					            <div class="table-item flex">
					                <div class="flex-2">
					                	<div class="padding-b-10 m-padding-b-20">Referral discount code:</div>
					                	<div>
					                		{{-- <button class="blue-button sm-btn" id="copy-discount-code">copy</button> --}}
					                	</div>
					                </div>
					                <div class="margin-l-30 flex-2 flex jc-right full-width">
					                	<div class="blue-block" ng-bind="referalDiscountCode"></div>
					                </div>
					            </div>
					            <hr>
					            <div class="table-item flex">
					            	<div class="felx-1 margin-l-10 padding-b-10">Referral link:</div>
					            	<div class="flex-1 flex jc-right full-width padding-b-10"> 
										{{-- <button class="blue-button sm-btn" id="copy-referal-link">copy</button> --}}
					            	</div>
					            </div>
					            <div class="table-item">
					            	<div class="blue-block full-width" ng-bind="referalDiscountLink"></div>
					            </div>
								<hr>
								<div class="table-item">
									<p>
										All you need to do is to give your special promo code or your referral link to your friends. Once they use this code or follow your link to place an order at OriginalPapesOnly.com, they will receive 10% off the price and you will get 10% from the order.
									</p>
								</div>
					        </div>
        					
        				</div>
        			</div>

        			<div class="flex m-row margin-t-50">
        				<div class="flex-1 margin-r-30">
        					<div class="table">
					            <div class="table-header">  Payments detail  </div>
					            <div class="table-item">
				                	<p>
					                	Submit your payment details and withdraw money earned on your referrals. It is that easy!
					                </p>
					            </div>
					            @if(!request()->user()->payment_details)
					            <div class="table-item full-width">
					                <div class="full-width">
					                	<div class="bold-blue margin-l-30 m-margin-b-30">Payment Method:</div>
					                	<select-box placeholder="PayPal" >
					                		<select-box-header></select-box-header>
					                		<select-box-content>
					                			<select-box-option ng-click="paymentMethod = 'PayPal'">PayPal</select-box-option>
					                		</select-box-content>
					                	</select-box>
					                </div>
					            </div>
					            <div class="table-item full-width margin-t-20">
					                <div class="full-width">
					                	<div class="bold-blue margin-l-30 m-margin-b-30">Payment Details:</div>
					                	<input type="text" class="input" placeholder="Enter your payment email / wallet number" ng-model="paymentDetails">
					                </div>
					            </div>
					            <div class="table-item full-width margin-t-20">
					                <div class="full-width flex jc-right">
					                	<button class="blue-button submit" ng-click="sendPaymentMethod()">Submit</button>
					                </div>
					            </div>
					            @else
					            <div class="table-item full-width">
				                	<div class="margin-l-30 m-margin-b-30">Payment Method:</div>
				                	<div class="bold-blue margin-l-30 m-margin-b-30">{{ request()->user()->payment_details }}</div>
					            </div>
				             	<div class="table-item full-width margin-t-10">
				                	<div class="margin-l-30 m-margin-b-30">&nbsp;Payment Details:</div>
				                	<div class="bold-blue margin-l-30 m-margin-b-30">{{ request()->user()->payment_method }}</div>
					            </div>
					            @endif
					            
					        </div>
        				</div>
        				<div class="flex-1">
        					<div class="table">
					            <div class="table-header">  TOP 10 <span class="h-gray">referrals’ income for the last month</span>  </div>
					            <div class="table-item flex ai-top">
					                <div class="flex-1 margin-r-30">
					                	@for($i = 0; $i < 5; $i++)
					                		@if(isset($top10[$i]))
					                		<center>
					                			{{ $i+1 }} place - {{ $top10[$i] }} $
					                		</center>
					                		<hr class="sm-hr">
					                		@endif
					                	@endfor
					                </div>
					                <div class="flex-1">
					                	@for($i = 5; $i <= 10; $i++)
					                		@if(isset($top10[$i]))
					                		<center>
					                			{{ $i+1 }} place - {{ $top10[$i] }} $
					                		</center>
					                		<hr class="sm-hr">
					                		@endif
					                	@endfor
					                </div>
					            </div>

					        </div>
        					
        				</div>
        			</div>
        		</tab-content-item>
        		<tab-content-item>
        			<div class="flex m-row">
        				<div class="flex-1 margin-r-30">
        					<div class="table">
					            <div class="table-header bold-blue">  Offline  </div>
					            <div class="table-item">
					                <div class="full-width">
					                	<h4>
					                		This is your personal business card:
					                	</h4>
					                	<div class="business-card-img">
					                		<div>{$ referalDiscountCode $}</div>
					                	</div>
					                	<div class="flex jc-right">
					                		<a href="{{ url('/user/promo-card') }}"><button class="blue-button margin-r-30">Save</button></a>
					                	</div>
					                </div>
					            </div>
					            <hr>
					            <div class="table-item">
					            	<div>
					            		<h4>Easily print out your personal business cards and:</h4>
						            	<ul class="blue-list padding-l-10">
						            		<li>
						            			Give them out at your health club
						            		</li>
						            		<li>
						            			Give them to friends and family
						            		</li>
						            		<li>
						            			Give them to your colleagues at work
						            		</li>
						            		<li>
						            			Pass some out to your neighbors
						            		</li>
						            		<li>
						            			Add one to your personal mailing
						            		</li>
						            	</ul>
					            	</div>
					            </div>
					            
					            <div class="table-header bold-blue margin-t-50">  Blogs  </div>
					            <div class="table-item ">
					            	<div>
					            		<h4>
						            		When it comes to your blog, there are many ways to use it in order to promote your OriginalPapersOnly.com referral code:
						            	</h4>
						            	<ol class="blue-list padding-l-n">
						            		<li>
						            			Add a badge. Either create your own custom badge or download one from the ones we have created here. After you download the graphic, you can put your coupon code on the graphic itself and be sure to link this graphic to the website and provide your referral code.
						            		</li>
						            		<li>
						            			Write a post about essay writing and provide URLs to our website. Do not forget to mention your personal referral code.
						            		</li>
						            		<li>
												Use both Facebook and Twitter accounts to share the links to your blog posts.
						            		</li>
						            	</ol>
					            	</div>
					            </div>
					        </div>
        				</div>
        				<div class="flex-1">
        					<div class="table">
					            <div class="table-header bold-blue">  Facebook  </div>
					            <div class="table-item">
					                <div>
					                	<h4>
					                		There are several ways to promote your OriginalPapersOnly.com referral code on Facebook:
					                	</h4>
					                	<ol class="blue-list padding-l-n">
					                		<li>
					                			Create a Facebook page to promote your referral code. On that page, which can be geared around writing and helpful information for students, you can post links to OriginalPapersOnly.com and provide your referral code.
					                		</li>
					                		<li>
					                			Post Custom URLs to your current Facebook profile to specific OriginalPapersOnly.com samples or to the OriginalPapersOnly.com homepage, and provide your referral code.
					                		</li>
					                	</ol>
					                </div>
					            </div>
					            
					            <div class="table-header bold-blue margin-t-50">  Twitter  </div>
					            <div class="table-item ">
					            	<div>
					            		<h4>
						            		To promote your referral code on Twitter, you can do the following:
						            	</h4>
						            	<ol class="blue-list padding-l-n">
						            		<li>
						            			Create a special Twitter profile to promote your gift code. Within your Twitter account, which may be geared around writing tips and helpful information for students, you can post links to OriginalPapersOnly.com and provide your personal referral code.
						            		</li>
						            		<li>
						            			Post Custom URLs on your regular Twitter profile to OriginalPapersOnly.com samples or to the OriginalPapersOnly.com homepage, and provide your referral code.
						            		</li>
						            	</ol>
					            	</div>
					            </div>

					            <div class="table-header bold-blue margin-t-50">  Youtube  </div>
					            <div class="table-item ">
					            	<div>
					            		<h4>
						            		Find out how to promote your discount code on Youtube:
						            	</h4>
						            	<ol class="blue-list padding-l-n">
						            		<li>
						            			Create a special Youtube channel to promote your code. This channel will allow you to upload videos focusing on essay writing tips or student's help. Make sure to mention your referral code in the video itself and add it to the caption below the video.
						            		</li>
						            		<li>
						            			Use both Facebook and Twitter to share the links to your videos.
						            		</li>
						            	</ol>
					            	</div>
					            </div>
					        </div>
        				</div>
        			</div>
        		</tab-content-item>
        		<tab-content-item>
        			<table class='data-table table-flex m-table-normal'>
						<thead>
							<tr>
						        <th ng-repeat="item in statsColumns" ng-click="SortType(key)" ng-class="isHidden(key)">
					                <span ng-repeat="(key, value) in item"> {$ value $} </span>
						        </th>
						    </tr>
						</thead>
						<tbody>
							<tr ng-repeat="row in stats">
								<td ng-repeat="item in statsColumns" ng-class="isHidden(key)">
									<span ng-repeat="(key, value) in item"> <span ng-if="key == 'amount'">$</span>{$ row[key] $} </span>
								</td>
							</tr>
						</tbody>
					</table>
         		</tab-content-item>
        		<tab-content-item>
        			<table class='data-table table-flex m-table-normal'>
						<thead>
							<tr>
						        <th ng-repeat="item in paymentsColumns" ng-click="SortType(key)" ng-class="isHidden(key)">
					                <span ng-repeat="(key, value) in item"> {$ value $} </span>
						        </th>
						    </tr>
						</thead>
						<tbody>
							<tr ng-repeat="row in payments">
								<td ng-repeat="item in paymentsColumns" ng-class="isHidden(key)">
									<span ng-repeat="(key, value) in item"> {$ row[key] $} </span>
								</td>
							</tr>
						</tbody>
					</table>
        		</tab-content-item>
        		<tab-content-item>
        			<drop-down-ordered-list class="none-order" data-open-only-one>
        				<ordered-list-item>
        					<ordered-list-header>How can I sign up for the Program?</ordered-list-header>
        					<ordered-list-content>
        						1. Locate the referral discount code in your account in the section Referral Program.
								2. Share the 10% discount code with your friends and receive 10% from all their orders.
        					</ordered-list-content>
        				</ordered-list-item>
        				<ordered-list-item>
        					<ordered-list-header>How many times will the code work?</ordered-list-header>
        					<ordered-list-content>
        						However, all the future orders can get a one-time 5% or 10% discount depending on the price of your order. Read more about our discount policy on <a href="{{url('/prices')}}">this page.</a>
        					</ordered-list-content>
        				</ordered-list-item>
        				<ordered-list-item>
        					<ordered-list-header>Both my friend and I will be able to use the discount code, right?</ordered-list-header>
        					<ordered-list-content>
        						It's only your friend who can enter the discount code in the order form. Please note, however, that we offer one time discounts, which you are welcome to benefit from. Find out more about our one-time discounts <a href="{{url('/prices')}}">here.</a>
        					</ordered-list-content>
        				</ordered-list-item>
        				<ordered-list-item>
        					<ordered-list-header>Where can I see my earnings?</ordered-list-header>
        					<ordered-list-content>
        						See the section Referral Program in your personal account to find out how much money you've earned.
        					</ordered-list-content>
        				</ordered-list-item>
        				<ordered-list-item>
        					<ordered-list-header>What is the money withdrawal procedure?</ordered-list-header>
        					<ordered-list-content>
        						In order to withdraw your earnings, you will need to send a request to support@OriginalPapersOnly.com before the 12th of every month. Please note you can only withdraw $100 or more. The money will come between the 16th and the 20th.
        					</ordered-list-content>
        				</ordered-list-item>
        				<ordered-list-item>
        					<ordered-list-header>What are the supported payment systems?</ordered-list-header>
        					<ordered-list-content>
        						We support PayPal.
        					</ordered-list-content>
        				</ordered-list-item>
        			</drop-down-ordered-list>
        		</tab-content-item>
        	</tab-content>
        </tab-panel>
    </div>

    <script>
    	function selectText(containerid) {
	        if (document.selection) {
	            var range = document.body.createTextRange();
	            range.moveToElementText(document.querySelector(containerid));
	            range.select();
	        } else if (window.getSelection) {
	            var range = document.createRange();
	            range.selectNode(document.querySelector(containerid));
	            window.getSelection().addRange(range);
	        }
	    }

		$('#copy-discount-code').click(function(){ 
	    	// $('[ng-bind="referalDiscountCode"]').removeClass('selected-n');
			selectText('[ng-bind="referalDiscountCode"]'); 
			document.execCommand('copy'); 
	    	// $('[ng-bind="referalDiscountCode"]').addClass('selected-n');
		});
	    $('#copy-referal-link').click(function(){ 
	    	// $('[ng-bind="referalDiscountLink"]').removeClass('selected-n');
	    	selectText('[ng-bind="referalDiscountLink"]'); 
	    	document.execCommand('copy'); 
	    	// $('[ng-bind="referalDiscountLink"]').addClass('selected-n');
	    });

		setTimeout( function(){
			document.querySelector('tab-panel tab-content-item:first-child').setActive();
		}, 100);

		$(document).ready(function(){
			var appElement = document.querySelector('[ng-app=app]');
		    
		    var $scope = angular.element(appElement).scope();
		    
		    $scope.$apply(function() {
		    	
		    	$scope.orderFilter = '';
    			$scope.sortType = '';
    			$scope.sortReverse = false;

    			$scope.referalDiscountCode = '{{Auth::user()->ref_code}}';
				$scope.referalDiscountLink = '{{url('/order')."/?discount_code=".Auth::user()->ref_code}}';

		        $scope.stats = {!! $stats->toJson() !!};
		        $scope.statsColumns = [
		        	{ "date": "Date" },
		        	{ "client_id": "Client ID" },
		        	{ "amount": "Amount" },
		        	{ "out": "Out" }
		        ]

		        $scope.payments = {!! $payments->toJson() !!};
		        $scope.paymentsColumns = [
		        	{ "date": "Date" },
		        	{ "type": "Type"},
		        	{ "amount": "Amount"},
		        ];
		        
  				$scope.setOrderFilter = function(val){ $scope.orderFilter = val; };

		        $scope.isHidden = function(x) {
		        	var hiddenColumn = [];
		        	return (hiddenColumn.indexOf(x) != -1) ? 'mobile-display-none' : '';
		        }

		        $scope.remove = function(x){
		        	var removeColumn = [];
		        	return (removeColumn.indexOf(x) != -1) ? false : true;
		        }

		        $scope.SortType = function(key){
			        $scope.sortReverse = !$scope.sortReverse;
			        $scope.sortType = key;
			        $scope.data.sort(function (a, b) {
			            if (a[key] > b[key]) {
			                return $scope.sortReverse? -1 : 1;
			            }
			            if (a[key] < b[key]) {
			                return $scope.sortReverse? 1: -1;
			            }
			            return 0;
			        });
			    };

			    
				$scope.paymentMethod = 'PayPal';
				$scope.paymentDetails = '';

			    $scope.sendPaymentMethod = function(){
					$.ajax({
			            url: "/user/set-ref-payment-method",
			            data: {
			            	"method": $scope.paymentMethod,
			            	"details": $scope.paymentDetails
			            },
			            method: "POST",
			            headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
			            beforeSend: function(){
			                $('#container-floatingCirclesG').css('opacity', 1);
			            },
			            complete: function(){
			                $('#container-floatingCirclesG').css('opacity', 0);
			            },
			            success: function(data){
			                $scope.leftPanelClick('{{ url("/user/referral-program") }}');
			            },
			            error: function(data){
			            	popUpError.open();
			            }
			        });
			    	
			    }
		    });
		})
    </script>
</div>