<div class="content">
    <link rel="stylesheet" href="{{url('css/user/authorization.css')}}">
    <div class="content-header margin-b-30">
        <span class="header-name">Authorization</span>
        <div class="inf-container">
            <div class="inf inf-bottom blue-color">
                There are a lot of bad guys out there and we want to keep you safe. Please help us do so by authorizing your email, phone and payment. This will also allow us to keep you fully updated about the progress of your order
            </div>
        </div>
    </div>
    <div class="inner-content">
        <div class="table margin-r-30 float-l mobile-margin-b-50">
            <div class="table-header orange-color orange-background">Email authorization <span class="not-authorized font-size-087em">Not authorized</span><span class="table-icon email-icon"></span></div>
            <div class="table-item orange-color"><span class="line-height-25 mobile-line-height-45 mobile-font-size-08em max-height-9">To confirm your email simply follow the link in the authorization email sent to <span class="bold-blue"> {{ request()->user()->email }} </span> or copy the nine-digit code in the field below. You should receive the email within 5 minutes. In case you need to change your email, you can do so at any type by contacting our Customer Support Team.</span></div>

            <hr>
            <div class="attempts">
                <p class="blue-color font-size-087em bold-blue">You have 3 attempts left to submit the code</p>
                <input type="text" placeholder="Type code" class="mobile-margin-bottom-35">
                <a href="" class="send-code font-size-087em blue-color"> <span class="load-icon"></span>Send email authorization code
                    <div class="inf-container">
                        <div class="inf">
                            Send authorization code again
                        </div>
                    </div>
                </a>
                <div class="authorization-button-container mobile-margin-top-35 m-margin-b-50">
                    <button class="blue-button">Submit</button>
                </div>
            </div>
        </div>
        <div class="table mobile-margin-bottom-75">
            <div class="table-header orange-color orange-background">Phone authorization <span class="not-authorized font-size-087em">Not authorized</span><span class="table-icon smartphone-icon"></span></div>
            <div class="table-item orange-color"><span class="line-height-25 mobile-line-height-45 max-height-9 mobile-font-size-08em">You indicated that your phone number is <span class="bold-blue"> {{ request()->user()->phone }} </span> . You can change it at any time by contacting our Customer Support Team. Just click send phone authorization code and input the code from the text message in the field below</span></div>
            <a href="" class="send-code font-size-087em blue-color mobile-margin-left-20"> <span class="load-icon"></span>Send phone authorization code
                <div class="inf-container">
                    <div class="inf">
                        Send authorization code again
                    </div>
                </div>
            </a>
        </div>
        <div class="table margin-top-20 mobile-margin-bottom-75">
            <div class="table-header">Payment authorization <span class="authorized font-size-087em">Authorized</span><span class="table-icon credit-icon"></span></div>
            <div class="table-item"><span class="line-height-25 mobile-line-height-45 mobile-font-size-08em">Unfortunately, credit card theft occurs too often in the modern world. That's why we may ask you to provide copies of your documents to secure your payment. There's a nice picture guide about it <a class="blue-href" href="{{url('/payment-auth')}}">here</a></span></div>
        </div>
    </div>
</div>
