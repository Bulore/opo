<div class="content">

    <link rel="stylesheet" href="{{url('css/user/preferred.css')}}">

    <div class="content-header margin-b-15">
        <span class="header-name">Preferred writers</span>
    </div>
    <script>
 	$(document).ready(function(){
		var appElement = document.querySelector('[ng-app=app]');
	    var $scope = angular.element(appElement).scope();
	    $scope.$apply(function() {
	        $scope.data = {!! json_encode($writers) !!};
	        $scope.columns = $scope.data[0];
	        
	        $scope.isHover = false;
	        
	        $scope.orders = [];	
        	$scope.currentPage = 1;
        	$scope.pageChangeHandler = function(num){
		        $scope.currentPage = num;
		    };
	        $scope.setOrders = function(index){
	        	$scope.orders = $scope.data[index].orders;
	        	$scope.isHover = true;
	        	var constTop = 190;
	        	if($(document).width()<1169){
        			constTop = 333;
	        	}
	        	var top = (constTop + index*52)+'px';
	        	$('.orders-eye').css({'top':top});
	        }
	        $scope.unSetOrders = function(){
	        	$scope.isHover = false;
	        }

	        $scope.removeWriter = function(id){
        		$.ajax({
		            url: "{{url('user/delete-preferred-writer')}}",
		            data: { 'writer_id':id },
		            method: "POST",
                    headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
		            success: function(data){
		                alert('success');
		            },
		            error: function(){
		                alert('error');
		            }
        		});
	        };

	        $scope.makeOrder = function(id){
	        	$scope.leftPanelClick("{{url('user/new-order')}}", undefined, {'preferrd_writer_id':id});
	        }
	        $scope.isHidden = function(x) {
	        	var hiddenColumn = ['Bookmark Date'];
	        	return (hiddenColumn.indexOf(x) != -1) ? 'mobile-display-none' : '';
	        }
	    });
	})
	</script>
	<div class="inner-content" >
		<p class="oops ng-scope" ng-if="data.length == 0">No Data!</p>
		<table class='data-table'>
			<thead>
				<tr>
			        <th ng-repeat="(key, value) in columns" ng-click="SortType(key)" ng-class="isHidden(key)" ng-if="key != 'orders'">
		                <span ng-class="(sortType==key)?'active':''">{$ key $}</span>
			        </th>
			        <th></th>
			    </tr>
			</thead>
			<tbody>
				<tr ng-repeat="row in data" class="visible-on-hover" ng-init="rowIndex = $index">
					<td ng-repeat="(key, value) in row" ng-class="isHidden(key)" ng-if="key != 'orders'">
						<span ng-if="key=='Orders'" 
						      class='eye-icon middle item-hover' 
							  ng-mouseenter="setOrders(rowIndex)"
							  ng-mouseleave="unSetOrders()"
							  >
						  </span>
						{$ value $}
					</td>
					<td>
						<span class="plus-icon-block float-l item-hover" ng-click="makeOrder(row['Writer ID'])"></span>
						<span class="middle orange-color item-hover make-order" ng-click="makeOrder(row['Writer ID'])">Make order</span>
						<span class="remove-icon-block float-r item-hover" ng-click="removeWriter(row['Writer ID'])"></span>
					</td>
				</tr>
				<div ng-class="(isHover)?'open':''" class="orders-eye" 
					 ng-mouseenter="isHover=true"
					 ng-mouseleave="unSetOrders()"
					 >
					<div class="orders-container" 
					     dir-paginate="order in orders | itemsPerPage: 1 : 'pagination1'"
					     pagination-id="pagination1"
					    >
						<div class="row">
							<div>Order ID</div>
							<div>{$ order.id $}</div>
						</div>
						<div class="row">
							<div>Creation date</div>
							<div>{$ order.date $}</div>
						</div>
						<div class="row">
							<div>Topic</div>
							<div>{$ order.topic $}</div>
						</div>
						<div class="row">
							<div>Subject</div>
							<div>{$ order.subject $}</div>
						</div>	
					</div>
					<dir-pagination-controls 
						 pagination-id="pagination1" 
						 max-size="1" 
						 direction-links="true" 
						 boundary-links="true" 
						 on-page-change="pageChangeHandler(newPageNumber)"
						></dir-pagination-controls> 
				</div>
			</tbody>
		</table>
	</div>
</div>
