<div class="content">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            
            $scope.$apply(function() {

                var urlsMessage = {
                    showMore: '{{ url('/user/more-messages') }}',
                    save: '{{ url('/user/save-message') }}',
                    read: "{{url('/user/read-message')}}",
                }

                $scope.messages = new $scope.UserData({!! $messages->toJson() !!},'Messages',urlsMessage, '{{request()->user()->name}}','{{csrf_token()}}');

                $scope.typesMessage = ['Additional questions about the paper', 'I need a revision', 'Compliant', 'Other'];
                
                $scope.short = function(text){
                    if(String(text).length < 100) return false;
                    return true;
                }

                $scope.setRverse = function(item){
                    if($scope.messages.isUser(item)) return 'message-reverse';
                    return '';
                }
                
                $scope.getName = function(message){
                    if(!$scope.messages.isUser(message)) return "Dear " + "{{$user->name}}";
                    return "To "+message.to_type+" #" + message.to_id;
                }
            });
            deInitialise();
            initialise();
        })
    </script>
    <div class="content-header margin-b-30">
        <span class="header-name">Messages</span>
    </div>
    <div class="inner-content">
        <div class="tabs-container">
            <div class="tabs">
                <span class="tab active-tab" data-block-id="first-tab">All</span>
                <span class="tab" data-block-id="second-tab">
                    Unread 
                    <span class="amount-unread">
                        {$ sectionWithAmmount['Messages'] $}
                    </span>
                </span>
                <hr>
                <div id="first-tab" class="tab-content active-tab-content">
                    <div class="inner-tab-content">
                        <div class="message-container" 
                             ng-repeat="message in messages.data" 
                            >
                            <div class="icon-container">
                                <div class="writer-icon bold-blue" ng-class="messages.getIcon(message)">
                                    <div ng-click="messages.setRecipient(message)" 
                                         ng-if="!messages.isUser(message)" 
                                         data-popup-id="popup-id"
                                         class="popup-button">
                                        Reply
                                    </div>
                                </div>
                                <p ng-bind="messages.getNameSender(message)"></p>
                                <p ng-if="!messages.isUser(message)">#{$ message.from_id $}</p>
                            </div>
                            <div class="message" ng-click="messages.setRead(message.id, messages.isUser(message))">
                                <div class="header-inf">
                                    <div ng-bind="getName(message)"></div>
                                    <div>
                                        <span class="new" ng-class="(message.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="message.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <div class="message-text">{$ message.text $}</div>
                                    <span class="read-more inline-block" 
                                          ng-if="short(message.text)">
                                        Read more
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="second-tab" class="tab-content">
                    <div class="inner-tab-content">
                        <div class="message-container" 
                             ng-repeat="message in messages.unreadData" 
                            >
                            <div class="icon-container">
                                <div class="writer-icon bold-blue" ng-class="messages.getIcon(message)">
                                    <div ng-click="messages.setRecipient(message)" 
                                         ng-if="!messages.isUser(message)" 
                                         data-popup-id="popup-id"
                                         class="popup-button">
                                        Reply
                                    </div>
                                </div>
                                <p ng-bind="messages.getNameSender(message)"></p>
                                <p ng-if="!messages.isUser(message)">#{$ message.from_id $}</p>
                            </div>
                            <div class="message" ng-click="messages.setRead(message.id, messages.isUser(message))">
                                <div class="header-inf">
                                    <div ng-bind="getName(message)"></div>
                                    <div>
                                        <span class="new" ng-class="(message.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="message.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <div class="message-text">{$ message.text $}</div>
                                    <span class="read-more inline-block" 
                                          ng-if="short(message.text)">
                                        Read more
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inner-tab-content-block">
                <center>
                    <button type="button" class="blue-button border-radius-n mobile-font-size-08em" ng-click="messages.showMore()" ng-if="!messages.isLast">
                        Show More Messages
                    </button>
                </center>
            </div>
        </div>
    </div>
    <div class="popup_overlay" id="popup-id">
        <div class="popup">
            <span class="close_popup">X</span>
            <div class="tabs-container">
                <div class="tabs">
                    <span class="tab active-tab" data-block-id="first-popup-tab">Create new message</span>
                    <hr>
                    <div id="first-popup-tab" class="tab-content active-tab-content">
                        <form action="" id="new-message-form">
                            {{ csrf_field() }}
                            <div class="form-item">
                                <div class="form-text">Recipient</div>
                                <div class="input-select">
                                    <div class="select-box">
                                        <span class="selected">{$ messages.newUserData.to $}</span>
                                        <span></span>
                                    </div>
                                    <div class="select-option">
                                        <span class="select message-recipient active"
                                              ng-click="messages.newUserData.setTo('Writer')">
                                            Writer #{$ messages.newUserData.content.to_id $}
                                        </span>
                                        <span class="select message-recipient"
                                              ng-click="messages.newUserData.setTo('Support')">
                                            Support
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-info">
                                Mind that for security purposes all questions regarding personal or billing information must be addressed directly to the Customer Support Representatives. The writer will not be able to assist you on this matter. Moreover, this way we will ensure your privacy and safety.
                            </div>
                            <div class="form-item">
                                <div class="form-text">Type of message</div>
                                <div class="input-select">
                                    <div class="select-box">
                                        <span class="selected">Choose type of message</span>
                                        <span></span>
                                    </div>
                                    <div class="select-option">
                                        <span class="select message-type"
                                              ng-repeat="type in typesMessage"
                                              ng-click="messages.newUserData.content.type = type">
                                            {$ type $}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-item">
                                <div class="form-text">Message</div>
                                <textarea name="text" id="" cols="30" rows="10" placeholder="1000 symbols max" ng-model="messages.newUserData.content.text"></textarea>
                            </div>
                            <div class="popup-footer">
                                <button class="user-button" type="button" ng-click="messages.newUserData.send()">&nbsp&nbsp Send &nbsp&nbsp</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
