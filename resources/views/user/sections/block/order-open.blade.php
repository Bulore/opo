<div class="content">
    <link rel="stylesheet" href="{{url('css/user/order-open.css')}}">
    <script>
        $(document).ready(function(){
            var appElement = document.querySelector('[ng-app=app]');
            var $scope = angular.element(appElement).scope();
            
            $scope.$apply(function() {
                
                
                
                var urlsMessage = {
                    showMore: '{{ url('/user/order-more-messages').'/?order_id='.$order->id }}',
                    save: '{{ url('/user/save-message/') }}',
                    read: "{{ url('/user/read-message')}}",
                }

                $scope.messages = new $scope.UserData({!! $order->messages()->with('sender', 'recipient')->orderBy('created_at', 'desc')->limit(10)->get()->toJson() !!}, 'Messages', urlsMessage, '{{request()->user()->name}}', '{{csrf_token()}}', true);
                $scope.messages.newUserData.content.paper_id = '{{$order->id}}';
                $scope.messages.newUserData.content.to_type = '{{ ($order->writer_id != 0) ? 'Writer' : 'Manager' }}';
                $scope.messages.newUserData.content.to_id = '{{ ($order->writer_id != 0) ? $order->writer_id : $order->support_id }}';


                var urlsFile = {
                    showMore: '{{ url('/user/order-more-files').'/?order_id='.$order->id  }}',
                    save: '{{ url('/user/save-file') }}',
                    read: "{{ url('/user/read-file')}}",
                }

                $scope.files = new $scope.UserData({!! $order->files()->with('sender', 'recipient')->orderBy('created_at', 'desc')->limit(10)->get()->toJson() !!},'Files', urlsFile, '{{request()->user()->name}}', '{{csrf_token()}}', true);
                $scope.files.newUserData.content.paper_id = '{{$order->id}}';
                $scope.files.newUserData.content.to_type = '{{ ($order->writer_id != 0) ? 'Writer' : 'Manager' }}';
                $scope.files.newUserData.content.to_id = '{{ ($order->writer_id != 0) ? $order->writer_id : $order->support_id }}';

                $scope.typesMessage = ['Additional questions about the paper', 'I need a revision', 'Compliant', 'Other'];
                
                $scope.short = function(text){
                    if(String(text).length < 100) return false;
                    return true;
                }

                $scope.setRverse = function(item){
                    if($scope.messages.isUser(item)) return 'message-reverse';
                    return '';
                }
                
                $scope.getName = function(message){
                    if(!$scope.messages.isUser(message)) return "Dear " + "{{request()->user()->name}}";
                    return "To "+message.to_type+" #" + message.to_id;
                }
                
                $scope.sectionWithAmmount['CurrentMessages'] = parseInt('{{$order->messages()->where('isread', 0)->count()}}');
                $scope.sectionWithAmmount['CurrentFiles'] = parseInt('{{$order->files()->where('isread', 0)->count()}}');
               
                $scope.getIcon = function(accessLevel){
                    var iconName = 'message-support-icon';
                    if(accessLevel=='{{App\User::USER_ACCESS_LEVEL}}'){
                        iconName = 'message-user-icon';
                    }else if(accessLevel=='{{App\User::WRITER_ACCESS_LEVEL}}'){
                        iconName = 'message-writer-icon';
                    }
                    return iconName;
                }
               
            });
            deInitialise();
            initialise();
        })
    </script>
    <div class="content-header">
        <span class="header-name">Order</span>
        <span class="id">#{{$order->id}}</span>
        <div class="order-info">
            <span>
                <span class="bold-blue">
                    Status 
                    <span class="arrow"></span>
                </span> 
                {{$order->status}}
            </span>
            @if($order->writer_id != 0)
            <span class="mobile-display-none">
                <span class="bold-blue">
                    Writer ID 
                    <span class="arrow"></span>
                </span>
                #{{$order->writer_id}} 
                <span class="icon-hearth" ng-click="addPreferredWriter()" ng-class="preferredWriter()"></span>
            </span>
            @endif
            <span class="mobile-display-none">
                <span class="bold-blue mobile-display-none">
                    Price 
                    <span class="arrow"></span>
                </span>
                {{$order->price}} $
            </span>
        </div>
    </div>
    <div class="status-bar">
        <div class="item @if($order->internal_status >= 1) completed @endif">Payment Received</div>
        <div class="item @if($order->internal_status >= 3) completed @endif">Writer Assigned</div>
        <div class="item @if($order->internal_status >= 5) completed @endif">Paper Completed</div>
        <div class="item @if($order->internal_status >= 7) completed @endif">Paper Delivered</div>
        <div class="item @if($order->internal_status >= 8) completed @endif">Approved</div>
        <div class="active">
            <div class="item @if($order->internal_status == 1 || $order->internal_status == 2) show @endif"></div>
            <div class="item @if($order->internal_status == 3 || $order->internal_status == 4) show @endif"></div>
            <div class="item @if($order->internal_status == 5 || $order->internal_status == 6) show @endif"></div>
            <div class="item @if($order->internal_status == 7 ) show @endif"></div>
            <div class="item @if($order->internal_status == 8 ) show @endif"></div>
        </div>
    </div>
    <div class="buttons">
        <button class="user-button orange-user-button">Send final to Email</button>
        <button class="user-button grey-user-button">Order a revision</button>
    </div>
    <div class="inner-content">
        <div class="tabs-container">
            
            <div class="tabs">
                <span class="tab @if($activePage == '1') active-tab @endif" data-block-id="first-tab">Information</span>
                <span class="tab @if($activePage == '2') active-tab @endif" data-block-id="second-tab">
                    Messages
                    <span class="amount-unread">
                        {$ sectionWithAmmount['CurrentMessages'] $}
                    </span>
                </span>
                <span class="tab @if($activePage == '3') active-tab @endif" data-block-id="third-tab">
                    Files
                     <span class="amount-unread">
                        {$ sectionWithAmmount['CurrentFiles'] $}
                    </span>
                </span>
                <hr>
                <div id="first-tab" class="tab-content @if($activePage == '1') active-tab-content @endif">
                    <div class="table margin-r-30 mobile-margin-bottom-75 mobile-margin-top-50">
                        <div class="table-header">
                            Payments 
                            <span class="table-icon credit-icon"></span>
                        </div>
                        <div class="table-item">
                            <span class="table-column bold-blue">Date:</span>
                            <span>{{ $order->internal_status >= \App\Paper::STATUS_PAID ? $order->paid_at : 'Not paid' }}</span>
                        </div>
                        <hr>
                        @if($order->internal_status >= \App\Paper::STATUS_PAID)
                        <div class="table-item">
                            <span class="table-column bold-blue">Reference:</span>
                            <span>{{ $order->internal_status >= \App\Paper::STATUS_PAID ? $order->payment_id : 'Not paid' }} </span>
                        </div>
                        <hr>
                        @endif
                        
                        @if($order->internal_status >= \App\Paper::STATUS_PAID)
                        <div class="table-item">
                            <span class="table-column bold-blue">Payment system:</span>
                            <span>Paypal</span>
                        </div>
                        <hr>
                        @endif
                        <div class="table-item">
                            <span class="table-column bold-blue">Amount</span>
                            <span>$ {{$order->price}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Status</span>
                            <span>{{$order->status}}</span>
                        </div>
                        <div class="table-footer">Suggested Extras <span class="table-icon window-icon"></span></div>
                    </div>
                    <div class="table float-r float-none-for-tablet">
                        <div class="table-header">Order <span class="table-icon file-icon"></span></div>
                        <div class="table-item">
                            <span class="table-column bold-blue">Subject</span>
                            <span>{{$order->subject_name}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Topic:</span>
                            <span>{{$order->topic}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Academic Level:</span>
                            <span>{{$order->level->name}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Type of service:</span>
                            <span>{{$order->type_of_service}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Type of paper:</span>
                            <span>{{$order->type->name}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Paper Format:</span>
                            <span>{{$order->format}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Additional Materials:</span>
                            <span>{{$order->additional_materials}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Pages:</span>
                            <span> {{$order->pages_count}} </span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Spacing:</span>
                            <span>{{$order->spacing}}</span>
                        </div>
                        <hr>
                        <div class="table-item">
                            <span class="table-column bold-blue">Deadline:</span>
                            <span> {{$order->deadline_name}} </span>
                        </div>
                        <hr>
                        @if($order->writer_id != 0)
                        <div class="table-item">
                            <span class="table-column bold-blue">Writer ID:</span>
                            <div class="inline-block">
                                <span>{{$order->writer_id}}</span>
                                <span class="icon-hearth" ng-click="addPreferredWriter()" ng-class="preferredWriter()"></span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="table-under-info">
                        <div class="under-block under-info-report">
                            <div class="inf right-240">
                                Please make sure you have entered the correct email address to receive notifications of order completion status
                            </div>
                            Purchase Plagiarism Report: $9.99
                        </div>
                        <div class="under-block under-info-check">
                            <div class="inf right-125 mobile-left-245">
                                Please make sure you have entered the correct email address to receive notifications of order completion status
                            </div>
                            Purchase Editor's Check
                        </div>
                        <p>Please note that the price might be increased due to VAT if you are paying from the territory of the EU.</p>
                    </div>
                </div>
                


                <div id="second-tab" class="tab-content @if($activePage == '2') active-tab-content @endif">
                    @if($order->support_id != 0)
                    <div class="inner-tab-content-block">
                        <open-pop-up class="blue-button" pop-up-id="pop-up-send-message-file" onclick="PopUpMFTabs.message.setActive()">Create new message</open-pop-up>
                    </div>
                    @endif
                    <div class="inner-tab-content">
                        <div class="message-container"  ng-repeat="message in messages.data">
                            <div class="icon-container">
                                <div class="writer-icon bold-blue" ng-class="messages.getIcon(message)">
                                    <div ng-click="messages.setRecipient(message)" 
                                         ng-if="!messages.isUser(message)" 
                                         data-popup-id="popup-id"
                                         class="popup-button">
                                        Reply
                                    </div>
                                </div>
                                <p ng-bind="messages.getNameSender(message)"></p>
                                <p ng-if="!messages.isUser(message)">#{$ message.from_id $}</p>
                            </div>
                            <div class="message" ng-click="messages.setRead(message.id, messages.isUser(message))">
                                <div class="header-inf">
                                    <div ng-bind="getName(message)"></div>
                                    <div>
                                        <span class="new" ng-class="(message.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="message.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <div class="message-text">{$ message.text $}</div>
                                    <span class="read-more inline-block" 
                                          ng-if="short(message.text)">
                                        Read more
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <center>
                        <button type="button" class="blue-button border-radius-n mobile-font-size-08em" ng-click="messages.showMore()" ng-if="!messages.isLast">
                            Show More Messages
                        </button>
                    </center> 
                </div>
                


                <div id="third-tab" class="tab-content @if($activePage == '3') active-tab-content @endif">
                    @if($order->support_id != 0)
                    <div class="inner-tab-content-block">
                        <open-pop-up class="blue-button" pop-up-id="pop-up-send-message-file" onclick="PopUpMFTabs.file.setActive()">Upload file</open-pop-up>
                    </div>
                    @endif
                    <div class="inner-tab-content">
                        <div class="message-container" ng-repeat="file in files.data">
                            <div class="icon-container">
                                <div class="writer-icon bold-blue popup-button" 
                                     ng-class="files.getIcon(file)"
                                     data-popup-id="popup-id"
                                     ng-click="files.setRecipient(file)">
                                    Reply
                                </div>
                                <p ng-bind="files.getNameSender(file)"></p>
                                <p ng-if="!files.isUser(file)">#{$ file.from_id $}</p>
                            </div>
                            <div class="message" ng-click="files.setRead(file.id, files.isUser(file))" >
                                <div class="header-inf">
                                    <div ng-bind="getName(file)"></div>
                                    <div>
                                        <span class="new" ng-class="(file.isread==1)?'width-0':''">NEW</span>
                                        <span class="message-date" ng-bind="file.created_at"></span>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <span class="inner-message-icon middle" ng-class="getPicture(file.isimage)"></span>
                                    <span class="middle inline-block">
                                        {$ file.filename $}
                                    </span>
                                    <a href="{{url('/user/download')}}/{$ file.id $}">
                                        <span class="download inline-block middle">Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <center>
                        <button type="button" class="blue-button border-radius-n mobile-font-size-08em" ng-click="files.showMore()" ng-if="!files.isLast">
                            Show More Files
                        </button>
                    </center> 
                </div>
            </div>
        </div>
    </div>
    @include('user.popups.pop-up-send-message-file')
</div>
