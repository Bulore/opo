@extends('user.layouts.base')
@section('content')
    @include('user.sections.block.profile', compact('user', 'countries'))
@stop