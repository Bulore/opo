@extends('user.layouts.base')
@section('content')
    @include('user.sections.block.new-order', compact('academicLevels', 'types', 'subjects', 'deadlines', 'countries', 'countryCode'))
@stop