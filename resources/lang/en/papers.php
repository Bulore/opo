<?php

return [
    'subject' => 'Subject',
'format' => 'Format',
'abstract_page' => 'Abstract Page',
'sources' => 'Sources',
'topic' => 'Topic',
'details' => 'Details',
'additional_materials' => 'Additional Materials',
'type_of_service' => 'Type Of Service',
'academic_level' => 'Academic Level',
'vip' => 'Vip',
'pages_count' => 'Pages Count',
'slides_count' => 'Slides Count',
'spacing' => 'Spacing',
'first_deadline' => 'First Deadline',
'final_deadline' => 'Final Deadline',
'status' => 'Status',
'preferred_writer' => 'Preferred Writer',
'discount_code' => 'Discount Code',
];
