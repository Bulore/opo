<?php

return [
    'id' => 'ID',
    'topic' => 'Topic',
    'type_id' => 'Type ID',
    'subject_name' => 'Subject',
    'pages_count' => 'Quantity',
    'price' => 'Price',
    'status' => 'Status',
    'deadline_name' => 'Deadline',
    'writer_id' => 'Writer ID',
];
