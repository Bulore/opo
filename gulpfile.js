var elixir = require('laravel-elixir');
var path = ['./vendor/bower_components/bootstrap-sass-official/assets/stylesheets/',
    './vendor/bower_components/ionrangeslider/css/',
    './resources/assets/css/'];

// elixir(function(mix) {
//   mix.sass('login.scss', 'public/css/login.css', {includePath: path});
//   mix.sass('admin.scss', 'public/css/admin.min.css', {includePaths: path});
//   mix.sass('app.scss', 'public/css/app.css', {includePaths: path});
//   mix.sass('priceCalc.scss', 'public/css/priceCalc.css', {includePaths: path});
//   mix.sass('howItWorks.scss', 'public/css/howItWorks.css', {includePaths: path});
//   mix.sass('samples.scss', 'public/css/samples.css', {includePaths: path});
//   mix.sass('sideblocks.scss', 'public/css/sideblocks(2).css', {includePaths: path});
//   mix.sass('faq.scss', 'public/css/faq.css', {includePaths: path});
//   mix.sass('prices.scss', 'public/css/prices.css', {includePaths: path});
//   mix.sass('testimonials.scss', 'testimonials/css/prices.css', {includePaths: path});
//   mix.sass('contacts.scss', 'public/css/contacts.css', {includePaths: path});
//   mix.sass('payment-auth.scss', 'public/css/payment-auth.css', {includePaths: path});
//   mix.sass('extras.scss', 'public/css/extras.css', {includePaths: path});
//   mix.sass('our-writers.scss', 'public/css/our-writers.css', {includePaths: path});
//   mix.sass('free-inquiry.scss', 'public/css/free-inquiry.css', {includePaths: path});
//  mix.sass('testimonials.scss', 'public/css/testimonials.css', {includePaths: path});

// });

// var gulp = require('gulp'),
//     inlineCss = require('gulp-inline-css');
 
// gulp.task('default', function() {
//     return gulp.src('/resources/views/user/sections/block/order-open.blade.html')
//         .pipe(inlineCss())
//         .pipe(gulp.dest('build/'));
// });
// var gulp = require('gulp');
// var jshint = require('gulp-jshint');
// var inlinesource = require('gulp-inline-source');
// var htmlmin = require('gulp-htmlmin');
// var inlineCss = require('gulp-inline-css');
// var replace = require('gulp-replace')

// gulp.task('lint', function() {
//     return gulp.src('./src/js/*.js')
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'));
// });

// gulp.task('inlinesource-htmlmin', function() {
//     return gulp.src('./resources/views/user/sections/block/*.php')
//         .pipe(inlinesource())
//         .pipe(inlineCss())
//         .pipe(replace(/<(html|body)>/g, ''))
//         .pipe(htmlmin({collapseWhitespace: true}))
//         .pipe(gulp.dest('./resources/views/user/sections/block/compiled'));
// });

// gulp.task('default', ['inlinesource-htmlmin']);