var documentWidth = document.documentElement.clientWidth;

var closest = function(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}




/*    --------- DROP DOWN ORDERED LIST ---------    */

var DropDownOrderedListPrototype = Object.create(HTMLElement.prototype);
DropDownOrderedListPrototype.createdCallback = function() {
    var self = this;

    this.style.visibility = 'visible';
    this.lastClickedItem;

    this.openOnlyOne = this.getAttribute('data-open-only-one');
    
    this.closeAll = function(currentItem) {

        var items = self.querySelectorAll('ordered-list-item');

        Array.prototype.forEach.call(items, function(item) {
            item.close();
        });
    }
};




var OrderedListItemPrototype = Object.create(HTMLElement.prototype);
OrderedListItemPrototype.createdCallback = function() {
    var self = this;
    this.style.display = 'block';
    
    this.togleOpen = function() {
        var content = self.querySelector('ordered-list-content');
        var normalHeight = content.scrollHeight;

        if(content.style.height == '0px'){
            content.style.height = normalHeight+'px';
        } else {
            content.style.height = '0px';
        }
        self.classList.toggle('open');
    }

    this.openOnlyOne = function() {
        var dropDownMenu = self.parentNode;
        if(self != dropDownMenu.lastClickedItem){
            if(dropDownMenu.lastClickedItem) dropDownMenu.lastClickedItem.close();
            self.open();
            dropDownMenu.lastClickedItem = self;
        }
    }

    this.close = function() {
        var content = self.querySelector('ordered-list-content');
        content.style.height = '0px';
        self.classList.remove('open');
    }

    this.open = function() {
        var content = self.querySelector('ordered-list-content');
        var normalHeight = content.scrollHeight;
        content.style.height = normalHeight+'px';
        self.classList.add('open');
    }
    
    this.addEventListener('click', function(e) {
        if(self.parentNode.openOnlyOne != undefined) self.openOnlyOne();
        else self.togleOpen();
    });
};



var OrderedListHeaderPrototype = Object.create(HTMLElement.prototype);
OrderedListHeaderPrototype.createdCallback = function() {
    this.style.display = 'block';
};



var OrderedListContentPrototype = Object.create(HTMLElement.prototype);
OrderedListContentPrototype.createdCallback = function() {
    this.style.display = 'block';
    this.style.height = '0px';
};



document.registerElement('drop-down-ordered-list', {prototype: DropDownOrderedListPrototype});
document.registerElement('ordered-list-item', {prototype: OrderedListItemPrototype});
document.registerElement('ordered-list-header', {prototype: OrderedListHeaderPrototype});
document.registerElement('ordered-list-content', {prototype: OrderedListContentPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/














/*    --------- SELECT BOX ---------    */

setTimeout(function(){
    document.querySelector('body').addEventListener('click', function(event){
        var arraySelectBox = document.querySelectorAll('select-box');
        var e = event || window.event;
        Array.prototype.forEach.call(arraySelectBox, function(item){
            if (e.target != item.querySelector('select-box-header')) {
                item.querySelector('select-box-content').classList.remove('open');
                item.querySelector('select-box-content').style.height = '0px';
                item.querySelector('select-box-content').style.zIndex = '1';
            }
        })
    })
}, 100);


var SelectBoxPrototype = Object.create(HTMLElement.prototype);
SelectBoxPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box';
    this.text = this.getAttribute('placeholder');

    this.valid = 0;
    var header = this.querySelector('select-box-header');

    header.innerHTML = this.getAttribute('placeholder');

    this.addEventListener('click', function(e) {
        
        var content = this.querySelector('select-box-content');
        var normalHeight = content.scrollHeight;
        var dif = documentWidth > 1000 ? 3 : 3;
        var height = documentWidth > 1000 ? 300 : 500;

        if(content.style.height == '0px'){
            if(normalHeight > height){
                content.style.height = height + 'px';
            }
            else{
                content.style.height = normalHeight + dif + 'px';
            }
        } else {
            content.style.height = '0px';
        }
        content.classList.toggle('open');
    });


    this.reset = function(){
        self.text = self.getAttribute('placeholder');
        self.querySelector('select-box-header').innerHTML = self.text;
    }

    this.setOption = function(value){
        self.valid = true;
        self.text = value;
        self.querySelector('select-box-header').innerHTML = value;
        self.hideErrorElement();
    }




    this.isValid = function(){
        if(self.valid) return true;
        return false
    }

    this.createErrorElement = function(){
        var spanError = document.createElement("span");
        spanError.classList.add('data-error-message');
        spanError.innerHTML = self.getAttribute('data-error-message');
        if(self.getAttribute('data-error-message')) self.parentNode.insertBefore(spanError, self);
    }

    this.showErrorElement = function(){
        var spanError = self.parentNode.querySelector('.data-error-message');
        if(spanError) spanError.classList.add('show-eror');
    }

    this.hideErrorElement = function(){
        var spanError = self.parentNode.querySelector('.data-error-message');
        if(spanError) spanError.classList.remove('show-eror');
    }


    this.createErrorElement();

    
};
var SelectBox = document.registerElement('select-box', {prototype: SelectBoxPrototype});




var SelectBoxContentPrototype = Object.create(HTMLElement.prototype);
SelectBoxContentPrototype.createdCallback = function() {
    this.name = 'select-box-content';
    
    this.style.display = 'block';
    this.style.height = '0px';
    this.style.zIndex = '50';
};
var SelectBoxContent = document.registerElement('select-box-content', {prototype: SelectBoxContentPrototype});



var SelectBoxHeaderPrototype = Object.create(HTMLElement.prototype);
SelectBoxHeaderPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box-header';
    this.style.lineHeight = '32px';
};
var SelectBoxHeader = document.registerElement('select-box-header', {prototype: SelectBoxHeaderPrototype});



var SelectBoxOptionPrototype = Object.create(HTMLElement.prototype);
SelectBoxOptionPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box-option';
    this.disable = this.getAttribute('disable') == "" ? true : false;  

    self.style.display = 'block';

    self.addEventListener('click', function(e) {
        if(self.disable) return;
        var selectBox = this.parentNode.parentNode;
        var header = selectBox.querySelector('select-box-header');
        header.innerHTML = this.innerHTML;
        selectBox.text = this.innerHTML;
        selectBox.valid = 1;
        selectBox.classList.remove('valid-error');
        selectBox.hideErrorElement();
    });
   

    (function(){
        var tmp = self.getAttribute('data-default');
        if(tmp != null){
            self.classList.add('default');
        }
    })();
};
var SelectBoxOption = document.registerElement('select-box-option', {prototype: SelectBoxOptionPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/











/*    --------- SELECT-BOX VALIDATION ---------    */

function selectBoxValidator(parentSelector){
    
    var items = document.querySelector(parentSelector).querySelectorAll('select-box[required]');
    var isValid = true;

    Array.prototype.forEach.call(items, function(item){
        item.classList.remove('valid-error');
        item.hideErrorElement();
        if(!item.isValid()) {
            isValid = false;
            item.classList.add('valid-error');
            if(item.getAttribute('data-error-message')) item.showErrorElement();
        }
    })
    return isValid;
}

/*--------------------------------------------------------------------------------------------------------------------*/













/*    --------- POP UP ---------    */

var PopupPrototype = Object.create(HTMLElement.prototype);
PopupPrototype.createdCallback = function() {
    var self = this;
    this.style.visibility = 'hidden';
    this.style.position = 'fixed';
    this.style.zIndex = '10';
    this.style.height = '100%';
    this.style.width = '100%';
    this.style.top = '0px';
    this.style.left = '0px';
    this.style.background = 'rgba(0,0,0,.5)';
    this.style.overflow = 'auto';
    this.addEventListener('click', function(event) {
        e = event || window.event;
        if (e.target == this) {
            document.querySelector('body').style.overflow = 'auto';
            this.style.visibility = 'hidden';
            this.querySelector('pop-up-content').style.top = '-1000px';
        }
    })

    this.open = function(){
        // var content = self.querySelector('pop-up-content');
        // var content = self.querySelector('pop-up-content');

        // content.innerHTML = arg.content ? arg.content : content.innerHTML;

        this.style.visibility = 'visible';
        this.querySelector('pop-up-content').style.top = '0px';
        document.querySelector('body').style.overflow = 'hidden';


    }

    this.close = function(){
        document.querySelector('body').style.overflow = 'auto';
        this.style.visibility = 'hidden';
        this.querySelector('pop-up-content').style.top = '-1000px';
    }
};

var ContentPopupPrototype = Object.create(HTMLElement.prototype);
ContentPopupPrototype.createdCallback = function() {
    this.style.position = 'relative';
    this.style.display = 'block';
    this.style.margin = 'auto';
    this.style.height = 'auto';
    this.style.top = '-1000px';
    this.style.background = 'white';
};

var ClosePopupPrototype = Object.create(HTMLElement.prototype);
ClosePopupPrototype.createdCallback = function() {
    this.addEventListener('click', function(e) {
        document.querySelector('body').style.overflow = 'auto';
        closest(this, function(el) {return el.tagName === 'POP-UP'} ).style.visibility = 'hidden';
        closest(this, function(el) {return el.tagName === 'POP-UP-CONTENT'}).style.top = '-1000px';

    });
};

var OpenPopupPrototype = Object.create(HTMLElement.prototype);
OpenPopupPrototype.createdCallback = function() {
    this.addEventListener('click', function(event) {
        var id = this.getAttribute('pop-up-id');
        var popUp = document.querySelector('pop-up[pop-up-id="'+id+'"]');
        
        if(popUp != undefined){
            popUp.style.visibility = 'visible';
            popUp.querySelector('pop-up-content').style.top = '0px';
            document.querySelector('body').style.overflow = 'hidden';
        }
    });
};

var Popup = document.registerElement('pop-up', {prototype: PopupPrototype});
var ClosePopup = document.registerElement('close-pop-up',{prototype: ClosePopupPrototype});
var OpenPopup = document.registerElement('open-pop-up',{prototype: OpenPopupPrototype});
var ContentPopup = document.registerElement('pop-up-content',{prototype: ContentPopupPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/














/*    --------- TAB PANEL ---------    */



var TabPanelPrototype = Object.create(HTMLElement.prototype);
TabPanelPrototype.createdCallback = function() {
    var self = this;

    this.style.display = "block";
};




var TabNavPrototype = Object.create(HTMLElement.prototype);
TabNavPrototype.createdCallback = function() {
    var self = this;
    
    var autoActive = this.getAttribute('data-autoactive-disable') == "" ? false : true;

    this.style.display = "block";

    this.lastClickedItem = undefined;
};



var TabNavItemPrototype = Object.create(HTMLElement.prototype);
TabNavItemPrototype.createdCallback = function() {
    var self = this;

    this.style.display = 'inline-block';

    this.setActive = function(){
        var parentNode = self.parentNode;
        if(parentNode.lastClickedItem){
            parentNode.lastClickedItem.classList.remove('active');
            self.parentNode.lastClickedItem = self;
            self.classList.add('active');
        } else { 
            parentNode.lastClickedItem = self;
            self.classList.add('active'); 
        };
    }
    
    this.addEventListener('click', function(e) {
        if(!self.parentNode.autoActive) return 0;
        self.setActive();
        var index = Array.prototype.indexOf.call(this.parentNode.children, self);
        self.parentNode.parentNode.querySelector('tab-content').setActive(index);
    });

    (function(){
        if(self.classList.contains('active')) self.parentNode.lastClickedItem = self;
    })();
};



var TabContentPrototype = Object.create(HTMLElement.prototype);
TabContentPrototype.createdCallback = function() {
    var self = this;

    this.style.display = 'block';
    
    this.lastActiveItem = undefined;

    this.setActive = function(index){
        if(this.parentNode.getAttribute('data-only-one-tab') == "") return 0;
        
        var item = self.querySelectorAll('tab-content-item').item(index);
        
        if(self.lastActiveItem){
            self.lastActiveItem.classList.remove('active');
            self.lastActiveItem = item;
            item.classList.add('active');
        } else { 
            self.lastActiveItem = item;
            item.classList.add('active'); 
        }
    }
};


var TabContentItemPrototype = Object.create(HTMLElement.prototype);
TabContentItemPrototype.createdCallback = function() {
    var self = this;
    
    this.setActive = function(){
        var parentNode = self.parentNode;
        if(parentNode.lastActiveItem){
            parentNode.lastActiveItem.classList.remove('active');
            self.parentNode.lastActiveItem = self;
            self.classList.add('active');
        } else { 
            parentNode.lastActiveItem = self;
            self.classList.add('active'); 
        }
    }
};


document.registerElement('tab-panel', {prototype: TabPanelPrototype});
document.registerElement('tab-nav', {prototype: TabNavPrototype});
document.registerElement('tab-nav-item', {prototype: TabNavItemPrototype});
document.registerElement('tab-content', {prototype: TabContentPrototype});
document.registerElement('tab-content-item', {prototype: TabContentItemPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/












/*    --------- INPUT VALIDATION ---------    */

function validWithServer(data, formId, parentSelector, dataShow){
    parentSelector = parentSelector ? parentSelector : '.content';
    dataShow = dataShow ? dataShow : '.data-label';
    
    $(formId).find('.data-error').remove();
    $(formId).find(dataShow).show();
    $form = $(formId);

    if(data == "") return 0;
    var data = typeof data === "string" ? JSON.parse(data) : data;
    
    var errors = data.errors;
    for(var key in errors){
        var input = $form.find('[name='+key+']');
        var additionalClasses = input.attr('data-additional-classes') || '';
        var label = input.closest(parentSelector);
        if(label.find('.data-error').length != 0){
            label.find('.data-error').innerHTML = errors[key];
        } else{
            label.prepend("<span class='data-error " + additionalClasses + " '>"+errors[key]+"</span>");
        }
        
        label.find(dataShow).hide();
    }

    return 0;
}





function ValidWithFrontEnd(dataError, generalContainer, inputContainer, labelConteiner){
    var self = this;

    this.dataError = typeof dataError === "string" ? JSON.parse(dataError) : dataError;
    this.generalContainer = $(generalContainer);
    this.inputContainer = inputContainer;
    this.labelConteiner = labelConteiner;
    
    

    this.isValid = function(){
        var isValid = true;
        Array.prototype.forEach.call(self.dataError, function(item){
            var input =  self.generalContainer.find('[name='+item.name+']');
            
            var tmpValid = true;
            
            if(input.get(0).name == "select-box"){ 
                if(!input.get(0).isValid()) tmpValid = false;
            } else if(!item.pattern.test(input.val())) tmpValid = false;
            
            if(!tmpValid){
                var label = input.closest(self.inputContainer);
                if(label.find('.data-error').length != 0){
                    label.find('.data-error').innerHTML = item.errorMessage;
                } else{
                    label.prepend("<span class='data-error'>"+item.errorMessage+"</span>");
                }
                input.closest(self.inputContainer).find(self.labelConteiner).hide();
            } else {
                input.closest(self.inputContainer).find('.data-error').hide();
                input.closest(self.inputContainer).find(self.labelConteiner).show();
            }
        })
        return isValid;
    }
}

/*--------------------------------------------------------------------------------------------------------------------*/












$('.orange-items .section, .items .section').click(function() {
    var $this = $(this).closest('.section').find('.content');
    $this.slideToggle('slow');
    $this = $this.closest('.section');
    if($this.find('.open')[0]!=undefined){
        $this.find('.content').removeClass('open');
        $this.find('.header').removeClass('open');
    }else{
        $this.find('.content').addClass('open');
        $this.find('.header').addClass('open');
    }
});


$(".left-panel-arrow").click(function(){
    $(this).closest(".left-block").find(".left-panel-body").slideToggle("slow");
    $(this).closest(".left-block").find(".left-panel-header").toggleClass("close");
    $(this).closest(".left-block").find(".left-panel-arrow").toggleClass("rotate-arrow");
});
$(".select-box").click(function () {
    $(this).toggleClass("open");
    $(this).next(".select-option").slideToggle("slow");
});
$(".select").click(function () {
    var content = $(this).html();
    $(this).closest('.input-select').find('.selected').html(content).css({"opacity":"1"});
    $(".select").removeClass("active");
    $(this).addClass("active");
    $(".select-option").slideUp("slow");
    $(".select-box").removeClass("open");
});
function changeCountry(){
    $('#selected-country').attr('class', 'flag '+$(this).attr('code'));
    var tax = ($(this).attr('tax') != undefined)?'+'+$(this).attr('tax'):'';
    $('#phone-prefix').html(tax);
}

var currentCountryCode = '{{$currentCountryCode}}';
var currentCountry = $('.country[code="'+currentCountryCode+'"]');
changeCountry.call(currentCountry);
$('.country').click(changeCountry);


$(document).scroll(function(){
    if($('body').scrollTop() > 120){
        $('.container-header').addClass('min');
    }else{
        $('.container-header').removeClass('min');
    } 
});

$(document).ready(function(){

    var check = true;
    $("#mobile-menu").click(function () {
        if(check){
            $(".mobile-left-panel").css({left: '0%'});
        }
        else{
            $(".mobile-left-panel").css({left: '-65%'});
        }
        check =! check;
    });
    $('#touch-zone').on('swiperight', function(e, direction){
        if(direction.startEvnt.position.x > 40) return;
        $(".mobile-left-panel").css({left: '0%'});
        check =! check;
    });
    $('#touch-zone').on('swipeleft', function(e, direction){

        if(direction.xAmount < 20) return;
        $(".mobile-left-panel").css({left: '-65%'});
        check =! check;
    })
});





$('[pop-up-id="call-me-back"] .call-button').click(function(e){
    e.preventDefault();
    var callForm = $('#call-form');
    var data = {
        "subject": callForm.find('#select-box').get(0).text,
        "name": callForm.find('input[name="name"]').val(),
        "phone": callForm.find('input[name="phone"]').val(),
        "text": callForm.find('textarea[name="text"]').val()
    }
    
    $.ajax({
        url: "/submit-callback",
        data: data,
        method: "GET",
        beforeSend: function(){
            $('#container-floatingCirclesG').css('opacity', 1);
        },
        complete: function(){
            $('#container-floatingCirclesG').css('opacity', 0);
        },
        success: function(data){
            validWithServer("", '#call-form', '.input-select', 'p');
            document.querySelector('pop-up[pop-up-id="call-me-back"]').close();
            document.querySelector('pop-up[pop-up-id="thanks"]').open();
        },
        error: function(data){
            validWithServer(data.responseText, '#call-form', '.input-select', 'p');
        }
    });
})




 // -------------------------------------------------------- follow and share
$(".shared-button").click(function() {
    var share = window.open($(this).attr("data-href"), "Share", "width=600,height=400,top=100,left=200,resizable=no,scrollbars=no");
    share.focus();
});
$(".follow-button").click(function() {
    var share = window.open($(this).attr("data-href"), "Share", "width=600,height=400,top=100,left=200,resizable=no,scrollbars=no");
    share.focus();
});

// ---------------------------------------------
