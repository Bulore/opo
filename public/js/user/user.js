var documentWidth = document.documentElement.clientWidth;

function setMinHeight(){
    if($(document).width()>1169){
        var constHeight = 153;
        var height = $(window).height() - constHeight - $('header').height() - $('footer').height();
        $('.content-header ~ .inner-content').css('min-height', height);
    }else{
       $('.user-container').css('height', 'auto'); 
    }
    
}


function deInitialise(){
	$(".item").off("click");
	$(".tab").off("click");
    $(".select-box").off("click");
    $(".select").off("click");
    $(".tabs-container .tab").off("click");
    $(".popup-button").off("click");
    $('.popup_overlay').off("click");
    $('.message').off('click');
    $(".upload-file-button").off('click');
    $('#search-button').off('click');
    $('#mobile-menu').off('click');
    $('[pop-up-id="call-me-back"] .call-button').off('click');
}

function initialise(){
    setMinHeight();

    var check = true;
    $("#mobile-menu").click(function () {
        if(check){
            $(".left-panel").css({left: '0%'});
        }
        else{
            $(".left-panel").css({left: '-65%'});
        }
        check =! check;
    });


    if(documentWidth < 1159){
        $('#touch-zone').on('swiperight', function(e, direction){
            if(direction.startEvnt.position.x > 40) return;
            $(".left-panel").css({left: '0%'});
            check =! check;
        });
        $('#touch-zone').on('swipeleft', function(e, direction){
            if(direction.xAmount < 20) return;
            $(".left-panel").css({left: '-65%'});
            check =! check;
        })
    }
    

    $(".upload-file-button").click(function(){
        var $this = $(this).closest('label');
        $this.find('input[type="file"]').click();
        
        $('#image_file').val()
    })
    $('input[type="file"]').change(function() {
        var filename = $(this).val();
         $(this).closest('.form-item').find('input[type="text"]').val(filename);
    });

	$(".tabs-container .tab").click(function () {
		$(this).closest(".tabs-container").find(".tab").removeClass("active-tab");
		$(this).addClass("active-tab");
		var id = $(this).attr('data-block-id');
		$(this).closest(".tabs-container").find('.tab-content').removeClass("active-tab-content");
		$('#'+id).addClass("active-tab-content");
	});

    $(".popup_overlay .popup").css({'top':'-1000px'});
    $(".popup-button").click(function () {
        $(".popup_overlay").css('display', 'none');
        $(".popup_overlay").find('.popup').css({'top':'-1000px'});
        var id = $(this).attr('data-popup-id');
        $('#'+id).css("display","block");
        setTimeout(function(){ 
            $('#'+id).find('.popup').css({'top':'0px'}); 
        },100);
    });
    $(function () {
        p = $('.popup_overlay');
        p.click(function(event) {
            e = event || window.event;
            if (e.target == this) {
                setTimeout(function(){$(p).css('display', 'none');},1000);
                $(p).find('.popup').css({'top':'-1000px'});
            }
        });
        $('.close_popup').click(function() {
            setTimeout(function(){$(p).css('display', 'none');},1000);
            $(p).find('.popup').css({'top':'-1000px'});
        })
    });

    $('.message, .file').click(function(){
        var $container = $(this).closest('.message-container');
        $container.find('.message-text').css({'max-height':'500px'});
        $container.find('.read-more').css({'height':'0px'});
        $container.find('.read-more').css({'padding-top':'15px'});
    })

    $('#search-button').click(function(){
        if($('.search-block-container.open').length != 0){
            $('.search-block-container').removeClass('open');
            $('.search-block-container').css('overflow','hidden');
            $('#search-button').removeClass('open');
        }else{
            $('.search-block-container').addClass('open');
            setTimeout(function(){
                $('.search-block-container').css('overflow','visible');
            },800)
            $('#search-button').addClass('open');
        }
    })
    $('[pop-up-id="call-me-back"] .call-button').click(function(e){
        e.preventDefault();
        var callForm = $('#call-form');
        var data = {
            "subject": callForm.find('#select-box').get(0).text,
            "name": callForm.find('input[name="name"]').val(),
            "phone": callForm.find('input[name="phone"]').val(),
            "text": callForm.find('textarea[name="text"]').val()
        }
        
        $.ajax({
            url: "/submit-callback",
            data: data,
            method: "GET",
            beforeSend: function(){
                $('#container-floatingCirclesG').css('opacity', 1);
            },
            complete: function(){
                $('#container-floatingCirclesG').css('opacity', 0);
            },
            success: function(data){
                validWithServer("", '#call-form', '.input-select', 'p');
                document.querySelector('pop-up[pop-up-id="call-me-back"]').close();
                document.querySelector('pop-up[pop-up-id="thanks"]').open();
            },
            error: function(data){
                validWithServer(data.responseText, '#call-form', '.input-select', 'p');
            }
        });
    })
}

$(function(){
    initialise();
    window.onresize = function(){setMinHeight()};
});


var app = angular.module('app', ['angularUtils.directives.dirPagination']);
app.filter('lessOrEqual', function () {
    return function ( items, value ) {
        var filteredItems = [];
        angular.forEach(items, function ( item ) {
            if ( item.hours <= value ) { filteredItems.push(item); }
        });
        return filteredItems;
    }
})
app.filter('greater', function () {
    return function ( items, value ) {
        var filteredItems = []
        angular.forEach(items, function ( item ) {
            if ( item.hours > value ) { filteredItems.push(item); }
        });
        return filteredItems;
    }
})
app.controller('appCtrl', function($scope, $compile, $http) {
    const constWidth = 1170;
    $scope.$http = $http;
    $scope.data = [];

    $scope.setActiveSection = function(href){
        $('.left-panel').find('.active-item').removeClass('active-item');
        $('.left-panel').find('[href*="'+href+'"] .item').addClass('active-item');
    },

    $scope.appendAjaxData = function(data, subHref){
        $scope.setActiveSection(subHref);
        setTimeout(function(){
            var tmp = $(data).html();
            $('.user-container .content').html('');
            $('.user-container .content').append($compile(tmp)($scope));
        }, 10)
        setTimeout(function(){
            $('.user-container .content').css('opacity',1);
            deInitialise();
            initialise();
        }, 200);
    }
    
    $(window).on('popstate', function(e){
        e.preventDefault();
        // if(history.state != null){
            var href = history.state.url + '-block';
            $('.user-container .content').css('opacity',0);
            $.ajax({
                url: href,
                data: {},
                method: "GET",
                beforeSend: function(){
                    $('#container-floatingCirclesG').css('opacity',1);
                },
                complete: function(){
                    $('#container-floatingCirclesG').css('opacity',0);
                },
                success: function(data){
                    $scope.appendAjaxData(data, history.state.url);
                },
                error: function(){
                    alert('error');
                }
            });
        // }
    });
    
    $scope.leftPanelClick = function(url,e,data){
        if(e !== undefined){
            e.preventDefault();
        }
        data = (data != undefined)? data : {}; 
       
        if($(document).width() < constWidth){
            $(".left-panel").animate({left: '-65%'}, "slow" );
        }
        var subHref = url;
        var href = subHref+'-block';
        $('.user-container .content').css('opacity',0);
        $.ajax({
            url: href,
            data: {},
            method: "GET", 
            beforeSend: function(){
                $('#container-floatingCirclesG').css('opacity',1);
            },
            complete: function(){
                $('#container-floatingCirclesG').css('opacity',0);
            },
            success: function(data){
                history.pushState(null, null, subHref);
                $scope.appendAjaxData(data, subHref);
            },
            error: function(){
               alert('error');
            }
        });
    }

    $scope.order = function(id, e, inf){
        e.preventDefault();
        var subHref = "/user/order-open";
        var href = subHref+"-block";
        subHref += "/" +id;
        $('.user-container .content').css('opacity',0);
        $.ajax({
            url: href,
            data: {'id':id, 'active_page': inf},
            method: "GET",
            beforeSend: function(){
                $('#container-floatingCirclesG').css('opacity',1);
            },
            complete: function(){
                $('#container-floatingCirclesG').css('opacity',0);
            },
            success: function(data){
                history.pushState(null, null, subHref);
                $scope.appendAjaxData(data, subHref);
            },
            error: function(){
                alert('error');
            }
        });
    }

    $scope.UserData = function(data, section, urls, userName, token, withoutUnread){
        var self = this;
        self.token = token;
        self.userName = userName;
        self.data = data;
        self.unreadData = getUnreadData(data);
        self.urls = urls;
        self.section = section;
        self.showMoreLen = 10;
        self.ammount = data.length;
        self.isLast = isEmptyArray(self.data) ? true : false;

        self.index = undefined;
        self.id = undefined;

        function isEmptyArray(array){
            if(array == [] || array == '[]' || array.length == 0) return true;
            return false;
        }

        function getUnreadData(array){
            var unreadData = []
            for(var index in data){
                if(data[index]['isread'] == 0){
                    unreadData.push(data[index]);
                } 
            }
            return unreadData;
        }

        function getIndexById(data, id){
            for(var index in data){
                if(data[index]['id'] == id){
                    return index;
                } 
            }
            return -1;
        }

        this.isUser = function(item){
            if(item.from_type == 'client') return 1;
            return 0;
        };

        this.isWriter = function(item){
            if(item.from_type == 'writer') return 1;
            return 0;
        }

        this.isSupport = function(item){
            if(item.from_type == 'manager') return 1;
            return 0;
        }

        this.getIcon = function(item){
            var iconName = 'message-support-icon';
            if(self.isUser(item)){
                iconName = 'message-user-icon';
            }else if(self.isWriter(item)){
                iconName = 'message-writer-icon';
            }
            return iconName;
        }

        this.appendData = function(newData){
            newData = $.parseJSON(newData);
            if(withoutUnread){
                if(isEmptyArray(newData)){
                    self.isLast = true;
                } else{
                    self.ammount += self.showMoreLen;
                    self.data = [].concat(self.data, newData);
                }
            } else {
                if(isEmptyArray(newData.read) && isEmptyArray(newData.unread)){
                    self.isLast = true;
                } else{
                    self.ammount += self.showMoreLen;
                    self.data = [].concat(self.data, newData.read, newData.unread);
                    self.unreadData = [].concat(self.unreadData, newData.unread);
                }
            }
            
        }

        this.setRead = function(id, isUser){
            if(isUser == 1) return 0;
            $.ajax({
                url: urls.read,
                data: {'id': id},
                method: "GET",
                success: function(){
                    var indexAll = getIndexById(self.data, id);
                    if(self.data[indexAll].isread == 0){
                        $scope.sectionWithAmmount[self.section] -= 1;
                        self.data[indexAll].isread = 1;
                    }

                    var indexUnread = getIndexById(self.unreadData, id);
                    if(self.unreadData[indexUnread].isread == 0){
                        $scope.sectionWithAmmount[self.section] -= 1;
                        self.unreadData[indexUnread].isread = 1;
                    }
                     
                    $scope.$apply();
                },
                error: function(){
                   alert('error');
                }
            });
        }

        this.showMore = function(){
            $.ajax({
                method: 'GET',
                url: urls.showMore,
                data:  {'length': self.showMoreLen, 'offset': self.ammount},
                success: function(data) {
                    self.appendData(data);
                    $scope.$apply();
                },
                error: function(data) {
                    alert('error');
                },
            });
        };

        this.getNameSender = function(item){
            if(self.isWriter(item)) return "Writer";
            if(self.isSupport(item)) return "Support";
            return self.userName;
        }

        this.setRecipient = function(item){
            if(self.isWriter(item)){
                self.newUserData.to = "Writer #" + item.from_id;
            }else{
                self.newUserData.to = "Support";
            }
            self.newUserData.content.to_id = item.from_id;
            self.newUserData.content.paper_id = item.paper_id;
            self.newUserData.content.to_type = item.from_type;
        }

        self.newUserData = {
            content: {
                text: '',
                type: '',
                to_type: '',
                to_id: '',
                paper_id: '',
            },
            to: '',
            send: function(){
                var file = document.querySelector('pop-up[pop-up-id="pop-up-send-message-file"] #file');
                var data  = new FormData();
                if(file) data.append('file', file.files[0]);
                data.append('text', self.newUserData.content.text);
                data.append('type', self.newUserData.content.type);
                data.append('to_type', self.newUserData.content.to_type);
                data.append('to_id', self.newUserData.content.to_id);
                data.append('paper_id', self.newUserData.content.paper_id);
                $.ajax({
                    method: 'POST',
                    url: urls.save,
                    headers: {'X-CSRF-TOKEN': self.token},
                    data:  data,
                    processData: false,
                    contentType: false,
                    success: function(newData) {
                        PopUpMF.close();
                        var data =  $.parseJSON(newData);
                        $('.close_popup').click();
                        self.data = [].concat([data], self.data);
                        self.unreadData = [].concat([data],self.unreadData);
                        $scope.sectionWithAmmount['Messages'] += 1;
                        $scope.sectionWithAmmount['CurrentMessages'] += 1;
                        popUpShowMessage('Message sending');
                        $scope.$apply();
                    },
                    error: function(data) {
                        popUpError.open();
                    },
                });
                deInitialise();
                initialise();
            },
            setTo: function(name){
                if(name == 'Writer'){
                    self.newUserData.to = 'Writer #' + self.newUserData.content.to_id;
                } else{
                    self.newUserData.to = 'Support';
                }
            }
        }
    }

});






/*    --------- SELECT BOX ---------    */
setTimeout(function(){
    document.querySelector('body').addEventListener('click', function(event){
        var arraySelectBox = document.querySelectorAll('select-box');
        var e = event || window.event;
        Array.prototype.forEach.call(arraySelectBox, function(item){
            if (e.target != item.querySelector('select-box-header')) {
                item.querySelector('select-box-content').classList.remove('open');
                item.querySelector('select-box-content').style.height = '0px';
                item.querySelector('select-box-content').style.zIndex = '1';
            }
        })
    })
}, 100);




var SelectBoxPrototype = Object.create(HTMLElement.prototype);
SelectBoxPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box';
    this.text = this.getAttribute('placeholder');

    this.valid = 0;
    var header = this.querySelector('select-box-header');

    header.innerHTML = this.getAttribute('placeholder');

    this.addEventListener('click', function(e) {
        
        var content = this.querySelector('select-box-content');
        var normalHeight = content.scrollHeight;
        var dif = documentWidth > 1000 ? 3 : 3;
        var height = documentWidth > 1000 ? 300 : 500;

        if(content.style.height == '0px'){
            if(normalHeight > height){
                content.style.height = height + 'px';
            }
            else{
                content.style.height = normalHeight + dif + 'px';
            }
        } else {
            content.style.height = '0px';
        }
        content.classList.toggle('open');
    });


    this.reset = function(){
        self.text = self.getAttribute('placeholder');
        self.querySelector('select-box-header').innerHTML = self.text;
    }

    this.setOption = function(value){
        self.valid = true;
        self.text = value;
        self.querySelector('select-box-header').innerHTML = value;
        self.hideErrorElement();
    }




    this.isValid = function(){
        if(self.valid) return true;
        return false
    }

    this.createErrorElement = function(){
        var spanError = document.createElement("span");
        spanError.classList.add('data-error-message');
        spanError.innerHTML = self.getAttribute('data-error-message');
        if(self.getAttribute('data-error-message')) self.parentNode.insertBefore(spanError, self);
    }

    this.showErrorElement = function(){
        var spanError = self.parentNode.querySelector('.data-error-message');
        if(spanError) spanError.classList.add('show-eror');
    }

    this.hideErrorElement = function(){
        var spanError = self.parentNode.querySelector('.data-error-message');
        if(spanError) spanError.classList.remove('show-eror');
    }


    this.createErrorElement();

    
};
var SelectBox = document.registerElement('select-box', {prototype: SelectBoxPrototype});




var SelectBoxContentPrototype = Object.create(HTMLElement.prototype);
SelectBoxContentPrototype.createdCallback = function() {
    this.name = 'select-box-content';
    
    this.style.display = 'block';
    this.style.height = '0px';
    this.style.zIndex = '50';
};
var SelectBoxContent = document.registerElement('select-box-content', {prototype: SelectBoxContentPrototype});



var SelectBoxHeaderPrototype = Object.create(HTMLElement.prototype);
SelectBoxHeaderPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box-header';
    this.style.lineHeight = '32px';
};
var SelectBoxHeader = document.registerElement('select-box-header', {prototype: SelectBoxHeaderPrototype});



var SelectBoxOptionPrototype = Object.create(HTMLElement.prototype);
SelectBoxOptionPrototype.createdCallback = function() {
    var self = this;
    this.name = 'select-box-option';
    this.disable = this.getAttribute('disable') == "" ? true : false;  

    self.style.display = 'block';

    self.addEventListener('click', function(e) {
        if(self.disable) return;
        var selectBox = this.parentNode.parentNode;
        var header = selectBox.querySelector('select-box-header');
        header.innerHTML = this.innerHTML;
        selectBox.text = this.innerHTML;
        selectBox.valid = 1;
        selectBox.classList.remove('valid-error');
        selectBox.hideErrorElement();
    });
   

    (function(){
        var tmp = self.getAttribute('data-default');
        if(tmp != null){
            self.classList.add('default');
        }
    })();
};
var SelectBoxOption = document.registerElement('select-box-option', {prototype: SelectBoxOptionPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/











/*    --------- SELECT-BOX VALIDATION ---------    */

function selectBoxValidator(parentSelector){
    
    var items = document.querySelector(parentSelector).querySelectorAll('select-box[required]');
    var isValid = true;

    Array.prototype.forEach.call(items, function(item){
        item.classList.remove('valid-error');
        item.hideErrorElement();
        if(!item.isValid()) {
            isValid = false;
            item.classList.add('valid-error');
            if(item.getAttribute('data-error-message')) item.showErrorElement();
        }
    })
    return isValid;
}

/*--------------------------------------------------------------------------------------------------------------------*/







/*    --------- POP UP ---------    */

window.addEventListener('resize', function(event){
   $('pop-up').css('height', document.documentElement.clientHeight + "px");
});

var PopupPrototype = Object.create(HTMLElement.prototype);
PopupPrototype.createdCallback = function() {
    var self = this;
    this.style.visibility = 'hidden';
    this.style.position = 'fixed';
    this.style.zIndex = '10';
    this.style.height = document.documentElement.clientHeight + "px";
    this.style.width = '100%';
    this.style.top = '0px';
    this.style.left = '0px';
    this.style.background = 'rgba(0,0,0,.5)';
    this.style.overflow = 'auto';
    this.addEventListener('click', function(event) {
        e = event || window.event;
        if (e.target == this) {
            document.querySelector('body').style.overflow = 'auto';
            this.style.visibility = 'hidden';
            this.querySelector('pop-up-content').style.top = '-1000px';
        }
    })

    this.open = function(time){
        if(time) setTimeout(function(){ self.open(); }, time);
        else {
            self.style.visibility = 'visible';
            self.querySelector('pop-up-content').style.top = '0px';
            document.querySelector('body').style.overflow = 'hidden';
        }
    }

    this.close = function(){
        document.querySelector('body').style.overflow = 'auto';
        this.style.visibility = 'hidden';
        this.querySelector('pop-up-content').style.top = '-1000px';
    }
};

var ContentPopupPrototype = Object.create(HTMLElement.prototype);
ContentPopupPrototype.createdCallback = function() {
    this.style.position = 'relative';
    this.style.display = 'block';
    this.style.margin = 'auto';
    this.style.height = 'auto';
    this.style.top = '-1000px';
    this.style.background = 'white';
};

var ClosePopupPrototype = Object.create(HTMLElement.prototype);
ClosePopupPrototype.createdCallback = function() {
    this.addEventListener('click', function(e) {
        document.querySelector('body').style.overflow = 'auto';
        this.closest('pop-up').style.visibility = 'hidden';
        this.closest('pop-up-content').style.top = '-1000px';

    });
};

var OpenPopupPrototype = Object.create(HTMLElement.prototype);
OpenPopupPrototype.createdCallback = function() {
    this.addEventListener('click', function(event) {
        var id = this.getAttribute('pop-up-id');
        var popUp = document.querySelector('pop-up[pop-up-id="'+id+'"]');
        
        if(popUp != undefined){
            popUp.style.visibility = 'visible';
            popUp.querySelector('pop-up-content').style.top = '0px';
            document.querySelector('body').style.overflow = 'hidden';
        }
    });
};

var Popup = document.registerElement('pop-up', {prototype: PopupPrototype});
var ClosePopup = document.registerElement('close-pop-up',{prototype: ClosePopupPrototype});
var OpenPopup = document.registerElement('open-pop-up',{prototype: OpenPopupPrototype});
var ContentPopup = document.registerElement('pop-up-content',{prototype: ContentPopupPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/





/*    --------- TAB PANEL ---------    */



var TabPanelPrototype = Object.create(HTMLElement.prototype);
TabPanelPrototype.createdCallback = function() {
    var self = this;

    this.style.display = "block";
};




var TabNavPrototype = Object.create(HTMLElement.prototype);
TabNavPrototype.createdCallback = function() {
    var self = this;
    
    this.style.display = "block";

    this.lastClickedItem = undefined;
};



var TabNavItemPrototype = Object.create(HTMLElement.prototype);
TabNavItemPrototype.createdCallback = function() {
    var self = this;

    this.style.display = 'inline-block';

    this.setActive = function(){
        var parentNode = self.parentNode;
        if(parentNode.lastClickedItem){
            parentNode.lastClickedItem.classList.remove('active');
            self.parentNode.lastClickedItem = self;
            self.classList.add('active');
        } else { 
            parentNode.lastClickedItem = self;
            self.classList.add('active'); 
        };
        var index = Array.prototype.indexOf.call(this.parentNode.children, self);
        self.parentNode.parentNode.querySelector('tab-content').setActive(index);
    }
    
    this.addEventListener('click', function(e) {
        self.setActive();
    });

    (function(){
        if(self.classList.contains('active')) self.parentNode.lastClickedItem = self;
    })();
};



var TabContentPrototype = Object.create(HTMLElement.prototype);
TabContentPrototype.createdCallback = function() {
    var self = this;

    this.style.display = 'block';
    
    this.lastActiveItem = undefined;

    this.setActive = function(index){
        if(this.parentNode.getAttribute('data-only-one-tab') == "") return 0;
        
        var item = self.querySelectorAll('tab-content-item').item(index);
        
        if(self.lastActiveItem){
            self.lastActiveItem.classList.remove('active');
            self.lastActiveItem = item;
            item.classList.add('active');
        } else { 
            self.lastActiveItem = item;
            item.classList.add('active'); 
        }
    }
};


var TabContentItemPrototype = Object.create(HTMLElement.prototype);
TabContentItemPrototype.createdCallback = function() {
    var self = this;
    
    this.setActive = function(){
        var parentNode = self.parentNode;
        if(parentNode.lastActiveItem){
            parentNode.lastActiveItem.classList.remove('active');
            self.parentNode.lastActiveItem = self;
            self.classList.add('active');
        } else { 
            parentNode.lastActiveItem = self;
            self.classList.add('active'); 
        }
    }
};


document.registerElement('tab-panel', {prototype: TabPanelPrototype});
document.registerElement('tab-nav', {prototype: TabNavPrototype});
document.registerElement('tab-nav-item', {prototype: TabNavItemPrototype});
document.registerElement('tab-content', {prototype: TabContentPrototype});
document.registerElement('tab-content-item', {prototype: TabContentItemPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/










/*    --------- DROP DOWN ORDERED LIST ---------    */

var DropDownOrderedListPrototype = Object.create(HTMLElement.prototype);
DropDownOrderedListPrototype.createdCallback = function() {
    var self = this;

    this.style.visibility = 'visible';
    this.lastClickedItem;

    this.openOnlyOne = this.getAttribute('data-open-only-one');
    
    this.closeAll = function(currentItem) {

        var items = self.querySelectorAll('ordered-list-item');

        Array.prototype.forEach.call(items, function(item) {
            item.close();
        });
    }
};




var OrderedListItemPrototype = Object.create(HTMLElement.prototype);
OrderedListItemPrototype.createdCallback = function() {
    var self = this;
    this.style.display = 'block';
    
    this.togleOpen = function() {
        var content = self.querySelector('ordered-list-content');
        var normalHeight = content.scrollHeight;

        if(content.style.height == '0px'){
            content.style.height = normalHeight+'px';
        } else {
            content.style.height = '0px';
        }
        self.classList.toggle('open');
    }

    this.openOnlyOne = function() {
        var dropDownMenu = self.parentNode;
        if(self != dropDownMenu.lastClickedItem){
            if(dropDownMenu.lastClickedItem) dropDownMenu.lastClickedItem.close();
            self.open();
            dropDownMenu.lastClickedItem = self;
        }
    }

    this.close = function() {
        var content = self.querySelector('ordered-list-content');
        content.style.height = '0px';
        self.classList.remove('open');
    }

    this.open = function() {
        var content = self.querySelector('ordered-list-content');
        var normalHeight = content.scrollHeight;
        content.style.height = normalHeight+'px';
        self.classList.add('open');
    }
    
    this.addEventListener('click', function(e) {
        if(self.parentNode.openOnlyOne != undefined) self.openOnlyOne();
        else self.togleOpen();
    });
};



var OrderedListHeaderPrototype = Object.create(HTMLElement.prototype);
OrderedListHeaderPrototype.createdCallback = function() {
    this.style.display = 'block';
};



var OrderedListContentPrototype = Object.create(HTMLElement.prototype);
OrderedListContentPrototype.createdCallback = function() {
    this.style.display = 'block';
    this.style.height = '0px';
};



document.registerElement('drop-down-ordered-list', {prototype: DropDownOrderedListPrototype});
document.registerElement('ordered-list-item', {prototype: OrderedListItemPrototype});
document.registerElement('ordered-list-header', {prototype: OrderedListHeaderPrototype});
document.registerElement('ordered-list-content', {prototype: OrderedListContentPrototype});

/*--------------------------------------------------------------------------------------------------------------------*/












/*    --------- INPUT VALIDATION ---------    */

function validWithServer(data, formId, parentSelector, dataShow){
    parentSelector = parentSelector ? parentSelector : '.content';
    dataShow = dataShow ? dataShow : '.data-label';
    
    $(formId).find('.data-error').remove();
    $(formId).find(dataShow).show();
    $form = $(formId);

    if(data == "") return 0;
    var data = typeof data === "string" ? JSON.parse(data) : data;
    
    var errors = data.errors;
    for(var key in errors){
        var label = $form.find('[name='+key+']');
        label = label.closest(parentSelector);
        
        if(label.find('.data-error').length != 0){
            label.find('.data-error').innerHTML = errors[key];
        } else{
            label.prepend("<span class='data-error'>"+errors[key]+"</span>");
        }
        
        label.find(dataShow).hide();
    }

    return 0;
}





function ValidWithFrontEnd(dataError, generalContainer, inputContainer, labelConteiner){
    var self = this;

    this.dataError = typeof dataError === "string" ? JSON.parse(dataError) : dataError;
    this.generalContainer = $(generalContainer);
    this.inputContainer = inputContainer;
    this.labelConteiner = labelConteiner;
    
    

    this.isValid = function(){
        var isValid = true;
        Array.prototype.forEach.call(self.dataError, function(item){
            var input =  self.generalContainer.find('[name='+item.name+']');
            
            var tmpValid = true;
            
            if(input.get(0).name == "select-box"){ 
                if(!input.get(0).isValid()) tmpValid = false;
            } else if(!item.pattern.test(input.val())) tmpValid = false;
            
            if(!tmpValid){
                var label = input.closest(self.inputContainer);
                if(label.find('.data-error').length != 0){
                    label.find('.data-error').innerHTML = item.errorMessage;
                } else{
                    label.prepend("<span class='data-error'>"+item.errorMessage+"</span>");
                }
                input.closest(self.inputContainer).find(self.labelConteiner).hide();
            } else {
                input.closest(self.inputContainer).find('.data-error').hide();
                input.closest(self.inputContainer).find(self.labelConteiner).show();
            }
        })
        return isValid;
    }
}

/*--------------------------------------------------------------------------------------------------------------------*/


