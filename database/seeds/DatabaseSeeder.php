<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


//         $this->call(UsersTableSeeder::class);
//         $this->call(CountriesSeeder::class);
//         $this->call(AcademicLevelsSeeder::class);
//         $this->call(DeadlinesSeeder::class);
//        $this->call(DeadlinesMonthSeeder::class);
//         $this->call(AcademicLevelDeadlinePriceSeeder::class);

//         $this->call(PaperTypeSeeder::class);
//         $this->call(CommentsSeeder::class);
//         $this->call(PaperSubjectsSeeder::class);
//        $this->call(MessagesSeeder::class);
//        $this->call(WriterCommentsSeeder::class);
//        $this->call(BonusesSeeder::class);
//        $this->call(FilesSeeder::class);
        $this->call(Examples::class);

    }
}
