<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;
use App\AcademicLevelDeadlinePrice;
use App\DiscountCode;

class Paper extends Model
{
//    protected $guarded = ['id'];

    const STATUS_UNPAID = 0;
    const STATUS_PAID = 1;
    const STATUS_ASSIGNED_MANAGER = 2;
    const STATUS_ASSIGNED_WRITER = 3;
    const STATUS_ASSIGNED_EDITOR = 4;
    const STATUS_PAPER_COMPLETED = 5;
    const STATUS_EDITOR_APPROVED = 6;
    const STATUS_PAPER_DELIVERED = 7;
    const STATUS_CLIENT_APPROVED = 8;

    public static $STATUSES = [
        self::STATUS_UNPAID => 'Unpaid',
        self::STATUS_PAID => 'Payment Received',
        self::STATUS_ASSIGNED_MANAGER => 'Manager Assigned',
        self::STATUS_ASSIGNED_WRITER => 'Writer Assigned',
        self::STATUS_ASSIGNED_EDITOR => 'Editor Assigned',
        self::STATUS_PAPER_COMPLETED => 'Paper Completed',
        self::STATUS_EDITOR_APPROVED => 'Editor Approved',
        self::STATUS_PAPER_DELIVERED => 'Paper Delivered',
        self::STATUS_CLIENT_APPROVED => 'Approved',
    ];

    const PREFERRED_WRITERS = [
        1 => 'Regular writer',
        2 => 'Advanced regular writer',
        3 => 'My previous writer',
        4 => 'TOP writer: Fulfilled by top 10 writers',
    ];

    const SERVICE_TYPES = [
        1 => 'Writing from scratch',
        2 => 'Editing/proofreading',
    ];

    const vipPrice = 15;
    const plagiarismReportPrice = 10;
    const abstractPagePrice = 15;

    protected $fillable = [
        'academic_level_id',
        'subject_id',
        'type_id',
        'client_id',
        'format',
        'abstract_page',
        'sources',
        'topic',
        'details',
        'plagiarism_report',
        'additional_materials',
        'type_of_service',
        'academic_level',
        'vip',
        'plagiarism_report',
        'pages_count',
        'slides_count',
        'spacing',
        'deadline_id',
        'status',
        'preferred_writer',
        'discount_code_id',
        'discount_code',
        'preferred_writer_index',
        'service_type_index',
        'price',
        'payment_id',
        'preferred_writer_id',
        'paid_at',
    ];

    protected $hidden = [
         'deadline',
         'preferred_writer_index',
         'service_type_index',
         'service_type_id',
         'created_at',
         'update',
         'client_id',
         'format',
         'abstract_page',
         'sources',
         'plagiarism_report',
         'additional_materials',
         'vip',
         'slides_count',
         'spacing',
         'first_deadline',
         'final_deadline',
         'academic_level_id',
         'discount_code_id',
         'deadline_id',
         'subject_id',
         'preferred_writer',
         'discount_code',
         'academic_level',
         'subject',
         'updated_at',
         'details',
         'type_id',
         'support_id',
         'payment_id',
         'internal_status',
         'editor_id',
         'preferred_writer_id',
         'type',
         'paid_at',
    ];

    protected $appends = ['subject_name', 'deadline_name', 'messages_count', 'type_name'];


    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function writer()
    {
        return $this->belongsTo('App\User', 'writer_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\PaperSubject', 'subject_id');
    }

    public function type()
    {
        return $this->belongsTo('App\PaperType', 'type_id');
    }

    public function deadline()
    {
        return $this->belongsTo('App\Deadline', 'deadline_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Message', 'paper_id');
    }

    public function unread_messages()
    {
        return $this->hasMany('App\Message', 'paper_id')->where('isread', 0);
    }

    public function level()
    {
        return $this->belongsTo('App\AcademicLevel', 'academic_level_id');
    }

    public function files()
    {
        return $this->hasMany(UserFile::class, 'paper_id');
    }

    public static function inquiryCreate(array $data = [])
    {
        return static::create($data);
    }

    public static function orderCreate(array $data = [])
    {
        $discount = DiscountCode::where('used', 0)->where('code', $data['discount_code'])->first();
        $data['status'] = "Unpaid";
        $data['client_id'] = request()->user()->id;
        $paper = static::create($data);
        if($discount == null) {

            $ref = User::where('ref_code', $data['discount_code'])->first();
            $user = request()->user();

            if($ref != null && $user->ref_id == null) {
                $user->ref_id = $ref->id;
                $user->save();
            } else {
                $paper['discount_code'] = '';
                $paper->save();
            }

        } else {
            $discount->update(['used' => 1, 'user_id' => request()->user()->id, 'order_id' => $paper->id]);
        }

        $paper->calcPrice();
        return $paper;
    }

    public function getSubjectNameAttribute()
    {
        return (bool)$this->subject == false ? '' : $this->subject->name;
    }

    public function getDeadlineNameAttribute()
    {
        return (bool)$this->deadline == false ? '' : $this->deadline->name;
    }

    public function getMessagesCountAttribute()
    {
        return $this->unread_messages->count();
    }

    public function getTypeNameAttribute()
    {
        if( isset($this->relations['type']) ) {
            return $this->type->name;
        }

        return '';
    }


    public function toTranslatedArray()
    {
        $res = [];
        $data = $this->toArray();
        array_walk($data, function($item, $key) use (&$res) {
            $res[trans('paper.'.$key)] = $item;
        });
        return $res;
    }

    public function calcPrice()
    {
        $basePrice = AcademicLevelDeadlinePrice::where('academic_level_id', $this->academic_level_id)
            ->where('deadline_id', $this->deadline_id)->where('type_id', $this->type_id)->first()->price;

        if($this->service_type_index == 2) $basePrice*= 0.6;
        if($this->preferred_writer_index == 2) $basePrice*= 1.3;
        else if($this->preferred_writer_index == 4) $basePrice*= 1.58;

        $pagesPrice = $basePrice*($this->pages_count*$this->spacing);
        $slidesPrice = ($basePrice/2)*$this->slides_count;

        $price = $pagesPrice + $slidesPrice + ($this->vip * self::vipPrice) + ($this->plagiarism_report * self::plagiarismReportPrice) + ($this->abstract_page * self::abstractPagePrice);

        $discount = DiscountCode::where('order_id', $this->id)->first();
        if($discount) {

            $price *= 0.9;

        } else if(request()->user()->ref_id != null && $this->discount_code != '' && $this->discount_code != null) {

            $ref = User::where('ref_code', $this->discount_code)->first();

            if($ref != null && request()->user()->ref_id == $ref->id) {
                $price *= 0.9;
            }
        }

        $this->price = $price;
        $this->save();
    }

    public function newCollection(array $models = [])
    {
        return new Extensions\PaperCollection($models);
    }
}
