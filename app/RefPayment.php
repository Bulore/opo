<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefPayment extends Model
{
    protected $table = 'ref_payments';

    protected $appends = ['date'];

    public function getDateAttribute()
    {
        return $this->created_at->format('M d Y g:i A');
    }
}
