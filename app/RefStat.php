<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefStat extends Model
{
    protected $table = 'ref_stats';

    protected $appends = ['date'];

    protected $fillable = [
        'recipient_id',
        'client_id',
        'amount'
    ];

    public function getDateAttribute()
    {
        return $this->created_at->format('M d Y g:i A');
    }
}
