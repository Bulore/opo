<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deadline extends Model
{
    public function newCollection(array $models = [])
    {
        return new Extensions\DeadlinesCollection($models);
    }

    public function scopeLessThanMonth($query)
    {
        return $query->where('hours', '<', 30*24);
    }
}
