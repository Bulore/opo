<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table = 'bonuses';
    protected $guarded = ['id'];

    public function paper()
    {
        $this->belongsTo('App\Paper', 'paper_id');
    }
}
