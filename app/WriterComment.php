<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WriterComment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'writer_comments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['text', 'value', 'rating', 'client_id', 'writer_id'];

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function writer()
    {
        return $this->belongsTo('App\User', 'writer_id');
    }
}
