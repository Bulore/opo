<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class IsClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() && $request->user()->access_level == User::USER_ACCESS_LEVEL) {
            return $next($request);
        }

        return redirect('/panel');
    }
}
