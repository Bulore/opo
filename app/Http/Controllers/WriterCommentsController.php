<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\WriterComment;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class WriterCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $writercomments = WriterComment::paginate(15);

        return view('writercomments.index', compact('writercomments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $users = User::all();
        $writers = $users->where('access_level', User::WRITER_ACCESS_LEVEL);
        $clients = $users->where('access_level', User::USER_ACCESS_LEVEL);

        return view('writercomments.create', compact('writers', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        WriterComment::create($request->all());

        Session::flash('flash_message', 'WriterComment added!');

        return redirect('writer-comments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $writercomment = WriterComment::findOrFail($id);

        return view('writercomments.show', compact('writercomment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $writercomment = WriterComment::findOrFail($id);

        return view('writercomments.edit', compact('writercomment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $writercomment = WriterComment::findOrFail($id);
        $writercomment->update($request->all());

        Session::flash('flash_message', 'WriterComment updated!');

        return redirect('writer-comments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        WriterComment::destroy($id);

        Session::flash('flash_message', 'WriterComment deleted!');

        return redirect('writer-comments');
    }
}
