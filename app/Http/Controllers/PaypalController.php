<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paper;
use Carbon\Carbon;

use Event;
use App\Events\OrderPaid;

use App\Http\Requests;
use Mockery\CountValidator\Exception;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PaypalController extends Controller
{
    public function createOrder()
    {
        if(!\Auth::check()) return response('user is not authorized', 400);
        $paper = Paper::orderCreate(request()->all());
        return static::createPayment($paper);
    }

    public function paymentApproved()
    {
//        var_dump(request()->all());
        $apiContext = static::getApiContext();
        try {
            $paymentId = request('paymentId');
            $payment = Payment::get($paymentId, $apiContext);
            $payerId = request('PayerID');
            $paymentExecution= new PaymentExecution();
            $paymentExecution->setPayerId($payerId);
            $payment->execute($paymentExecution, $apiContext);

            $paper = Paper::where('payment_id', $paymentId)->first();
            $paper->update([
                'status' => Paper::$STATUSES[Paper::STATUS_PAID],
                'internal_status' => Paper::STATUS_PAID,
                'paid_at' => Carbon::now(),
            ]);

            Event::fire(new OrderPaid($paper));

        } catch(\PayPal\Exception\PayPalConnectionException $ex) {
//            echo $ex->getData(), '<br>';
//            echo $ex->getMessage(), '<br>';
        }


        return redirect('/user');
    }

    public function paymentCancelled()
    {
        return redirect('/user');
    }

    public function handleIpn()
    {
        \DB::table('ipn_test')->insert([
            'data' => json_encode(request()->all()),
        ]);

        return 'success';
    }

    protected static function getApiContext()
    {
//        return config('paypal.mode');
        $clientId = config('paypal.client_id');
        $clientSecret = config('paypal.secret');
        $mode = config('paypal.mode');
        $apiContext = new ApiContext(new OAuthTokenCredential(
            $clientId, $clientSecret));
        $apiContext->setConfig(['mode' => $mode]);
        return $apiContext;
    }

    protected static function createPayment(Paper $paper)
    {
        $apiContext = static::getApiContext();
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $item1 = new Item();
        $item1->setName('Paper at OriginalPapersOnly.com')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku($paper->id) // Similar to `item_number` in Classic API
            ->setPrice($paper->price);
        $amount = new Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($paper->price);
        $itemList = new ItemList();
        $itemList->setItems(array($item1));
        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Payment to OriginalPapersOnly.com');
        $transaction->setItemList($itemList);
        $payment = new Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setTransactions(array($transaction));

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('/payment-approved'))
            ->setCancelUrl(url('/payment-cancelled'));
        $payment->setRedirectUrls($redirectUrls);
        $payment->create($apiContext);

        $paper->payment_id = $payment->getId();
        $paper->save();
//        foreach ($payment->getLinks() as $link) {
//            if ($link->getMethod() == 'REDIRECT') {
//                return response()->json(['url' => $link->getHref()]);
//            }
//        }
//        return response()->json(['url' => url('/user')]);
        return response()->json(['url' => $payment->getApprovalLink()]);
    }

    public function test()
    {
        try {
            static::getApiContext();
        } catch(Exception $e) {
            return $e->getMessage();
        }

        return 'success';

    }
}
