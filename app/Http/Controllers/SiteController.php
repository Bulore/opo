<?php

namespace App\Http\Controllers;

use App\AcademicLevel;
use App\PaperType;
use App\PaperSubject;
use App\Deadline;
use App\Subscription;
use App\User;
use App\Paper;
use App\Country;
use Auth;
use Validator;
use App\ExampleArticle;
use Mail;

class SiteController extends Controller
{
    public function getIndex()
    {
        $deadlineNames = ["14 Days", "10 Days", "6 Days", "3 Days", "2 Days", "24 Hours", "3 Hours"];
        $deadlines = Deadline::whereIn('name', $deadlineNames)->get();

        $levelNames = ["Undergraduate", "Bachelor", "Professional"];
        $levels = AcademicLevel::whereIn('name', $levelNames)->get();

        $mtx = json_encode([
            [11.99, 16.00, 18.00, 20.00, 22.00, 25.00, 43.00],
            [16.00, 18.00, 20.00, 23.00, 25.00, 27.00, 45.00],
            [19.00, 21.00, 24.00, 28.00, 31.00, 35.00, 56.00],
        ]);

        return view('sections.index', compact('deadlines', 'levels', 'mtx'));
    }

    public function getFaq()
    {
        return view('sections.faq');
    }

    public function getHowitworks()
    {
        return view('sections.howitworks');
    }

    public function getPrices()
    {
        return view('sections.prices');
    }

    public function getSamples()
    {
        return view('sections.samples');
    }

    public function getTestimonials()
    {
//        $ids = [1,20,23];
        $comments = \App\Comment::select(\DB::raw("DATE_FORMAT(created_at, '%M %d %Y') as `date`, subject, pages, stars, text, `from`, name "))
            ->orderBy('created_at')->paginate(7);
        $sliderComments = \App\Comment::select(\DB::raw("DATE_FORMAT(created_at, '%M %d %Y') as `date`, subject, pages, stars, text, `from`, name "))
            ->orderByRaw("RAND()")->take(3)->get();

        return view('sections.testimonials', compact('comments', 'sliderComments'));
    }

    public function order()
    {
        $academicLevels = AcademicLevel::all();
        $types = PaperType::with('prices')->get();
        $subjects = PaperSubject::all();
        $deadlines = Deadline::orderBy('hours')->get();

        $countries = \DB::table('countries')->orderBy('name')->get();

        $is_bot = preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
        $countryCode = '';
        if(!$is_bot) {
            try {
                $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                $countryCode = $countryData['country']['iso'];
            } catch (\Exception $e) {}

        }
        return view('sections.order', compact('academicLevels', 'types', 'subjects', 'deadlines', 'countries', 'countryCode'));
    }

    public function ajaxGetUser()
    {
        $messages = [
            'remember.accepted' => 'This field must be accepted.',
        ];
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            'remember' => 'accepted',
        ], $messages);
        if ($validator->fails()) {
            return response(json_encode(['errors' => $validator->getMessageBag()]), 400);
        }

        $email = request('email');
        $password = request('password');
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = User::where('email', $email)->first();
            return json_encode(['id' => $user->id]);
        }
        return response(json_encode(['errors'=>['email'=>'no such user']]), 400);
    }

    public function ajaxRegisterUser()
    {
        $messages = [
            'remember.accepted' => 'This field must be accepted.',
            'phone.regex' => 'The phone field is required ( +19293672627 )',
        ];
        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|regex:/^\+[0-9]{10,13}$/i|unique:users',
            'remember' => 'accepted',

        ], $messages);

        if ($validator->fails()) {
            return response(json_encode(['errors' => $validator->getMessageBag()]), 400);
        }

        $data = request()->all();
        $data['country_id'] = Country::where('code', $data['country'])->first()->id;
        $user = User::createClientWithoutPassword($data);
        Auth::login($user, true);
        return json_encode(['id' => $user->id]);
    }


    public function placeOrder()
    {
        if(!Auth::check()) {
            return response('no such user', 400);
        }
//        $user = request()->user();

//        $data = request()->all();

        $paper = Paper::orderCreate(request()->all());
//        $paper->calcPrice();

        return 'success';
    }

    public function emailCallback()
    {
        $messages = [
            'phone.regex' => 'The phone field is required ( +19293672627 )',
        ];

        $validator = Validator::make(request()->all(), [
            'subject' => 'required',
            'name' => 'required',
            'phone' => 'required|regex:/^\+[0-9]{10,13}$/i',
            'text' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return response(json_encode(['errors' => $validator->getMessageBag()]), 400);
        }

	   $subject = request('subject');
        $name = request('name');
        $phone = request('phone');
        $text = request('text');

        Mail::send('emails.email-callback', compact('subject', 'name', 'phone', 'text'),
            function($message)
            {
                $message->to('support@originalpapersonly.com')->from( 'no-reply@originalpapersonly.com', 'Admin' );

//                foreach($emails as $email)
//                {
//                    $message->to($email->email)->from( 'no-reply@ridanta.com', 'Admin' );
//                }
            }
        );


        return 'success';
    }

    public function getContacts()
    {
        return view('sections.contacts');
    }

    public function getFreeInquiry()
    {
        $academicLevels = AcademicLevel::all();
        $types = PaperType::all();
        $subjects = PaperSubject::all();
        $deadlines = Deadline::orderBy('hours')->get();

        $countries = \DB::table('countries')->orderBy('name')->get();

        $is_bot = preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
        $countryCode = '';
        if(!$is_bot) {
            try {
                $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                $countryCode = $countryData['country']['iso'];
            } catch (\Exception $e) {}

        }

        return view('sections.free-inquiry', compact('academicLevels', 'types', 'subjects', 'deadlines', 'countries', 'countryCode'));
    }

    public function postFreeInquiry()
    {
        //TODO: validation
        $type = request('type');
        if($type == 'register') {
            $data = request()->all();
            $data['password'] = bcrypt($data['password']);
            $user = User::createClient($data);
            $paper = Paper::inquiryCreate($data);
            $paper->client_id = $user->id;
            $paper->save();
            return 'success';
        } else {
            $email = request('email');
            $password = request('password');
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $user = User::where('email', $email)->first();
                $paper = Paper::inquiryCreate(request()->all());
                $paper->client_id = $user->id;
                $paper->save();
                return 'success';
            }
            return response('no such user', 400);
        }

    }

    public function getOurExtras()
    {
        return view('sections.our-extras');
    }

    public function getOurWriters()
    {
        $comments = \App\WriterComment::with('client', 'writer')->paginate(5);
        return view('sections.our-writers', compact('comments'));
    }

    public function getPaymentAuth()
    {
        return view('sections.payment-auth');
    }

    public function getPaymentSystem()
    {
        return view('sections.payment-system');
    }

    public function getPrivacy()
    {
        return view('sections.privacy');
    }

    public function getReferalProgram()
    {
        return view('sections.referal-program');
    }

    public function getRevision()
    {
        return $this->getTermsOfUse();
    }

    public function getTermsOfUse()
    {
        return view('sections.terms-of-use');
    }

    public function examples()
    {
        $items = ExampleArticle::orderBy('priority')->paginate(7);

        return view('sections.examples-and-samples', compact('items'));
    }

    public function example($id)
    {
        $item = ExampleArticle::find($id);
        if($item == null) {
            return redirect()->back();
        }

        return view('sections.example-of-essay', compact('item'));
    }

    public function whyNotTurnitin()
    {
        return view('sections.why-not-turnitin');
    }

    public function subscribe()
    {
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|max:255|unique:subscriptions',
        ]);

        if($validator->fails()) {
            return response(json_encode(['errors' => $validator->getMessageBag()]), 400);
        }

        $email = request('email');

        $subscription = Subscription::create([
            'email' => $email,
        ]);

        $code = str_random(32);

        \DB::table('discount_codes')->insert([
            'code' => $code,
            'used' => 0,
        ]);

        Mail::send('emails.subscription', compact('code', 'subscription'),
            function($message) use ($email)
            {
                $message->subject('Thanks for subscription');
                $message->to($email)->from( 'no-reply@originalpapersonly.com', 'originalpapersonly.com');
            }
        );

        return 'success';
    }

    public function unsubscribe($id)
    {
        Subscription::destroy($id);

        return 'successfully unsubscribed';
    }

    public function checkDiscount()
    {
        $code = request('code');

        if( \DB::table('users')->where('ref_code', $code)->count() > 0 && request()->user()->ref_id == null && $code != request()->user()->ref_code) {
            return '1';
        }

        $model = \DB::table('discount_codes')->where('code', $code)->where('used', 0)->first();

        return $model ? '1' : '0';
    }
}
