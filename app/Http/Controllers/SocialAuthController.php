<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;

class SocialAuthController extends Controller
{
    public function facebookAuth()
    {
        //---------------------------------------------//Parameters//-----------------------------------------------------//
        $params = [
            'client_id' => "205520129862970",
            'client_secret' => "178d7f8f1a2e08ac990948968d9e895c",
            'redirect_uri' => url('/social-auth/facebook')
        ];
        //----------------------------------------------------------------------------------------------------------------//
        $code = request('code');
        if ($code) {
            $access_token = $this->facebookGetToken($params, $code);
            $data = $this->facebookGetUser($access_token);
            $user = User::getSocialUser($data['id']);
            if (!$user) {
                $user = User::create([
                    'name' => $data['first_name'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => "id{$data['id']}@facebook.com",
                    'social_id' => $data['id'],
                    'access_level' => User::USER_ACCESS_LEVEL,
                ]);
            }
            Auth::login($user, true);
            return redirect(url('/user'));
        } else {
            return $this->facebookGetCode($params);
        }

    }

//----------------------------------//Helper-functions-for-facebookAuth()//-------------------------------------------//
    private function facebookGetCode($params)
    {
        $code_url = "https://www.facebook.com/dialog/oauth?" .
            "client_id={$params['client_id']}&" .
            "client_secret={$params['client_secret']}&" .
            "redirect_uri={$params['redirect_uri']}&" .
            "response_type=code";
        return redirect($code_url);
    }

    private function facebookGetToken($params, $code)
    {
        $token_url = "https://graph.facebook.com/oauth/access_token?" .
            "client_id={$params['client_id']}&" .
            "client_secret={$params['client_secret']}&" .
            "redirect_uri={$params['redirect_uri']}&" .
            "code=$code";
        parse_str(@file_get_contents($token_url), $data);
        return $data['access_token'];
    }

    private function facebookGetUser($access_token)
    {
        $user_url = "https://graph.facebook.com/me?" .
            "access_token=$access_token" .
            "&fields=id,first_name,last_name";
        return json_decode(@file_get_contents($user_url), true);
    }
//--------------------------------------------------------------------------------------------------------------------//
}
