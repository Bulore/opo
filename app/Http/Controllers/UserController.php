<?php

namespace App\Http\Controllers;

use App\AcademicLevel;
use App\PaperType;
use App\PaperSubject;
use App\Deadline;
use App\RefPayment;
use App\RefStat;
use App\User;
use App\Paper;
use App\Message;
use App\UserFile;
use App\EmailToken;
use App\PreferredWriter;
use Auth;
use Carbon\Carbon;
use Storage;
use Hash;
use DB;
use Log;
use Image;


class UserController extends Controller
{

    public function getOrder()
    {
        $user_id = request()->user()->id;
        $orders = Paper::with('type','unread_messages')->where('client_id', $user_id)->get()->makeHidden('unread_messages')->toTranslatedArray();
        $paperTypes = PaperType::select('name')->get();
        $subjects = PaperSubject::select('name')->get();
        $types = array_values(Paper::SERVICE_TYPES);
        return view('user.sections.order', compact('orders', 'paperTypes', 'subjects', 'types' ));
    }

    public function getOrderOpen($id)
    {
        $order = Paper::findOrFail($id);
        $activePage = request('active_page', 1);
        if($order->internal_status >= Paper::STATUS_ASSIGNED_WRITER) {
            $prefWriter = PreferredWriter::where('writer_id', $order->writer)->count() > 0;
        } else {
            $prefWriter = false;
        }
        $prefWriter = (int)$prefWriter;

        return view('user.sections.order-open', compact('order', 'activePage', 'prefWriter'));
    }

    public function getAuthorization()
    {
        $user = request()->user();
        return view('user.sections.authorization', compact('user'));
    }

    public function getBonuses()
    {
        $bonuses = \App\Bonus::select(DB::raw("DATE_FORMAT(created_at, '%Y %M %d %h:%i %p') as `Date`, paper_id as `Order`, status as `Status`, amount as `Amount`"))
            ->whereIn('paper_id', request()->user()->orders->lists('id') )->get();
        return view('user.sections.bonuses', compact('bonuses'));
    }

    public function getFiles()
    {
        $user = request()->user();
        $files = UserFile::getUserFiles(10, 0, 'client');

        $recipients = request()->user()->getRecipients();
        return view('user.sections.files', compact('files', 'recipients', 'user'));
    }

     public function getProfile()
    {
        $user = request()->user();
        $countries = \DB::table('countries')->orderBy('name')->get();
        if($user->survey_completed == 1) {
            $code = DB::table('discount_codes')->where('user_id', $user->id)->first()->code;
        } else {
            $code = '';
        }

        return view('user.sections.profile', compact('user', 'countries', 'code'));
    }

    public function getHowItWorks()
    {
        return view('user.sections.how-it-works');
    }

    public function getMessages()
    {
        $user = request()->user();
//        $messages = request()->user()->outgoingMessages->merge(request()->user()->incomingMessages)->sortBy('created_at');
        $messages = Message::getUserMessages(10, 0, 'client');

        $recipients = request()->user()->getRecipients();
        return view('user.sections.messages', compact('messages', 'recipients', 'user'));
    }

    public function getPreferredWriters()
    {
        $writers = \DB::select('
            SELECT t1.`writer_id` as `Writer ID` , t1.`created_at` as `Bookmark Date` , COUNT( * ) as `Orders` , t2.`subject` AS Subjects
            FROM  `preferred_writers` AS t1
            JOIN (
            SELECT tt1.`writer_id` , tt1.`client_id` , tt2.`name` AS subject
            FROM  `papers` AS tt1
            JOIN  `paper_subjects` AS tt2 ON tt1.`subject_id` = tt2.`id`
            ) as t2
            ON t1.`writer_id` = t2.`writer_id`
            WHERE t1.`user_id` = ?
            GROUP BY t1.`writer_id`
            ',
            [request()->user()->id]
        );

        $orders = Paper::where('client_id', request()->user()->id)->where('writer_id', '<>', 0)->get()->groupBy('writer_id');

        foreach($writers as $writer) {
            $writer->orders = $orders[$writer->{'Writer ID'}];
        }

        return view('user.sections.preferred-writers', compact('writers'));
    }

    public function getReferralProgram()
    {
        $referrals = request()->user()->subusers()->select('id')->with('orders')->get()
            ->makeHidden('orders')
            ->each(function($x) {
                $x->orders_count = $x->orders->count();
            });

        $payments = RefPayment::orderBy('created_at', 'desc')->get();
        $stats = RefStat::orderBy('created_at', 'desc')->get();

        $top10 = RefStat::select(DB::raw('SUM(amount) as sum'))
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupBy('recipient_id')
            ->orderBy('sum', 'desc')
            ->limit(10)
            ->get()->pluck('sum');

        return view('user.sections.referral-program', compact('referrals', 'payments', 'stats', 'top10'));
    }

    public function getStoreCredit()
    {
        return view('user.sections.store-credit');
    }

    public function getNewOrder()
    {
        $academicLevels = AcademicLevel::all();
        $types = PaperType::with('prices')->get();
        $subjects = PaperSubject::all();
        $deadlines = Deadline::orderBy('hours')->get();

        $countries = \DB::table('countries')->orderBy('name')->get();

        $is_bot = preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
        $countryCode = '';
        if(!$is_bot) {
            try {
                $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                $countryCode = $countryData['country']['iso'];
            } catch (\Exception $e) {}

        }

        $preferredWriterId = request('preferred_writer_id', '');
        return view('user.sections.new-order', compact('academicLevels', 'types', 'subjects', 'deadlines', 'countries', 'countryCode', 'preferredWriterId'));
    }

    public function getContactUs()
    {
        return view('user.sections.contact-us');
    }

    public function ajaxAuthorization()
    {
        $user = request()->user();
        return view('user.sections.block.authorization', compact('user'));
    }

    public function ajaxBonuses()
    {
        $bonuses = \App\Bonus::select(DB::raw("DATE_FORMAT(created_at, '%Y %M %d %h:%i %p') as `Date`, paper_id as `Order`, status as `Status`, amount as `Amount`"))
                ->whereIn('paper_id', request()->user()->orders->lists('id') )->get();
        return view('user.sections.block.bonuses', compact('bonuses'));
    }

    public function ajaxFiles()
    {
        $user = request()->user();
        $files = UserFile::getUserFiles(10, 0, 'client');
        return view('user.sections.block.files', compact('files', 'user'));
    }

    public function ajaxHowItWorks()
    {
        return view('user.sections.block.how-it-works');
    }

    public function ajaxMessages()
    {
        $user = request()->user();
//        $messages = Message::where('from_id', $user_id)->orWhere('to_id', $user_id)->orderBy('created_at')->get();
        $messages = Message::getUserMessages(10, 0, 'client');
        return view('user.sections.block.messages', compact('messages', 'user'));
    }

    public function ajaxOrderOpen()
    {
        $id = request('id');
        $order = Paper::find($id);
        if($order == null) {
            return response('not found', 400);
        }

        if($order->internal_status >= Paper::STATUS_ASSIGNED_WRITER) {
            $prefWriter = PreferredWriter::where('writer_id', $order->writer)->count() > 0;
        } else {
            $prefWriter = false;
        }
        $prefWriter = (int)$prefWriter;

        $activePage = request('active_page', 1);

        return view('user.sections.block.order-open', compact('order', 'activePage', 'prefWriter'));
    }

    public function ajaxOrder()
    {
        $user_id = request()->user()->id;
        $orders = Paper::with('unread_messages')->where('client_id', $user_id)->get()->makeHidden('unread_messages')->toTranslatedArray();
        $paperTypes = PaperType::select('name')->get();
        $subjects = PaperSubject::select('name')->get();
        $types = array_values(Paper::SERVICE_TYPES);
        return view('user.sections.block.order', compact('orders', 'paperTypes', 'subjects', 'types' ));
    }

    public function ajaxPreferredWriters()
    {
        $writers = \DB::select('
            SELECT t1.`writer_id` as `Writer ID` , t1.`created_at` as `Bookmark Date` , COUNT( * ) as `Orders` , t2.`subject` AS Subjects
            FROM  `preferred_writers` AS t1
            JOIN (
            SELECT tt1.`writer_id` , tt1.`client_id` , tt2.`name` AS subject
            FROM  `papers` AS tt1
            JOIN  `paper_subjects` AS tt2 ON tt1.`subject_id` = tt2.`id`
            ) as t2
            ON t1.`writer_id` = t2.`writer_id`
            WHERE t1.`user_id` = ?
            GROUP BY t1.`writer_id`
            ',
            [request()->user()->id]
        );

        $orders = Paper::where('client_id', request()->user()->id)->where('writer_id', '<>', 0)->get()->groupBy('writer_id');

        foreach($writers as $writer) {
            $writer->orders = $orders[$writer->{'Writer ID'}];
        }

        return view('user.sections.block.preferred-writers', compact('writers'));
    }

    public function ajaxProfile()
    {
        $user = request()->user();
        $countries = \DB::table('countries')->orderBy('name')->get();
        return view('user.sections.block.profile', compact('user', 'countries'));
    }

    public function ajaxReferralProgram()
    {
        $referrals = request()->user()->subusers()->select('id')->with('orders')->get()
            ->makeHidden('orders')
            ->each(function($x) {
                $x->orders_count = $x->orders->count();
            });

        $payments = RefPayment::where('user_id', request()->user()->id)->orderBy('created_at', 'desc')->get();
        $stats = RefStat::where('recipient_id', request()->user()->id)->orderBy('created_at', 'desc')->get();

        $top10 = RefStat::select(DB::raw('SUM(amount) as sum'))
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupBy('recipient_id')
            ->orderBy('sum', 'desc')
            ->limit(10)
            ->get()->pluck('sum');

        return view('user.sections.block.referral-program', compact('referrals', 'payments', 'stats', 'top10'));
    }

    public function ajaxStoreCredit()
    {
        return view('user.sections.block.store-credit');
    }

    public function ajaxNewOrder()
    {
         $academicLevels = AcademicLevel::all();
        $types = PaperType::with('prices')->get();
        $subjects = PaperSubject::all();
        $deadlines = Deadline::orderBy('hours')->get();

        $countries = \DB::table('countries')->orderBy('name')->get();

        $is_bot = preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
        $countryCode = '';
        if(!$is_bot) {
            try {
                $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                $countryCode = $countryData['country']['iso'];
            } catch (\Exception $e) {}

        }
        $preferredWriterId = request('preferred_writer_id', '');
        return view('user.sections.block.new-order', compact('academicLevels', 'types', 'subjects', 'deadlines', 'countries', 'countryCode', 'preferredWriterId'));
    }

    public function ajaxContactUs()
    {
        return view('user.sections.block.contact-us');
    }

    public function readMessage()
    {
        $id = request('id');
        $message = Message::find($id);
        if($message === null || $message->to_id != request()->user()->id) {
            return response('No such message', 400);
        }

        $message->isread = true;
        $message->save();
        return response('success');
    }

    public function saveMessage()
    {
        $data = request()->all();
        $data['isread'] = false;
        $data['from_id'] = request()->user()->id;
        $data['from_type'] = 'client';
        $message = Message::create($data);
        if($message->to_type == 'manager') $message->to_type = 'support';
        return $message->toJson();
    }

    public function saveFile()
    {
        if(!request()->hasFile('file')) {
            return response('no file', 400);
        }

        $file = request()->file('file');
        $filename = time().'.'.$file->getClientOriginalExtension();
        Storage::put($filename,  file_get_contents($file));

        $data = request()->all();
        $data['original_filename'] = $file->getClientOriginalName();
        $data['filename'] = $filename;
        $data['isimage'] = (bool)getimagesize(storage_path('app/'. $filename));
        $data['isread'] = false;
        $data['from_id'] = request()->user()->id;
        $data['from_type'] = 'client';
        $userFile = UserFile::create($data);
        if($userFile->to_type == 'manager') $userFile->to_type = 'support';

        return $userFile->toJson();
    }

    public function readFile()
    {
        $id = request('id');
        $file = UserFile::find($id);
        if($file === null || $file->to_id != request()->user()->id) {
            return response('No such file', 400);
        }

        $file->isread = true;
        $file->save();
        return response('success');
    }

    public function createAuthToken()
    {
        $user = request()->user();
        $token = str_random(128);
        \App\EmailToken::create([
            'token' => $token,
            'user_id' => $user->id,
        ]);

        $url = url("/user/auth-email/$token");

        \Mail::send('emails.auth-email', compact('url', 'user'),
            function($message) use ($user)
            {
                $message->to($user->email)->subject('Authorize your account')->from( 'no-reply@originalpapersonly.com', 'originalpapersonly.com');
            }
        );

        return 'success';
    }

    public function authEmail($tokenStr)
    {
        $token = EmailToken::where('token', $tokenStr)->first();
        if($token != null && $token->user_id == request()->user()->id) {
            $user = $token->user;
            $user->email_authorized = true;
            $user->save();
            $token->delete();
        }

        return redirect('/user/profile');
    }

    public function addPreferredWriter()
    {
        $writerId = request('writer_id');
        $userId = request()->user()->id;
        $query = PreferredWriter::where('writer_id', $writerId)->where('user_id', $userId);
        if($query->count() > 0) {
            $query->delete();
            $res = 0;
        } else {
            PreferredWriter::create([
                'user_id' => $userId,
                'writer_id' => $writerId,
            ]);
            $res = 1;
        }

        return json_encode(compact('res'));
    }

    public function deletePreferredWriter()
    {
        $writerId = request('writer_id');
        $userId = request()->user()->id;
        PreferredWriter::where('writer_id', $writerId)->where('user_id', $userId)->delete();
        return 'success';
    }

    public function completeSurvey()
    {
        $user = request()->user();

        if($user->survey_completed == true) {
            return response('user has already completed survey', 400);
        }

        $survey = DB::table('surveys')->insert([
            'json_data' => json_encode(request()->all()),
            'user_id' => $user->id,
        ]);
        $user->survey_completed = true;
        $user->save();

        $code = str_random(32);
        DB::table('discount_codes')->insert([
            'code' => $code,
            'user_id' => $user->id,
            'used' => false,
        ]);

        return json_encode(['code' => $code]);
    }

    public function updateAdditionalInfo()
    {
        request()->user()->update(request()->all());
        return 'success';
    }

    public function changePassword()
    {
        $user = request()->user();
        $oldPassword = request('old_password');
        $newPassword = request('password');
        $confirmation = request('password_confirmation');
        if(Hash::check($oldPassword, $user->password)) {

            if($newPassword === '') {
                return response(json_encode(['errors'=>['password'=>'Password should not be empty']]), 400);
            }

            if($newPassword != $confirmation) {
                return response(json_encode(['errors'=>['password'=>'Passwords don\'t match']]), 400);
            }

            if(strlen($newPassword) < 6) {
                return response(json_encode(['errors'=>['password'=>'Passwords length should be at least 6 symbols']]), 400);
            }

            $user->fill([
                'password' => Hash::make($newPassword)
            ])->save();

        } else {
            return response(json_encode(['errors'=>['old_password'=>'Wrong password']]), 400);
        }

        return 'success';
    }

    public function getMoreMessages()
    {
        $length = request('length');
        $offset = request('offset');
        $messages = Message::getUserMessages($length, $offset, 'client');
        $unread = [];
        $read = [];
        foreach($messages as $message) {
            if($message->isread == false) {
                $unread[] = $message;
            } else {
                $read[] = $message;
            }
        }
        $messages = json_encode(compact('read', 'unread'));
        return $messages;
    }

    public function getMoreFiles()
    {
        $length = request('length');
        $offset = request('offset');
        $files = UserFile::getUserFiles($length, $offset, 'client');
        $unread = [];
        $read = [];
        foreach($files as $file) {
            if($file->isread == false) {
                $unread[] = $file;
            } else {
                $read[] = $file;
            }
        }
        $files = json_encode(compact('read', 'unread'));

        return $files;
    }

    public function orderMoreMessages()
    {
        $orderId = request('order_id');
        $length = request('length');
        $offset = request('offset');
        $messages = Message::where('paper_id', $orderId)->orderBy('created_at', 'desc')->limit($length)->offset($offset)->get();

        return $messages->toJson();
    }

    public function orderMoreFiles()
    {
        $orderId = request('order_id');
        $length = request('length');
        $offset = request('offset');
        $files = UserFile::where('paper_id', $orderId)->orderBy('created_at', 'desc')->limit($length)->offset($offset)->get();

        return $files->toJson();
    }

    public function setRefPaymentMethod()
    {
        $user = request()->user();

        $user->payment_method = request('method');
        $user->payment_details = request('details');

        $user->save();

        return 'success';
    }

    public function storeBonuses()
    {
        $user = request()->user();

        $user->store_credit = $user->store_credit+$user->bonuses;

        $user->bonuses = 0;

        $user->save();

        return 'success';
    }

    public function getPromoCard()
    {
        $image = Image::make(public_path('/images/originalpapersonly.png'));

        $code = request()->user()->ref_code;

        $image->text($code, 127, 145, function($font) {
            $font->file(public_path('/fonts/arial.ttf'));
            $font->size(12);
            $font->color('#58658c');
        });

        return response($image->encode('png'), 200, [
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => "attachment; filename=card.png"
        ]);
    }

}
