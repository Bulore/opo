<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Paper;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class PapersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $papers = Paper::paginate(15);

        return view('papers.index', compact('papers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('papers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        Paper::create($request->all());

        Session::flash('flash_message', 'Paper added!');

        return redirect('papers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $paper = Paper::findOrFail($id);

        return view('papers.show', compact('paper'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $paper = Paper::findOrFail($id);

        return view('papers.edit', compact('paper'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        
        $paper = Paper::findOrFail($id);
        $paper->update($request->all());

        Session::flash('flash_message', 'Paper updated!');

        return redirect('papers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Paper::destroy($id);

        Session::flash('flash_message', 'Paper deleted!');

        return redirect('papers');
    }

    public function login()
    {
        $email = request('email');
        $password = request('password');
        \Auth::attempt(['email' => $email, 'password' => $password]);

        return redirect()->back();
    }

}
