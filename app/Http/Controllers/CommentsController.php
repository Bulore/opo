<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\comment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $comments = comment::paginate(15);

        return view('comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['subject' => 'required', 'pages' => 'required', 'stars' => 'required', ]);

        comment::create($request->all());

        Session::flash('flash_message', 'comment added!');

        return redirect('comments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $comment = comment::findOrFail($id);

        return view('comments.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $comment = comment::findOrFail($id);

        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['subject' => 'required', 'pages' => 'required', 'stars' => 'required', ]);

        $comment = comment::findOrFail($id);
        $comment->update($request->all());

        Session::flash('flash_message', 'comment updated!');

        return redirect('comments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        comment::destroy($id);

        Session::flash('flash_message', 'comment deleted!');

        return redirect('comments');
    }
}
