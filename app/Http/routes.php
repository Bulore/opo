<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'SiteController@getIndex');
Route::get('/faq', 'SiteController@getFaq');
Route::get('/howitworks', 'SiteController@getHowitworks');
Route::get('/prices', 'SiteController@getPrices');
Route::get('/samples', 'SiteController@getSamples');
Route::get('/testimonials', 'SiteController@getTestimonials');
Route::get('/order', [
    'uses' => 'SiteController@order',
    'as' => 'order',
]);
Route::post('/order', [
    'uses' => 'PaypalController@createOrder',
    'as' => 'placeOrder',
]);
Route::post('/ajax-get-user', 'SiteController@ajaxGetUser');
Route::post('/ajax-register-user', 'SiteController@ajaxRegisterUser');

Route::get('/contacts', 'SiteController@getContacts');
Route::get('/free-inquiry', 'SiteController@getFreeInquiry');
Route::post('/free-inquiry', 'SiteController@postFreeInquiry');
Route::get('/our-extras', 'SiteController@getOurExtras');
Route::get('/our-writers', 'SiteController@getOurWriters');
Route::get('/payment-auth', 'SiteController@getPaymentAuth');
Route::get('/payment-system', 'SiteController@getPaymentSystem');
Route::get('/privacy', 'SiteController@getPrivacy');
Route::get('/referal-program', 'SiteController@getReferalProgram');
Route::get('/revision', 'SiteController@getRevision');
Route::get('/terms-of-use', 'SiteController@getTermsOfUse');
Route::get('/why-not-turnitin', 'SiteController@whyNotTurnitin');
Route::get('/subscribe', 'SiteController@subscribe');
Route::get('/unsubscribe/{id}', 'SiteController@unsubscribe');
Route::get('/submit-callback', 'SiteController@emailCallback');
Route::get('/check-discount', 'SiteController@checkDiscount');


Route::auth();

Route::get('/home', function() {
    return redirect('/panel');
});
Route::get('/admin', ['middleware' => ['auth.basic', 'admin'], 'uses' => 'AdminController@index']);

Route::group(['middleware' => 'auth'], function() {
    Route::get('/paper/order', 'PapersController@create');
    Route::post('/paper/order', 'PapersController@store');
});

Route::resource('papers', 'PapersController');
Route::post('/papers/login', 'PapersController@login');

//tests
Route::get('/cc', function(){
    
});
Route::get('/pricetest', function() {
    return view('sideblocks.getaPriceBlock');
});
Route::get('/geo', function() {
    $is_bot = preg_match(
        "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
        $_SERVER['HTTP_USER_AGENT']
    );
    $geo = !$is_bot ? json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true) : [];
    var_dump($geo);
    return 'success';
});


Route::group(['middleware' => ['auth.basic', 'admin']], function() {
    Route::resource('writer-comments', 'WriterCommentsController');
    Route::get('/seo', function() {
        $seo = App\Seo::all()->toJson();
        return view('seo', compact('seo'));
    });
    Route::post('/seo', function() {
        $data = request()->input('data');
        $data = json_decode($data, true);

        foreach($data as $id => $values) {
            DB::table('seo')->where('id', $id)->update([
                'description' => $values['description'],
                'keywords' => $values['keywords'],
                'title' => $values['title']
            ]);
        }
        return 'success';
    });
});


Route::resource('comments', 'CommentsController');

Route::get('/callback', function (){
   return view('popups.callback');
});
//Route::get('/user', function () {
//    return view('user.layouts.base');
//});
//Route::get('/order-open', function () {
//    return view('user.sections.order-open');
//});

Route::get('/panel', [
    'middleware' => 'auth',
    'as' => 'panel',
    'uses' => function() {
        $routes = [
            App\User::USER_ACCESS_LEVEL => '/user',
            App\User::WRITER_ACCESS_LEVEL => '/writer',
            App\User::SUPPORT_ACCESS_LEVEL => '/support',
            App\User::ADMIN_ACCESS_LEVEL => '/admin',
        ];

        $access_level = request()->user()->access_level;

        if(array_key_exists($access_level, $routes)) {
            return redirect($routes[$access_level]);
        }

        return redirect('/');
    },
]);

Route::group(['prefix' => 'user', 'middleware' => ['auth', 'client']], function () {
    Route::get('/', ['as'=>'user', 'uses'=>function() {
        return redirect()->route('user.order');
    }]);

    Route::get('/order', [
        'uses' =>'UserController@getOrder',
        'as' => 'user.order',
    ]);

    Route::get('/order-open/{id}', [
        'uses' =>'UserController@getOrderOpen',
        'as' => 'user.order-open',
    ]);

    Route::get('/authorization', [
        'uses' =>'UserController@getAuthorization',
        'as' => 'user.authorization',
    ]);

    Route::get('/bonuses', [
        'uses' =>'UserController@getBonuses',
        'as' => 'user.bonuses',
    ]);

    Route::get('/preferred-writers', [
        'uses' =>'UserController@getPreferredWriters',
        'as' => 'user.preferred-writers',
    ]);

    Route::get('/files', [
        'uses' =>'UserController@getFiles',
        'as' => 'user.files',
    ]);

     Route::get('/profile', [
        'uses' =>'UserController@getProfile',
        'as' => 'user.profile',
    ]);

    Route::get('/how-it-works', [
        'uses' =>'UserController@getHowItWorks',
        'as' => 'user.how-it-works',
    ]);

    Route::get('/messages', [
        'uses' =>'UserController@getMessages',
        'as' => 'user.messages',
    ]);

    Route::get('/referral-program', [
        'uses' =>'UserController@getReferralProgram',
        'as' => 'user.referral-program',
    ]);

    Route::get('/store-credit', [
        'uses' =>'UserController@getStoreCredit',
        'as' => 'user.store-credit',
    ]);

    Route::get('/contact-us', [
        'uses' =>'UserController@getContactUs',
        'as' => 'user.contact-us',
    ]);

//    Route::get('/user/authorization-block', function() {
//        return view('user.sections.block.authorization');
//    });
    Route::get('/authorization-block', 'UserController@ajaxAuthorization');

//    Route::get('/bonuses-block', function() {
//        return view('user.sections.block.bonuses');
//    });
    Route::get('/bonuses-block', 'UserController@ajaxBonuses');

//    Route::get('/user/files-block', function() {
//        return view('user.sections.block.files');
//    });
    Route::get('/files-block', 'UserController@ajaxFiles');

//    Route::get('/user/how-it-works-block', function() {
//        return view('user.sections.block.how-it-works');
//    });
    Route::get('/how-it-works-block', 'UserController@ajaxHowItWorks');

//    Route::get('/user/messages-block', function() {
//        return view('user.sections.block.messages');
//    });
    Route::get('/messages-block', 'UserController@ajaxMessages');

//    Route::get('/user/order-open-block', function() {
//        return view('user.sections.block.order-open');
//    });
    Route::get('/order-open-block', 'UserController@ajaxOrderOpen');

//    Route::get('/user/order-block', function() {
//        return view('user.sections.block.order');
//    });
    Route::get('/order-block', 'UserController@ajaxOrder');

//    Route::get('/user/preferred-writers-block', function() {
//        return view('user.sections.block.preferred-writers');
//    });
    Route::get('/preferred-writers-block', 'UserController@ajaxPreferredWriters');

//    Route::get('/user/profile-block', function() {
//        return view('user.sections.block.profile');
//    });
    Route::get('/profile-block', 'UserController@ajaxProfile');

//    Route::get('/user/referral-program-block', function() {
//        return view('user.sections.block.referral-program');
//    });
    Route::get('/referral-program-block', 'UserController@ajaxReferralProgram');


//    Route::get('/user/store-credit-block', function() {
//        return view('user.sections.block.store-credit');
//    });
    Route::get('/store-credit-block', 'UserController@ajaxStoreCredit');
    Route::get('/contact-us-block', 'UserController@ajaxContactUs');

    Route::get('/new-order', 'UserController@getNewOrder');
    Route::get('/new-order-block', 'UserController@ajaxNewOrder');


    Route::get('/download/{id}', [
        'as' => 'user.download',
        'uses' => function($id) {
            $model = App\UserFile::find($id);
            // Check if file exists in app/storage/file folder
            $file_path = storage_path('app/'.$model->filename);
            if (file_exists($file_path))
            {
                // Send Download
                return Response::download($file_path, $model->original_filename, [
                    'Content-Length: '. filesize($file_path)
                ]);
            }
            else
            {
                exit('Requested file does not exist on our server!');
            }
        }
    ]);

    Route::get('/read-message', 'UserController@readMessage');
    Route::post('/save-message', 'UserController@saveMessage');
    Route::get('/read-file', 'UserController@readFile');
    Route::post('/save-file', 'UserController@saveFile');

    Route::get('/create-auth-token', 'UserController@createAuthToken');
    Route::get('/auth-email/{tokenStr}', 'UserController@authEmail');
    Route::post('/edit', 'UserController@updateAdditionalInfo');

    Route::post('/add-preferred-writer', 'UserController@addPreferredWriter');
    Route::post('/delete-preferred-writer', 'UserController@deletePreferredWriter');

    Route::post('/change-password', 'UserController@changePassword');

    Route::get('/more-messages', 'UserController@getMoreMessages');
    Route::get('/more-files', 'UserController@getMoreFiles');
    Route::get('/order-more-messages', 'UserController@orderMoreMessages');
    Route::get('/order-more-files', 'UserController@orderMoreFiles');

    Route::get('/complete-survey', 'UserController@completeSurvey');

    Route::post('/set-ref-payment-method', 'UserController@setRefPaymentMethod');

    Route::get('/bonuses-as-store-credit', 'UserController@storeBonuses');

    Route::get('/promo-card', 'UserController@getPromoCard');
});

Route::get('/write-my-essay', function () { return view('sections.write-my-essay');});
Route::get('/write-my-assignment', function () { return view('sections.write-my-assignment');});
Route::get('/write-my-homework', function () { return view('sections.write-my-homework');});
Route::get('/write-my-report', function () { return view('sections.write-my-report');});
Route::get('/write-my-thesis', function () { return view('sections.write-my-thesis');});
Route::get('/write-my-paper', function () { return view('sections.write-my-paper');});
Route::get('/write-my-research-paper', function () { return view('sections.write-my-research-paper');});
Route::get('/examples-and-samples', 'SiteController@examples');
Route::get('/example/{id}', 'SiteController@example');
Route::get('/write-my-papers', function () { return view('sections.write-my-papers');});


Route::get('/essay-writing', function () { return view('sections.essay-writing');});
Route::get('/report-writing', function () { return view('sections.report-writing');});
Route::get('/speech-writing', function () { return view('sections.speech-writing');});
Route::get('/dissertation-writing', function () { return view('sections.dissertation-writing');});
Route::get('/thesis-writing', function () { return view('sections.thesis-writing');});
Route::get('/research-paper-writing', function () { return view('sections.research-paper-writing');});

Route::get('/term-paper-writing', function () { return view('sections.term-paper-writing');});
Route::get('/assignment-writing', function () { return view('sections.assignment-writing');});
Route::get('/homework-writing', function () { return view('sections.homework-writing');});
Route::get('/personal-statement', function () { return view('sections.personal-statement');});
Route::get('/cause-work-writing', function () { return view('sections.cause-work-writing');});
Route::get('/academic-paper-writing', function () { return view('sections.academic-paper-writing');});
Route::get('/college-paper-writing', function () { return view('sections.college-paper-writing');});
Route::get('/article-writing', function () { return view('sections.article-writing');});
Route::get('/writing-tips', function () { return view('sections.writing-tips');});


//Route::get('/order-step2', function() {
//    if(Auth::check()) {
//        return view();
//    }
//
//    return response('fail', 400);
//
//});
//Route::get('/order-step3', function() {
//    return view()
//});


Route::get('/pp', 'PaypalController@test');
Route::get('/payment-approved', 'PaypalController@paymentApproved');
Route::get('/payment-cancelled', 'PaypalController@paymentCancelled');
Route::any('/ipn', 'PaypalController@handleIpn');

//Facebook auth
Route::get('/social-auth/facebook', 'SocialAuthController@facebookAuth');
Route::get('/outer-download/{filename}', function($filename) {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path('app/'.$filename);
        if (file_exists($file_path))
        {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        }
        else
        {
            return response('file not found', 400);
        }
    }
);