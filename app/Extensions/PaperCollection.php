<?php
/**
 * Created by PhpStorm.
 * User: Vic
 * Date: 09.06.2016
 * Time: 13:22
 */

namespace App\Extensions;


class PaperCollection extends \Illuminate\Database\Eloquent\Collection
{
    public function indexByCategory()
    {
//        $data = static::with('prices')->orderBy('category')->get();

        $res = [];

        foreach($this as $item)
        {
            if(!array_key_exists($item->category, $res)) {
                $res[$item->category] = [];
            }
            $res[$item->category][] = $item;
        }

        return $res;
    }

    public function indexById()
    {
        $res = [];

        foreach($this as $item)
        {
            if(!array_key_exists($item->id, $res)) {
                $res[$item->id] = [];
            }
            $res[$item->id][] = $item;
        }

        return $res;
    }

    public function toTranslatedArray()
    {
        return $this->map(function($item) {
            return $item->toTranslatedArray();
        });
    }
}