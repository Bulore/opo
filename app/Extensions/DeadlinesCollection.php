<?php
/**
 * Created by PhpStorm.
 * User: Vic
 * Date: 09.06.2016
 * Time: 13:22
 */

namespace App\Extensions;


class DeadlinesCollection extends \Illuminate\Database\Eloquent\Collection
{
    public function indexByCategory()
    {
//        $data = static::with('prices')->orderBy('category')->get();

        $res = [];

        foreach($this as $item)
        {
            if(!array_key_exists($item->category, $res)) {
                $res[$item->category] = [];
            }
            $res[$item->category][] = $item;
        }

        return $res;
    }

    public function indexById()
    {
        $res = [];

        foreach($this as $item)
        {
            if(!array_key_exists($item->id, $res)) {
                $res[$item->id] = [];
            }
            $res[$item->id][] = $item;
        }

        return $res;
    }

    public function hours()
    {
        return $this->filter(function($item) {
            return $item->hours <= 24;
        });
    }

    public function days()
    {
        return $this->filter(function($item) {
            return $item->hours > 24 && $item->hours < 24*30;
        });
    }

    public function months()
    {
        return $this->filter(function($item) {
            return $item->hours >=24*30;
        });
    }
}