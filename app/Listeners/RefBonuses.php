<?php

namespace App\Listeners;

use App\Events\OrderPaid;
use App\RefStat;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefBonuses
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPaid  $event
     * @return void
     */
    public function handle(OrderPaid $event)
    {
        $client = User::find($event->order->client_id);

        if(!$client || $client->ref_id == null) return;

        $refUser = User::find($client->ref_id);

        $refUser->balance = $refUser->balance + ($event->order->price*0.1);

        $refUser->save();

        RefStat::create([
            'recipient_id' => $refUser->id,
            'client_id' => $client->id,
            'type' => 'PayPal',
            'amount' => $event->order->price*0.1,
        ]);
    }
}
