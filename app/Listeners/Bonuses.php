<?php

namespace App\Listeners;

use App\Events\OrderPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Bonuses
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPaid  $event
     * @return void
     */
    public function handle(OrderPaid $event)
    {
        $client = User::find($event->order->client_id);

        $amount = $event->order->price/10;

        $client->bonuses = $client->bonuses+$amount;

        $client->save();

        DB::table('bonuses')->insert([
            'paper_id' => $event->order->id,
            'status' => 'Bonus recieved',
            'amount' => $amount,
        ]);
    }
}
