<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaperType extends Model
{
    protected $table = 'paper_types';

    public static function groupByCategory($data)
    {
//        $data = static::with('prices')->orderBy('category')->get();

        $res = [];

        foreach($data as $item)
        {
            if(!array_key_exists($item->category, $res)) {
                $res[$item->category] = [];
            }
            $res[$item->category][] = $item;
        }

        return $res;
    }

    public function prices()
    {
        return $this->hasMany('App\AcademicLevelDeadlinePrice', 'type_id');
    }

    public function newCollection(array $models = [])
    {
        return new Extensions\PaperTypeCollection($models);
    }
}
