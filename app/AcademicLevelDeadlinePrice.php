<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicLevelDeadlinePrice extends Model
{
    protected $table = 'academic_level_deadline_price';

    public function academicLevel()
    {
        return $this->belongsTo('App\AcademicLevel', 'academic_level_id');
    }

    public function deadline()
    {
        return $this->belongsTo('App\Deadline', 'deadline_id');
    }
}
