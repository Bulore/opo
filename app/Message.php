<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function sender()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User', 'to_id');
    }

    public static function getUserMessages($length, $offset, $userType)
    {
        $user_id = request()->user()->id;
        $messages = static::where(function($q) use ($user_id, $userType) { $q->where('to_id', $user_id)->where('to_type', $userType); })
            ->orWhere(function($q) use ($user_id, $userType) { $q->where('from_id', $user_id)->where('from_type', $userType); })
            ->orderBy('created_at', 'desc')->limit($length)->offset($offset)->get();
        return $messages;
    }

    protected $fillable = ["from_id", "to_id", "type", "text", "paper_id", "isread", "from_type", "to_type"];
}
