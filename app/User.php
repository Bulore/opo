<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Mail;
use Hash;

class User extends Authenticatable
{
     const USER_ACCESS_LEVEL = 1;
     const WRITER_ACCESS_LEVEL = 2;
     const SUPPORT_ACCESS_LEVEL = 3;
     const ADMIN_ACCESS_LEVEL = 100;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'phone', 'access_level', 'country_id',
        'alt_email', 'alt_phone', 'night_calls', 'preferred_language', 'social_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->access_level == static::ADMIN_ACCESS_LEVEL;
    }

    public function orders()
    {
        return $this->hasMany('App\Paper', 'client_id');
    }

    public function jobs()
    {
        return $this->hasMany('App\Paper', 'writer_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function subusers()
    {
        return $this->hasMany(self::class, 'ref_id');
    }

    public function refuser()
    {
        return $this->belongsTo(self::class, 'ref_id');
    }

    public static function createClient(array $data)
    {
        $data['access_level'] = static::USER_ACCESS_LEVEL;
        $user = static::create($data);
        return $user;
    }

    public static function createClientWithoutPassword(array $data)
    {
        $password = str_random(10);
        $data['password'] = bcrypt($password);
        $data['access_level'] = self::USER_ACCESS_LEVEL;
        $user = static::create($data);
        Mail::send('emails.register', compact('user', 'password'),
            function($message) use ($user)
            {
                $message->to($user->email)->subject('Your login information')->from( 'no-reply@originalpapersonly.com', 'originalpapersonly.com');

//                foreach($emails as $email)
//                {
//                    $message->to($email->email)->from( 'no-reply@ridanta.com', 'Admin' );
//                }
            }
        );
        return $user;
    }

    public function getRecipients()
    {
        $res = [];
        switch($this->access_level) {
            case self::USER_ACCESS_LEVEL:
                $writer_ids = \App\Paper::select('writer_id')->where('client_id',18)->distinct()->get()->lists('writer_id')->toArray();
                $res = self::whereIn('id', $writer_ids)->get();
                break;

            case self::WRITER_ACCESS_LEVEL:

                break;

            case self::SUPPORT_ACCESS_LEVEL:

                break;

            case self::ADMIN_ACCESS_LEVEL:

                break;
        }

        return $res;
    }

    public function incomingMessages()
    {
        return $this->hasMany('App\Message', 'to_id');
    }

    public function outgoingMessages()
    {
        return $this->hasMany('App\Message', 'from_id');
    }

    public function getIsWriterAttribute()
    {
        return $this->access_level == self::WRITER_ACCESS_LEVEL;
    }

    public function getIsSupportAttribute()
    {
        return $this->access_level == self::SUPPORT_ACCESS_LEVEL;
    }

    public function getIsClientAttribute()
    {
        return $this->access_level == self::USER_ACCESS_LEVEL;
    }

    public static function getSocialUser($sid)
    {
        return static::where('social_id', $sid)->first();
    }
}
