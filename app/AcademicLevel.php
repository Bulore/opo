<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcademicLevel extends Model
{
    public function prices()
    {
        return $this->hasMany('App\AcademicLevelDeadlinePrice', 'academic_level_id');
    }
}
