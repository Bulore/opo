<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailToken extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    protected $fillable = [
        'token',
        'user_id',
    ];
}
