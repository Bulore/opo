<?php

namespace App\Writers;

use Illuminate\Database\Eloquent\Model;

class CompletedOrder extends WriterModel
{
    public function writer()
    {
        return $this->belongsTo(Writer::class, 'writer_id');
    }

    public function order()
    {
        return $this->belongsTo(\App\Papers\Paper::class, 'order_id');
    }
}
