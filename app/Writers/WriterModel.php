<?php

namespace App\Writers;

use Illuminate\Database\Eloquent\Model;

class WriterModel extends Model
{
    protected $connection = 'writers';
}
