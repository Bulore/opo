<?php

namespace App\Writers;

use Illuminate\Database\Eloquent\Model;

class Writer extends WriterModel
{
    protected $table = 'users';

    protected $guarded = [
        'id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
}
