<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreferredWriter extends Model
{
    protected $guarded = ['id'];
}
