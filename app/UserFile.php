<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    protected $table = 'files';

    public function getUrl()
    {
        return route('user.download', $this->id);
    }

    public function sender()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\User', 'to_id');
    }

    public static function getUserFiles($length, $offset, $userType)
    {
        $user_id = request()->user()->id;

        $files = static::where(function($q) use ($user_id, $userType) {

            $q->where('to_id', $user_id)->where('to_type', $userType);

        })->orWhere(function($q) use ($user_id, $userType) {

            $q->where('from_id', $user_id)->where('from_type', $userType);

        })->orderBy('created_at', 'desc')->limit($length)->offset($offset)->get();

        return $files;
    }


    protected $fillable = ["from_id", "to_id", "paper_id", "isread", "isimage", "filename", "original_filename", 'to_type', 'from_type'];
}
