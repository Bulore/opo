<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\User::created(function ($user) {

            $user->ref_code = str_random(10);
            $user->save();

            $token = str_random(128);
            \App\EmailToken::create([
                'token' => $token,
                'user_id' => $user->id,
            ]);

            $url = url("/user/auth-email/$token");

//            \Mail::send('emails.auth-email', compact('url', 'user'),
//                function($message) use ($user)
//                {
//                    $message->to($user->email)->subject('Authorize your account')->from( 'no-reply@originalpapersonly.com', 'originalpapersonly.com');
//
////                foreach($emails as $email)
////                {
////                    $message->to($email->email)->from( 'no-reply@ridanta.com', 'Admin' );
////                }
//                }
//            );
        });

        \App\Paper::created(function ($order) {
             \Mail::send('emails.new-order',[],
                function($message) use ($order)
                {
                    $message->to('support@originalpapersonly.com')->from( 'no-reply@originalpapersonly.com', 'Robot');
                }
             );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
