<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Deadline;
use App\AcademicLevel;
use App\PaperType;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('auth.register', function ($view) {

            $countries = \DB::table('countries')->orderBy('name')->get();

            $is_bot = preg_match(
                "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
                $_SERVER['HTTP_USER_AGENT']
            );
            $countryCode = '';
            if(!$is_bot) {
                try {
                    $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                    $countryCode = $countryData['country']['iso'];
                } catch (\Exception $e) {}

            }

            $view->with(['countries' => $countries, 'currentCountryCode' => strtolower($countryCode)] );
        });

        view()->composer('popups.callback', function ($view) {

            $countries = \DB::table('countries')->orderBy('name')->get();

            $is_bot = preg_match(
                "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
                $_SERVER['HTTP_USER_AGENT']
            );
            $countryCode = '';
            if(!$is_bot) {
                try {
                    $countryData = json_decode(file_get_contents('http://api.sypexgeo.net/json/'), true);
                    $countryCode = $countryData['country']['iso'];
                } catch (\Exception $e) {}

            }

            $view->with(['countries' => $countries, 'currentCountryCode' => strtolower($countryCode)] );
        });

        view()->composer(['sideblocks.priceBlock'], function($view) {
            $deadlines = Deadline::lessThanMonth()->get();
            $academicLevels = AcademicLevel::with('prices')->get();
            $types = PaperType::with('prices')->get();

            $view->with(['deadlines' => $deadlines, 'academicLevels' => $academicLevels, 'types' => $types]);
        });

        view()->composer('layouts.header', function($view) {
            $sections = ['Home'=>"/",
                         'How it works'=>"howitworks",
                         'Prices'=>"prices",
                         'Testimonials'=>"testimonials",
                         'FAQ'=>"faq",
                         'Samples'=>"samples"];

            $view->with(compact('sections'));
        });

        view()->composer('layouts.base', function($view) {

            $seo = \App\Seo::firstOrCreate(['url' => request()->url()]);

            $view->with(compact('seo'));

        });

        view()->composer('user.layouts.leftPanel', function($view) {
            $user_id = request()->user()->id;
            $messagesCount = \App\Message::where('isread', false)
                ->where(function($q) use ($user_id) { $q->where('to_id', $user_id)->where('to_type', 'client'); })
                ->orWhere(function($q) use ($user_id) { $q->where('from_id', $user_id)->where('from_type', 'client'); })
                ->count();

            $filesCount = \App\UserFile::where('isread', false)
                ->where(function($q) use ($user_id) { $q->where('to_id', $user_id)->where('to_type', 'client'); })
                ->orWhere(function($q) use ($user_id) { $q->where('from_id', $user_id)->where('from_type', 'client'); })
                ->count();
            $sections = [
                ['name' => 'Orders', 'url' => route('user.order'), 'icon' => 'icon-item', 'amount' => ''],
                ['name' => 'Messages', 'url' => route('user.messages'), 'icon' => 'icon-message', 'amount' => $messagesCount],
                ['name' => 'Files', 'url' => route('user.files'), 'icon' => 'icon-files', 'amount' => $filesCount],
                ['name' => 'Profile', 'url' => route('user.profile'), 'icon' => 'icon-profile', 'amount' => ''],
                ['name' => 'Authorization', 'url' => route('user.authorization'), 'icon' => 'icon-auth', 'amount' => ''],
                ['name' => 'Referral Program', 'url' => route('user.referral-program'), 'icon' => 'icon-ref', 'amount' => ''],
                ['name' => 'My Bonuses', 'url' => route('user.bonuses'), 'icon' => 'icon-bonuses', 'amount' => ''],
                ['name' => 'My Store Credit', 'url' => route('user.store-credit'), 'icon' => 'icon-credit', 'amount' => ''],
                ['name' => 'Preferred writers', 'url' => route('user.preferred-writers'), 'icon' => 'icon-preffered', 'amount' => ''],
                ['name' => 'How It Works', 'url' => route('user.how-it-works'), 'icon' => 'icon-how', 'amount' => ''],
                ['name' => 'Contact Us', 'url' => route('user.contact-us'), 'icon' => 'icon-contact', 'amount' => '']
            ];

            $view->with(compact('sections'));
        });

        view()->composer('sections.prices', function($view) {
            $deadlines = Deadline::lessThanMonth()->get();
            $academicLevels = AcademicLevel::with('prices')->get();
            $types = PaperType::with('prices')->get();

            $view->with(['deadlines' => $deadlines, 'academicLevels' => $academicLevels, 'types' => $types]);
        });

        view()->composer('user.layouts.header', function($view) {
            $user = request()->user();

            $view->with(compact('user'));
        });

        view()->composer('layouts.mobile-left-panel', function($view) {
            $sections = ['Home'=>"/",
                         'Prices'=>"prices",
                         'FAQ'=>"faq",
                         'How it works'=>"howitworks",
                         'Testimonials'=>"testimonials",
                         'Terms of use'=>"terms-of-use",
                         'Samples'=>"samples",
                         'Contact Us'=>"contacts",
                         ];

            $view->with(compact('sections'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
